<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
     protected $guarded=[];
     public function account_categories()
     {
     	return $this->belongsTo(AccountCategorie::class , 'account_cat_id');
     }
     public function user(){
     	return $this->belongsTo(Account::class, 'admin_id');
     }
}

