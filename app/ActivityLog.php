<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model
{
    protected $fillable = ['log_name', 'description', 'subject_id', 'subject_type', 'causer_id', 'causer_type', 'properties'];

    protected $table = 'activity_log';
    protected $primaryKey = 'id';

    function admin()
    {
    	return $this->belongsTo(Admin::class , 'causer_id' );
    }
}
