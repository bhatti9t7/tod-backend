<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
class Admin extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use LogsActivity;

    protected $guard = 'admin';
    
    
    protected $fillable = [
         'name', 'phone','email', 'password','api_token','phone_code', 'images', 'cnic' ,'parrent_admin' ,'country_id' ,

         'city_id', 'franchise_id', 'branch_id', 'belongs'
    ];

    protected static $logAttributes = ['name','email', 'phone' , 'images','status' ,'cnic'];
    protected static $logOnlyDirty = true;

    protected $hidden = [
        'password', 'remember_token','api_token','phone_code'
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPasswordNotification($token));
    }
}
