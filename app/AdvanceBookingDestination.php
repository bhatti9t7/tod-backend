<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvanceBookingDestination extends Model
{
	protected $fillable = ['booking_id', 'bd_longitude', 'bd_latitude'];
	protected $hidden = ['bd_status'];

	public function AdvanceBooking()
    {
        return $this->belongsTo(AdvanceBooking::class);
    }
}
