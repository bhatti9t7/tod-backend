<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = array('name', 'address' , 'citi_id');

    public function city()
    {
    	return $this->belongsTo(City::class , 'citi_id');
    }
}
