<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\AdminResetPasswordNotification;
use Spatie\Activitylog\Traits\LogsActivity;
class Captain extends Authenticatable
{
	use Notifiable;
    protected $guard = 'captain'; 
use LogsActivity;

    
    protected $fillable = ['fname', 'lname','email', 'phone' ,'device_token', 'images','driving_licenes_images',
         'vehicle_registration','cnic_images','licence_no', 'vehicle_no','vehical_cat_id', 'owner_city' , 'owner_name','owner_fname','chesi_no' , 'engin_no','city','activity','api_token', 'latitude', 'longitude' ,'remarks_owner' ,'remarks_captain' ,'remarks_vehicle', 'created_by' , 'labour_unloader' ,'labour_loader','status','captain_cnic' , 'owner_cnic', 'vehicle_image' ,'owner_image' ,'bank' ,'branch_code' ,'account_no' ,'vehicle_registration_image' , 'description' ,'vehical_sub_id' ,'vehicle_name' ];

    protected $hidden = [
        'password','phone_verified','email_verified','fb_token'
    ];

    protected static $logAttributes = ['fname', 'lname','email', 'phone' , 'images', 'status' ,'activity', 'captain_cnic' , 'owner_cnic',];
    protected static $logOnlyDirty = true;

     public function wallet()
    {
        return $this->hasOne(CaptainWallet::class);
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class , 'captain_id');
    }

    public function VehicalCategorie(){

        return $this->belongsTo(VehicalCategorie::class ,'vehical_cat_id');
    }

    public function VehicalSubCategorie(){

        return $this->belongsTo(VehicleSubCategorie::class ,'vehical_sub_id');
    }

    public function CaptainStats(){

        return $this->hasOne(CaptainStats::class, 'captain_id' );
    }


    public function Rides()
    {
        return $this->hasMany(Ride::class);
    }

    public function isStatus()
    {
    	$activity = 'Offline';
    	switch($this->status){
    	 case 1:
    	 $activity = 'Online';
    	 break;	
    	 case 2:
    	 $activity = 'Riding';
    	 break;	
    	 default:
    	 $activity = 'Offline';
    	 break;
    	}
    	return $activity;
    }
}
