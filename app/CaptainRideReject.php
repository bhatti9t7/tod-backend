<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaptainRideReject extends Model
{
    protected $fillable = ['captain_id','booking_id', 'ride_id','status' ,'reject_reasons' , 'reject_message' ,'file_reject'];

    public function booking(){
    	return $this->belongsTo(Booking::class , 'booking_id');
    }
    public function ride(){
    	return $this->belongsTo(Ride::class , 'ride_id');
    }
}
