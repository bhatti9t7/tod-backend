<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaptainWallet extends Model
{
    protected $fillable = ['captain_id', 'amount','debit_amount','credit_amount' ];

    protected $hidden = ['status'];

    public function captain()
    {
    	return $this->belongsTo(Captain::class, 'captain_id');
    }
}
