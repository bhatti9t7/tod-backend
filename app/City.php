<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
   protected $fillable = array('name', 'status' , 'countrie_id');

   public function country()
   {
   		return $this->belongsTo(Country::class, 'countrie_id' );
   }
}

