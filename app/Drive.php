<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drive extends Model
{
    protected $fillable = ['drive_image' , 'drive_content'];
 	protected $hidden = [ 'status' ];

}
