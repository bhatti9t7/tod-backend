<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Financial extends Model
{
    protected $fillable = ['commisions','Serg_fee','wait_amount','labour_fare'  ,'destination_charge' , 'reject_booking_amount'];

    protected $hidden = [''];

}
