<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Franchise extends Model
{
    protected $fillable = array('name', 'address' , 'branch_id');

    public function branch()
    {
    	return $this->belongsTo(Branch::class ,'branch_id');
    }
}
