<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoodCategorie extends Model
{
    
    protected $fillable = ['name' , 'status'];
 	protected $hidden = [  ];


 	public function good()
    {
    	return $this->hasOne(Good::class );
    }
}
 