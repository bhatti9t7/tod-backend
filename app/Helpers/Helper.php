<?php

if (!function_exists('user_send_notification')) {

function user_send_notification($tokens, $message,$routeName)
{
//	$url = 'https://fcm.googleapis.com/fcm/send';
//    $fields = array(
//       'registration_ids' => $tokens,
//       'data' => $message
//      );
//
//    $headers = array(
//      'Authorization:key = AAAArXMoRWE:APA91bHHgeafb85zokS3tv3ZOMAvkBrB69xdIdXPizlPuOTOWiEa0g9mVFqinUPozJqoVxk6nftv6kx3AY-eqTGZqtlCu1JxbZND-7HTtP_S8OYizXSJufO_Ntw5rFrXfAJ8xI_hAtWR',
//      'Content-Type: application/json'
//      );
//
//     $ch = curl_init();
//       curl_setopt($ch, CURLOPT_URL, $url);
//       curl_setopt($ch, CURLOPT_POST, true);
//       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));


    $data = array(
        'to' => $tokens,
        'title' => '',
        'body' => $message['message'],
        'data' => array(
            'routeName' => $routeName,
            'params' => $message
        )
    );
    $data_string = json_encode($data);
    $ch = curl_init('https://exp.host/--/api/v2/push/send');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
    );
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

    $result = curl_exec($ch);
    if ($result === FALSE) {
        die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
    return $result;
}

}


if (!function_exists('captain_send_notification')) {
 function captain_send_notification ($tokens, $message,$routeName)
  {
//    $url = 'https://fcm.googleapis.com/fcm/send';
//    $fields = array(
//       'registration_ids' => $tokens,
//       'data' => $message
//      );
//
//    $headers = array(
//      'Authorization:key = AAAArXMoRWE:APA91bHHgeafb85zokS3tv3ZOMAvkBrB69xdIdXPizlPuOTOWiEa0g9mVFqinUPozJqoVxk6nftv6kx3AY-eqTGZqtlCu1JxbZND-7HTtP_S8OYizXSJufO_Ntw5rFrXfAJ8xI_hAtWR',
//      'Content-Type: application/json'
//      );
//
//     $ch = curl_init();
//       curl_setopt($ch, CURLOPT_URL, $url);
//       curl_setopt($ch, CURLOPT_POST, true);
//       curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
//       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//       curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//       $result = curl_exec($ch);
//       if ($result === FALSE) {
//           die('Curl failed: ' . curl_error($ch));
//       }
//       curl_close($ch);
//       return $result;

     $data = array(
         'to' => $tokens,
         'title' => '',
         'body' => $message['message'],
         'data' => array(
             'routeName' => $routeName,
             'params' => $message
         )
     );
     $data_string = json_encode($data);
     $ch = curl_init('https://exp.host/--/api/v2/push/send');
     curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
     curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     curl_setopt($ch, CURLOPT_HTTPHEADER, array(
             'Content-Type: application/json',
             'Content-Length: ' . strlen($data_string))
     );
     curl_setopt($ch, CURLOPT_TIMEOUT, 5);
     curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

     $result = curl_exec($ch);
     if ($result === FALSE) {
         die('Curl failed: ' . curl_error($ch));
     }
     curl_close($ch);
     return $result;
  }
}

if (!function_exists('isActiveRoute')) {

    function isActiveRoute($route, $output = "m-menu__item--active")
    {
        if (Route::currentRouteName() == $route) return $output;
    }

}

if (!function_exists('areActiveRoutes')) {

    function areActiveRoutes(Array $routes, $output = "m-menu__item--active")
    {
        foreach ($routes as $route)
        {
            if (Route::currentRouteName() == $route) return $output;
        }

    }
}

if (!function_exists('find_in_array_by_key')) {
  function find_in_array_by_key($data,$key,$val)
{

    foreach ($data as $row) {
      if($row->{$key}==$val)
      return $row->{$key};     
    }

    return false; 

}
}

if (!function_exists('find_row_in_array_by_key')) {
  function find_row_in_array_by_key($data,$key,$val)
{

    foreach ($data as $row) {

      if($row->{$key}==$val)
       
      return $row;     
    }

    return false; 

}
}



if (!function_exists('get_month_name_by_no')) {
  function get_month_name_by_no($month)
{
  $month_name = "";
  switch ($month) {
      case 1:
     $month_name = "January";
      break;
       case 2:
     $month_name = "February";
      break;   
       case 3:
     $month_name = "March";
      break;  
       case 4:
     $month_name = "April";
      break; 
       case 5:
     $month_name = "May";
      break; 
       case 6:
     $month_name = "June";
      break;
        case 7:
     $month_name = "July";
      break; 
       case 8:
     $month_name = "August";
      break; 
       case 9:
     $month_name = "September";
      break; 
       case 10:
     $month_name = "October";
      break; 
       case 11:
     $month_name = "November";
      break; 
      case 12:
     $month_name = "December";
      break;
    
      default:
      $month_name = "January";
      break;
  }


      return $month_name;

}

}
function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long)
{
  // Calculate the distance in degrees
  $degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));
  $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
  return round($distance, 2);
}
function dateDiffInDays($date1, $date2)
{
  // Calulating the difference in timestamps 
  $diff = strtotime($date2) - strtotime($date1);
  // 1 day = 24 hours 
  // 24 * 60 * 60 = 86400 seconds 
  // 32598
  return abs(round($diff / 86400));
}
function timeDiffInSec($time1,$time2='',$format='secs'){
  if(empty($time2)){
    if($format == 'secs'){
      return '600000';
    }else{
      return convertSecToMin(6000);
    }
  }else{
    $time1 = strtotime($time1);
    $time2 = strtotime($time2);
    $diff = $time1 - $time2;
    if($format == 'secs'){
      return gmdate('h:i:s', abs($diff));
    }else{
      return convertSecToMin(abs($diff));
    }
  }
}
function changeDateFormat($date,$format="d-m-Y")
{
  return  date( $format,strtotime($date));
}
function convertSecToMin($seconds){
  // echo $seconds;exit;
  return floor($seconds/60);
}
?>