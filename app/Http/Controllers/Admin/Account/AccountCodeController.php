<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Setting;
use App\AccountCategorie;
use App\Account;
use Session;
class AccountCodeController extends Controller
{
    public function __construct()
  {
    $this->middleware(['auth:admin']);
  }
    public function index()
    {
        
        return view('admin.Account.code');
    }
     public function create_form(Request $request)
    {
      $code = $_POST['data'];
      $account = AccountCategorie::where('account_cat_code' , $code)->first();
      // dd($account);
      $id = Account::orderBy('id' , 'DESC')->first();
       if($id)
      $codes =($account->account_cat_code.'00'.$id->id);
      else
        $codes =($account->account_cat_code.'00'.'1');

      $detail  = array('codes' => $codes , 'account' => $account );
        if($detail){
            return response()->json($detail, 200);
          }else{
        $message = 'record not found';
        return response()->json($message , 200);
        }
    }

   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
        'code' => 'required|max:40||unique:accounts',
        'title' => 'required',
        ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       }else{
        $account_affected = Account::create([
            'code'  => $request->code,
            'title'  => $request->title,
            'account_cat_id'  => $request->account_cat_id?$request->account_cat_id:0,
            'c_balance' => $request->c_balance,
            'admin_id' => $request->admin_id,
            ]);
            if($account_affected){
                session()->flash('success', 'Record has been added. successfully.');
            }else{
                session()->flash('danger', 'Fail to update the record.');
            }
            return redirect()->route('code.all_account');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {    
        $code=Account::where('id',$id)->first();
        return view('admin.Account.account_edit', compact('code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
        [
        'code' => 'required|max:40||unique:accounts,code,'.$id,
        'title' => 'required',
        ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       }else{
        // dd($request->all());
        $data['code']=$request->code;
        $data['title']=$request->title;
        $data['c_balance']=$request->c_balance;
            $account_affected= Account::where('id',$id)->update($data);
            if($account_affected){
                session()->flash('success', 'Record has been added. successfully.');
            }else{
                session()->flash('danger', 'Fail to update the record.');
            }
            return redirect()->route('code.all_account');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function get_account(Request $request){
        $limit = $request->input('length');
        $start = $request->input('start');
        $start = $start?$start+1:$start;
        $search = $request->input('search.value');
        $order_column_no = $request->input('order.0.column');
        $order_dir = $request->input('order.0.dir');
        $order_column_name = $request->input("columns.$order_column_no.data");

          $user = Account::orderBy('id' , 'DESC')->with('account_categories')->offset($start)->limit($limit)->orderBy($order_column_name,$order_dir);

          if(!empty($search)){
            $user->where('title', 'like', "%{$search}%")
            ->orWhere('code','like',"%{$search}%")
            ->orWhere('created_at','like',"%{$search}%");    
          }

          $data = $user->get();

          $totalFiltered = Account::count();

          $json_data = array(
          "draw"      => intval($request->input('draw')),
          "recordsTotal"  => count($data),
          "recordsFiltered" => intval($totalFiltered),
          "data"      => $data
        );

           return response()->json($json_data, 200);
    }
    public function code_destroy($id){
        $users = Account::find($id);
        $effected= $users->delete();
        $response= array();
        if($effected){
            session()->flash('success_message', 'Record has been updated. successfully.');
            return redirect()->route('code.all_account');
        }else{
            session()->flash('failed', 'Fail to delete User.');
            return redirect()->route('code.all_account');
        }
    }

}
