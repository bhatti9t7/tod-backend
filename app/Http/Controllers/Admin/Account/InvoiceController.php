<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Setting;
use Session;
use DB;
use App\Invoice;
use App\Account;
class InvoiceController extends Controller
{
      public function __construct()
  {
    $this->middleware(['auth:admin']);
  }
    public function index()
    {
         return view('admin.Account.invoice');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
        $validator = Validator::make($request->all(),
        [
         'code' => 'required',
         
         'recpno' => 'required',
         'amount' => 'required',
         'posting_date' => 'required',
       ]);
       if($validator->fails())
        {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            DB::beginTransaction();
            $posting_date=date('Y-m-d', strtotime($request->posting_date)). ' ' .date('H:i:s',strtotime(now()));
            $master_affected = Invoice::insert([
            'code' => request('code'),
            'recpno' => request('recpno'),
            'description' => request('description'),
            'reciveable_amount' => request('amount'),
            'date' => $posting_date,
            'created_by' => request('created_by'),
            'chqno' => request('chqno')?request('chqno'):''
            ]);
            if(!$master_affected){
                DB::rollback();
                session()->flash('danger', 'Fail to insert the record.');
                return Redirect()->route('cash-payment.index');
            }else{
                    $acc_upd=Account::where('code', $request->code)->first();
                    $upd['c_balance']=$acc_upd->c_balance - $request->amount;
                    $acc_upd->update($upd);
                    DB::commit();
                    session()->flash('success', 'Record has been inserted. successfully.');
                    return Redirect()->route('invoice.index');
            }
        }
          
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
