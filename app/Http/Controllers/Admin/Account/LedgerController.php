<?php

namespace App\Http\Controllers\Admin\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Setting;
use App\Master;
use App\Account;
use App\Invoice;
use Session;
use DB;

class LedgerController extends Controller
{
       public function __construct()
  {
    $this->middleware(['auth:admin']);
  }
    public function index()
    {
        return view('admin.Account.ledger');
    }

    public function show_ledger_code(Request $request){
      $code= $request->code;
      $test2 = Invoice::orderBy('id', 'DESC')->orWhere('code',$code)->orWhere('payable_cd',$code)->orWhere('income_cd',$code)->orWhereBetween('created_at',[$request->start_date,$request->from_date])->get();
        $user = Master::orderBy('id', 'DESC')->where('code',$code)->orWhereBetween('created_at',[$request->start_date,$request->from_date])->get();
      $aray1=array();
      $len = count($test2)>count($user)?count($test2):count($user);
      for($i=0; $i< $len ; $i++){
        if(count($test2)>$i){
          $aray2= $test2[$i];
          array_push($aray1, $aray2);
        }
        if(count($user) > $i){
          $aray3= $user[$i];
          array_push($aray1, $aray3);
        }
      }

      $val=$aray1;

      $val2=array_merge($val);
      // dd($val2);
      $code = $code;
      $credit = Master::where('code',$code)->where('amt_type' , 'CR')->groupBy('amt_type')->sum('amount');
      $debit = Master::where('code',$code)->where('amt_type' , 'DB')->groupBy('amt_type')->sum('amount');

      $html = view('Admin.Account.show_ledger_record')->with(compact('user' , 'credit' , 'debit' , 'test2' , 'val', 'val2' ,'code'))->render();

        return response()->json(['success' => true, 'html' => $html]);
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search_ledger_amount(Request $request){
        $limit = $request->input('length');
        $start = $request->input('start');
        $start = $start?$start+1:$start;
        $search = $request->input('search.value');
        $order_column_no = $request->input('order.0.column');
        $order_dir = $request->input('order.0.dir');
        $order_column_name = $request->input("columns.$order_column_no.data");

        $dabit_amount=0;
        $credit_amount=0;
        $records=array();

        $accounts = Account::offset($start)->limit($limit)->orderBy($order_column_name,$order_dir);
          if(!empty($search)){
            $accounts->where('code', 'like', "%{$search}%")
            ->orWhere('title','like',"%{$search}%")
            ->orWhere('c_balance','like',"%{$search}%");    
          }
           $account = $accounts->get();
          // dd($account);
      $masters = Master::get();
      $invoices = Invoice::get();
      $records = array();
      $index = 0;
        foreach($account as $codes){
          $test2 = Invoice::orWhere('code',$codes->code)->orWhere('payable_cd',$codes->code)->orWhere('income_cd',$codes->code)->get();
          $user = Master::where('code',$codes->code)->get();
          // dd($codes->code);
            $dabit_amont=0;
            $credit_total=0;
            $opening_balance= $codes->balance;
            $code= $codes->code;
            $title = $codes->title;
            $c_balance=$codes->c_balance;
            foreach($test2 as $test1){
              if($test1->payable_cd == $code)
                $credit_total += $test1->payable_amount;
              elseif($test1->income_cd == $code)
                $credit_total += $test1->income;
              elseif($test1->code == $code)
               $dabit_amont +=$test1->reciveable_amount;
            }
            foreach ($user as $key2 ) {
              if($key2->amt_type == 'CR') 
                $credit_total += $key2->amount;
              else
                $dabit_amont +=$key2->amount;
            }
            $data= new \stdClass();
            $data->id  =$index;
            $data->dabit  =$dabit_amont;
            $data->credit  =$credit_total;
            $data->code  =$code;
            $data->title  =$title;
            $data->opening_balance  =$opening_balance;
            $data->c_balance = $c_balance;             
            $records[$index++]=$data;
        }
          $data = $records;
          // dd($data);
          $totalFiltered = Account::count();
          $json_data = array(
          "draw"      => intval($request->input('draw')),
          "recordsTotal"  => count($data),
          "recordsFiltered" => intval($totalFiltered),
          "data"      => $data
        );
           return response()->json($json_data, 200);
    }

    public function ledger_detail(Request $request){

       
        $user = Master::orderBy('id', 'DESC')->where('code',$request->code)->orWhereBetween('created_at',[$request->start_date,$request->from_date])->get();
      $aray1=array();
      $len = count($user);
      for($i=0; $i< $len ; $i++){
        if(count($user) > $i){
          $aray3= $user[$i];
          array_push($aray1, $aray3);
        }
      }

      $val=$aray1;

      $val2=array_merge($val);
      // dd($val2);
      $code = $request->code;
      $credit = Master::where('code',$request->code)->where('amt_type' , 'CR')->groupBy('amt_type')->sum('amount');
      $debit = Master::where('code',$request->code)->where('amt_type' , 'DB')->groupBy('amt_type')->sum('amount');

      return view('admin.Account.ledger' , compact('user' , 'credit' , 'debit' , 'test2' , 'val', 'val2' ,'code'));
    
    }
    public function all_account_record(){
      return view('admin.Account.all_account');
    }

}
