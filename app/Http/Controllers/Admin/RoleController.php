<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Auth;
class RoleController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function giveme()
    {
    	//https://github.com/spatie/laravel-permission
    	//https://laravelcode.com/post/laravel-54-usesr-authorization-with-spatie-laravel-permission
    	//$role = Role::create(['name' => 'Super Admin']);
         //$permission = Permission::create(['name' => 'Edit Good']);
        //$permission = Permission::create(['guard_name' => 'admin', 'name' => 'Show Good']);
        //$permission = Permission::create(['guard_name' => 'admin', 'name' => 'Index Good']);
        //$permission = Permission::create(['guard_name' => 'admin', 'name' => 'Create Good']);
        //$permission = Permission::create(['guard_name' => 'admin', 'name' => 'Edit Good']);
        //$permission = Permission::create(['guard_name' => 'admin', 'name' => 'Delete Good']);
        
         //$role->syncPermissions($permission);
         //$permission->syncRoles($role);

        //A permission can be assigned to a role using 1 of these methods:
        //$role->givePermissionTo($permission);
        //$permission->assignRole($role);

         //$role->revokePermissionTo($permission);
         //$permission->removeRole($role);

    	return [$permission];
    }

    public function checkme()
    {
    	//$user = User::find($id);
        //$user->assignRole($resqust->role_id);

    	//$roles = Auth::guard('admin')->user()->syncRoles(['staff']);
    	
    	//$roles = Auth::guard('admin')->user()->getDirectPermissions();
    	
    	//$roles = Auth::guard('admin')->user()->getPermissionsViaRoles();
    	//$roles = Auth::guard('admin')->user()->assignRole('Super Admin');

        $user = User::find(2);
        $permissions = $user->givePermissionTo('Edit Good');
    	$permissions = Auth::guard('admin')->user()->revokePermissionTo('Edit Good');
    	//$permissions = Auth::guard('admin')->user()->getAllPermissions();

    	//$roles = Auth::guard('admin')->user()->removeRole('Super Admin');

    	//$roles = Auth::guard('admin')->user()->getRoleNames();
    	return [$permissions];
    }
}


/*givePermissionTo(): Allows us to give persmission to a user or role
revokePermissionTo(): Revoke permission from a user or role
hasPermissionTo(): Check if a user or role has a given permission
assignRole(): Assigns role to a user
removeRole(): Removes role from a user
hasRole(): Checks if a user has a role
hasAnyRole(Role::all()): Checks if a user has any of a given list of roles
hasAllRoles(Role::all()): Checks if a user has all of a given list of role*/

