<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Admin;
use App\User;
use App\Captain;
use App\Ride;
use Auth;
use DB;
use App\MainWallet;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

    public function index()
    {
        $admin = Admin::orderBy('id', 'DESC')->paginate(5);
        $user= User::orderBy('id', 'DESC')->paginate(3);
        $captain= Captain::orderBy('id', 'DESC')->paginate(5); 
        $users =User::count();
        $captains= Captain::count(); 
        $captains_login= Captain::where('status' ,1)->where('activity' ,1)->count(); 
        $admins = Admin::count();
        $rides = Ride::count();




        ///captain Graphs 
    //  DB::enableQueryLog();  
    $captain_stats = Captain::select(DB::raw("count('id') as count,Month(created_at) as month"))
    ->orderBy("created_at")
    ->groupBy(DB::raw("Month(created_at)"))
    ->where(DB::raw('Year(created_at)'), '=', date("Y"))
    ->get(); 
    
    $captain_months = array();
    for ($i=1; $i <=12 ; $i++) {
        $tmp = array();
       $isMonth=find_in_array_by_key($captain_stats,'month',$i);
       if($isMonth)
        $tmp['total_captain']=$isMonth;
        else
        $tmp['total_captain']=0;
        
        $tmp['month']=get_month_name_by_no($i);

        $captain_months[] = $tmp;

    }

        
        /// user Graphs
        $user_stats = User::select(DB::raw("count('id') as count,Month(created_at) as month"))
        ->groupBy(DB::raw("month(created_at)"))
        ->where(DB::raw('Year(created_at)'), '=', date("Y"))
        ->get(); 
        // DD($user_stats);exit;
    
     $user_months = array();
    for ($i=1; $i <=12 ; $i++) {
        $tmp = array();
       $isMonth=find_in_array_by_key($user_stats,'month',$i);

       if($isMonth)
        $tmp['total_user']=$isMonth;
        else
        $tmp['total_user']=0;
        
        $tmp['month']=get_month_name_by_no($i);

        $user_months[] = $tmp;
 
    }

         $collect_commission=  DB::table('transactions')
             ->select(DB::raw('month(created_at) as month'), DB::raw('sum(amount) as amount') , DB::raw('sum(collect_amount) as collect_amount'))
            ->groupBy(DB::raw("month(created_at)"))->get();


            $collected_amount = array();
            $total_amount = array();
            $year_months = array();
                for ($i=1; $i <=12 ; $i++) {

               $isRow=find_row_in_array_by_key($collect_commission,'month',$i);
           
               $month=get_month_name_by_no($i);
               if($isRow){
                $collected_amount[] =$isRow->amount; 
                $total_amount[] =$isRow->collect_amount;
                $year_months[] =$month;
               }else{
                $collected_amount[] =0; 
                $total_amount[] =0;
                $year_months[] =$month;
               }
                }



        
       

       return View('admin.adminindex', compact(['admin', 'user', 'captain'  , 'users',  'captains', 'admins' , 'rides','captain_months','user_months','commision_month','collected_amount','total_amount','year_months' ,'captains_login']));

    }

    
}
