<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use App\file;
use App\Admin;
use App\Captain;
use App\User;
use Excel;
use App\Country;
use App\City;
use App\Branch;
use App\Franchise;
use Auth;
class AdminResource extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth:admin']);
  }

  public function index()
  {
    return view('admin.adminRosource');
  }
  public function create()
  {
   $permissions = Permission::all();
   $roles = Role::all();

   $Franchise = Franchise::all();
   $Branch = Branch::all();
   $City = City::all();
   $Country = Country::all();

   return view('admin.form', compact('permissions','roles' ,'Country' , 'City' , 'Branch' , 'Franchise'));
 }


 public function store(Request $request)
 { 

  $data = $request->except(['roles','permissions']);
  $validate = Validator::make($request->all(),
    [
     'name' => 'required',
     'email' => 'required|email',
     'phone' => 'required',
     'images' => 'required' ,
     'password' => 'required|min:6|confirmed',
     'password_confirmation' => 'required_with:password|same:password|min:6'

   ]);

  if($validate->fails())
  {
    $message = $validate->errors()->first();
    Session::flash('info', " $message") ;
    return Redirect::back()->withErrors($validate);
  } 
  else {
   $obj = new Admin;  
   $profile = $request->file('images')->getClientOriginalName();
   if($request->hasfile('images')){
    $Extension_profile = $request->file('images')->getClientOriginalExtension();
    $profile = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
    $request->file('images')->move('images/admin/profile/', $profile);
  }
  $obj->name = $request->name;       
  $obj->phone = $request->phone;
  $obj->email = $request->email;
  $obj->images = $profile;
  $obj->password = Hash::make(request('password'));       
  $obj->api_token = str_random(60);
  $obj->parrent_admin = Auth::guard('admin')->id();

  // $obj->country_id = isset($request->country_id)?$request->country_id:0;
  // $obj->city_id = isset($request->city_id)?$request->city_id:0;
  // $obj->branch_id = isset($request->branch_id)?$request->branch_id:0;
  // $obj->franchise_id = isset($request->franchise_id)?$request->franchise_id:0;

//   if(isset($request->country_id) && !empty($request->country_id))
//   {
//     $obj->belongs = "country-".($request->country_id);
//   }
//   if (isset($request->city_id) && !empty($request->city_id)) {

//    $obj->belongs = "city-".($request->city_id);
//  }
//  if(isset($request->branch_id) && !empty($request->branch_id))
//  {
//   $obj->belongs = "branch-".($request->branch_id);
// }
// if(isset($request->franchise_id) && !empty($request->franchise_id))
// {
//   $obj->belongs = "franchise-".($request->franchise_id);
// }
$admin_affected =$obj->save();

if(isset($request['roles']) && !empty($request['roles']))
  $obj->syncRoles($request['roles']);

if(isset($request['permissions']) && !empty($request['permissions']))
  $obj->syncPermissions($request['permissions']);

if($admin_affected)
  session()->flash('success', 'Record has been updated. successfully.');
else
  session()->flash('danger', 'Fail to update the record.');

return redirect()->route('adminResource.index');
}
}

public function show($id)
{
  $admin = Admin::find($id);
  return view('admin.profile', compact('admin'));  
}


public function edit($id)
{
  $admin = Admin::find($id);
  $permissions = Permission::all();
  $roles = Role::all();
  return view('admin.adminEdit', compact('admin','permissions','roles'));
}


public function update(Request $request, $id)
{
  $admin = Admin::findOrFail($id);

  $data = $request->except(['roles','permissions']);
  if(empty($data['password']))
    unset($data['password']);

  $permissions = Permission::all();
  foreach ($permissions as $p) {
    if($admin->hasDirectPermission($p))
      $admin->revokePermissionTo($p); 
  }
  $roles = Role::all();
  foreach ($roles as $role) {
    if($admin->hasRole($role))
      $admin->removeRole($role);
  }

  if(isset($request['roles']) && !empty($request['roles']))
    $admin->syncRoles($request['roles']);

  if(isset($request['permissions']) && !empty($request['permissions']))
    $admin->syncPermissions($request['permissions']);


  if($request->hasfile('images')){
    $profiles = $request->file('images')->getClientOriginalName();
    $Extension_profile = $request->file('images')->getClientOriginalExtension();
    $profile = 'profiles'.'_'.date('YmdHis').'.'.$Extension_profile;
    $request->file('images')->move('images/admin/profile/', $profile);
    $data['images'] =$profile;
  }
  $affected_admin =$admin->update($data);

  if($affected_admin)
    session()->flash('success', 'Record has been updated. successfully.');
  else
    session()->flash('danger', 'Fail to update the record.');

  return redirect()->route('adminResource.index');
}


public function delete_record($id)
{

  $User = Admin::find($id);
  $effected=$User->delete();
  
  if($effected){
    $response['status'] = true;
    $response['message'] = "User has been delete Successfully.";
    session()->flash('success_message', 'Record has been updated. successfully.');
  }else{
    $response['status'] = false; 
    $response['message'] = "Fail to delete User.";
    session()->flash('danger', 'Fail to delete User.');
  }
  
  return redirect()->route('adminResource.index');
}

public function assignRole( $id)
{
  $permissions = Permission::all();
  $roles = Role::all();
  $admin = Admin::find($id);
  return view('admin.asignRole', compact(['admin', 'permissions', 'roles' ]));

}
public function giveRole(request $request){
  $role = Role::create(['guard_name' => 'admin','name' =>  $name]);
  $permissions = $request['permissions'];
  if(!empty($permissions))
    $role->syncPermissions($permissions);

  $user->assignRole($request->role_id);

}
public function get_admin(Request $request)
{

  $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
  $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

  $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
  $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

  $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
  $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

  $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

  $pages = 1;
  $total = Admin::count();

        // $perpage 0; get all data
  if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
              $offset = 0;
            }

          }
          $users = Admin::offset($offset)->limit($perpage)->orderBy($field,$sort);

          if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
            ->orWhere('fname','like',"%{$filter}%")
            ->orWhere('email','like',"%{$filter}%")
            ->orWhere('phone','like',"%{$filter}%")
            ->orWhere('status','like',"%{$filter}%")
            ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

          }

          $data=$users->get();
          $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
          );

          $result = array(
            'meta' => $meta + array(
              'sort'  => $sort,
              'field' => $field,
            ),
            'data' => $data
          );
          // print_r($result);exit();
          return  response()->json($result);
        }

        public function downloadExcel($type)
        {
          $data = Admin::all()->toArray();

          return Excel::create('laravelcode', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
              $sheet->fromArray($data);
            });
          })->download($type);
        }

        public function admin_change_status(Request $request)
        {

          $id = $request->input('user_id');
          $status = $request->input('activity');
          $user = Admin::find($id) ;
          $user->status  = $status;
          $effected=$user->save();

          $response = array();
          if($effected){
            $response['activity'] = true;
            $response['message']=" Status has been changed Successfully.";
          }else{
            $response['activity'] = false;
            $response['message']="Fail to change the user status.";
          }
          return response()->json($response, 200);

        }



      }
