<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\AdvanceBooking;
use Illuminate\Support\Facades\Input;

class AdvanceBookingController extends Controller
{

	public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {
        $book = AdvanceBooking::orderBy('id', 'DESC')->with("user")->with("vehical")->with("good")->paginate(5);
       
        $books =AdvanceBooking::count();
        return view('admin.advanceBooking' , compact([ 'book', 'books']));
    }
}
