<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Booking;
use App\Captain;
use App\BookingDestination;
use App\RidesCompleted;
use App\AdvanceBooking;
use App\AdvanceBookingDestination;
use App\Ride;
use App\VehicalCategorie;
use App\VehicleSubCategorie;
use App\Financial;
use App\User;
use App\Emergency;
use DB;
use Auth;
use App\Http\Resources\BookingCollection;
use App\Http\Resources\RideCollection;
use Illuminate\Support\Facades\Log;

class ApiBookingController extends Controller
{
    function user_booking(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'pickup_longitude' => 'required',
                'pickup_latitude' => 'required',
                'booking_date' => 'required',
                'vehical_sub_id' => 'required',
                'good_id' => 'required',
                'amount' => 'required',
                'distance' => 'required',
                'destination' => 'required',
                'labour_loader' => 'required',
                'labour_unloader' => 'required',
                'booking_type' => 'required',
                'PaymentMethod' => 'required',
                'pickup_place' => 'required',
                'type' => 'required',
                'serge' => 'required',
            ]
        );

        Log::info('All request user_booking:' . json_encode($request->all()));
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {

            DB::beginTransaction();
            $booking = Booking::create([
                'user_id' => Auth::guard('api')->id(),
                'pickup_longitude' => request('pickup_longitude'),
                'pickup_latitude' => request('pickup_latitude'),
                'booking_date' => request('booking_date'),
                'vehical_sub_id' => request('vehical_sub_id'),
                'vehical_cat_id' => request('vehical_cat_id'),
                'PaymentMethod' => request('PaymentMethod'),
                'good_id' => request('good_id'),
                'pickup_place' => request('pickup_place'),
                'amount' => request('amount'),
                'distance' => request('distance'),
                'labour_loader' => request('labour_loader'),
                'labour_unloader' => request('labour_unloader'),
                'booking_type' => request('booking_type'),
                'old_booking_id' => request('old_booking_id'),
                'captain_id' => request('captain_id'),
                'serge' => request('serge'),
            ]);

            $affected_booking = $booking->save();
            if ($affected_booking) {
                $destinations = json_decode(request('destination'));

                $data_dest = array();
                foreach ($destinations as $dest) {
                    $tmp_dest = array();
                    $tmp_dest['longitude'] = $dest->longitude;
                    $tmp_dest['latitude'] = $dest->latitude;
                    $tmp_dest['booking_id'] = $booking->id;
                    $tmp_dest['drop_place'] = $dest->drop_place;
                    $tmp_dest['created_at'] = now();
                    $tmp_dest['updated_at'] = now();
                    $data_dest[] = $tmp_dest;
                }

                $booking_dest = BookingDestination::insert($data_dest);

                if ($booking_dest) {
                    DB::commit();

                    if ($request->booking_type == "suplimentory_booking") {
                        $data = Captain::where('id', $request->captain_id)->first();
                        Log::info('suplimentory_booking captains ids :' . $data);
                        $tokens[] = trim($data->device_token);
                    } else {

                        $captains = Captain::select('device_token')->where('labour_loader', '>=', $request->labour_loader)->where('vehical_cat_id', $request->vehical_cat_id)->where('vehical_sub_id', $request->vehical_sub_id)->where('labour_unloader', '>=', $request->labour_unloader)->where('activity', 1)->where('status', 1)->get();
                        Log::info('All captains ids :' . $captains);

                        $tokens = array();
                        if (count($captains) >= 1) {
                            $index = 0;
                            foreach ($captains as $captain) {
                                if (isset($captain->device_token) && !empty($captain->device_token))
                                    $tokens[$index++] = $captain->device_token;
                            }
                        } else {
                            $status = true;
                            $message = "Vehicle not found related to your booking";
                            return response()->json(['status' => $status, 'message' => $message, 'booking_id' => $booking->id], 200);
                        }
                    }


                    if (count($tokens) > 0) {

                        $message = array('booking_id' => $booking->id, 'destination_place' => $destinations[0]->drop_place, 'pickup_place' => request('pickup_place'), 'distance' => $request->distance, 'type' => $request->type, 'vehical_cat_id' => $request->vehical_cat_id, 'good_id' => $request->good_id, 'user_id' => Auth::guard('api')->id(), 'message' => 'You have received a booking Request. Tap to View');

                        Log::info('Message booking notification to captains :' . json_encode($message));
                        $message_status = captain_send_notification($tokens, $message, "NewTripRequest");
                        Log::info('booking notification to captains   :' . $message_status);
                        $total_captain = Captain::select('device_token')->where('labour_loader', '>=', $request->labour_loader)->where('vehical_cat_id', $request->vehical_cat_id)->where('vehical_sub_id', $request->vehical_sub_id)->where('labour_unloader', '>=', $request->labour_unloader)->where('activity', 1)->where('status', 1)->count();
                        $input['total_captain'] = $total_captain;
                        $updated_booking_record = Booking::where('id', $booking->id)->update($input);
                        //echo $message_status;
                        $status = true;
                        $message = "Your ride booked successfully.";
                        return response()->json(['message' => $message, 'status' => $status, 'booking_id' => $booking->id], 200);
                    } else {
                        $status = true;
                        $message = "Vehicle not found related to your booking";
                        return response()->json(['status' => $status, 'message' => $message, 'booking_id' => $booking->id], 200);
                    }
                } else {
                    DB::rollBack();
                    $status = false;
                    $message = "Booking Fail at this time.";
                    return response()->json(['message' => $message, 'status' => $status], 200);
                }
            } else {

                $status = false;
                $message = "Booking Fail at this time.";
                return response()->json(['message' => $message, 'status' => $status], 200);
            }
        }
    }

    public function captain_ride_history(Request $request)
    {
        $response = Ride::where('status', '<>', -1)->orderby('id', 'DESC')->with('RideCompleted')->with('booking')->with('booking.BookingDestination')->with('user')->with('captain_rate')->where('captain_id', Auth::guard('captainapi')->id())->get();
        // return ($response);
        if (count($response) > 0) {
            $status = true;
            $responsen = new RideCollection($response);
            $message = "history found successfully";
            return response()->json(['status' => $status, 'message' => $message, 'data' => $responsen], 200);
        } else {
            $status = false;
            $message = " record not found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    public function captain_ride_reject_history(Request $request)
    {

        $response = Ride::where('status', -1)->orderby('id', 'DESC')->with('RideCompleted')->with('booking')->with('booking.BookingDestination')->with('user')->where('captain_id', Auth::guard('captainapi')->id())->with('transaction')->get();

        if (count($response) > 0) {
            $status = true;
            $responsen = new RideCollection($response);
            $message = "history found successfully";
            return response()->json(['status' => $status, 'message' => $message, 'data' => $responsen], 200);
        } else {
            $status = false;
            $message = " record not found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    public function user_ride_history(Request $request)
    {

        $responsed = Booking::orderby('id', 'DESC')->where('user_id', Auth::guard('api')->id())->with('BookingDestination')->with('good')->with('vehicleSubCategorie.vehiclecategorie')->with('Rides')->with('Rides.captain')->with('Rides.RideCompleted')->with('Rides.user_rate')->with('Rides.transaction')->get();

        if (count($responsed)) {

            $status = true;
            $responsen = new BookingCollection($responsed);
            $message = "history found successfully";
            return response()->json(['status' => $status, 'message' => $message, 'data' => $responsen], 200);
        } else {
            $status = false;
            $message = " record not found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    public function get_job_data(request $request)
    {
        $record = Booking::with('BookingDestination')->where('id', $request->id)->with('good')->with('user')->first();
        if (!empty($record)) {
            $status = true;
            $message = "history found successfully";
            return response()->json(['status' => $status, 'message' => $message, 'record' => $record], 200);
        } else {
            $status = false;
            $message = " record not found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    function user_notification(Request $request)
    {

        $rideDetail = Ride::where('id', $request->id)->where('user_id', Auth::guard('api')->id())->with('captain')->with('vehical')->first();

        if (!empty($rideDetail)) {
            $status = true;
            $message = "Ride detail successfully.";
            return response()->json(['status' => $status, 'message' => $message, 'data' => $rideDetail], 200);
        } else {
            $status = false;
            $message = "Ride detail not getting.";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    function fareAccount(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'booking_id' => 'required',
                'ride_id' => 'required',
            ]
        );
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {
            $loading_waiting_price2 = 0;
            $base_fare2 = 0;
            $distance2 = 0;
            $total_serg2 = 0;
            $toll_plaza2 = 0;
            $destiation_amount2 = 0;
            $booking_destination2 = 0;
            $total_ride_time2 = 0;
            $total_commision2 = 0;
            $ride_complete2 = 0;
            $loading_time_consumed2 = 0;
            $Unloading_time_consumed2 = 0;
            $finance = Financial::first();
            $booking = Booking::select('*')->where('id', $request->booking_id)->with('vehicleSubCategorie')->with('vehicleSubCategorie.vehiclecategorie')->first();

            $ride = Ride::where('id', $request->ride_id)->first();
            $ride_complete = RidesCompleted::where('ride_id', $request->ride_id)->sum('unloading_waiting_time');

            // return $booking;
            $booking_destination = BookingDestination::where('booking_id', $request->booking_id)->count();

            $distance = $booking->distance;
            $commessions = $finance->commisions;
            $wait_amount = $finance->wait_amount;
            $Serg_fee = $booking->Serg_fee;
            $toll_plaza = $booking->toll_plaza;
            $labour_unloader = $booking->labour_unloader;
            $labour_loader = $booking->labour_loader;
            $amount_per_km = $booking->vehicleSubCategorie->vehiclecategorie->amount_per_km;
            $amount_per_min = $booking->vehicleSubCategorie->vehiclecategorie->amount_per_min;
            $destination_charge = $booking->vehicleSubCategorie->vehiclecategorie->destination_charge;
            $base_fare = $booking->vehicleSubCategorie->vehiclecategorie->base_fare;
            $free_time = $booking->vehicleSubCategorie->vehiclecategorie->free_time;
            // dd($free_time);
            $loading_waiting_price = $booking->vehicleSubCategorie->vehiclecategorie->loading_waiting_price;
            $unloading_waiting_price = $booking->vehicleSubCategorie->vehiclecategorie->unloading_waiting_price;
            ///calculation fare
            $amount_km = $amount_per_km * $distance;
            $amount_min = $amount_per_min * $distance;
            $total_commision = ($commessions * $amount_km) / 100;
            $total_commision_min = ($commessions * $amount_min) / 100;
            $total_amount = $amount_km + $amount_min + $total_commision_min + $total_commision;
            ///calculation serg farer
            $total_serg = ($Serg_fee * $total_amount) - $total_amount;
            $destiation_amount = $booking_destination * $destination_charge;

            ///calculate the free time loading_waiting_time
            $loading_waiting_time = $ride->loading_time - $free_time;
            $unloading_time = $ride->unloading_time - $free_time;
            $total_ride_time = $ride->total_ride_time;
            $time = $total_ride_time + $unloading_time + $loading_waiting_time;
            //////// calculate loading waiting time
            if ($free_time < $loading_waiting_time) {
                $loading_time_consumed = ($loading_waiting_time - $free_time) * $loading_waiting_price;
            } else {
                $loading_time_consumed = 0;
            }
            //////// Unloading waiting time
            if ($free_time < $ride_complete) {
                $Unloading_time_consumed = ($unloading_time - $free_time) * $unloading_waiting_price;
            } else {
                $Unloading_time_consumed = 0;
            }

            //         if($booking->booking_type == 'dispute_booking' or $booking->booking_type =='suplimentory_booking'){

            //           $bookings = Booking::where('id',$booking->old_booking_id)->with('vehicleSubCategorie')->with('vehicleSubCategorie.vehiclecategorie')->first();
            //           $ride = Ride::where('booking_id',$bookings->id)->first();

            //         $ride_complete2 = RidesCompleted::where('ride_id',$ride->id)->sum('unloading_waiting_time');
            //        $finance = Financial::first();
            // // return $booking;
            //        $booking_destination2 = BookingDestination::where('booking_id',$bookings->id)->count();
            //        $distance2 = $bookings->distance;
            //        $commessions = $finance->commisions;
            //        $wait_amount = $finance->wait_amount;
            //        $Serg_fee = $bookings->Serg_fee;    
            //        $toll_plaza2 = $bookings->toll_plaza;
            //        $labour_unloader = $bookings->labour_unloader;
            //        $labour_loader = $bookings->labour_loader;
            //        $amount_per_km = $bookings->vehicleSubCategorie->vehiclecategorie->amount_per_km;
            //        $amount_per_min = $bookings->vehicleSubCategorie->vehiclecategorie->amount_per_min;
            //        $destination_charge = $bookings->vehicleSubCategorie->vehiclecategorie->destination_charge;
            //        $base_fare2 = $bookings->vehicleSubCategorie->vehiclecategorie->base_fare;
            //        $free_time = $bookings->vehicleSubCategorie->vehiclecategorie->free_time;
            //     // dd($free_time) ;
            //        $loading_waiting_price2 = $bookings->vehicleSubCategorie->vehiclecategorie->loading_waiting_price; 
            //        $unloading_waiting_price2 = $bookings->vehicleSubCategorie->vehiclecategorie->unloading_waiting_price;
            //        ///calculation fare 
            //        $amount_km = $amount_per_km * $distance; 
            //        $total_commision2 = ($commessions * $amount_km)/100;

            //        $total_amount = $amount_km + $base_fare2  + $total_commision2;
            //        ///calculation serg farer 
            //        $total_serg2 = ($Serg_fee * $total_amount) - $total_amount;
            //        $serge_charge = ($Serg_fee * $total_amount) - $total_amount;
            //         $destiation_amount2= $booking_destination2 * $destination_charge;


            //        // /calculate the free time loading_waiting_time
            //        $loading_waiting_time2 = $ride->loading_waiting_time;
            //        $total_ride_time2 = $ride->total_ride_time;
            //        $time = $loading_waiting_time2 - $free_time ;
            //             //////// calculate loading waiting time 
            //            if($free_time < $loading_waiting_time2){
            //             $loading_time_consumed2 = ($loading_waiting_time2 - $free_time) * $loading_waiting_price2;
            //           }else{
            //             $loading_time_consumed2 = 0;
            //           }
            //           //////// Unloading waiting time
            //           if($free_time < $ride_complete2){
            //             $Unloading_time_consumed = ($ride_complete2 - $free_time) * $unloading_waiting_price2;
            //           }else{
            //             $Unloading_time_consumed2 = 0;
            //           }
            //        }
            // dd($destiation_amount);exit;
            if ($time > 0) {
                if ($loading_waiting_time > 0) {
                    $loading_wait_cost = $time * ($loading_waiting_price + $loading_waiting_price2);
                } else {
                    $loading_wait_cost = 0;
                }
                // + ($destiation_amount +$destiation_amount2)
                $total_fare = ($base_fare) + ($toll_plaza + $toll_plaza2) + ($loading_time_consumed + $loading_time_consumed2) + ($Unloading_time_consumed + $Unloading_time_consumed2) + $loading_wait_cost;
            } else {
                // destiation_amount2 60
                // + ($destiation_amount + $destiation_amount2)
                // echo $Unloading_time_consumed + $Unloading_time_consumed2;exit;
                $total_fare = ($base_fare) + ($toll_plaza + $toll_plaza2) + ($loading_time_consumed + $loading_time_consumed2) + ($Unloading_time_consumed + $Unloading_time_consumed2) + ($distance * $amount_per_km) + ($distance2 * $amount_per_km) + ($distance2 * $amount_per_min);
            }


            if (!empty($booking)) {
                $status = true;
                $All_fare = [
                    "base_fare" => $base_fare + $amount_per_min * round(abs($ride->total_ride_time), 2),
                    'distance' => round(abs($distance + $distance2)),
                    'amount_per_km' => $amount_per_km,
                    'amount_per_min' => $amount_per_min,
                    'toll_plaza' => ($toll_plaza + $toll_plaza2),
                    'total_ride_time' => round(abs($ride->total_ride_time), 2) . ' Min ',
                    'commessions' => round($total_fare * 20 / 100),
                    'total_commision' => round(abs($total_fare * 20 / 100), 2),
                    'unloading_waiting_time' => ($ride_complete + round(abs($ride_complete2), 2)) . ' Min',
                    'total_fare' => $total_fare
                ];
                $booking_updation['amount'] = $total_fare;
                $booking_updation['serge_charge'] = ($total_serg ? $total_serg : 0) + ($total_serg2 ? $total_serg2 : 0);
                $booking_updation['loading_charge'] = $loading_time_consumed + $loading_time_consumed2;
                $booking_updation['unloading_charge'] = $Unloading_time_consumed + $Unloading_time_consumed2;
                $booking_updation['distance_charge'] = ($distance * $amount_per_km) + ($distance2 * $amount_per_km) + ($distance2 * $amount_per_min) + $amount_per_min * round(abs($ride->total_ride_time), 2);
                $update_booking_amount = Booking::findOrFail($request->booking_id)->update($booking_updation);

                $message = "finance detail found successfully";
                return response()->json(['status' => $status, 'message' => $message, 'data' => $total_fare, 'All_fares' => $All_fare], 200);
            } else {
                $status = false;
                $message = "finance detail not found.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    function userfare(Request $request)
    {
        $finance['fare'] = Financial::get();
        if (!empty($finance)) {
            $status = true;
            $message = "finance detail found successfully.";
            return response()->json(['status' => $status, 'message' => $message, 'data' => $finance], 200);
        } else {
            $status = false;
            $message = "finance detail not found.";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    public function update_loading_time(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'loading_time' => 'required',
            'captain_id' => 'required',
            'booking_id' => 'required',
        ]);

        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {
            $update_Booking_Status = Ride::where('booking_id', $request->booking_id)->where('user_id', Auth::guard('api')->id())->update(array('loading_time' => $request->loading_time));
            Log::info(' update booking in loading_time :' . $update_Booking_Status);
            if (!empty($update_Booking_Status)) {
                $get_token = Captain::where('id', $request->captain_id)->first();
                $token[] = trim($get_token->device_token);
                $message = ["captain_id" => $request->captain_id, 'booking_id' => $request->booking_id, 'message' => 'loading is done??', 'type' => 'loading_time'];

                Log::info('Notify: captain Tokens : ' . json_encode($token));
                $message_status = captain_send_notification($token, $message, null);
                Log::info('Notify: loading time is over: ' . $message_status);
                $message = "loading_time";
                return response()->json(['message' => $message, 'notify' => $message_status], 200);
            } else {
                $status = false;
                $message = "loading time is not updated.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    public function captainLoadTimeStart(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {
            $get_token = User::where('id', $request->user_id)->first();
            $token[] = trim($get_token->device_token);
            $message = ["user_id" => $request->user_id, 'booking_id' => $request->booking_id, 'message' => 'Loading Time Start Now', 'type' => 'startLoadingTime'];

            $message_status = user_send_notification($token, $message, null);
            if ($message_status) {
                $status = true;
                $message = "Loading Time Start Now";
                return response()->json(['status' => $status, 'message' => $message, 'notify' => $message_status], 200);
            } else {
                $status = false;
                $message = "loading time is not updated.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }


    public function captain_loading_time(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'loading_time' => 'required',
            'user_id' => 'required',
            'booking_id' => 'required',
        ]);

        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {
            $update_Booking_Status = Ride::where('booking_id', $request->booking_id)->where('captain_id', Auth::guard('captainapi')->id())->update(array('loading_time' => $request->loading_time));
            Log::info(' update booking in loading_time :' . $request->loading_time);
            if (!empty($update_Booking_Status)) {
                $get_token = User::where('id', $request->user_id)->first();
                $token[] = trim($get_token->device_token);
                $message = ["user_id" => $request->user_id, 'booking_id' => $request->booking_id, 'message' => 'loading is done??', 'type' => 'end_loading_time'];

                Log::info('Notify: captain Tokens : ' . json_encode($token));
                $message_status = user_send_notification($token, $message, null);
                Log::info('Notify: loading time is over: ' . $message_status);
                $message = "loading_time";
                return response()->json(['message' => $message, 'notify' => $message_status], 200);
            } else {
                $status = false;
                $message = "loading time is not updated.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    function booking_reject_request(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'status' => 'required',
                'booking_id' => 'required',
                'reject_booking_image' => 'required',
            ]
        );
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            if ($image = $request->file('reject_booking_image')) {
                $reject_booking_image = $request->file('reject_booking_image')->getClientOriginalName();
                $Extension_profile = $request->file('reject_booking_image')->getClientOriginalExtension();
                $reject_booking_image = 'reject_booking_image' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('reject_booking_image')->move('images/reject_booking_image/', $reject_booking_image);
            }
            $status = true;
            $data_booking['status'] = 0;
            $data_booking['amount'] = 0;
            $data_booking['reject_booking_image'] = $reject_booking_image;
            $data_booking['rejected_by'] = Auth::guard('api')->id();
            $update_booking_status = Booking::where('id', $request->booking_id)->update($data_booking);

            // $update_Ride_Status = Booking::where('id', $request->booking_id)->where('user_id', Auth::guard('api')->id())->update(array('rejected_by'=> Auth::guard('api')->id()));

            if (isset($update_booking_status) && !empty($update_booking_status)) {
                $captains = Captain::select('device_token')->where('activity', 1)->where('status', 1)->get();
                Log::info('All captains ids :' . $captains);
                $tokens = array();
                $index = 0;
                foreach ($captains as $captain) {
                    if (isset($captain->device_token) && !empty($captain->device_token))
                        $tokens[$index++] = $captain->device_token;
                }

                if (count($tokens) > 0) {

                    $message = array('message' => 'booking has been rejected by user', 'type' => 'user_rejected_booking');
                    $message_status = captain_send_notification($tokens, $message, null);
                    Log::info('booking rejected by notification   :' . $message_status);
                }
            }
            if ($update_booking_status) {
                $status = true;
                $message = "User Rejected the Booking";
                return response()->json(['status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = "Booking not Reject";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }


    function captain_booking(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'pickup_longitude' => 'required',
                'pickup_latitude' => 'required',
                'booking_date' => 'required',
                'vehical_sub_id' => 'required',
                'good_id' => 'required',
                'amount' => 'required',
                'distance' => 'required',
                'booking_type' => 'required',
                'PaymentMethod' => 'required',
                'type' => 'required',
                'vehical_cat_id' => 'required',
                'covered_distance' => 'required',
                'pickup_place' => 'required',
                'serge' => 'required',
            ]
        );
        if (isset($covered_distance) && !empty($covered_distance)) {
            $booking_updation['distance'] = $covered_distance;
            $booking_updation['old_booking_id'] = $request->booking_id;
            $booking_updation['emergencies_reasons'] = $request->emergencies_reasons;
            $booking_updation['status'] = 4;
            $update_booking_Status = Booking::findOrFail($request->booking_id)->update($booking_updation);
            Log::info('total Emergencies trip distance:' . $covered_distance);
        }

        Log::info('All request captain_booking:' . json_encode($request->all()));
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {
            DB::beginTransaction();
            $booking = Booking::create([
                'captain_id' => Auth::guard('captainapi')->id(),
                'pickup_longitude' => $request->pickup_longitude,
                'pickup_latitude' => $request->pickup_latitude,
                'booking_date' => $request->booking_date,
                'vehical_sub_id' => $request->vehical_sub_id,
                'vehical_cat_id' => $request->vehical_cat_id,
                'PaymentMethod' => $request->PaymentMethod ? $request->PaymentMethod : 'cash',
                'good_id' => $request->good_id,
                'pickup_place' => $request->pickup_place,
                'amount' => $request->amount ? $request->amount : 0,
                'distance' => $request->distance,
                'old_booking_id' => $request->booking_id,
                'booking_type' => $request->booking_type ? $request->booking_type : 'dispute_booking',
                'user_id' => $request->user_id,
                'labour_unloader' => $request->labour_unloader ? $request->labour_unloader : 0,
                'labour_loader' => $request->labour_loader ? $request->labour_loader : 0,
                'estimated_time' => $request->estimated_time ? $request->estimated_time : 0,
                'emergencies_reasons' => $request->emergencies_reasons,
                'serge' => $request->serge,

            ]);

            $affected_booking = $booking->save();
            if ($affected_booking) {
                if (!empty($request->destination)) {
                    $destinations = json_decode(request('destination'));
                    $data_dest = array();
                    foreach ($destinations as $dest) {
                        $tmp_dest = array();
                        $tmp_dest['longitude'] = $dest->longitude;
                        $tmp_dest['latitude'] = $dest->latitude;
                        $tmp_dest['booking_id'] = $booking->id;
                        $tmp_dest['drop_place'] = $dest->drop_place;
                        $tmp_dest['created_at'] = now();
                        $tmp_dest['updated_at'] = now();
                        $data_dest[] = $tmp_dest;
                    }
                    $booking_dest = BookingDestination::insert($data_dest);
                } else {
                    $booking_dest = 1;
                }

                if ($booking_dest) {
                    DB::commit();
                    if ($request->user_id) {
                        $get_token = User::where('id', $request->user_id)->first();
                        $token[] = $get_token->device_token;
                        $message = ['message' => 'The Vehicle is halted', 'emergencies_reasons' => $request->emergencies_reasons, 'booking_id' => $request->booking_id, 'type' => 'vehicle_halted'];
                        $message_statu = user_send_notification($token, $message, null);
                        Log::info('Emergencies Notification   :' . $message_statu);
                    }
                    $captains = Captain::select('device_token')->whereNotIn('id', [$booking->captain_id])->where('labour_loader', '>=', $booking->labour_loader)->where('labour_unloader', '>=', $booking->labour_unloader)->where('activity', 1)->where('status', 1)->get();

                    Log::info('All captains ids :' . $captains);
                    $tokens = array();
                    $index = 0;
                    foreach ($captains as $captain) {
                        if (isset($captain->device_token) && !empty($captain->device_token))
                            $tokens[$index++] = $captain->device_token;
                    }

                    if (count($tokens) > 0) {
                        $message = array('booking_id' => $booking->id, 'vehical_cat_id' => $request->vehical_cat_id, 'good_id' => $request->good_id, 'vehicle_id' => $request->vehical_cat_id, 'type' => $request->type, 'user_id' => $request->user_id, 'message' => 'booking_success');
                        $message_status = captain_send_notification($tokens, $message, null);
                        Log::info('captain Booking notification to captains   :' . $message_status);
                        Log::info('without destination booking notification to captains   :' . $message_status);
                        $total_captain = Captain::select('device_token')->where('labour_loader', '>=', $request->labour_loader)->where('vehical_cat_id', $request->vehical_cat_id)->where('vehical_sub_id', $request->vehical_sub_id)->where('labour_unloader', '>=', $request->labour_unloader)->where('activity', 1)->where('status', 1)->count();
                        $input['total_captain'] = $total_captain;
                        $updated_booking_record = Booking::where('id', $booking->id)->update($input);
                        //echo $message_status;
                        $status = true;
                        $message = "You have booking successfully.";
                        return response()->json(['message' => $message, 'status' => $status, 'booking_id' => $booking->id], 200);
                    } else {
                        $status = true;
                        $message = "Vehicle not found related to your booking";
                        return response()->json(['status' => $status, 'message' => $message, 'booking_id' => $booking->id], 200);
                    }
                } else {
                    DB::rollBack();
                    $status = false;
                    $message = "Booking Fail at this time.";
                    return response()->json(['message' => $message, 'status' => $status], 200);
                }
            } else {
                $status = false;
                $message = "Booking Fail at this time.";
                return response()->json(['message' => $message, 'status' => $status], 200);
            }
        }
    }

    function without_destination_booking(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'pickup_longitude' => 'required',
                'pickup_latitude' => 'required',
                'booking_date' => 'required',
                'vehical_sub_id' => 'required',
                'good_id' => 'required',
                'amount' => 'required',
                'distance' => 'required',
                'booking_type' => 'required',
                'PaymentMethod' => 'required',
                'type' => 'required',
                'serge' => 'required',
            ]
        );

        Log::info('All request with_out_destination_booking:' . json_encode($request->all()));
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {
            $booking = Booking::create([
                'user_id' => Auth::guard('api')->id(),
                'pickup_longitude' => request('pickup_longitude'),
                'pickup_latitude' => request('pickup_latitude'),
                'booking_date' => request('booking_date'),
                'vehical_sub_id' => request('vehical_sub_id'),
                'vehical_cat_id' => request('vehical_cat_id'),
                'PaymentMethod' => request('PaymentMethod') ? $request->PaymentMethod : 'cash',
                'good_id' => request('good_id'),
                'pickup_place' => request('pickup_place'),
                'amount' => request('amount') ? $request->amount : 0,
                'distance' => request('distance') ? $request->distance : 0,
                'labour_loader' => request('labour_loader'),
                'labour_unloader' => request('labour_unloader'),
                'booking_type' => request('booking_type'),
                'old_booking_id' => request('old_booking_id'),
                'captain_id' => request('captain_id'),
                'serge' => $request->serge,
            ]);

            $affected_booking = $booking->save();
            Log::info('without destination booking successfully done:' . $affected_booking);
            if ($affected_booking) {

                $captains = Captain::select('device_token')->where('labour_loader', '>=', $request->labour_loader)->where('labour_unloader', '>=', $request->labour_unloader)->where('vehical_cat_id', $request->vehical_cat_id)->where('vehical_sub_id', $request->vehical_sub_id)->where('activity', 1)->where('status', 1)->get();
                Log::info('All captains ids :' . $captains);
                $tokens = array();
                $index = 0;
                foreach ($captains as $captain) {
                    if (isset($captain->device_token) && !empty($captain->device_token))
                        $tokens[$index++] = $captain->device_token;
                }

                if (count($tokens) > 0) {
                    $message = array('booking_id' => $booking->id, 'user_id' => Auth::guard('api')->id(), 'vehical_cat_id' => $request->vehical_cat_id, 'good_id' => $request->good_id, 'type' => $request->type, 'message' => 'without_destination_booking');
                    $message_status = captain_send_notification($tokens, $message, null);

                    Log::info('without destination booking notification to captains   :' . $message_status);
                    $total_captain = Captain::select('device_token')->where('labour_loader', '>=', $request->labour_loader)->where('vehical_cat_id', $request->vehical_cat_id)->where('vehical_sub_id', $request->vehical_sub_id)->where('labour_unloader', '>=', $request->labour_unloader)->where('activity', 1)->where('status', 1)->count();
                    $input['total_captain'] = $total_captain;
                    $updated_booking_record = Booking::where('id', $booking->id)->update($input);
                    //echo $message_status;
                    $status = true;
                    $message = "You have booking successfully.";
                    return response()->json(['message' => $message, 'status' => $status, 'booking_id' => $booking->id], 200);
                } else {
                    $status = true;
                    $message = "Vehicle not found related to your booking";
                    return response()->json(['status' => $status, 'message' => $message, 'booking_id' => $booking->id], 200);
                }
            } else {
                $status = false;
                $message = "Booking Fail at this time.";
                return response()->json(['message' => $message, 'status' => $status], 200);
            }
        }
    }
}
