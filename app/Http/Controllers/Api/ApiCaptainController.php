<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Captain;
use Validator;
use App\Rate;
use App\CaptainStats;
use App\Booking;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Log;
use App\User;
use App\RejectBookingRecord;
class ApiCaptainController extends Controller
{

    public function update_captain_location(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'longitude' => 'required',
                'latitude' => 'required',
            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {
            $longitude = $request->longitude;
            $latitude = $request->latitude;
            $device_token = $request->device_token;
            $booking_id = isset($request->booking_id) ? $request->booking_id : null;
            $captain = Captain::select('device_token')->where('id', Auth::guard('captainapi')->id())->get()->first();
            if ($captain->device_token == $device_token) {
                $driverlocation = Captain::where('id', Auth::guard('captainapi')->id())->update(array('longitude' => $longitude, 'latitude' => $latitude, 'last_loc_update' => date('Y-m-d H:i:s'), 'status' => 1));
                if ($driverlocation) {
                    $status = true;
                    if ($booking_id && $request->status == 'Start') {
                        $booking = Booking::select('pickup_longitude', 'pickup_latitude')->where('id', $booking_id)->get()->first();
                        $km = distanceCalculation($booking->pickup_latitude, $booking->pickup_longitude, $request->latitude, $request->longitude);
                        Booking::where('id', $booking_id)->update(['distance_aft_ride' => $km, 'last_dis_up' => date('Y-m-d H:i:s')]);
                    }
                    $message = "Location updated successfully.";
                    Log::info($message . Auth::guard('captainapi')->id() . ' Latitude: ' . $request->latitude . ' Longitude: ' . $request->longitude . ' device: ' . $driverlocation);
                    return response()->json(['status' => $status, 'message' => $message], 200);
                } else {
                    $status = false;
                    $message = "Fail to update.";
                    return response()->json(['status' => $status, 'message' => $message], 200);
                }
            } else {
                $status = false;
                $message = "Another Device Detected";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    public function get_captain_location()
    {
        $location = Captain::where('id', Auth::guard('captainapi')->id())->first();
        if (!empty($location)) {
            $status = true;
            $message = "captain location tracking successfully.";
            return response()->json(['status' => $status, 'message' => $message, 'location' => $location], 200);
        } else {
            $status = false;
            $message = "no record found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    public function get_captain_location_by_id(Request $request)
    {
        $location = Captain::where('id', $request->id)->first();
        if (!empty($location)) {
            $status = true;
            $message = "captain location tracking successfully.";
            return response()->json(['status' => $status, 'message' => $message, 'location' => $location], 200);
        } else {
            $status = false;
            $message = "no record found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }
    }

    public function update_device_token(Request $request)
    {
        // validation the form data;
        $validator = Validator::make($request->all(),
            [
                'device_token' => 'required',
            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {
            $request->request->add(['status' => '1', 'last_loc_update' => date('Y-m-d h:i:s')]);
            $deviceToken = Captain::where('id', Auth::guard('captainapi')->id())->update($request->all());

            if ($deviceToken) {

                $status = true;

                $message = "update_device_token  successfully.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = "no record found";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }

    }

    public function update_captain_activity(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'status' => 'required'
            ]);
        Log::info('update_captain_activity: ' . json_encode($request->all()));
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {

            $update = Captain::where('id', Auth::guard('captainapi')->id())->update($request->all());
            if ($update) {
                $status = true;
                $message = 'Captain Activity Updated';
                return response()->json(['status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = 'Fail Captain Activity Updated';
                return response()->json(['status' => $status, 'message' => $message], 200);
            }

        }
    }

    public function get_captain_activity()
    {
        $activity = Captain::select('status')->where('id', Auth::guard('captainapi')->id())->first();

        if (!empty($activity)) {

            $status = true;

            $message = "Captain Activity is found.";
            $activity->activity_status = $activity->isStatus();
            return response()->json(['status' => $status, 'message' => $message, 'activity' => $activity], 200);
        } else {
            $status = false;
            $message = "no record found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }

    }

    public function get_device_token()
    {

        $dtoken = Captain::select('device_token')->where('id', Auth::guard('captainapi')->id())->first();

        if (!empty($dtoken)) {

            $status = true;

            $message = "device_token is found.";
            return response()->json(['status' => $status, 'message' => $message, 'dtoken' => $dtoken], 200);
        } else {
            $status = false;
            $message = "no record found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }

    }

    public function get_captain_by_phone(request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'phone' => 'required'
            ]);

        if ($validator->fails()) {
            $status = false;
            $error = $validator->errors()->first();
            return response()->json(['message' => $error, 'status' => $status], 200);
        } else {
            $get_token = Captain::select('api_token')->where('phone', $request->phone)->first();
            return response()->json(['message' => 'success', 'api_token' => $get_token], 200);
        }
    }

    public function update_captain_password(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            $status = false;
            $error = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $error], 200);
        } else {
            $hash['password'] = Hash::make($request->password);
            $update = Captain::where('id', Auth::guard('captainapi')->id())->update($hash);
            if ($update) {
                $status = true;
                $message = 'Password Update Successfully';
                return response()->json(['message' => $message, 'status' => $status], 200);
            } else {
                $status = false;
                $message = 'Failed! Passsword not Matched, try again';
                return response()->json(['message' => $message, 'status' => $status], 200);
            }
        }
    }

    public function update(Request $request
        , Captain $Captain
    )
    {
        $Captain->update($request->all());

        return response()->json($Captain, 200);
    }

    public function captain_rating(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'captain_id' => 'required',
                'rate' => 'required',
                'ride_id' => 'required',
            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {

            $rates = Rate::create([
                'user_id' => Auth::guard('api')->id(),
                'captain_id' => $request->captain_id,
                'rate' => $request->rate,
                'feedback' => $request->feedback,
                'ride_id' => $request->ride_id,
            ]);
            if ($rates->save()) {
                $get_token = Captain::where('id', $request->captain_id)->first();
                $token[] = $get_token->device_token;
                $message = ['rate' => $request->rate, 'feedback' => $request->feedback ? $request->feedback : '', 'message' => 'user rate you successfully.', 'type' => 'captain_rate'];
                $message_status = captain_send_notification($token, $message, null);

                $status = true;
                $message = "Thanks for your feedback ";
                return response()->json(['status' => $status, 'message' => $message], 200);
            } else {
                $status = false;
                $message = "rate is not found";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }


        }
    }

    public function update_captain_labour(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'labour_unloader' => 'required',
                'labour_loader' => 'required',
            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {

            $driverlabour = Captain::where('id', Auth::guard('captainapi')->id())->update($request->all());

            if ($driverlabour) {
                $status = true;
                $message = "labour updated successfully.";

                Log::info($message . Auth::guard('captainapi')->id() . ' labour_unloader: ' . $request->labour_unloader . ' labour_loader: ' . $request->labour_loader);
                return response()->json(['status' => $status, 'message' => $message], 200);


            } else {
                $status = false;
                $message = "Fail to labour update.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }

        }
    }

    function captainProfile(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                // 'images' => 'required',
            ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $data = Captain::findOrFail(Auth::guard('captainapi')->id());
            $input = $request->all();
            ///here captain image update
            $captain_image = $data->images;
            if ($images = $request->file('images')) {
                $captain_image = $request->file('images')->getClientOriginalName();
                $Extension_profile = $request->file('images')->getClientOriginalExtension();
                $captain_image = 'profile' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('images')->move('images/captain/profiles/', $captain_image);
            }
            $output_file = 'images/captain/profiles/' . $captain_image;
            $profile['captain'] = URL($output_file);
            $input['images'] = $captain_image;
            $input['fname'] = $request->fname != null ? $request->fname : $data->fname;
            $input['lname'] = $request->lname != null ? $request->lname : $data->lname;
            $input['email'] = $request->email != null ? $request->email : $data->email;
            $update_image = $data->update($input);

            if (!empty($update_image)) {
                $status = true;
                $message = "Captain Image update successfully";
                return response()->json(['status' => $status, 'message' => $message, 'data' => $update_image, 'profile' => $profile], 200);
            } else {
                $status = false;
                $message = " captain image not update. ";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    public function get_profile_image()
    {
        $location = Captain::where('id', Auth::guard('captainapi')->id())->first();

        $output_file = 'images/captain/profiles/' . $location->images;
        $profile = URL($output_file);

        if (!empty($location->images)) {
            $status = true;
            $message = "captain profile found successfully.";
            return response()->json(['status' => $status, 'message' => $message, 'profile' => $profile], 200);
        } else {
            $status = false;
            $message = "captain profile image not found";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }

    }

    public function captain_reject_booking(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'booking_id' => 'required',
                // 'ride_id' => 'required',
            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {

            $reject_captain = RejectBookingRecord::create([
                'captain_id' => Auth::guard('captainapi')->id(),
                'booking_id' => $request->booking_id,
                'ride_id' => $request->ride_id,
            ]);

            $rejected_booking = $reject_captain->save();
            if ($rejected_booking) {
                $status = true;
                $rejected = RejectBookingRecord::where('booking_id', $request->booking_id)->count();
                $total_cap = Booking::where('id', $request->booking_id)->first();
                if ($total_cap->total_captain == $rejected) {
                    Log::info('Notify: user which notificaiton goes: ' . $total_cap->user_id);
                    $data = User::where('id', $total_cap->user_id)->first();
                    $token[] = trim($data->device_token);
                    $message = ['booking_id' => $request->booking_id, 'message' => 'Captain not Available', 'type' => 'all_captain_reject_booking'];
                    $message_status = user_send_notification($token, $message, null);
                    Log::info('Notify: All Captain Reject Booking: ' . $message_status);
                }
                $message = "captian reject booking successfully.";
                return response()->json(['status' => $status, 'message' => $message], 200);

            } else {
                $status = false;
                $message = "Fail to reject booking.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }

        }

    }

    public function forgot_password(request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            if (isset($request->phone) && !empty($request->phone)) {
                $user = Captain::select('api_token')->where(['phone' => $request->phone])->first();
                if (isset($user) && !empty($user)) {
                    $status = true;
                    $message = 'Enter new password.';
                    return response()->json(['status' => $status, 'message' => $message, 'data' => $user], 200);
                } else {
                    $status = false;
                    $message = "phone is not matched.";
                    return response()->json(['status' => $status, 'message' => $message], 200);
                }
            } else {
                $status = false;
                $message = "phone is required.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
}





