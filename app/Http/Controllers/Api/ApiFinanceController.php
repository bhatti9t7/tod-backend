<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\CaptainWallet;
use App\UserWallet;
use App\MainWallet;
use App\Captain;
use App\User;
use App\Admin;
use App\Booking;
use App\Transaction;
use DB;
use Auth;
use Illuminate\Support\Facades\Log;
class ApiFinanceController extends Controller
{
  function commission_detect(Request $request)
   {
     Log::info('on financial request: '.json_encode($request->all()));
      $validate = Validator::make($request->all(),
            [
            'captain_id' => 'required',
            'user_id' => 'required',
            'amount' => 'required',
            'collect_amount'  =>  'required',
            'booking_id' => 'required',
            'ride_id' => 'required',
            'commisions' => 'required'

           ]);
        if($validate->fails()){
          $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {
        	$amount = (int)request('amount');
        	$collect_amount = (int)request('collect_amount');
        	$commisions = (int)request('commisions');
          $total_commision = $commisions*$amount/100;
            $user = request('user_id');
            $captain = request('captain_id');
          DB::beginTransaction();
          $transaction = Transaction::create([
                'user_id'=>request('user_id'),
                'captain_id' => request('captain_id'),
                'ride_id' => request('ride_id'),
                'amount' => $amount,
                'collect_amount' => $collect_amount,
               	'left_amount' => $amount-$collect_amount,
               	'commisions' => $commisions,
               	'commisions_amount' => $commisions*$amount/100,
  
            ]);
            $affected_transaction  = $transaction->save();
////////user wallet data transition ////////            
            if($affected_transaction){
             $userupdate =   UserWallet::where('user_id', $user)->first();
            $new = ($userupdate->amount)+($amount);
            $new_debit_amount = round(abs(($userupdate->debit_amount)+($amount-$collect_amount)),2);
            $new_credit_amount = round(abs(($userupdate->credit_amount)+($collect_amount-$amount)),2);
            $affected_userwallet = $userupdate->update(['amount'=>$new ,'debit_amount'=>$new_debit_amount,'credit_amount'=>$new_credit_amount]);
        }
            if($affected_userwallet){ 

            $captainupdate =   CaptainWallet::where('captain_id', $captain)->first();
            $captain = ($captainupdate->amount)+($amount);

            $update_captain_debit= round(abs($captainupdate->debit_amount + $total_commision - ($captainupdate->credit_amount) - ($collect_amount -$amount) ),2);
            $captain_debit_amount = round(abs(($captainupdate->debit_amount)+($total_commision)- ($captainupdate->credit_amount)),2);
            $captain_credit_amount =  round(abs(($captainupdate->credit_amount) - ( $amount - $collect_amount ) - ($update_captain_debit)),2);
            
            $affected_captainwallet = $captainupdate->update(['amount'=>$captain ,'debit_amount'=>$update_captain_debit,'credit_amount'=>$captain_credit_amount]);

        }
            if($affected_captainwallet){
            $walletupdate =   MainWallet::where('id', 1)->first();
          
            $main = ($walletupdate->amount)+($amount);
            $main_pending_commission = ($walletupdate->pending_commission)+($total_commision);
            
            $affected_mainwallet = $walletupdate->update(['amount'=>$main ,'pending_commission'=>$main_pending_commission]);
        }
            if($affected_mainwallet){
            DB::commit();

            $update_Booking_Status = Booking::where('id', $request->booking_id)->where('user_id', $request->user_id)->update(array('status' => 0));
            $get_token = User::where('id' ,$request->user_id)->first();
            $token[] = $get_token->device_token;
              if($request->type == 'custom'){
                  $message = [ "captain_id" => $request->captain_id , 'user_amount' =>$collect_amount-$amount , "collect_amount" => $request->collect_amount , 'booking_id' => $request->booking_id, 'message' => 'Your Ride has been successfully completed', 'type' => 'custom_ride_completed'];
              }
              else{
                 $message = [ "captain_id" => $request->captain_id, 'user_amount' =>$collect_amount-$amount ,  "collect_amount" => $request->collect_amount ,  'booking_id' => $request->booking_id, 'message' => 'Your Ride has been successfully completed', 'type' => 'ride_completed'];
              }
           
            $message_status = user_send_notification($token, $message, null);

            $status = true;
            $message = "You have transaction successfully.";
            return response()->json(['message' => $message, 'status' => $status], 200);
            }          
            else{
            DB::rollBack();
          	$status = false;
            $message = "transaction is not successfully submit.";
            return response()->json(['message' => $message, 'status' => $status], 200);
            }
        }
    	}
    public function userwallet()
   {

     $response = UserWallet::where('user_id', Auth::guard('api')->id())->first();

        if(!empty($response)){
          $status = true;
          $message = "wallet found successfully";
        return response()->json(['status'=>$status,'message'=>$message, 'data'=>$response], 200);
      }
      else{
          $status = false;
          $message = " user wallet is not found ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }
    public function captainwallet()
   {

     $response = CaptainWallet::where('captain_id', Auth::guard('captainapi')->id())->first();

        if(!empty($response)){
          $status = true;
          $message = "wallet found successfully";
        return response()->json(['status'=>$status,'message'=>$message, 'data'=>$response], 200);
      }
      else{
          $status = false;
          $message = " Captain wallet is not found ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    
    }
	
}