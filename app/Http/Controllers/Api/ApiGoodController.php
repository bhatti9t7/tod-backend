<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Good;

class ApiGoodController extends Controller
{
     function get_all_goods()
  {

 		  $goodrecord = Good::orderby('id' ,'DESC')->where('status',1)->get();

 		if(!empty($goodrecord)){

          $status = true;

          $message = "goods found  successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'goods'=>$goodrecord], 200);
     	}
     	else{
        
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
  } 

  function get_goods_by_id($id)
  {

      $goodrecord = Good::where('id',$id)->first();

    if(!empty($goodrecord)){

          $status = true;

          $message = "goods found  successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'goodrecord'=>$goodrecord], 200);
      }
      else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
  } 

}
