<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Help;
use App\Emergency;
use App\BookingDestination;
use App\User;
use App\Ride;
use App\UserRate;
use Auth;
use App\Http\Resources\DatashowCollection;
class ApiHelpController extends Controller
{
	public function add_complaint(Request $request)
	{
		$validator = Validator::make($request->all(),
			[        
				'complaint_type' => 'required',

			]);

		if ($validator->fails())
		{
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}else{

			$add_complaints=Help::create([
				'complaint_type' => $request->complaint_type, 
				'complaint_description' => $request->complaint_description,
				'additional_detail' => $request->additional_detail,
				'captain_refuse_desc' => $request->captain_refuse_desc,
				'destination_desc' => $request->destination_desc,
				'behavior_detail' => $request->behavior_detail,
				'vehical_detail' => $request->vehical_detail,
				'license_detail' => $request->license_detail,
				'person_detail' => $request->person_detail, 
				'passanger_detail' => $request->passanger_detail, 
				'profile_detail' =>$request->profile_detail,
				'mention_captain' => $request->mention_captain,
				'addition_amount' => $request->addition_amount,
				'parking_location' => $request->parking_location, 
				'parking_amount' => $request->parking_amount, 
				'toll_charge' =>$request->toll_charge,
				'date_accident' => $request->date_accident,
				'time_accident' => $request->time_accident, 
				'location_accident' => $request->location_accident, 
			]);
			if($add_complaints->save()){
				$status = true;
				$message = " Complaint has been added successfully ...";
				return response()->json(['status'=>$status,'message'=>$message, 'Complaint' => $add_complaints  ], 200);
			}
			else{
				$status = false;
				$message = " Complaint is not added ...";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}
		}
	}


	public function accidental_causion()
	{
		$causion = Emergency::orderBy('id' , 'DESC')->get();

		if(!empty($causion)){
			$status = true;
			$message = "Emergencies Causion found successfully";
			return response()->json(['status'=>$status,'message'=>$message, 'data'=>$causion], 200);
		}
		else{
			$status = false;
			$message = " record is not found ";
			return response()->json(['status'=>$status,'message'=>$message], 200);
		}
	}


	public function skip_destination_notify(Request $request)
	{
		$validator = Validator::make($request->all(),
			[ 
				'user_id'  =>  'required',
				'destiantion_id' =>  'required',
				'booking_id' => 'required',
			]);

		if($validator->fails()){
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['message' => $message, 'status' => $status], 200);
		} else {

			$destination = BookingDestination::select('drop_place')->where('id' , $request->destiantion_id)->first();

			if(!empty($destination)){
				$status = true;
				$message = "skip Destination Drop off.";

				$data = User::select('device_token')->where('id', $request->user_id)->first();
				$token[] = $data['device_token'];
				$message = ["skip_destination"=>$destination, 'message'=>'your Destination is skiped','type'=>'skip_destination'];
				$message_status = user_send_notification($token, $message, null);

				return response()->json([ 'status' => $status , 'message' => $message], 200);
			} else {
				$status = false;
				$message = 'something is going to wrong.';
				return response()->json(['message' => $message, 'status' => $status], 200);
			}
		}
	}

	public function custom_ride_complete(Request $request)
	{

		$validator = Validator::make($request->all(),
			[
				'booking_id'   => 'required',
				'id'  => 'required',
			]);
		if($validator->fails()){
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['status' => $status, 'message' => $message], 200);
		} else {

       //$response = Ride::where('id', $request->id)->with('RideCompleted')->with('booking')->with('booking.BookingDestination')->with('user')->where('captain_id',Auth::guard('captainapi')->id())->first();
			$response = Ride::orderby('id' ,'DESC')->where('id', $request->id)->with('RideCompleted')->with('captain')->with('captain.CaptainStats')->with('booking')->get();
			if(!empty($response)){
				$status = true;
				$responsen = new DatashowCollection($response);
				$message = "Ride complete successfully";
				return response()->json(['status'=>$status,'message'=>$message, 'data'=>$responsen ], 200);
			}
			else{
				$status = false;
				$message = " ride is not still complete ";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}
		}
	}

	public function user_rating(Request $request)
	{

		$validator = Validator::make($request->all(),
			[
				'user_id' => 'required',
				'rate' => 'required',
				'ride_id' => 'required',
			]);

		if ($validator->fails())
		{
			$status = false;
			$message = $validator->errors()->first();
			return response()->json(['status'=>$status,'message'=>$message], 200);

		}else{

			$rates=UserRate::create([
				'captain_id' => Auth::guard('captainapi')->id(),
				'user_id' => $request->input('user_id'),
				'rate' => $request->input('rate'),
				'feedback' => $request->input('feedback'),
				'ride_id' => $request->input('ride_id'),         
			]);

			if($rates->save()){

					$get_token = User::where('id' ,$request->user_id)->first();
              		$token[] = $get_token->device_token;
              		$message = [  'rate' => $request->rate,'feedback'=> $request->feedback?$request->feedback:'', 'message' => 'captain rate you successfully', 'type' => 'user_rate'];
              		$message_status = user_send_notification($token, $message, null);

				$status = true;
				$message = "Thanks for your feedback ";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}
			else{
				$status = false;
				$message = "rate is not found";
				return response()->json(['status'=>$status,'message'=>$message], 200);
			}


		}
	}
}
