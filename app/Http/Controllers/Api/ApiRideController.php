<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Booking;
use App\Captain;
use App\BookingDestination;
use App\RidesCompleted;
use App\AdvanceBooking;
use App\AdvanceBookingDestination;
use App\Ride;
use App\SupBooking;
use App\SupBookingDestination;
use App\Financial;
use App\VehicalCategorie;
use App\UserRejectRide;
use App\CaptainRideReject;
use App\CaptainWallet;
use App\UserWallet;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\DatashowCollection;

class ApiRideController extends Controller
{
    public function ride_accept(request $request)
    {
        Log::info('ride_accept request:' . json_encode($request->all()));
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required',
                'captain_id' => 'required',
                'status' => 'required',
                'booking_id' => 'required',
            ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $booking_status = Booking::where('id', $request->booking_id)->first();
            if ($booking_status == 'dispute_booking') {
                $free_status['status'] = 1;
                $captain_free = Captain::findOrFail($booking_status->captain_id)->update($free_status);
            }
            $bookings_status = (int)$booking_status->status;
            if (($bookings_status === 1)) {
                $captain_details = Captain::select('id', 'fname', 'lname', 'phone', 'vehicle_no', 'vehicle_image', 'vehicle_name', 'status')->where('id', $request->captain_id)->first();
                $categoryDetails = VehicalCategorie::select('free_time')->where('id', $booking_status->vehical_cat_id)->first();
                Log::info('Current Ride Status:' . $captain_details->status);
                if ($captain_details->status != 2 && $captain_details->status != 0) {
                    $rides = Ride::create([
                        'user_id' => $request->input('user_id'),
                        'booking_id' => $request->input('booking_id'),
                        'captain_id' => $request->input('captain_id'),
                        'status' => $request->input('status'),
                        'loading_waiting_time' => $categoryDetails->free_time
                    ]);
                    if ($rides->save()) {
                        $data_status['status'] = 2;
                        $update = Booking::where('id', $rides->booking_id)->update($data_status);
                        $data = User::select('device_token')->where('id', $request->user_id)->first();
                        $token[] = $data['device_token'];
                        $booking_detail = Booking::where('id', $request->booking_id)->first();
                        $message = ["captain_id" => $captain_details->id, 'booking_detail' => $booking_detail, 'ride_id' => $rides->id, 'booking_id' => $request->input('booking_id'), 'old_booking_id' => $booking_detail->old_booking_id ? $booking_detail->old_booking_id : 0, 'message' => 'Ride has been accepted', 'type' => 'ride_accept'];
                        $message_status = user_send_notification($token, $message, null);
                        Log::info('Notify: Ride Accepted: ' . $message_status);
                        $captains = Captain::select('device_token')->where('id', '<>', $request->captain_id)->get();
                        $tokens = array();
                        $index = 0;
                        foreach ($captains as $captain) {
                            if (isset($captain->device_token) && !empty($captain->device_token))
                                $tokens[$index++] = $captain->device_token;
                        }
                        $captain_updated['status'] = 2;
                        $captain_status = Captain::where('id', $request->captain_id)->update($captain_updated);
                        $booking_destinations = DB::table('booking_destinations')->select('drop_place', 'booking_id')->where('booking_id', $rides->booking_id)->first();
                        if (count($tokens) > 0) {
                            $message = ['ride_id' => $rides->id, 'destination_place' => $booking_destinations->drop_place, 'pickup_place' => $booking_detail->pickup_place, 'message' => 'Ride has been rejected', 'type' => 'ride_reject'];
                            $message_status = captain_send_notification($tokens, $message, null);
                            Log::info('Notify: Ride Rjected: ' . $message_status);
                        }
                        $status = true;
                        $message = "Ride has been successfully starting";
                        Log::info($message);
                        $record = Booking::with('BookingDestination')->where('id', $request->booking_id)->with('good')->with('user')->first();
                        if (!empty($record)) {
                            $status = true;
                            $message = "history found successfully";
                        } else {
                            $status = false;
                            $message = " record not found";
                        }
                        return response()->json(['status' => $status, 'message' => $message, 'ride_id' => $rides->id, 'record' => $record], 200);
                    } else {
                        $status = false;
                        $message = "Ride is not Accepted please try again";
                        Log::info($message);
                        return response()->json(['status' => $status, 'message' => $message], 200);
                    }
                } else {
                    $status = false;
                    $message = 'Captain hired already';
                    Log::info($message);
                    return response()->json(['status' => $status, 'message' => $message], 200);
                }
            } else {
                $status = false;
                $message = 'Booking is already accepted';
                return response()->json(['status' => $status, 'message' => $message, 'request_all' => $request->all()], 200);
            }
        }

    }

    public function ride_complete(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'booking_id' => 'required',
                'id' => 'required',
            ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $response = Ride::where('id', $request->id)->with('RideCompleted')->with('captain')->with('captain.CaptainStats')->with('booking')->with('booking.BookingDestination')->get();
            if (count($response) > 0) {
                $status = true;
                $responsen = new DatashowCollection($response);
                $sup_booking = SupBooking::where('old_booking_id', $request->booking_id)->with('BookingDestination')->get();
                $message = "Ride complete successfully";
                return response()->json(['status' => $status, 'message' => $message, 'data' => $responsen, 'suplimentory_booking' => $sup_booking], 200);
            } else {
                $status = false;
                $message = " ride is not still complete ";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    public function user_detail_for_captain()
    {

        $user_detail = Ride::where('captain_id', Auth::guard('captainapi')->id())->with('user')->with('booking')->get();
        if (!empty($user_detail)) {
            $status = true;
            $message = "Ride successfully complete ";
            Log::info(' user_detail_for_captain:' . $user_detail);
            return response()->json(['status' => $status, 'message' => $message,
                'User_detail' => $user_detail], 200);
        } else {
            $status = false;
            $message = "Ride in pending ";
            return response()->json(['status' => $status, 'message' => $message], 200);
        }

    }

    function get_ride_detail(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'ride_id' => 'required',
            ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $rideDetail = Ride::where('id', $request->ride_id)->with('captain')->with('booking')->first();

            if (!empty($rideDetail)) {
                $status = true;
                $message = "Ride detail successfully.";
                Log::info(' get_ride_detail:' . $rideDetail);
                return response()->json(['status' => $status, 'message' => $message, 'data' => $rideDetail], 200);
            } else {
                $status = false;
                $message = "Ride datial not getting.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    function arrival(request $request)
    {


        Log::info('All request arrival :' . json_encode($request->all()));
        $validator = Validator::make($request->all(),
            [
                'status' => 'required',
                'captain_id' => 'required',
                'device_token' => 'required',
                'user_id' => 'required',
                'id' => 'required',
                'booking_id' => 'required',
                'vehical_cat_id' => 'required'
            ]);


        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $currentTime = date('Y-m-d H:i:s');
            $Status_Update = $request->status;
            // dd($Status_Update);exit;

            Log::info('Request Data: ' . json_encode($request->all()));
            Log::info('Status Incoming: ' . $Status_Update);
            switch ($Status_Update) {
                case '2':
                    // Arrived
                    /*
                    * 1 => On Ride History
                    * 2 => Pending History
                    * 0 => Past History or Complete
                    */
                    $status = true;
// For History of User
                    Log::info('on Arrival Data: ' . json_encode($request->all()));
                    $update_Ride_Status = Ride::where('id', $request->id)->update(array('status' => 2, 'arrival_time' => $currentTime));
                    $update_Booking_Status = Booking::where('id', $request->booking_id)->where('user_id', $request->user_id)->update(array('status' => 2, 'loadingProofTimeStart' => $currentTime));

                    $Captain_record_updated['status'] = 2;
                    $Captain_record_updated['device_token'] = $request->device_token;

                    $update_captain_status_device_token = Captain::where('id', Auth::guard('captainapi')->id())->update($Captain_record_updated);

                    Log::info('Ride Status Update: ' . $update_Ride_Status);

                    $get_token = User::where('id', $request->user_id)->first();
                    $token[] = trim($get_token->device_token);
                    $message = ["captain_id" => $request->captain_id, 'ride_id' => $request->id, 'booking_id' => $request->booking_id, 'message' => 'captain is arrived', 'type' => 'arrived'];

                    Log::info('Notify: Users Tokens : ' . json_encode($token));
                    $message_status = user_send_notification($token, $message, null);
                    Log::info('Notify: Captain Arrived: ' . $message_status);
                    $message = "captain arrived";
                    return response()->json(['status' => $status, 'message' => $message, 'notify' => $message_status], 200);
                    break;

                case '3':
                    // Ride Start
                    Log::info('on Ride Start Data: ' . json_encode($request->all()));

                    $status = true;

                    $update_Booking_Status = Booking::where('id', $request->booking_id)->where('user_id', $request->user_id)->update(array('status' => '2', 'loadingProofTimeEnd' => $currentTime));
                    if ($image = $request->file('loading_image')) {
                        $load_image = $request->file('loading_image')->getClientOriginalName();
                        $Extension_profile = $request->file('loading_image')->getClientOriginalExtension();
                        $load_image = 'loading_image' . '_' . date('YmdHis') . '.' . $Extension_profile;
                        $request->file('loading_image')->move('images/loading_image/', $load_image);
                    }
                    if (isset($load_image) && !empty($load_image)) {
                        $bookingDetails = Booking::select('loadingProofTimeStart')->where('id', $request->booking_id)->first();
                        $startTime = $bookingDetails->loadingProofTimeStart;
                        $loadingEndTimeInMin = timeDiffInSec($startTime, $currentTime, 'minutes');
                        $update_Ride_Status = Ride::where('id', $request->id)->update(['loading_image' => $load_image, 'ride_start_time' => $currentTime, 'loading_time' => $loadingEndTimeInMin]);
                        Log::info(' update_Ride in arrival:' . $update_Ride_Status);
                    }
                    $load_img = 'images/loading_image/' . $load_image;
                    $loading_img = URL($load_img);
                    $get_token = User::where('id', $request->user_id)->first();
                    $token[] = $get_token->device_token;
                    $message = ['status' => $update_Ride_Status['status'], "captain_id" => $request->captain_id, 'loading_image' => $loading_img, 'booking_id' => $request->booking_id, 'ride_id' => $request->id, 'message' => 'Your loading proof image', 'type' => 'loading_image'];
                    Log::info('Notify: Users Tokens : ' . json_encode($token));
                    $message_status = user_send_notification($token, $message, null);
                    Log::info('Notify: Ride_Start: ' . $message_status);

                    $message = "Ride start";
                    return response()->json(['status' => $status, 'message' => $message], 200);
                    break;

                case '0':

                    // Complete
                    $validator = Validator::make($request->all(), [
                        // 'ride_start_time' => 'required',
                        // 'ride_end_time' => 'required',
                        'distance' => 'required',
                        'vehical_cat_id' => 'required',
                        'id' => 'required',
                        'booking_id' => 'required',
                        // 'flag' => 'required',
                        'user_id' => 'required'
                    ]);

                    if ($validator->fails()) {
                        $status = false;
                        $message = $validator->errors()->first();
                        return response()->json(['status' => $status, 'message' => $message], 200);
                    } else {
                        Log::info('on Ride Complete: ' . json_encode($request->all()));
                        $status = true;
                        $id = $request->id;
                        $rideDetails_ = Ride::where('id', $id)->first();
                        $loading_waiting_time = $rideDetails_->loading_waiting_time;
                        $ride_start_time = $rideDetails_->ride_start_time;
                        $loading_time = abs($rideDetails_->loading_time - $loading_waiting_time);
                        $unloading_time = abs($rideDetails_->unloading_time - $loading_waiting_time);
                        $end_time = date('Y-m-d H:i:s');
                        $ride_complete_tme = timeDiffInSec($ride_start_time, $end_time, 'minutes');
                        if (isset($ride_complete_tme) && !empty($ride_complete_tme)) {
                            $input['ride_end_time'] = $end_time;
                            $input['unloading_time'] = $unloading_time;
                            $input['loading_time'] = $loading_time;
                            $input['total_ride_time'] = $ride_complete_tme;
                            $input['status'] = 0;
                            $update_Ride_Status = Ride::findOrFail($id)->update($input);
                            Log::info('total ride_complete_time:' . $ride_complete_tme);
                            Log::info('total ride_end_time:' . $end_time);
                        }

                        $update__status = Captain::where('id', Auth::guard('captainapi')->id())->update(array('status' => 1));

                        Log::info(' update_captain in arrival status 0 :' . $update__status);

                        if ($request->flag == 'continue') {
                            $get_token = User::where('id', $request->user_id)->first();
                            $token[] = $get_token->device_token;
                            $message = ['booking_id' => $request->booking_id, 'ride_id' => $request->id, 'message' => 'Your skip destination data send successfully', 'type' => 'continue_booking'];
                            $message_status = user_send_notification($token, $message, null);
                        }

                        $amount_update = VehicalCategorie::where('id', $request->vehical_cat_id)->first();
                        $base_amount = $amount_update->base_fare;
                        $amount_per_km = $amount_update->amount_per_km;
                        $trip = $request->distance;
                        $distance_amount = $amount_per_km * $request->distance;
                        $total_amount = $distance_amount + $base_amount;

                        Log::info('  updated distance:' . $trip);
                        if (isset($trip) && !empty($trip)) {
                            $booking_updation['distance'] = $trip;
                            $booking_updation['amount'] = $total_amount;
                            $booking_updation['status'] = 0;
                            // $booking_updation['loadingProofTimeEnd'] = date('Y-m-d H:i:s');
                            $update_booking_Status = Booking::findOrFail($request->booking_id)->update($booking_updation);
                            Log::info('total trip distance:' . $trip);
                        }
                        if ($request->type == 'suplimentory_booking') {
                            Log::info('on suplimentory_booking  Data: ' . json_encode($request->all()));
                            DB::beginTransaction();
                            $booking = SupBooking::create([
                                'pickup_longitude' => $request->pickup_longitude,
                                'pickup_latitude' => $request->pickup_latitude,
                                'booking_date' => $request->booking_date,
                                'vehical_sub_id' => $request->vehical_sub_id,
                                'vehical_cat_id' => $request->vehical_cat_id,
                                'PaymentMethod' => $request->PaymentMethod,
                                'good_id' => $request->good_id,
                                'pickup_place' => $request->pickup_place,
                                'amount' => $request->amount,
                                'distance' => $request->distance,
                                'labour_loader' => $request->labour_loader,
                                'labour_unloader' => $request->labour_unloader,
                                'old_booking_id' => $request->booking_id,
                                'captain_id' => $request->captain_id,
                                'user_id' => $request->user_id,
                                'estimated_time' => isset($request->estimated_time) ? $request->estimated_time : 0,
                            ]);

                            $affected_booking = $booking->save();
                            if ($affected_booking) {
                                $destinations = json_decode(request('destination'));
                                $data_dest = array();
                                foreach ($destinations as $dest) {
                                    $tmp_dest = array();
                                    $tmp_dest['longitude'] = $dest->longitude;
                                    $tmp_dest['latitude'] = $dest->latitude;
                                    $tmp_dest['sup_booking_id'] = $booking->id;
                                    $tmp_dest['drop_place'] = $dest->drop_place;
                                    $tmp_dest['created_at'] = now();
                                    $tmp_dest['updated_at'] = now();
                                    $data_dest[] = $tmp_dest;
                                }

                                $booking_dest = SupBookingDestination::insert($data_dest);

                                if ($booking_dest) {
                                    DB::commit();
                                } else {
                                    DB::rollBack();
                                }
                            }
                        }
                    }

                    $message = "ride has been successfully completed";
                    return response()->json(['status' => $status, 'message' => $message], 200);
                    break;

                default:
                    $status = false;
                    $message = "Ride is not Accepted please try again";
                    Log::info('Ride_Fail: ' . $message);
                    return response()->json(['status' => $status, 'message' => $message], 200);

                    break;
            }
        }
    }

    function user_reject_request(Request $request)
    {
        Log::info('on user reject request: ' . json_encode($request->all()));
        $validator = Validator::make($request->all(),
            [
                'status' => 'required',
                'captain_id' => 'required',
                'id' => 'required',
                'booking_id' => 'required',
                'time' => 'required',
            ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $status = true;
            $update_captain_status_device_token = Captain::where('id', $request->captain_id)->update(array('status' => '1'));
            $update_Booking_Status = Booking::where('id', $request->booking_id)->where('user_id', $request->user_id)->update(['status' => 1]);

            $data['rejected_by'] = Auth::guard('api')->id();
            $data['status'] = -1;
            $update_Ride_Status = Ride::where('id', $request->id)->update($data);
            Log::info('user reject ride and ride table update: ' . $update_Ride_Status);
            $reject_user = UserRejectRide::create([
                'user_id' => Auth::guard('api')->id(),
                'booking_id' => $request->booking_id,
                'ride_id' => $request->id,
            ]);
            $UserRejectRide_affected = $reject_user->save();
            $userupdate = UserWallet::where('user_id', Auth::guard('api')->id())->first();
            $vehicle = Booking::where('id', $request->booking_id)->with('vehicle_Categorie')->first();
            $new_credit_amount['credit_amount'] = round(abs(($userupdate->credit_amount) - ($vehicle->vehicle_Categorie->base_fare)), 2);
            $affected_userwallet = $userupdate->update($new_credit_amount);
            $fine_to_user['amount'] = $vehicle->vehicle_Categorie->base_fare;
            $value_update_amount = Booking::where('id', $request->id)->update($fine_to_user);
            $Booking_amount = Booking::select('amount')->where('id', $request->booking_id)->first();

            $captainupdate = CaptainWallet::where('captain_id', $request->captain_id)->first();

            $captain_credit_amount['credit_amount'] = round(abs(($captainupdate->credit_amount) + ($vehicle->vehicle_Categorie->base_fare)), 2);
            $affected_captainwallet = $captainupdate->update($captain_credit_amount);

            $get_token = Captain::where('id', $request->captain_id)->first();
            $token[] = $get_token->device_token;
            $message = ['status' => $request->status, 'captain_id' => $request->captain_id, 'booking_id' => $request->booking_id, 'ride_id' => $request->id, 'message' => 'ride has been rejected.', 'type' => 'reject_booking', 'amount' => $Booking_amount];
            $message_status = captain_send_notification($token, $message, null);

            Log::info('user reject ride and send notification to captain: ' . $message_status);
            $message = "User Rejected the Ride";
            return response()->json(['status' => $status, 'message' => $message, 'notify' => $message_status, 'transaction' => $Booking_amount], 200);
        }
    }

    function captain_reject_request(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'status' => 'required',
                'user_id' => 'required',
                'booking_id' => 'required',
                'id' => 'required',
                'time' => 'required'
            ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $status = true;
            $bookingss = Booking::where('id', $request->booking_id)->first();
            $update_captain_status_device_token = Captain::where('id', Auth::guard('captainapi')->id())->update(array('status' => 1));
            $update_Booking_Status = Booking::where('id', $request->booking_id)->where('user_id', $request->user_id)->update(array('status' => -1));

            $update_Ride_Status = Ride::where('id', $request->id)->where('captain_id', Auth::guard('captainapi')->id())->update(array('rejected_by' => Auth::guard('captainapi')->id(), 'status' => -1));
            ///////////////
            $reject_user = UserRejectRide::create([
                'user_id' => $request->user_id,
                'booking_id' => $request->booking_id,
                'ride_id' => $request->id,
            ]);
            $UserRejectRide_affected = $reject_user->save();
            ///////////////
            $ride_time = Ride::where('id', $request->id)->first();
            $ride_start_time = $ride_time->created_at;
            $end_time = $ride_time->updated_at;
            $datetime1 = strtotime($ride_start_time);
            $datetime2 = strtotime($end_time);
            $secs = $datetime2 - $datetime1;// == <seconds between the two times>
            $ride_reject_time = round(abs($datetime2 - $datetime1) / 60, 2);
            ///////////////
            if ($ride_reject_time >= 5) {
                $captainupdate = CaptainWallet::where('captain_id', Auth::guard('captainapi')->id())->first();
                $reject_deduction = Financial::first();
                $captain_credit_amount['credit_amount'] = round(abs(($captainupdate->credit_amount) - ($reject_deduction->reject_booking_amount)), 2);
                $affected_captainwallet = $captainupdate->update($captain_credit_amount);
            }
            ////////////
            $file_reject = '';
            if ($images = $request->file('file_reject')) {
                $file_reject = $request->file('file_reject')->getClientOriginalName();
                $Extension_profile = $request->file('file_reject')->getClientOriginalExtension();
                $file_reject = 'file_reject' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('file_reject')->move('images/file_reject/', $file_reject);
            }
            //////////////////
            $captain_ride_reject = CaptainRideReject::insert(['captain_id' => Auth::guard('captainapi')->id(), 'booking_id' => $request->booking_id, 'ride_id' => $request->id, 'file_reject' => $file_reject, 'reject_message' => $request->reject_message, 'reject_reasons' => $request->reject_reasons,]);

            $get_token = User::where('id', $request->user_id)->first();
            $token[] = $get_token->device_token;
            $message = ['status' => $update_Ride_Status['status'], "user_id" => $request->user_id, 'booking_id' => $request->booking_id, 'ride_id' => $request->id, 'reject_reasons' => $request->reject_reasons ? $request->reject_reasons : '', 'message' => 'No pilot has been available.', 'type' => 'Rejected_Ride'];
            $message_status = user_send_notification($token, $message, null);
            ///// here again notification is captain
            $captain_request = Captain::where('id', Auth::guard('captainapi')->id())->first();
            if ($captain_ride_reject) {
                if (!empty($request->reject_reasons) !== 'Over Weight and Volume' or !empty($request->reject_reasons !== 'Packing not proper')) {
                    $captains = Captain::select('device_token')->where('id', '<>', Auth::guard('captainapi')->id())->where('vehical_cat_id', $captain_request->vehical_cat_id)->where('vehical_sub_id', $captain_request->vehical_sub_id)->where('status', 1)->get();
                    Log::info('after reject ride All captains ids :' . $captains);
                    $tokens = array();
                    $index = 0;
                    foreach ($captains as $captain) {
                        if (isset($captain->device_token) && !empty($captain->device_token))
                            $tokens[$index++] = $captain->device_token;
                    }
                }
                if (count($tokens) > 0) {
                    $message = array('booking_id' => $request->booking_id, 'user_id' => $request->user_id, 'type' => 'normal', 'message' => 'booking_success');

                    $message = array('booking_id' => $request->id, 'type' => 'normal', 'vehical_cat_id' => $captain_request->vehical_cat_id, 'good_id' => $bookingss->good_id, 'user_id' => $bookingss->user_id, 'message' => 'booking_success');

                    $message_status = captain_send_notification($tokens, $message, null);
                    Log::info('booking notificaiton to captians   :' . $message_status);
                }
            }
            $message = "Captain Rejected the Ride";
            return response()->json(['status' => $status, 'message' => $message, 'notify' => $message_status], 200);
        }
    }

    function deliveryproof(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'ride_id' => 'required',
                'image' => 'required',
                'user_id' => 'required',
                'destination_id' => 'required',
                'map_image' => 'required',
            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {

            ///here delivery proof image
            $image = $request->input('image');
            // $data = explode( ',', $image);
            $image_name = time() . '.png';
            $output_file = 'images/ride_complete/' . $image_name;

            $ifp = fopen($output_file, 'wb');
            $img = fwrite($ifp, base64_decode($image));

            fclose($ifp);
////////////////here is map image ///////////////////
            $map_image = $request->input('map_image');
            //$data = explode( ',', $map_image);
            $map_image_name = time() . '.png';
            $map_file = 'images/ride_complete/' . $map_image_name;

            $ifp = fopen($map_file, 'wb');
            $img = fwrite($ifp, base64_decode($map_image));

            fclose($ifp);
            //dd($map_file);exit;
            $map_file = URL($map_file);
            $delivery_image = URL($output_file);

            $delivery = RidesCompleted::create([
                'ride_id' => $request->ride_id,
                'image' => $image_name,
                'map_image' => $map_image_name,
                'destination_id' => $request->destination_id,
                'unloading_waiting_time' => $request->unloading_waiting_time,
                'complete_at' => now(),
            ]);

            if ($delivery->save()) {

                $status = true;
                $get_token = User::where('id', $request->user_id)->first();
                $token[] = $get_token->device_token;
                $message = ['destination_id' => $request->destination_id, 'id' => $delivery->id, 'message' => 'ride proof has been successfully send', 'type' => 'ride_proof'];

                $message_status = user_send_notification($token, $message, null);

                $message = "delivery has been successfully done...";
                return response()->json(['status' => $status, 'message' => $message, 'delivery_image' => $delivery_image,
                    'map_file' => $map_file], 200);
            } else {
                $status = false;
                $message = "delivery is not done yet...";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }


        }

    }

    public function proofImage(request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'destination_id' => 'required',
                'id' => 'required',

            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {

            $RidesCompleted = RidesCompleted::where('id', $request->id)->with('bookingDestination')->first();

            //image for delivery proof
            $destination_image = $RidesCompleted['image'];
            $map_image = $RidesCompleted['map_image'];

            $output_file = 'images/ride_complete/' . $destination_image;
            $android['delivery'] = URL($output_file);
            //map image
            $map_route = 'images/ride_complete/' . $map_image;
            $android['map_image'] = URL($map_route);

            /// drop off
            $dropoff = $RidesCompleted->bookingDestination['drop_place'];

//dd($dropoff);exit;
            if (!empty($RidesCompleted)) {
                $status = true;
                $message = "Ride complete image";
                return response()->json(['status' => $status, 'message' => $message, 'android' => $android, 'dropoff' => $dropoff], 200);
            } else {
                $status = false;
                $message = "Ride in pending.. ";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    function fileupload(Request $request)
    {
        Log::info('File Request:' . json_encode($request->all()));
        $validator = Validator::make($request->all(), [
            'image' => 'required|file',
            'user_id' => 'required',
            'destination_id' => 'required',
            'ride_id' => 'required',
            'map_image' => 'required|file',
            'booking_id' => 'required',
            'unload_start_time' => 'required',
            'ride_end_time' => 'required',
            'toll_plaza' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'drop_place' => 'required',
        ]);

        Log::info('File Uploads:' . json_encode($_FILES));
        Log::info('File Data:' . json_encode($_POST));
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            ///here delivery proof image
            if (!empty($request->destination_id)) {
                $drop_off['longitude'] = $request->longitude;
                $drop_off['latitude'] = $request->latitude;
                $drop_off['drop_place'] = $request->drop_place;
                $destination_off = BookingDestination::find($request->destination_id)->update($drop_off);
            }
            if ($image = $request->file('image')) {
                $Get_Doc = $request->file('image')->getClientOriginalName();
                $Extension_profile = $request->file('image')->getClientOriginalExtension();
                $Get_Doc = 'delivery_image' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('image')->move('images/ride_complete/', $Get_Doc);
            }
            if ($image = $request->file('map_image')) {
                $map_image = $request->file('map_image')->getClientOriginalName();
                $Extension_profile = $request->file('map_image')->getClientOriginalExtension();
                $map_image = 'map_image' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('map_image')->move('images/ride_complete/', $map_image);
            }
            $arrival_time = $request->unload_start_time;
            $end_time = $request->ride_end_time;

            $datetime1 = strtotime($arrival_time);
            $datetime2 = strtotime($end_time);

            $secs = $datetime2 - $datetime1;// == <seconds between the two times>
            $unloading_time = $secs / 60;

            $delivery = RidesCompleted::create([
                'ride_id' => (int)$request->ride_id,
                'image' => isset($Get_Doc) ? $Get_Doc : 'no-image.png',
                'map_image' => isset($map_image) ? $map_image : 'no-image.png',
                'destination_id' => $request->destination_id,
                'unloading_waiting_time' => $unloading_time,
                'complete_at' => now(),
            ]);
            if ($delivery->save()) {

                $status = true;
                $tollplaza = Booking::where('id', $request->booking_id)->first();
                $plus_tolplaza = $tollplaza->toll_plaza + $request->toll_plaza;
                $input_booking['toll_plaza'] = $plus_tolplaza;
                $update_Booking_Status = Booking::findOrFail($request->booking_id)->update($input_booking);

                $get_token = User::where('id', $request->user_id)->first();
                $token[] = $get_token->device_token;
                $message = ['destination_id' => $request->destination_id, 'id' => $delivery->id, 'message' => 'ride proof has been successfully send', 'type' => 'ride_proof'];
                $message_status = user_send_notification($token, $message, null);
                $message = "delivery has been successfully done...";
                return response()->json(['status' => $status, 'message' => $message, 'delivery_image' => isset($Get_Doc) ? $Get_Doc : 'no-image.png', 'map_file' => isset($map_image) ? $map_image : 'no-image.png'], 200);
            } else {
                $status = false;
                $message = "delivery is not done yet...";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    function ride_start(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'status' => 'required',
                'user_id' => 'required',
                'id' => 'required',
                'booking_id' => 'required',
                'loading_image' => 'required',
                'loading_waiting_time' => 'required'
            ]);
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $status = true;

            $update_Booking_Status = Booking::where('id', $request->booking_id)->where('user_id', $request->user_id)->update(array('status' => '2'));
            $load_image = '';
            if ($image = $request->file('loading_image')) {
                $load_image = $request->file('loading_image')->getClientOriginalName();
                $Extension_profile = $request->file('loading_image')->getClientOriginalExtension();
                $load_image = 'loading_image' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('loading_image')->move('images/loading_image/', $load_image);
            }
            $update_Ride_Status = Ride::where('id', $request->id)->update(['status' => $request->status, 'loading_waiting_time' => $request->loading_waiting_time, 'loading_image' => $load_image]);

            $load_img = 'images/loading_image/' . $load_image;
            $loading_img = URL($load_img);

            $get_token = User::where('id', $request->user_id)->first();
            $token[] = $get_token->device_token;
            $message = ['status' => $update_Ride_Status['status'], "captain_id" => $request->captain_id, 'loading_image' => $loading_img, 'booking_id' => $request->booking_id, 'ride_id' => $request->id, 'message' => 'Your Ride has been started', 'type' => 'ride_start'];

            Log::info('Notify: Users Tokens : ' . json_encode($token));
            $message_status = user_send_notification($token, $message, null);
            Log::info('Notify: Ride_Start: ' . $message_status);
            $message = "Ride start";
            return response()->json(['status' => $status, 'message' => $message, 'loading_image' => $loading_img, 'notify' => $message_status], 200);
        }
    }

    function coustom_ride_proof(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required',
            'user_id' => 'required',
            'ride_id' => 'required',
            'booking_id' => 'required',
            'distance' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'drop_place' => 'required',
        ]);

        Log::info('File Request:' . json_encode($request->all()));
        Log::info('File Uploads:' . json_encode($_FILES));
        Log::info('File Data:' . json_encode($_POST));
        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            ///here delivery proof image
            if ($image = $request->file('image')) {
                $Get_Doc = $request->file('image')->getClientOriginalName();
                $Extension_profile = $request->file('image')->getClientOriginalExtension();
                $Get_Doc = 'delivery_image' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('image')->move('images/ride_complete/', $Get_Doc);
            }

            if ($image = $request->file('map_image')) {
                $map_image = $request->file('map_image')->getClientOriginalName();
                $Extension_profile = $request->file('map_image')->getClientOriginalExtension();
                $map_image = 'map_image' . '_' . date('YmdHis') . '.' . $Extension_profile;
                $request->file('map_image')->move('images/ride_complete/', $map_image);
            }

            $delivery = RidesCompleted::create([
                'ride_id' => (int)$request->ride_id,
                'image' => $Get_Doc,
                'map_image' => $map_image,
                'unloading_waiting_time' => $request->unloading_waiting_time,
                'complete_at' => now(),
            ]);
            if ($delivery->save()) {

                $status = true;
                $get_token = User::where('id', $request->user_id)->first();
                $token[] = $get_token->device_token;
                $message = ['ride_complete_id' => $delivery->id, 'message' => 'ride proof has been successfully send', 'type' => 'custom_ride_proof'];
                $message_status = user_send_notification($token, $message, null);
                ////// booking destination add  here in custom booking
                $destination = BookingDestination::create([
                    'longitude' => request('longitude'),
                    'latitude' => request('latitude'),
                    'booking_id' => request('booking_id'),
                    'drop_place' => request('drop_place'),
                ]);

                $affected_booking = $destination->save();
                if ($affected_booking) {
                    $updated_ride_complete = RidesCompleted::where('id', $delivery->id)->update(array('destination_id' => $destination->id));
                }
                ////////////////////////////////////////////////
                /////////////////////
                $validator = Validator::make($request->all(), [
                    'ride_start_time' => 'required',
                    'ride_end_time' => 'required',
                    'vehical_cat_id' => 'required',
                ]);

                if ($validator->fails()) {
                    $status = false;
                    $message = $validator->errors()->first();
                    return response()->json(['status' => $status, 'message' => $message], 200);
                } else {
                    Log::info('on Ride Complete: ' . json_encode($request->all()));
                    $status = true;
                    $ride_start_time = $request->ride_start_time;
                    $end_time = $request->ride_end_time;
                    $datetime1 = strtotime($ride_start_time);
                    $datetime2 = strtotime($end_time);

                    $secs = $datetime2 - $datetime1;// == <seconds between the two times>
                    $ride_complete_tme = $secs / 60;

                    if (isset($ride_complete_tme) && !empty($ride_complete_tme)) {
                        $input['ride_end_time'] = $end_time;
                        $input['total_ride_time'] = $ride_complete_tme;
                        $input['status'] = 0;
                        $id = $request->ride_id;

                        $update_Ride_Status = Ride::findOrFail($id)->update($input);
                        Log::info('total ride_complete_time:' . $ride_complete_tme);
                        Log::info('total ride_end_time:' . $end_time);
                    }
                    $captain_id = Auth::guard('captainapi')->id();
                    $captain['status'] = 1;
                    $update__status = Captain::findOrFail($captain_id)->update($captain);

                    Log::info(' update_captain in arrival status 0 :' . $update__status);

                    $amount_update = VehicalCategorie::where('id', $request->vehical_cat_id)->first();
                    $base_amount = $amount_update->base_fare;
                    $amount_per_km = $amount_update->amount_per_km;
                    $trip = $request->distance;
                    $distance_amount = $amount_per_km * $request->distance;
                    $total_amount = $distance_amount + $base_amount;

                    $tollplaza = Booking::where('id', $request->booking_id)->first();
                    $plus_tolplaza = $tollplaza->toll_plaza + $request->totoll_plaza;

                    $input['toll_plaza'] = $plus_tolplaza;
                    $input['distance'] = $request->distance;
                    $input['amount'] = $total_amount;
                    $input['status'] = 0;
                    $id = $request->booking_id;
                    $update_Booking_Status = Booking::findOrFail($id)->update(array($input));
                    Log::info('booking_update:' . $update_Booking_Status);
                }
                /////////////////////

                $message = "delivery has been successfully done...";
                return response()->json(['status' => $status, 'message' => $message, 'booking_id' => $request->booking_id], 200);
            } else {
                $status = false;
                $message = "delivery is not done yet...";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }

    function custom_fare_detail(Request $request)
    {
        $validate = Validator::make($request->all(),
            [
                'booking_id' => 'required',
                'ride_id' => 'required',
                'distance' => 'required'
            ]);
        if ($validate->fails()) {
            $status = false;
            $message = $validate->errors()->first();
            return response()->json(['message' => $message, 'status' => $status], 200);
        } else {

            $finance = Financial::first();

            $booking = Booking::select('*')->where('id', $request->booking_id)->with('vehicleSubCategorie')->with('vehicleSubCategorie.vehiclecategorie')->first();
// return $booking;

            $ride = Ride::select('*')->where('id', $request->ride_id)->first();
            $distance = $booking->distance;
            $commessions = $finance->commisions;
            $wait_amount = $finance->wait_amount;
            $Serg_fee = $finance->Serg_fee;

            $toll_plaza = $booking->toll_plaza;
            $labour_unloader = $booking->labour_unloader;
            $labour_loader = $booking->labour_loader;

            $amount_per_km = $booking->vehicleSubCategorie->vehiclecategorie->amount_per_km;

            $destination_charge = $booking->vehicleSubCategorie->vehiclecategorie->destination_charge;

            $base_fare = $booking->vehicleSubCategorie->vehiclecategorie->base_fare;
            $free_time = $booking->vehicleSubCategorie->vehiclecategorie->free_time;
            // dd($free_time) ;
            $loading_waiting_price = $booking->vehicleSubCategorie->vehiclecategorie->loading_waiting_price;

            $unloading_waiting_price = $booking->vehicleSubCategorie->vehiclecategorie->unloading_waiting_price;

            ///calculation fare
            $amount_km = $amount_per_km * $distance;
            $total_commision = ($commessions * $amount_km) / 100;;

            ///calculation fare
            $amount_km = $amount_per_km * $distance;
            $total_commision = ($commessions * $amount_km) / 100;
            // $total_amount = $amount_km + $total_commision ;
            ///calculation serg farer
            $total_serg = ($Serg_fee * $amount_km) + $total_commision;

            ///calculate the free time loading_waiting_time
            $loading_waiting_time = $ride->loading_waiting_time;
            $total_ride_time = $ride->total_ride_time;
            $time = $loading_waiting_time - $free_time;

            //////// calculate unloading waiting time

            $unloading_waiting_time = RidesCompleted::where('ride_id', $request->ride_id)->sum('unloading_waiting_time');

            // dd($destiation_amount);exit;
            //////// calculate loading waiting time
            if ($free_time < $loading_waiting_time) {
                $loading_time_consumed = ($loading_waiting_time - $free_time) * $loading_waiting_price;
            } else {
                $loading_time_consumed = 0;
            }
            //////// Unloading waiting time
            if ($free_time < $unloading_waiting_time) {
                $Unloading_time_consumed = ($unloading_waiting_time - $free_time) * $unloading_waiting_price;
            } else {
                $Unloading_time_consumed = 0;
            }

            if ($time > 0) {
                $loading_wait_cost = $time * $loading_waiting_price;

                $total_fare = $base_fare + $total_serg + $toll_plaza + $loading_wait_cost + $loading_time_consumed + $Unloading_time_consumed;
            } else {
                $total_fare = $base_fare + $total_serg + $toll_plaza + $loading_time_consumed + $Unloading_time_consumed;

            }
            // print_r($finance['booking']);exit();


            if (!empty($booking)) {
                $status = true;
                $All_fare = ["base_fare" => isset($base_fare) ? $base_fare : 0, 'distance' => isset($distance) ? $distance : 0, 'amount_per_km' => isset($amount_per_km) ? $amount_per_km : 0, 'toll_plaza' => isset($toll_plaza) ? $toll_plaza : 0, 'total_serg' => isset($total_serg) ? $total_serg : 0, 'total_ride_time' => isset($total_ride_time) ? $total_ride_time : 0, 'commessions' => isset($commessions) ? $commessions : 0, 'total_commision' => isset($total_commision) ? $total_commision : 0, 'unloading_waiting_time' => isset($unloading_waiting_time) ? $unloading_waiting_time : 0];
                $booking_updation['amount'] = $total_fare;
                $booking_updation['amount'] = $total_fare;
                $booking_updation['serge_charge'] = $total_serg ? $total_serg : 0;
                $booking_updation['loading_charge'] = $loading_time_consumed;
                $booking_updation['unloading_charge'] = $Unloading_time_consumed;
                $booking_updation['distance_charge'] = $distance * $amount_per_km;
                $update_booking_amount = Booking::findOrFail($request->booking_id)->update($booking_updation);

                $message = "finance detail found successfully";
                return response()->json(['status' => $status, 'message' => $message, 'data' => $total_fare, 'All_fares' => $All_fare], 200);
            } else {
                $status = false;
                $message = "finance datial not found.";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }

        }

    }

    public function custom_proofImage(request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'ride_complete_id' => 'required',

            ]);

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);

        } else {

            $RidesCompleted = RidesCompleted::where('id', $request->ride_complete_id)->first();
            //image for delivery proof
            $destination_image = $RidesCompleted['image'];
            $output_file = 'images/ride_complete/' . $destination_image;
            $android['delivery'] = URL($output_file);


            if (!empty($RidesCompleted)) {
                $status = true;
                $message = "Ride complete image";
                return response()->json(['status' => $status, 'message' => $message, 'android' => $android], 200);
            } else {
                $status = false;
                $message = "Ride in pending.. ";
                return response()->json(['status' => $status, 'message' => $message], 200);
            }
        }
    }
}
