<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\RidesCompleted;
use App\SkipReasons;
use App\Booking;
use App\User;
use App\BookingDestination;
use Illuminate\Support\Facades\Log;
class ApiSkipReasonController extends Controller
{
	public function skips_reasons()
   {
		$reasons = SkipReasons::orderBy('id' , 'DESC')->get();

		if(!empty($reasons)){
          $status = true;
          $message = "Record found successfully";
        return response()->json(['status'=>$status,'message'=>$message, 'data'=>$reasons], 200);
      }
      else{
          $status = false;
          $message = " record is not found ";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
	}


	public function skipdestination(Request $request)
   {
    Log::info('All skip_destination :'.json_encode($request->all()));
    $validator = Validator::make($request->all(),
     [    
      
      'skip_reasons_id' => 'required',
      'user_id' => 'required',
      'destination_id' => 'required',
      'ride_id' => 'required',
      'booking_id' => 'required',
      'status' =>'required'

    ]);
    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);
    }else{
      
     $delivery=RidesCompleted::create([
        'ride_id' => (int)$request->ride_id,
        'skip_reasons_id' => $request->skip_reasons_id,
        'destination_id' => $request->destination_id,
        'unloading_waiting_time' => $request->unloading_waiting_time,
        'status' => $request->status,
        'complete_at' => now(),      
      ]);
     if($delivery->save()){
          $status = true;
        $tollplaza = Booking::where('id', $request->booking_id)->first();
        $plus_tolplaza = $tollplaza->toll_plaza + isset($request->totoll_plaza)?$request->totoll_plaza:0;

          $update_Booking_Status = Booking::where('id', $request->booking_id)->where('user_id', $request->user_id)->update(array('toll_plaza'=> $plus_tolplaza));
           $destination = BookingDestination::where('id' , $request->destination_id)->first();
           $skip_reason = SkipReasons::where('id' , $request->skip_reasons_id)->first();

          $get_token = User::where('id' ,$request->user_id)->first();
          $token[] = $get_token->device_token;
          $message = [ 'destination_id' => $request->destination_id, 'reasons' =>$skip_reason->reasons , 'skip_drop_off' => $destination->drop_place ,  'id' =>$delivery->id , 'message' => 'destination has been skiped', 'type' => 'skip_destination'];         
          $message_status = user_send_notification($token, $message, null);
          $message = "Destination skip has been successfully done...";
          return response()->json(['status'=>$status,'message'=>$message ], 200);
     }
     else{
          $status = false;
          $message = "Destination  is not skip ...";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }
  }

}
