<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Validator;
use DB;
use App\User;
use App\Tracking;
use App\Captain;
use App\Ride;
use App\Booking;
use App\RidesCompleted;
use Illuminate\Support\Facades\Log;
class ApiTrackingController extends Controller
{
    public function store(Request $request)
  {
    // validation the form data;
    $validator = Validator::make($request->all(),
     [
      'driver_id' => 'required',
      'longitude' => 'required',
      'latitude' => 'required',
      'type' => 'required',
    ]);

    if ($validator->fails())
    {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);

    }else{

     $newUser=Tracking::create([
        'longitude' => $request->input('longitude'),
        'latitude' => $request->input('latitude'),
        'type' => $request->input('type'),
        
      ]);

     if($newUser->save()){

          $status = true;

          $message = "tracking  successfully.";
          return response()->json(['status'=>$status,'message'=>$message], 200);
     }
     else{
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }


     }

  }
  public function show(){

 		$Track =Captain::where('status','<>', 0)->get();
     
 		if(!empty($Track)){

          $status = true;

          $message = "tracking  successfully.";
          return response()->json(['status'=>$status,'message'=>$message,'tracking'=>$Track], 200);
     	}
     	else
      {
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }

  }

  public function loading_notification(Request $request)
  {
    Log::info('All request arrival :'.json_encode($request->all()));
      $validator = Validator::make($request->all(),
    [
      'type' => 'required',
      'captain_id' => 'required',
      'user_id' => 'required',
      'arrival_time' => 'required',
      'ride_start_time' => 'required',
      'booking_id' => 'required',
      'ride_id' => 'required'
    ]);

    if ($validator->fails()) {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);
    } else {
          $ride_start_time = $request->ride_start_time;
          $arrival_time = $request->arrival_time;
          // $load_time = date_sub($ride_start_time,$arrival_time);
          $datetime1 = strtotime( $ride_start_time);
          $datetime2 = strtotime($arrival_time);
          $secs = $datetime1 - $datetime2;// == <seconds between the two times>
         
          $days = $secs / (60);
          if(isset($days) && !empty($days)){
            $update_Ride_Status = Ride::where('id', $request->ride_id)->update(['arrival_time'=>$arrival_time , 'loading_waiting_time' => $days ,'ride_start_time' => $ride_start_time]);
            Log::info(' update_Ride in arrival:'.$update_Ride_Status);
          }

          if($request->type == 'captain'){
             $get_token = User::where('id' ,$request->user_id)->first();
             $token[] = $get_token->device_token;
             $message = ['message' => 'Captain end the loading time','ride_id' => $request->ride_id,'booking_id' => $request->booking_id,'type' => 'loading_time_end'];
            
            Log::info('Notify:captain end the loading time Users Tokens : '.json_encode($token));
             $message_status = user_send_notification($token, $message, null);
              Log::info('Notify: Ride_Start: '.$message_status);
          }
          if($request->type == 'user'){
              $get_token = Captain::where('id' ,$request->captain_id)->first();
              $token[] = $get_token->device_token;
              $message = ['message' => 'User end the loading time','type' => 'loading_time_end'];
            
              Log::info('Notify:user end the loading time Captains Tokens : '.json_encode($token));
              $message_status = captain_send_notification($token, $message, null);
              Log::info('Notify: loading end notification : '.$message_status);
          }
          $status= true;
          $message = "loading notification send successfully ";
          return response()->json(['status'=>$status,'message'=>$message,'notify'=>$message_status], 200);
      }
    }

    public function unloading_notification(Request $request)
  {
    Log::info('All request arrival :'.json_encode($request->all()));
      $validator = Validator::make($request->all(),
      [
        'user_id' => 'required',
        'booking_id' => 'required',
      ]);
    if ($validator->fails()) {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);
    }else{
          $get_token = User::where('id',$request->user_id)->first();
          Booking::where('id',$request->booking_id)->update(['loadingProofTimeEnd'=>date('Y-m-d H:i:s')]);
          $token[] = $get_token->device_token;
          $message = ['message' => 'Captain start the Unloading time','type' => 'Unloading_time_start'];
          Log::info('Notify:captain end the Unloading time Users Tokens : '.json_encode($token));
          $message_status = user_send_notification($token, $message, null);
          Log::info('Notify: unloading start time: '.$message_status);
    }
          $status= true;
          $message = "Unloading notification send successfully ";
          return response()->json(['status'=>$status,'message'=>$message,'notify'=>$message_status], 200);
    }

    public function captain_ride_start(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
          'ride_id' => 'required',
          'user_id' => 'required',
          'booking_id' => 'required',
        ]);
      
    if ($validator->fails()) {
      $status = false;
      $message = $validator->errors()->first();
      return response()->json(['status'=>$status,'message'=>$message], 200);
    } else {
      $ride = Ride::where('id' , $request->ride_id)->update(array('status' => 3));
      if( isset($ride) && !empty($ride)){
            $get_token = User::where('id' ,$request->user_id)->first();
            $token[] = $get_token->device_token;
            $message = ['message' => 'captain has been ride start', 'booking_id'=>$request->booking_id, 'ride_id'=>$request->ride_id ,'type' => 'ride_start'];
            
            Log::info('Notify:captain has been ride start  Users Tokens : '.json_encode($token));
            $message_status = user_send_notification($token, $message, null);
            Log::info('Notify: unloading start time: '.$message_status);

          $status= true;
          $message = "Ride start notification send successfully ";
          return response()->json(['status'=>$status,'message'=>$message,'notify'=>$message_status], 200);
      }
      else
      {
          $status = false;
          $message = "no record found";
          return response()->json(['status'=>$status,'message'=>$message], 200);
      }
    }
  }
}
