<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Http\Resources\UserRideRejectCollection;
use App\UserRejectRide;
use App\RidesCompleted;
use App\Booking;
use Validator;
use Auth;
use DB;
class ApiUserRideHistoryController extends Controller
{
    public function user_ride_reject(){
		$history = UserRejectRide::where('user_id' , Auth::guard('api')->id())->with('ride')->with('ride.captain')->with('booking')->with('booking.BookingDestination')->get();
    	if($history){
    		$status = true;
    		$response = new UserRideRejectCollection($history);
    		$message = "Record found successfully";
    		return response()->json(['status'=>$status, 'message'=> $message , 'data'=> $response] , 200);
    	}
    	else{
    		$status = true;
    		$message = "Record not found";
    		return response()->json(['status'=>$status, 'message'=> $message] , 200);
    	}
	}

	public function user_ride_completed()
	{
		$history = RidesCompleted::where('user_id',Auth::guard('api')->id())->with('ride')->with('ride.captain')->with('booking')->with('booking.BookingDestination')->get();
		if ($history) {
			$status = true;
			$response = new UserRideRejectCollection($history);
			$message = "Record found successfully";
			return response()->json(['status' => $status, 'message' => $message, 'data' => $response], 200);
		} else {
			$status = true;
			$message = "Record not found";
			return response()->json(['status' => $status, 'message' => $message], 200);
		}
	}
	public function user_ride_current()
	{
		$history = Booking::where('user_id',Auth::guard('api')->id())->where('status',2)->with('Rides','BookingDestination')->get();
		if ($history) {
			$status = true;
			$message = "Record found successfully";
			return response()->json(['status' => $status, 'message' => $message, 'data' => $history], 200);
		} else {
			$status = true;
			$message = "Record not found";
			return response()->json(['status' => $status, 'message' => $message], 200);
		}
	}
}
