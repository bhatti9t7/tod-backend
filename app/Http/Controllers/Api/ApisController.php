<?php

namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Captain;
use DB;
use Validator;
class ApisController extends Controller
{
    function get_active_cars(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'longitude' => 'required',
                'latitude' => 'required',
            ]
        );

        if ($validator->fails()) {
            $status = false;
            $message = $validator->errors()->first();
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $locations = Captain::select('longitude', 'latitude', 'api_token', 'status')->where('activity', 1)->where('status', 1)->get()->toArray();
            if (count($locations)) {
                $status = true;
                $message = 'following drivers available';
                $options = DB::table('options')->where('name', 'radius')->get()->toArray();
                if ($options) {
                    $radius = $options[0]->value;
                } else {
                    $radius = '15';
                }
                $vehicles = [];
                foreach ($locations as $key => $value) {
                    $km = distanceCalculation($value['latitude'], $value['longitude'], $request->latitude, $request->longitude);
                    $km = explode('.', $km)[0];
                    if ($km <= round($radius, '2')) {
                        $vehicles[] = array('vehicle_details' => array_merge($value, ['vehicle_current_radius' => $km]), 'radius' => $radius);
                    }
                }
                return response()->json(['message' => $message, 'status' => $status, 'vehicles' => $vehicles], 200);
            } else {
                $status = false;
                $message = 'no drivers available';
                return response()->json(['message' => $message, 'status' => $status], 200);
            }
        }
    }
    function activity_update_cron(){
        $data['users'] = Captain::select( 'activity','status', 'last_loc_update','api_token')->where('activity', 1)->where('status','!=', 0)->get();
        $url= "https://test.tod.com.pk/api/v1/captain/update-activity-status";
        if(!empty($data['users'])){
            foreach ($data['users'] as $key => $value) {
                $secs=[];
                if (!empty($value->last_loc_update)) {
                    $secs = explode(':', timeDiffInSec(explode(' ', date('Y-m-d h:i:s'))[1], explode(' ', $value->last_loc_update)[1]));
                }
                if (isset($secs[1]) && $secs[1] >= 1 || isset($secs[1]) && $secs[2] >= 50) {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_POSTFIELDS, ['api_token' => $value->api_token, 'status' => '0']);
                    curl_setopt($curl, CURLOPT_URL, $url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    $result = curl_exec($curl);
                    if (!$result) {
                        die("Connection Failure");
                    }
                    curl_close($curl);
                    echo $result;
                }
            }
        }
    }
}
