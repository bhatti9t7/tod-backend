<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Session;
class AdminLoginController extends Controller
{
	public function __construct()
	{
		$this->middleware("guest:admin", ['except' => ['logout']]);
	}

     public function showLoginForm()
    {
      
        return view('admin.AdminLogin');
    }

    public function login(Request $request){

    	// validation the form data;
       $validate = $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:6'
            ]);
      if($validate){
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password'=>$request->password], $request->remember)){
          session()->flash('success', 'You login successfully');
          return redirect(route('admin.dashboard'));
        }
        else{
          session()->flash('danger', 'Email or Password not match');
        }
      }
      else{
         session()->flash('warning', 'validation failed');
      }
        //if unsuccessful, then redirect to back to the login with the form data.
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

   public function logout()
   {
   	Auth::guard('admin')->logout();
   	return redirect('/admin');
   }
}
