<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class CaptainLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest',['except' => ['logout','userLogout']]);
    }

   public function showLoginForm()
    {
        return view('captain.CaptainLogin');
    }

    public function login(Request $request)
    {
        // validation the form data;
        $this->validate($request,[
            'phone' => 'required',
            'password' => 'required|min:6'
            ]);
        // Attempt to login the user in
        if(Auth::guard('captain')->attempt(['phone' => $request->phone, 'password'=>$request->password], $request->remember)){
        // if successful, then redirect to their intented location.
        //return redirect()->intented(route('/home'));
            return redirect(route('captain.dashboard'));
        }

        //if unsuccessful, then redirect to back to the login with the form data.

        return redirect()->back()->withInput($request->only('phone', 'remember'));
    }
     public function captainLogout()
   {
    Auth::guard('captain')->logout();
    return redirect('/captain');
   }

    
}
