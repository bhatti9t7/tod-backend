<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware("guest:web", ['except' => ['logout']]);
    }

     public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        // validddation the form data;
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required|min:6'
            ]);

        // Attempt to login the user in
        if(Auth::guard('web')->attempt(['email' => $request->email, 'password'=>$request->password], 
            $request->remember)){

            return redirect(route('home'));
        }
        //if unsuccessful, then redirect to back to the login with the form data.
        return redirect()->back()->withInput($request->only('email', 'remember'));
    }

   public function logout()
   {
    Auth::guard('web')->logout();
    return redirect('/');
   }

   
}
