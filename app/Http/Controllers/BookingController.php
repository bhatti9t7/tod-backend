<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Booking;
use App\BookingDestination;
use App\Ride;
use Illuminate\Support\Facades\Input;
use DB;
use Excel;
use Auth;
class BookingController extends Controller
{

	public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {

         $bookings = Booking::orderby('id' ,'DESC')->where('status' , '<>', 1)->with('BookingDestination')->with('user')->with('good')->with('vehicleSubCategorie.vehiclecategorie')->get();
          // dd($bookings);
        return view('admin.bookings' , compact('bookings'));
    }

    public function show($id)
    {
        $Booking = Booking::where('id', $id)->with('BookingDestination')->with('good')->with('vehicleSubCategorie.vehiclecategorie')->first();
        // dd($Booking);
        $ride = Ride::where('booking_id' , $id)->with('booking')->with('user')->with('captain.VehicalCategorie')->with('captain')->with('booking.good')->with('RideCompleted')->first();
        // dd($ride);exit();
        $destination = BookingDestination::where('booking_id', $id)->get();

      return view('admin.bookingDetail', compact('Booking', 'ride' , 'destination'));
    }

    public function get_booking(Request $request)
    {
    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = Booking::where('status' , '<>', 1)->count();


        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $airportOutbound = Booking::with('BookingDestination')->with('user')->with('good')->with('vehicleSubCategorie.vehiclecategorie')->where('status' , '<>', 1)->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $airportOutbound->where('id', 'like', "%{$filter}%")
                ->orWhere('amount','like',"%{$filter}%")
                ->orWhere('pickup_place','like',"%{$filter}%")
                ->orWhere('distance','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

                $airportOutbound->orWhereHas('user', function ($query) use ($filter){
                $query->where('fname', 'like', "%{$filter}%");
            });
             $airportOutbound->orWhereHas('good', function ($query) use ($filter){
                $query->where('good_name', 'like', "%{$filter}%");
            });
             

           


            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$airportOutbound->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);


    }


      public function downloadExcel($type)
 {
    //     $data = Ride::orderby('id' ,'DESC')->where('status', 0 )->with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->get()->toArray();->join('porperty_types', 'porperty_types.pt_id', '=', 'property.pro_type_id')

        $data = Booking::orderby('id' ,'DESC')->where('status' , '<>', 1)->with('BookingDestination')->with('user')->with('good')->with('vehicleSubCategorie.vehiclecategorie')->get();
// dd($data);exit();
        \Excel::create('Accepted Booking Record ', function($excel) use($data) {

            $excel->sheet('user', function($sheet) use($data) {
                $excelData = [];
                $excelData[] = [
                    'Id',
                    'User Name',
                    'Good Name',
                    'Pickup Place',
                    'Vehicle Type',
                    'Vehicle',
                    'Date/time',
                    'Amount',
                    'Distance',
                    'Status',
                ];

                foreach ($data as $key => $value) {

                    $excelData[] = [
                        $value->id,
                        $value->user->fname,
                        $value->good->good_name,
                        $value['pickup_place'],
                        $value->vehicleSubCategorie->vehiclecategorie->name,
                        $value->vehicleSubCategorie->name,
                        $value['created_at'],
                        $value['amount'],
                        $value['distance'] .'Km',
                        $value['status']==0?'accepted':'pending',
                    ];  

                    $sheet->setTitle('Loader');
                  
                }
        
        // Build the spreadsheet, passing in the payments array

        /////////////////////////////
        
                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download($type);

    }


}
