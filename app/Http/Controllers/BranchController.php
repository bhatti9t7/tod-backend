<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Branch;
use App\City;
use Session;
class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {
       
        return view('admin.branch.branch_index');
    }
    
    public function create()
    {
       $cities = City::orderBy('id' , 'DESC')->get();
        return view('admin.branch.branch_Create', compact(['cities']));
        
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
        'name' => 'required|max:40||unique:branches',
        'address' => 'required', 
        'citi_id' => 'required', 
   
        ]);
       if($validator->fails())
       {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator);
       } 
       else
       {
        $branch_affected = Branch::create([
            'name'  => $request['name'],
            'address'  => $request['address'],
            'citi_id'  => $request['citi_id'],
            ]);
        if($branch_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

        return redirect()->route('branch.index');
        }
    }
    public function show(Request $request)
    {
        
    }

    
    public function edit($id)
    {
        $citi_data = City::all();
        $branch_data = Branch::find($id);
        return view('admin.branch.branch_Edit', compact('branch_data' ,'citi_data'));
        
    }
    
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),
        [
        'name' => 'required|max:40||unique:branches',
        'address' => 'required', 
        'citi_id' => 'required', 
   
        ]);
       if($validator->fails())
       {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator);
       } 
       else
       {
        $branch_item = Branch::findOrFail($id);
        $input = $request->all();
        $branch_affected = $branch_item->update($input);
        if($branch_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('branch.index');
        }
    }

   
    public function delete_record($id)
    {
        $Branch_item = Branch::find($id);

        $branch_item_destory = $Branch_item->delete();

        if($branch_item_destory){
        $response['status'] = true;
         session()->flash('success_message', 'Record has been delete. successfully.');
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return redirect()->route('branch.index');
    } 

     public function get_branch(Request $request)
    {
    
    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = Branch::count();

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = Branch::with('city')->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('name','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();
        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );
        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);
    }

    public function branch_change_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('activity');
        $user = Branch::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']=" Status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);

    }
    public function branch_on_select(Request $request)
      {
        // print_r($_GET);exit();
        $branch_item = Branch::where('citi_id',$request->id)->get();

        if($branch_item)
         return response()->json($branch_item, 200);
       else
        $response = "no data found";
          return response()->json($response, 200);
      }


}


