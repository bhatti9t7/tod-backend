<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Captain;
use App\Ride;
use App\Booking;
use App\file;
use DB;

use PDF;
class CaptainReportController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
  
    public function report( $id )
    {

        $captains = Ride::where('captain_id' , $id)->with('captain')->with('booking')->with('user')->with('vehical')->get();
        $total_ride = Ride::where('captain_id' , $id)->count();
        $detail = Captain::where('id', $id)->first();       
        $amount = Booking::whereHas('Rides',function($query) use ($id){
            $query->where('captain_id', $id);
        })->sum('amount');
        //dd($captains);
        $pdf = PDF::loadView('admin.captain.reportpdf', compact('captains' , 'total_ride' , 'amount' ,'detail'));
         return $pdf->download('captindetail.pdf');
 
    }

    public function index()
    {

        $captain = Captain::all();
        return view('admin.captain.captainreport' , compact('captain'));
    }

    public function filtercaptain(request $request)
    {
    	$fromDate = $request['start_date'];
        $toDate = $request['last_date'];
         $captains = Ride::whereBetween('created_at', ['2018-05-14', '2018-07-14'])->with('captain')->with('booking')->with('user')->with('vehical')->get();
      //dd($captains);exit;
          $pdf = PDF::loadView('reports.captainpdf', compact('captains'));
         return $pdf->download('captindetail.pdf');
    	
    }
    
}
