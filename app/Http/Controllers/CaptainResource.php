<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Captain;
use App\Ride;
use App\CaptainWallet;
use App\BookingDestination;
use App\VehicalCategorie;
use App\file;
use Session;
use Illuminate\Support\Facades\Input;
use Auth;
use Excel;
use App\VehicleSubCategorie;
use App\Rate;
use App\User;
use App\CaptainStats;
use App\Account;
class CaptainResource extends Controller
{
  public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
  
    public function index()
    {
      $accounts = Captain::orderBy('id','desc')->with('wallet')->limit(20);
      // dd($accounts);
        return view('admin.captain.captainRosource');
    }
  
    public function create()
    {
        $vehicals = VehicalCategorie::where('status',1)->get();
        // dd($vehicals);
        $vehicals_categories = VehicleSubCategorie::all();
        return view('admin.captain.captain_create' , compact('vehicals' ,'vehicals_categories'));
    }
    public function store(Request $request){  
      $validator = Validator::make($request->all(),[
         'fname' => 'required',
         'lname' => 'required',
         'phone' => 'required',
         'vehicle_name' => 'required',
         'vehicle_image' => 'required',
        //  'vehical_sub_id' => 'required',
         'vehical_cat_id'=> 'required',
          'password' => 'required|min:6|confirmed',
         'password_confirmation' => 'required_with:password|same:password|min:6'
      ]);
       if($validator->fails()){
         $message = $validator->errors()->first();
          Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator)->withInput();
       } else {
       $obj = new captain;
        $Get_Doc ='no-image.png';
        
        $licenses = !empty($request->driving_licenes_images)?implode(',',$request->driving_licenes_images):'';
        $cnics = !empty($request->driving_cnic_images)?implode(',',$request->driving_cnic_images):'';
        $register_img = !empty($request->vehicle_registration_image)?implode(',',$request->vehicle_registration_image):'';
        $vehicle_img = !empty($request->vehicle_image)?implode(',',$request->vehicle_image):'';
        $CNIC_SET_String = 0;
        $Rename_CNIC = array();
        $Rename_License = array();

        if($request->hasfile('images')){
          $Get_Doc = $request->file('images')->getClientOriginalName();
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $Get_Doc = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/captain/profiles/', $Get_Doc);
        }
         if($request->hasfile('owner_image')){
          $owner_img = $request->file('owner_image')->getClientOriginalName();
          $Extension_profile = $request->file('owner_image')->getClientOriginalExtension();
          $owner_img = 'owner'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('owner_image')->move('owner_image/captain/owner_image/', $owner_img);
        }

        if($request->hasfile('vehicle_image')){
          $vehicle_image = $request->file('vehicle_image')->getClientOriginalName();
          $Extension_profile = $request->file('vehicle_image')->getClientOriginalExtension();
          $vehicle_image = 'vehicle_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('vehicle_image')->move('images/captain/vehicle_image/', $vehicle_image);
        }
        $obj->fname = $request->fname;
        $obj->lname = $request->lname;
        $obj->phone = "+92".$request->phone;
        $obj->email = $request->email;
        $obj->city = $request->city;
        $obj->vehical_sub_id = $request->vehical_sub_id;
        $obj->images = $Get_Doc;
        $obj->driving_licenes_images = $licenses;
        $obj->cnic_images = $cnics;
        $obj->vehicle_registration = $request->vehicle_registration;
        $obj->vehicle_name = $request->vehicle_name;
        $obj->licence_no = $request->licence_no;
        $obj->vehicle_no = $request->vehicle_no;
        $obj->vehical_cat_id = $request->vehical_cat_id;
        $obj->owner_city = $request->owner_city;
        $obj->owner_name = $request->owner_name;
        $obj->owner_fname = $request->owner_fname;
        $obj->chesi_no = $request->chesi_no;
        $obj->engin_no = $request->engin_no;
        $obj->created_by = Auth::guard('admin')->id();
        $obj->remarks_captain = $request->remarks_captain;
        $obj->remarks_vehicle = $request->remarks_vehicle;
        $obj->remarks_owner = $request->remarks_owner;
        $obj->password = Hash::make($request->password);
        $obj->captain_cnic = $request->captain_cnic;
        $obj->owner_cnic = $request->owner_cnic;
        $obj->bank = $request->bank;
        $obj->branch_code = $request->branch_code;
        $obj->account_no = $request->account_no;
        $obj->activity  = $request->status;
        $obj->status = 0;
        $obj->owner_image = isset($owner_img)?$owner_img:'';
        $obj->vehicle_image = isset($vehicle_img)?$vehicle_img:'';
        $obj->vehicle_registration_image = isset($register_img)?$register_img:'';
       $obj->api_token = str_random(60);
       $obj->description = $request->description;
       $captain_affected =$obj->save();
       if($captain_affected){
        $captain_rates = CaptainStats::insert([
          'captain_id'=> $obj->id,
          'users' => 1,
          'rating' => 5,
        ]);
        $id = Account::orderBy('id' , 'DESC')->first();
        if($id)
           $codes =('113'.'00'.$id->id);
        else
          $codes =('113'.'00'.'1');
        Account::insert([
            'code'=>$codes,
            'title'=>$request->fname. ' ' .$request->lname,
            'admin_id'=>$obj->id,
            'c_balance' =>0,
            'account_cat_id' =>2,
        ]);
        session()->flash('success', 'Record has been updated. successfully.');
        return redirect()->route('captainResource.index');
        }
        else{
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('captainResource.index');
        }
   }
}

    public function edit($id)
    {
        $vehicals = VehicalCategorie::all();
        $vehicals_categories = VehicleSubCategorie::all();
        $captain = Captain::where('id' ,$id)->with('VehicalSubCategorie')->first();
    
        return view('admin.captain.captain_edit', compact('captain' , 'vehicals' ,'vehicals_categories'));
    }

    
    public function update(Request $request, $id)
    {
      $validator = Validator::make($request->all(),
    [
     'fname' => 'required',
     'lname' => 'required',
     'phone' => 'required',
     'vehical_sub_id' => 'required',
     'vehical_cat_id'=> 'required',
   
   ]);

       if($validator->fails())
       {
         $message = $validator->errors()->first();
          Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator)->withInput();
       } else {
        $data = Captain::findOrFail($id);
        $input = $request->all();
            if(isset($input['password']) && !empty($input['password'])) 
              {
                Captain::broker($input);
                $input['password'] = Hash::make($input['password']);  
              }
              else{
                unset($input['password']);
              }
      if($request->hasfile('images')){
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $Get_Doc = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/captain/profiles/', $Get_Doc);
          $input['images'] =$Get_Doc;
        }
      if($request->hasfile('owner_image')){
          $owner_img = $request->file('owner_image')->getClientOriginalName();
          $Extension_profile = $request->file('owner_image')->getClientOriginalExtension();
          $owner_img = 'owner'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('owner_image')->move('owner_image/captain/owner_image/', $owner_img);
           $input['owner_image'] = $owner_img;
        }

        if($request->hasfile('vehicle_image')){
          $vehicle_image = $request->file('vehicle_image')->getClientOriginalName();
          $Extension_profile = $request->file('vehicle_image')->getClientOriginalExtension();
          $vehicle_image = 'vehicle_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('vehicle_image')->move('images/captain/vehicle_image/', $vehicle_image);
           $input['vehicle_image'] = $vehicle_image;
        }


// return $input;
       $updated_data = $data->update($input); 
        if($updated_data)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('captainResource.index');
        }
    }

    public function delete_record($id)
    {
        $captains = Captain::find($id);
        $effected= $captains->delete();
        $response= array();
        if($effected){
        $response['status'] = true;
        session()->flash('success_message', 'Record has been updated. successfully.');
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        session()->flash('failed', 'Fail to delete User.');
        $response['message'] = "Fail to delete Record.";
        }
        return redirect()->route('captainResource.index');
    }
    public function searchbar(Request $request)
    {

     $response = array();
     $response['status'] = false;
     $response['message']= "No, Record Found";
     $searching = $request['search'];

     $captains=Captain::where('name', 'like', '%'.$searching.'%')
                       ->orWhere('email', 'like', '%'.$searching.'%')
                       ->get();

     if($captains){
     $response['status'] = true;
     $response['message']= "Record Found";
     $response['captains']= $captains;
     }             
     return response()->json($response);    

    }

    public function show($id)
    {

      $captain = Ride::where('captain_id' ,$id)->with('booking')->with('BookingDestination')->with('captain')->with('user')->orderBy('id', 'DESC')->get();
      //dd($captain);exit;
      return view('admin.captain.captain_profile_Byid', compact('captain'));

    }

    public function walletShow($id)
    {
        //DB::enableQueryLog();
        $wallet = CaptainWallet::where('captain_id',$id)->with("captain")->first();
       // return $wallet;
       $captain = Ride::where('captain_id' ,$id)->with('booking')->with('BookingDestination')->with('captain')->with('user')->orderBy('id', 'DESC')->get();

        return view('admin.captain.captainwallets' , compact('wallet' ,'captain'));
    }

     public function get_captains_ajax()
    {
          $captains = Captain::orderBy('id', 'DESC');
          return datatables()->of($captains)->make(true);
       
    }

    public function activate($id) {
        
        $captain = Captain::find($id) ;
        $captain->activity  = 1;
        $captain->save() ;
        // return "USER WITH ID: $id  is now active"  ;
        return redirect()->back() ;
    }
        
    public function disable($id) {
        
        $captain = Captain::find($id) ;
        $captain->activity   = 0;
        $captain->save() ;

    Session::flash('success', 'User disabled') ;
        return redirect()->back() ;
    }


    public function get_captain(Request $request)
    {
      $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        if(Auth::user()->id==1){
          $total = Captain::count();
        }else{
        $total = Captain::where('created_by', Auth::user()->id)->count();
        }
        

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        // dd(Auth::user()->id);
        if(Auth::user()->id==1){
          $users = Captain::offset($offset)->limit($perpage)->orderBy($field,$sort);
        }else{
        $users = Captain::where('created_by', Auth::user()->id)->offset($offset)->limit($perpage)->orderBy($field,$sort);
        }

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('fname','like',"%{$filter}%")
                ->orWhere('email','like',"%{$filter}%")
                ->orWhere('phone','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);

    }
    public function captain_change_status(Request $request)
    {

        $id = $request->input('user_id');
        $activity = $request->input('activity');
        $user = Captain::find($id) ;
        $user->activity  = $activity;
       
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']="User status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);


    }

     public function upload_document_file(Request $request)
    { 
       $data = array();
       $upload_return = false;


        $folder_name =!empty($request->folder_name)?$request->folder_name:'default';
        $file_path = "images/captain/$folder_name/";

       $files = $request->file('file');
       if (!is_array($files)) {
        $files = [$files];
        }

      if (!is_dir($file_path)) {
        mkdir($file_path, 0777);
      }

      $files_names = array();

      for ($i = 0; $i < count($files); $i++) {
         $file = $files[$i];
         $name = sha1(date('YmdHis') . str_random(30));
         $save_name = $name . '.' . $file->getClientOriginalExtension();
         $file->move($file_path, $save_name);
         $files_names[] = $save_name;

         if(isset($request->captain_id) && !empty($request->captain_id)){
          $captain = Captain::findOrFail($request->captain_id);
          switch ($folder_name) {
            case 'licenses':
            $captain->driving_licenes_images = $this->add_element_array($save_name,$captain->driving_licenes_images);
            $captain->save();
            break;  
            case 'cnics':
            $captain->cnic_images = $this->add_element_array($save_name,$captain->cnic_images);
            $captain->save();
            break;  
            case 'registration_image':
            $captain->vehicle_registration_image = $this->add_element_array($save_name,$captain->vehicle_registration_image);
            $captain->save();
            break; 
            case 'vehicle_image':
            $captain->vehicle_image = $this->add_element_array($save_name,$captain->vehicle_image);
            $captain->save();
            break;
          }
        }
      }


        if (!empty($files_names)) 
        {
            $data['files_names'] = $files_names;
            $data['status'] = true;
            $data['message'] = 'File has been upload successfully';
        }
        else
        {
           // $data['file_name'] = $this->filedata['file_name'];
            $data['status'] = false;
            $data['message'] = 'Invalid request to upload file.';
        }
        return response()->json($data, 200);

    }

    public function remove_document_file(Request $request)
    {
     $data = array();
     $upload_return = false;
     $folder_name =!empty($request->folder_name)?$request->folder_name:'default';
     $file_path = "images/captain/$folder_name/";



     $file = $request->file;
     $file_with_path = $file_path. $file;
       if (file_exists($file_with_path)) {
            unlink($file_with_path);
            $upload_return = true;

            if(isset($request->captain_id) && !empty($request->captain_id)){
              $captain = Captain::findOrFail($request->captain_id);
              switch ($folder_name) {
                case 'licenses':
                $captain->driving_licenes_images = $this->remove_element_array($file,$captain->driving_licenes_images);
                $captain->save();
                break;  
                case 'cnics':
                $captain->cnic_images = $this->remove_element_array($file,$captain->cnic_images);
                $captain->save();
                break;  
                case 'registration_image':
                $captain->vehicle_registration_image = $this->remove_element_array($file,$captain->vehicle_registration_image);
                $captain->save();
                break; 
                case 'vehicle_image':
                $captain->vehicle_image = $this->remove_element_array($file,$captain->vehicle_image);
                $captain->save();
                break;
              }
            }

        }

        if ($upload_return) 
        {
          $data['files_name'] = $file;
          $data['status'] = true;
          $data['message'] = 'File has been deleted successfully';
        }
        else
        {
           // $data['file_name'] = $this->filedata['file_name'];
          $data['status'] = false;
          $data['message'] = 'Invalid request to deleted file.';
        }
        return response()->json($data, 200);  


    }

    function remove_element_array($file_name,$images)
    {
      $old_images = explode(",", $images);
      $index = array_search($file_name, $old_images);
      if($index !== false){
        unset($old_images[$index]);
      }
      $new_images = implode(",", $old_images);
      return $new_images;
    }

    function add_element_array($file_name,$images)
    {
      if(!empty($images))
      $old_images = explode(",", $images);
    else 
      $old_images = array();
      array_push($old_images, $file_name);
      $new_images = implode(",", $old_images);
      return $new_images;
    }

     public function downloadExcel($type)
 {
    //     $data = Ride::orderby('id' ,'DESC')->where('status', 0 )->with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->get()->toArray();->join('porperty_types', 'porperty_types.pt_id', '=', 'property.pro_type_id')


        $data = Captain::all()->toArray();

        return Excel::create('laravelcode', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }

    public function captain_account(Request $request){
       $accounts = Captain::orderBy('id','desc')->with('wallet')->limit(20);
       $filter = $request->input('q');
       if(!empty($filter)){  
        $accounts->where('fname', 'like', "%{$filter}%")
        ->orWhere('lname', 'like', "%{$filter}%")
        ->orWhere('email', 'like', "%{$filter}%")
        ->orWhere('phone', 'like', "%{$filter}%");
      }

      $data=$accounts->get();
      if($data){
        $status = true;
        return response()->json($data, 200);
      }
      else
      {
          return response()->json($data, 200);
      }
    }
     
}
