<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Captain;
use App\CaptainWallet;
use Session;

class CaptainWalletController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

    public function edit($id)
    {
        $wallet = CaptainWallet::where('id' , $id)->first();
        //dd(\DB::getQueryLog());
       // dd($wallet);exit;
        return view('admin.captain.captainwallet_edit' , compact('wallet'));

    }

    public function update(Request $request, $id)
    {
        //$data = CaptainWallet::findOrFail('id' , $request->id);
     
        $input = $request->all();
       
      session()->flash('message', 'Successfully updated the post');
      return redirect()->route('captainResource.index');
        
    }
    
}
