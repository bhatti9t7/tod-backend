<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Trad;
use Illuminate\Support\Facades\Input;
use Auth;

class BussinessController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

   
    public function create()
    {
        return view('admin.Cms.bussiness.bussiness_created');
    }

    
    public function store(Request $request)
    
    { 
       $validator = Validator::make($request->all(),
    [
     'bussiness_image' => 'required', 
     'bussiness_content' => 'required' ,
   ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       } 
       else {
       $obj = new Trad;
 
        $bussiness_image = $request->file('bussiness_image')->getClientOriginalName();

        if($request->hasfile('bussiness_image')){
          $Extension_profile = $request->file('bussiness_image')->getClientOriginalExtension();
          $bussiness_image = 'bussiness_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('bussiness_image')->move('bussiness/image/', $bussiness_image);
      }
         
        $obj->bussiness_image = $bussiness_image;
        $obj->bussiness_content =$request->bussiness_content;
       $obj->save();
        return redirect()->route('public-home-page.index');
        
    }
}
  
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $trad = Trad::find($id);
    
        return view('admin.Cms.bussiness.bussiness_edit', compact('trad'));
    }

    
    public function update(Request $request, $id)
    {
       $data = Trad::findOrFail($id);
        $input = $request->all();

     if ($Get_Doc = $request->file('bussiness_image'))
    {
          $Get_Docs = $Get_Doc->getClientOriginalName();                        
          $Extension_profile = $request->file('bussiness_image')->getClientOriginalExtension();
           $Get_Doc = 'bussiness_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('bussiness_image')->move('bussiness/image/', $Get_Doc);
                           
     }   
       $input['bussiness_image'] =$Get_Doc;
       $data->update($input); 
        session()->flash('message', 'Successfully updated the post');
        return redirect()->route('public-home-page.index');

        
    }
    
    public function destroy($id)
    {
        $captains = Trad::find($id);
        $captains->delete();
        return redirect()->route('public-home-page.index');
    }
}
