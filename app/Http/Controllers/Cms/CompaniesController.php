<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Companies;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
class CompaniesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

   
    public function create()
    {
        return view('admin.Cms.companies.companies_created');
    }

    
    public function store(Request $request)
    
    { 
       $validator = Validator::make($request->all(),
    [
     'news_image' => 'required', 
     'phone' => 'required',
     'email' => 'required',
     'address' => 'required',
   ]);

       if($validator->fails())
       {    $message = $validator->errors()->first();
              Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator);
       } 
       else {
       $obj = new Companies;
 
        $news_image = $request->file('news_image')->getClientOriginalName();

        if($request->hasfile('news_image')){
          $Extension_profile = $request->file('news_image')->getClientOriginalExtension();
          $news_image = 'news_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('news_image')->move('companies/image/', $news_image);
      }
         
        $obj->images = $news_image;
        $obj->phone =$request->phone;
        $obj->email =$request->email;
        $obj->address =$request->address;
        $obj->description =$request->description;
       $obj->save();
        return redirect()->route('public-home-page.index');
        
    }
}
  
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $Companies = Companies::find($id);
        
    
        return view('admin.Cms.companies.companies_edit', compact('Companies'));
    }

    
    public function update(Request $request, $id)
    {
       $data = Companies::findOrFail($id);
        $input = $request->all();

     if ($Get_Doc = $request->file('images'))
    {
          $Get_Docs = $Get_Doc->getClientOriginalName();                        
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
           $Get_Doc = 'images'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('companies/image/', $Get_Doc);
                           
     }   
       $input['images'] =$Get_Doc;
       $data->update($input); 
        session()->flash('message', 'Successfully updated the post');
        return redirect()->route('public-home-page.index');

        
    }
    
    public function destroy($id)
    {
         $captains = Companies::find($id);
        $captains->delete();
        return redirect()->route('public-home-page.index');
    }
}
