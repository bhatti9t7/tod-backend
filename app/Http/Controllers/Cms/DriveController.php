<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Drive;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;

class DriveController extends Controller
{
   public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

   

   
    public function create()
    {
        return view('admin.Cms.drive.drive_created');
    }

    
    public function store(Request $request)

    { 

       $validator = Validator::make($request->all(),
    [
     'drive_image' => 'required',
    
   ]);
// dd($request);exit();
       if($validator->fails())
       {
            $message = $validator->errors()->first();
           Session::flash('info', " $message") ;
          return Redirect::back()->withErrors($validator);
       } 
       else {
       $obj = new Drive;
 
        $drive = $request->file('drive_image')->getClientOriginalName();

        if($request->hasfile('drive_image')){
          $Extension_profile = $request->file('drive_image')->getClientOriginalExtension();
          $drive = 'drive'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('drive_image')->move('public/drive/', $drive);
      }
         
        $obj->drive_image = $drive;
        $obj->drive_content = $request->drive_content;
        $record_affected = $obj->save();

         if($record_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

        return redirect()->route('public-home-page.index');
        
    }
}
  
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $drive = Drive::find($id);
    
        return view('admin.Cms.drive.drive_edit', compact('drive'));
    }

    
    public function update(Request $request, $id)
    {
       $data = Drive::findOrFail($id);
        $input = $request->all();

     if ($Get_Doc = $request->file('drive_image'))
    {
          $Get_Docs = $Get_Doc->getClientOriginalName();                        
          $Extension_profile = $request->file('drive_image')->getClientOriginalExtension();
           $Get_Doc = 'drive'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('drive_image')->move('public/drive/', $Get_Doc);
                           
     }   
       $input['drive_image'] =$Get_Doc;
       $record_affected = $data->update($input); 
         if($record_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        
       return redirect()->route('public-home-page.index');

        
    }
    
    public function destroy($id)
    {
         $captains = Drive::find($id);
        $record_affected = $captains->delete();

         if($record_affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('public-home-page.index');
    }
}
