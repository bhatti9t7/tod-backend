<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Logo;
use App\Slider;
use Illuminate\Support\Facades\Input;
use Auth;

class LogoController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

    public function index()
    {
        $logo = Logo::orderBy('id', 'DESC')->paginate(5);
        $slid = Slider::orderBy('id', 'DESC')->paginate(5);
        
        return view('admin.Cms.logo.index' , compact([ 'logo' , 'slid']));
    }

   
    public function create()
    {
        return view('admin.Cms.logo.logo_created');
    }

    
    public function store(Request $request)
    
    { 
       $validator = Validator::make($request->all(),
    [
     'logo_image' => 'required', 
   ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       } 
       else {
       $obj = new Logo;
 
        $logo = $request->file('logo_image')->getClientOriginalName();

        if($request->hasfile('logo_image')){
          $Extension_profile = $request->file('logo_image')->getClientOriginalExtension();
          $logo = 'logo'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('logo_image')->move('logo_image/admin/logo/', $logo);
      }
         
        $obj->logo_image = $logo;
       $obj->save();
        return redirect()->route('logo.index');
        
    }
}
  
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $logo = Logo::find($id);
    
        return view('admin.Cms.logo.logo_edit', compact('logo'));
    }

    
    public function update(Request $request, $id)
    {
       $data = Logo::findOrFail($id);
        $input = $request->all();
    

     if ($logo = $request->file('logo_image'))
    {
          $logo = $request->file('logo_image')->getClientOriginalName();                        
          $Extension_profile = $request->file('logo_image')->getClientOriginalExtension();
           $logo = 'logo'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('logo_image')->move('logo_image/admin/logo/', $logo);
                           
     }   
       $input['logo_image'] =$logo;
       // dd($input['logo_image']);exit;
       $data->update($input); 
        session()->flash('message', 'Successfully updated the post');
        return redirect()->route('logo.index');

        
    }
    
    public function destroy($id)
    {
         $captains = Logo::find($id);
        $captains->delete();
        return Redirect::route('logo.index');
    }
}
