<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\News;
use Illuminate\Support\Facades\Input;
use Auth;
use Session;
class NewsController extends Controller
{
   public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

   
    public function create()
    {
        return view('admin.Cms.news.news_created');
    }

    
    public function store(Request $request)
    
    { 
       $validator = Validator::make($request->all(),
    [
     'news_image' => 'required', 
     'news_content' => 'required',

   ]);

       if($validator->fails())
       {     $message = $validator->errors()->first();
              Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator);
       } 
       else {
       $obj = new News;
 
        $news_image = $request->file('news_image')->getClientOriginalName();

        if($request->hasfile('news_image')){
          $Extension_profile = $request->file('news_image')->getClientOriginalExtension();
          $news_image = 'news_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('news_image')->move('Cms/news_image/', $news_image);
      }
         
        $obj->news_image = $news_image;
        $obj->news_content =$request->news_content;
       
       $obj->save();
        return redirect()->route('public-home-page.index');
        
    }
}
  
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $newsedit = News::find($id);

        return view('admin.Cms.news.news_edit', compact('newsedit'));
    }

    
    public function update(Request $request, $id)
    {
       $data = News::findOrFail($id);
        $input = $request->all();

     if ($news_image = $request->file('news_image'))
    {
          $news_images = $news_image->getClientOriginalName();                        
          $Extension_profile = $request->file('news_image')->getClientOriginalExtension();
           $news_image = 'news_image'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('news_image')->move('Cms/news_image/', $news_image);
                           
     }   
       $input['news_image'] =$news_image;
       $data->update($input); 
        session()->flash('message', 'Successfully updated the post');
        return redirect()->route('public-home-page.index');

        
    }
    
    public function destroy($id)
    {
         $captains = News::find($id);
        $captains->delete();
        return redirect()->route('public-home-page.index');
    }
}
