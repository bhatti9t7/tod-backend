<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\News;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Trad;
use App\Drive;
use App\Slider;
use App\Companies;

class PublicHomeController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

    public function index()
    {

        $news = News::orderBy('id', 'DESC')->paginate(5);
        $bussiness = Trad::orderBy('id', 'DESC')->paginate(5);
       $drive = Drive::orderBy('id', 'DESC')->paginate(5);
        $slider  = Slider::orderBy('id', 'DESC')->paginate(5);
        $companies = Companies::orderBy('id', 'DESC')->paginate(5);

        return view('admin.Cms.homePage.index' , compact([ 'news' , 'bussiness' , 'drive' , 'slider' , 'companies']));
    }

}
