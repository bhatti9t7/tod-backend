<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Emergency;

class EmergencyController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    public function index()
    {
        $emergencies = Emergency:: orderBy('id', 'DESC')->paginate(5);
        return view('admin.emergency', compact('emergencies'));
    }

  
    public function create()
    {
         return view('admin.emergencies_created');
    }


    public function store(Request $request)
    {
         $validator = Validator::make($request->all(),
    [
     'description' => 'required',
     'emergencies_name' => 'required', 
   ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       } 
       else {
       $obj = new Emergency;
 
         
        $obj->description =  $request->description;
        $obj->emergencies_name = $request->emergencies_name;
       $obj->save();

        return redirect()->route('emergencies.index');
        
        }
    }

    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
         $emergencies = Emergency::find($id);
    
        return view('admin.emergencies_edit', compact('emergencies'));
    }

   
    public function update(Request $request, $id)
    {
         $data = Emergency::findOrFail($id);
        $input = $request->all();

       $data->update($input); 
        session()->flash('message', 'Successfully updated the post');
        
       return redirect()->route('emergencies.index');
    }

 
    public function delete_record($id)
    {
        $captains = Emergency::find($id);
        $emergencies_affected = $captains->delete();
        if($emergencies_affected){
        $response['status'] = true;
        $response['message'] = "User has been delete Successfully.";
         session()->flash('success_message', 'Record has been updated. successfully.');
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete User.";
         session()->flash('danger', 'Fail to delete User.');
        }
  
        return redirect()->route('emergencies.index');
    }

         public function get_emergnecies(Request $request)
    {

    
    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = Emergency::count();

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = Emergency::offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('emergencies_name','like',"%{$filter}%")
                 ->orWhere('description','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);


    }

    public function emergnecies_change_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('activity');
        $user = Emergency::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']=" Status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);


    }



}



