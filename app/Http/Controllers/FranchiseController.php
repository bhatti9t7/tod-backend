<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Franchise;
use App\Branch;
use Session;
class FranchiseController extends Controller
{
   
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {
       
        return view('admin.franchise.franchise_index');
    }

    
    public function create()
    {
        $branch_data = Branch::all();
        return view('admin.franchise.franchiseCreate' , compact('branch_data'));
        
    }

    
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(),
        [
        'name' => 'required|max:40||unique:franchises',
        'address' => 'required',
        'branch_id' => 'required'
   
        ]);

       if($validator->fails())
       {
            $message = $validator->errors()->first();
            Session::flash('info', " $message") ;
            return Redirect::back()->withErrors($validator);
       } 
       else
       {
        $franchise_affected = Franchise::create([
            'name'          => $request['name'],
            'address'          => $request['address'],
            'branch_id'          => $request['branch_id'],
            ]);

        if($franchise_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

        return redirect()->route('franchise.index');
        }
    }
    
    public function show(Request $request)
    {
        
    }

    
    public function edit($id)
    {
         $franchise_data = Franchise::find($id);
          $branch_data = Branch::all();
    
        return view('admin.franchise.franchiseEdit', compact( 'franchise_data' ,'branch_data'));
        
    }

    
    public function update(Request $request, $id)
    {
        $branch_item = Franchise::findOrFail($id);
        $input = $request->all();

        $branch_affected = $branch_item->update($input);

        if($branch_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('franchise.index');
    }

   
    public function delete_record($id)
    {
        $Branch_item = Franchise::find($id);

        $branch_item_destory = $Branch_item->delete();

        if($branch_item_destory){
        $response['status'] = true;
        session()->flash('success_message', 'Record has been delete. successfully.');
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return redirect()->route('franchise.index');
    } 

     public function get_franchise(Request $request)
    {

    
    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
        $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

        $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
        $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

        $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
        $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

        $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

        $pages = 1;
        $total = Franchise::count();

        // $perpage 0; get all data
        if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = Franchise::with('branch')->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
                ->orWhere('name','like',"%{$filter}%")
                ->orWhere('status','like',"%{$filter}%")
                ->orWhere('created_at','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$users->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                    'sort'  => $sort,
                    'field' => $field,
                ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);

    }

    public function franchise_change_status(Request $request)
    {
         $id = $request->input('user_id');
        $status = $request->input('activity');
        $user = Franchise::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['activity'] = true;
        $response['message']=" Status has been changed Successfully.";
          }else{
        $response['activity'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);


    }


}



