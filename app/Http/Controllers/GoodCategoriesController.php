<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\GoodCategorie;

class GoodCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {
        //$datatypes = GoodCategorie::orderBy('id', 'DESC')->paginate(5);
       
        return view('admin.goodscategories', compact(['datatype' , 'datatypes']));
    }

    
    public function create()
    {
         return view('admin.goodCategoriesCreate');
    }

    
    public function store(Request $request)
    {
    	 $validator = Validator::make($request->all(),
    	[
     	'name' => 'required|max:40||unique:good_categories',
   
   		]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       } 
       else
       {

        $good_affected = GoodCategorie::create([
            'name' => $request['name'],
            ]);

        if($good_affected)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

       return redirect()->route('good-categories.index');
        }

    }
    public function show(datatype $datatype)
    {
        
    }

    
    public function edit($id)
    {
         $Datatypes = GoodCategorie::find($id);
    
        return view('admin.goodCategoriesEdit', compact('Datatypes'));
        
    }

    
    public function update(Request $request, $id)
    {
        $Datatypes = GoodCategorie::findOrFail($id);
        $input = $request->all();

        $affected = $Datatypes->update($input);

        if($affected)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('good-categories.index');
    }

   
    public function destroy($id)
    {
       	$datatyp = GoodCategorie::find($id);
        $cat_affected = $datatyp->delete();

        if($cat_affected){
        $response['status'] = true;
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return response()->json($response);
    }  


     public function get_cat(Request $request)
    {

    
    $limit = $request->input('length');
    $start = $request->input('start');
    
    $start = $start?$start+1:$start;

    $search = $request->input('search.value');


    $order_column_no = $request->input('order.0.column');
    $order_dir = $request->input('order.0.dir');
    $order_column_name = $request->input("columns.$order_column_no.data");


      $user = GoodCategorie::orderby('id' ,'DESC')->offset($start)->limit($limit)->orderBy($order_column_name,$order_dir);


      if(!empty($search)){

               $user->where('name', 'like', "%{$search}%")
               ->orWhere('status','like',"%{$search}%");

      }

      $data = $user->get();

      $totalFiltered = GoodCategorie::count();

      $json_data = array(
      "draw"      => intval($request->input('draw')),
      "recordsTotal"  => count($data),
      "recordsFiltered" => intval($totalFiltered),
      "data"      => $data
    );
       return response()->json($json_data, 200);

    }

    public function categories_change_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('status');
        $user = GoodCategorie::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['status'] = true;
        $response['message']="User status has been changed Successfully.";
          }else{
        $response['status'] = false;
        $response['message']="Fail to change the user status.";
          }
        return response()->json($response, 200);


    }
  
}
