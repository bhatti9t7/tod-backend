<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Offer;
use App\User;
use App\Captain;
use Session;
use Auth;

class OfferController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }

    public function index()
    {
        $off = Offer::orderby('id', 'DESC')->get();
        //dd($captain);exit;
        return view('admin.offers', compact('off'));
    }

    public function create()
    {
        return view('admin.offer_create');
    }

    public function store(Request $request)
    {

        $validator = Validator::make(
            $request->all(),
            [
                'offer_name' => 'required',
                'time_period' => 'required',
                'rides' => 'required',
                'amount' => 'required',
                'start_date' => 'required',
                'offer_type' => 'required',
                'offer_expairy' => 'required',
                'description' => 'required'
            ]
        );
        if ($validator->fails()) {
            $message = $validator->errors()->first();
            Session::flash('info', " $message");

            return Redirect::back()->withErrors($validator);
        } else {
            $offer_affected = Offer::create([

                'offer_name'  => $request->offer_name,
                'time_period' => $request->time_period,
                'rides'       => $request->rides,
                'amount'         => $request->amount,
                'offer_type'  => $request->offer_type,
                'offer_expairy'   => $request->offer_expairy,
                'description' => $request->description,
                'start_date' => $request->start_date,
            ]);

            if ($offer_affected) {
                if ($request->offer_type == 'user_offer') {
                    $user = User::select('device_token')->get();

                    $tokens = array();
                    $index = 0;
                    foreach ($user as $users) {
                        if (isset($users->device_token) && !empty($users->device_token))
                            $tokens[$index++] = $users->device_token;
                    }

                    if (count($tokens) > 0) {


                        $message = array('message' => "$request->description", 'amount' => "$request->amount", 'offer_name' => "$request->offer_name", 'time_period' => "$request->time_period", 'rides' => "$request->rides", 'offer_expairy' => "$request->offer_expairy", 'type' => 'captain_offer');
                        $message_status = user_send_notification($tokens, $message, null);
                    }
                }

                // captain offers send notification
                else {
                    $captains = Captain::select('device_token')->where('activity', 1)->get();

                    $tokens = array();
                    $index = 0;
                    foreach ($captains as $captain) {
                        if (isset($captain->device_token) && !empty($captain->device_token))
                            $tokens[$index++] = $captain->device_token;
                    }

                    if (count($tokens) > 0) {

                        $message = array('message' => "$request->description", 'amount' => "$request->amount", 'offer_name' => "$request->offer_name", 'time_period' => "$request->time_period", 'rides' => "$request->rides", 'offer_expairy' => "$request->offer_expairy", 'type' => 'captain_offer');
                        $message_status = captain_send_notification($tokens, $message, null);
                    }
                }


                // end the captain offer notification section


                session()->flash('success', 'Record has been added. successfully.');
            } else {
                session()->flash('danger', 'Fail to update the record.');
            }
            return redirect()->route('offers.index');
        }
    }

    public function edit($id)
    {
        $offers = Offer::find($id);

        return view('admin.offerEdit', compact('offers'));
    }


    public function update(Request $request, $id)
    {
        $Datatypes = Offer::findOrFail($id);
        $input = $request->all();

        $good_affected = $Datatypes->update($input);

        if ($good_affected)
            session()->flash('success', 'Record has been added. successfully.');
        else
            session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('offers.index');
    }


    public function delete_record($id)
    {
        $datatyp = Offer::find($id);
        $good_destroy = $datatyp->delete();
        if ($good_destroy) {
            $response['status'] = true;
            $response['message'] = "Record has been delete Successfully.";
        } else {
            $response['status'] = false;
            $response['message'] = "Fail to delete Record.";
        }
        return redirect()->route('offers.index');
    }

    public function get_offers(Request $request)
    {


        $datatable = !empty($request->datatable) ? $request->datatable : array();
        $datatable = array_merge(array('pagination' => array(), 'sort' => array(), 'query' => array()), $datatable);

        $sort  = !empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'desc';
        $field = !empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'id';

        $page    = !empty($datatable['pagination']['page']) ? (int) $datatable['pagination']['page'] : 1;
        $perpage = !empty($datatable['pagination']['perpage']) ? (int) $datatable['pagination']['perpage'] : -1;

        $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch']) ? $datatable['query']['generalSearch'] : '';

        $pages = 1;
        $total = Offer::count();

        // $perpage 0; get all data
        if ($perpage > 0) {
            $pages  = ceil($total / $perpage); // calculate total pages
            $page   = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ($page - 1) * $perpage;
            if ($offset < 0) {
                $offset = 0;
            }
        }
        $users = Offer::offset($offset)->limit($perpage)->orderBy($field, $sort);

        if (!empty($filter)) {
            $user->where('offer_name', 'like', "%{$filter}%")
                ->orWhere('status', 'like', "%{$filter}%")
                ->orWhere('time_period', 'like', "%{$filter}%")
                ->orWhere('rides', 'like', "%{$filter}%")
                ->orWhere('offer_type', 'like', "%{$filter}%")
                ->orWhere('offer_expairy', 'like', "%{$filter}%");

            unset($datatable['query']['generalSearch']);
        }

        $data = $users->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                'sort'  => $sort,
                'field' => $field,
            ),
            'data' => $data
        );
        // print_r($result);exit();
        return  response()->json($result);
    }

    public function offerschange_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('status');
        $user = Offer::find($id);
        $user->status  = $status;
        $effected = $user->save();

        $response = array();
        if ($effected) {
            $response['status'] = true;
            $response['message'] = "Offer status has been changed Successfully.";
        } else {
            $response['status'] = false;
            $response['message'] = "Fail to change the Offer status.";
        }
        return response()->json($response, 200);
    }
}
