<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\CaptainWallet;
use App\Captain;
use App\UserWallet;
use App\MainWallet;
use DB;
use Auth;
use DataTables;
use Session;
use Excel;
class OutflowController extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth:admin']);
  }

  public function index()
  {
       // $wallet = CaptainWallet::with('captain')->orderby('id','DESC')->get();
       //dd($captain);exit;
    return view('admin.outflow' , compact('wallet'));
  }
  public function outflow_data(Request $request)
  {

    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
    $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

    $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
    $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

    $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
    $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

    $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

    $pages = 1;
    $total = CaptainWallet::count();

        // $perpage 0; get all data
    if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
              $offset = 0;
            }

          } 
          $users = CaptainWallet::with('captain')->offset($offset)->limit($perpage)->orderBy($field,$sort);

          if ( ! empty( $filter ) ) {
            $users->where('id', 'like', "%{$filter}%")
            ->orWhere('amount','like',"%{$filter}%")
            ->orWhere('debit_amount','like',"%{$filter}%")
            ->orWhere('credit_amount','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

          }

          $data=$users->get();


          $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
          );

          $result = array(
            'meta' => $meta + array(
              'sort'  => $sort,
              'field' => $field,
            ),
            'data' => $data
          );
          // print_r($result);exit();
          return  response()->json($result);

        }



        public function main_wallet()
        {

         $wallet = MainWallet::first();
         $captain = Captain::get();

         return view('admin.mainwallet' , compact('wallet', 'captain'));
       }

       public function store(Request $request)
       {

        $validator = Validator::make($request->all(),
          [
            'captain_id' => 'required',
            'collect_commission' => 'required', 
            'pending_commission' => 'required', 
          ]);

        if($validator->fails())
        {
          $message = $validator->errors()->first();
          Session::flash('info', " $message") ;
          return Redirect::back()->withErrors($validator);
        } 
        else
        {

          $Datatypes = MainWallet::findOrFail($request->id);
          $input = $request->all();

          DB::beginTransaction();
          $input['pending_commission'] = $request->pending_commission- $request->collect_commission;

          $wallet_affected = $Datatypes->update($input);

          if($wallet_affected)
          {

            $captainupdate =   CaptainWallet::where('captain_id', $request->captain_id)->first();

            $captain_debit_amount = ($captainupdate->debit_amount)-($request->collect_commission);
            $captain_credit_amount = ($captainupdate->credit_amount)+($request->collect_commission);

            $affected_captainwallet = $captainupdate->update(['debit_amount'=>$captain_debit_amount,'credit_amount'=>$captain_credit_amount]);
          }

          if($affected_captainwallet){
            DB::commit();
          }
          else{
            DB::rollBack();
          }

          if($wallet_affected)
            session()->flash('success', 'Record has been updated. successfully.');
          else
            session()->flash('danger', 'Fail to update the record.');
          return Redirect::back();
        }
      }


      public function show($id)
      {

        $wallet = CaptainWallet::findOrFail($request->id);

        return response()->json($wallet, 200);

      }
      public function downloadExcel($type)
      {

        $data = CaptainWallet::orderBy('id' , 'DESC')->with('captain')->get();

        return Excel::create('Captain Commission Record', function($excel) use ($data) {
          $excel->sheet('user', function($sheet) use($data) {
            $excelData = [];
            $excelData[] = [
              'Id',
              'Captain Name',
              'Total Transaction Amount',
              'Commisions',
              'Current Balance',

            ];

            foreach ($data as $key => $value) {
              $excelData[] = [
                $value->id,
                $value->captain->fname . $value->captain->lname,
                $value->amount .'.Rs',
                $value->debit_amount .'.Rs',
                $value->credit_amount . '.Rs',

              ];  

              $sheet->setTitle('Loader');

            }

        // Build the spreadsheet, passing in the payments array

        /////////////////////////////

            $sheet->fromArray($excelData, null, 'A1', true, false);

          });

        })->download($type);
      }






    }
