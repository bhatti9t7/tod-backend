<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use Auth;

class PermissionController extends Controller
{
     public function __construct() {
        $this->middleware(['auth:admin']);
    }

    public function index() {
        $permissions = Permission::all(); 

        return view('permissions.index')->with('permissions', $permissions);
    }

    
    public function create() {
        $roles = Role::get(); 
        return view('permissions.create')->with('roles', $roles);
    }

    
    public function store(Request $request) 
    {
        $this->validate($request, [
            'name'=>'required|max:40||unique:permissions',
        ]);

        $name = $request['name'];
        $permission = Permission::create(['guard_name' => 'admin','name' => $name]);
        $roles = $request['roles'];

        if (!empty($roles)) 
            $permission->syncRoles($roles);
    
    

        return redirect()->route('permissions.index')->with('flash_message', 'Permission'. $permission->name.' added!');

    }

    
    public function show($id) {
        return redirect('permissions');
    }

    
    public function edit($id) 
    {
        $permission = Permission::findOrFail($id);

        return view('permissions.edit', compact('permission'));
    }

    
    public function update(Request $request, $id) 
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:40|unique:permissions',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return redirect()->route('permissions.index')->with('flash_message', 'Permission'. $permission->name.' updated!');

    }

    
    public function destroy($id) {
        $permission = Permission::findOrFail($id);

        $permision_destroy = $permission->delete();

        if($permision_destroy){
        $response['status'] = true;
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return response()->json($response);

    }
}