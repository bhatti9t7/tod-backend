<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Ride;
use App\RidesCompleted;
use Illuminate\Support\Facades\Input;
use Session;
use DB;
use Excel;
class RidesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
    {
       $rides_complete = Ride::orderby('id' ,'DESC')->where('status', 0 )->with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->paginate(10);
       return view('admin.rides');

       
   }

   public function pendingride()
   {
      $pending = Ride::orderby('id' ,'DESC')->where('status','<>',0)->with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->paginate(10);
      $current = Ride::where('status','<>',0)->count();

      return view('admin.pendingrides' , compact('current' , 'pending'));
  }

  public function show($id){
    $ride_complete = RidesCompleted::where('ride_id' ,$id)->with('BookingDestination')->get();
    return view('admin.rideproof' , compact([ 'ride_complete']));
}

public function get_ride(Request $request){
          // print_r("expression");exit();
            // print_r("no data found");exit();
    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
    $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

    $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
    $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

    $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
    $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

    $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

    $pages = 1;
    $total = Ride::where('status', 0 )->count();


        // $perpage 0; get all data
    if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $airportOutbound = Ride::with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->where('status', 0 )->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
            $airportOutbound->where('id', 'like', "%{$filter}%")
            ->orWhere('created_at','like',"%{$filter}%");

            $airportOutbound->orWhereHas('user', function ($query) use ($filter){
                $query->where('fname', 'like', "%{$filter}%");
            });
            $airportOutbound->orWhereHas('captain', function ($query) use ($filter){
                $query->where('fname', 'like', "%{$filter}%");
            });
            $airportOutbound->orWhereHas('booking', function ($query) use ($filter){
                $query->where('amount', 'like', "%{$filter}%");
                $query->where('pickup_place', 'like', "%{$filter}%");
                $query->where('distance', 'like', "%{$filter}%");
            });




            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$airportOutbound->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                'sort'  => $sort,
                'field' => $field,
            ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);

    }
    /* End Ajax Calls*/





    public function get_pending(Request $request)
    {

     $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
     $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

     $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
     $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

     $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
     $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

     $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

     $pages = 1;
     $total = Ride::where('status','<>',0)->count();


        // $perpage 0; get all data
     if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $pending_ride =Ride::where('status','<>',0)->with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {

            $pending_ride->where('id', 'like', "%{$filter}%")
            ->orWhere('created_at','like',"%{$filter}%");

            $pending_ride->orWhereHas('user', function ($query) use ($filter){
                $query->where('fname', 'like', "%{$filter}%");
            });
            $pending_ride->orWhereHas('captain', function ($query) use ($filter){
                $query->where('fname', 'like', "%{$filter}%");
            });
            $pending_ride->orWhereHas('booking', function ($query) use ($filter){
                $query->where('amount', 'like', "%{$filter}%");
                $query->where('pickup_place', 'like', "%{$filter}%");
                $query->where('distance', 'like', "%{$filter}%");
            });




            unset( $datatable[ 'query' ][ 'generalSearch' ] );

        }

        $data=$pending_ride->get();


        $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
        );

        $result = array(
            'meta' => $meta + array(
                'sort'  => $sort,
                'field' => $field,
            ),
            'data' => $data
        );
          // print_r($result);exit();
        return  response()->json($result);


    }

    public function pending_ride_download($type)
    {

        $data = Ride::orderby('id' ,'DESC')->where('status','<>',0)->with("user")->with("captain")->with("booking")->with('booking.vehicleSubCategorie')->with('BookingDestination')->with("RideCompleted")->get();
        \Excel::create('Current Rides Record ', function($excel) use($data) {

            $excel->sheet('user', function($sheet) use($data) {
                $excelData = [];
                $excelData[] = [
                    'Id',
                    'User Name',
                    'Captain Name',
                    'Pickup Place',
                    'Distance',
                    'Vehicle Categories',
                    'Vehicle Type',
                    'Labour Loading',
                    'Labour Unloading',
                    'Date/time',
                    'Status',
                ];

                foreach ($data as $key => $value) {
                    $excelData[] = [
                        $value->id,
                        $value->user->fname . $value->user->lname,
                        $value->captain->fname . $value->captain->lname,
                        $value->booking->pickup_place,
                        $value->booking->distance .'Km',
                        $value->captain->VehicalCategorie->name,
                        $value->captain->VehicalSubCategorie->name,

                        $value->booking->labour_loader,
                        $value->booking->labour_unloader,
                        $value['created_at'],
                        $value['status']==0?'accepted':'pending',
                    ];  

                    $sheet->setTitle('Loader');

                }

        // Build the spreadsheet, passing in the payments array

        /////////////////////////////

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download($type);

    }
    public function complete_ride_download($type)
    {

        $data = Ride::orderby('id' ,'DESC')->where('status', 0 )->with("user")->with("captain")->with("booking")->with('booking.vehicleSubCategorie')->with('BookingDestination')->with("RideCompleted")->get();
        \Excel::create('Ride Complete Record ', function($excel) use($data) {

            $excel->sheet('user', function($sheet) use($data) {
                $excelData = [];
                $excelData[] = [
                    'Id',
                    'User Name',
                    'Captain Name',
                    'Pickup Place',
                    'Distance',
                    'Amount',
                    'Vehicle Categories',
                    'Vehicle Type',
                    'Labour Loading',
                    'Labour Unloading',
                    'Date/time',
                    'Status',
                ];

                foreach ($data as $key => $value) {
                    $excelData[] = [
                        $value->id,
                        $value->user->fname . $value->user->lname,
                        $value->captain->fname . $value->captain->lname,
                        $value->booking->pickup_place,
                        $value->booking->distance .'Km',
                        $value->booking->amount .'.Rs',
                        $value->captain->VehicalCategorie->name,
                        $value->captain->VehicalSubCategorie->name,

                        $value->booking->labour_loader,
                        $value->booking->labour_unloader,
                        $value['created_at'],
                        $value['status']==0?'accepted':'pending',
                    ];  

                    $sheet->setTitle('Loader');

                }

        // Build the spreadsheet, passing in the payments array

        /////////////////////////////

                $sheet->fromArray($excelData, null, 'A1', true, false);

            });

        })->download($type);

    }


}       
