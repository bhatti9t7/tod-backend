<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;
use Auth;
class RoleController extends Controller
{
     public function __construct() {
         $this->middleware(['auth:admin']);
    }

    
    public function index() {

        $roles = Role::all();//Get all roles

        return view('roles.index')->with('roles', $roles);
    }

    
    public function create() {
        $permissions = Permission::all();

        return view('roles.create', ['permissions'=>$permissions]);
    }

    
    public function store(Request $request) {
        $this->validate($request, [
            'name'=>'required|unique:roles|max:30'
            ]
        );

        $name = $request['name'];

        $role = Role::create(['guard_name' => 'admin','name' =>  $name]);

        $permissions = $request['permissions'];
        if(!empty($permissions))
        $role->syncPermissions($permissions);

        return redirect()->route('roles.index')->with('flash_message', 'Role'. $role->name.' added!'); 
    }

    
    public function show($id) {
        return redirect('roles');
    }

    
    public function edit($id) {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();

        return view('roles.edit', compact('role', 'permissions'));
    }

    
    public function update($id,Request $request) {

        $role = Role::findOrFail($id);
    
        $this->validate($request, [
            'name'=>'required|max:20'
        ]);



        $input = $request->except(['permissions']);

    
        $permissions = $request['permissions'];
        $role->fill($input)->save();

        $role_permissions = Permission::all();
        foreach ($role_permissions as $p) {
            if($role->hasPermissionTo($p))
            $role->revokePermissionTo($p); 
        }

        if(!empty($permissions))
        $role->syncPermissions($permissions); 

        return redirect()->route('roles.index')->with('flash_message', 'Role'. $role->name.' added!'); 
    }

    
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role_destroy = $role->delete();

         if($role_destroy){
        $response['status'] = true;
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return response()->json($response);
 
    }
}