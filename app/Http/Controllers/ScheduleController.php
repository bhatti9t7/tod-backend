<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\UserOffer;
use App\CaptainOffer;
use App\Offer;
use App\Ride;
use App\User;
use App\Captain;
class ScheduleController extends Controller
{
    public function weakly_offer(){
    	$offer = Offer::where('start_date' , date('Y-m-d'))->where('status' , 1)->where('time_period' , 'weakly')->get();
    	if($offer){
	    	foreach ($offer  as $value) {
	    		if( $value->offer_type == 'captain_offer'){
	    			$captains = DB::table('rides')->select('captain_id', DB::raw('count(*) as total'))->groupBy('captain_id')->get();
	    				if(count($captains) >= $value->rides){
			    			foreach ($captains as $cap ) {
			    				$captains= CaptainOffer::create([
			    					'captain_id' => $cap->id,
			    					'offer_id' => $value->id,
			    					'amount' => $value->amount,
			    				]);
			    			}
			    		}
	    		}
	    		else{
	    			$users = DB::table('rides')->select('user_id', DB::raw('count(*) as total'))->groupBy('user_id')->get();
	    			foreach ($users as $cap ) {
		    				if(count($captains) >1){
				    			foreach ($captains as $cap ) {
				    				$captains= UserOffer::create([
				    					'user_id' => $cap->id,
				    					'offer_id' => $value->id,
				    					'amount' => $value->amount,
			    					]);			    				
				    			}
				    		}
		    			}
	    		}
	    	}
	    }else{
	    	$offer = Offer::where('offer_expairy' , date('Y-m-d'))->get();
	    		foreach ($offer  as $value) {
	    		if( $value->offer_type == 'captain_offer'){
	    			$captains = DB::table('rides')->select('captain_id', DB::raw('count(*) as total'))->groupBy('captain_id')->get();
	    				if(count($captains) >= $value->rides){
			    			foreach ($captains as $cap ) {
			    				$data['status'] = 0;
			    				$captains= CaptainOffer::where('status', 1)->where('captain_id' , $cap->id)->update($data);
			    			}
			    		}
	    		}
	    		else{
	    			$users = DB::table('rides')->select('user_id', DB::raw('count(*) as total'))->groupBy('user_id')->get();
	    			foreach ($users as $cap ) {
		    				if(count($users) >1){
				    			foreach ($users as $cap ) {
				    				$data['status'] = 0;
				    				$captains= UserOffer::where('status', 1)->where('user_id' , $cap->id)->update($data);
				    						    				
				    			}
				    		}
		    			}
	    		}
	    	}
	    }


    	 return 'Record has been added/updated successfully';
    }

    public function yearly_offer(){
    	$offer = Offer::where('start_date' , date('Y-m-d'))->where('status' , 1)->where('time_period' , 'yearly')->get();
    	foreach ($offer  as $value) {
    		if( $value->offer_type == 'captain_offer'){
    			$captains = DB::table('rides')->select('captain_id', DB::raw('count(*) as total'))->groupBy('captain_id')->get();
    				if(count($captains) >= $value->rides){
		    			foreach ($captains as $cap ) {
		    				$captains= CaptainOffer::create([
		    					'captain_id' => $cap->id,
		    					'offer_id' => $value->id,
		    					'amount' => $value->amount,
		    				]);
		    			}
		    		}
    		}
    		else{
    			$users = DB::table('rides')->select('user_id', DB::raw('count(*) as total'))->groupBy('user_id')->get();
    			foreach ($users as $cap ) {
	    				if(count($users) >1){
			    			foreach ($users as $cap ) {
			    				$user= UserOffer::create([
			    					'user_id' => $cap->id,
			    					'offer_id' => $value->id,
			    					'amount' => $value->amount,
		    					]);			    				
			    			}
			    		}
	    			}
    		}
    	}

    	 return 'Record has been added/updated successfully';
    }

    public function monthly_offer(){
    	$offer = Offer::where('start_date' , date('Y-m-d'))->where('status' , 1)->where('time_period' , 'monthly')->get();
    	foreach ($offer  as $value) {
    		if( $value->offer_type == 'captain_offer'){
    			$captains = DB::table('rides')->select('captain_id', DB::raw('count(*) as total'))->groupBy('captain_id')->get();
    				if(count($captains) >= $value->rides){
		    			foreach ($captains as $cap ) {
		    				$captains= CaptainOffer::create([
		    					'captain_id' => $cap->id,
		    					'offer_id' => $value->id,
		    					'amount' => $value->amount,
		    				]);
		    			}
		    		}
    		}
    		else{
    			$users = DB::table('rides')->select('user_id', DB::raw('count(*) as total'))->groupBy('user_id')->get();
    			foreach ($users as $cap ) {
	    				if(count($users) >1){
			    			foreach ($users as $userss ) {
			    				$user= UserOffer::create([
			    					'user_id' => $userss->id,
			    					'offer_id' => $value->id,
			    					'amount' => $value->amount,
		    					]);			    				
			    			}
			    		}
	    			}
    		}
    	}

    	 return 'Record has been added/updated successfully';
    }

    public function custom_offer(){
    	$offer = Offer::where('start_date' , date('Y-m-d'))->where('status' , 1)->where('time_period', 'custom_offer')->get();
    	foreach ($offer  as $value) {
    		if( $value->offer_type == 'captain_offer'){
    			$captains = DB::table('rides')->select('captain_id', DB::raw('count(*) as total'))->groupBy('captain_id')->get();
    				if(count($captains) >= $value->rides){
		    			foreach ($captains as $cap ) {
		    				$captains= CaptainOffer::create([
		    					'captain_id' => $cap->id,
		    					'offer_id' => $value->id,
		    					'amount' => $value->amount,
		    				]);
		    			}
		    		}
    		}
    		else{
    			$users = DB::table('rides')->select('user_id', DB::raw('count(*) as total'))->groupBy('user_id')->get();
    			foreach ($users as $cap ) {
	    				if(count($users) >1){
			    			foreach ($users as $userss ) {
			    				$user= UserOffer::create([
			    					'user_id' => $userss->id,
			    					'offer_id' => $value->id,
			    					'amount' => $value->amount,
		    					]);			    				
			    			}
			    		}
	    			}
    		}
    	}

    	 return 'Record has been added/updated successfully';
    }


}
