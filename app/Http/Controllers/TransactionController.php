<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Transaction;
use App\MainWallet;
use Illuminate\Support\Facades\Input;
use Auth;
use Excel;
class TransactionController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    public function index()
    {
      $transaction = Transaction::with('captian')->with('user')->orderBy('id', 'DESC')->paginate(10);
		//dd($transaction);exit;
      return view('admin.transaction' , compact([ 'transaction']));
  }

  public function edit($id)
  {
      $transaction = Transaction::where('id' , $id)->with('captian')->with('user')->first();
		//dd($transaction);exit;
      return view('admin.transaction_edit' , compact([ 'transaction']));
  }

  public function update(Request $request, $id)
  {
    $Datatypes = Transaction::findOrFail($id);
    $input = $request->all();

    $Datatypes->update($input);
    return redirect()->route('inflow.index');
}


public function tracking_datas(Request $request)
{

    $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
    $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

    $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
    $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

    $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
    $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

    $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

    $pages = 1;
    $total = Transaction::count();

        // $perpage 0; get all data
    if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
                $offset = 0;
            }

        }
        $users = Transaction::with('captian')->with('user')->offset($offset)->limit($perpage)->orderBy($field,$sort);

        if ( ! empty( $filter ) ) {
         $user->where('amount', 'like', "%{$filter}%")
         ->orWhere('collect_amount','like',"%{$filter}%")
         ->orWhere('left_amount','like',"%{$filter}%")
         ->orWhere('commisions','like',"%{$filter}%")
         ->orWhere('commisions_amount','like',"%{$filter}%")
         ->orWhere('created_at','like',"%{$filter}%");

         unset( $datatable[ 'query' ][ 'generalSearch' ] );

     }

     $data=$users->get();


     $meta = array(
        'page'    => $page,
        'pages'   => $pages,
        'perpage' => $perpage,
        'total'   => $total,
    );

     $result = array(
        'meta' => $meta + array(
            'sort'  => $sort,
            'field' => $field,
        ),
        'data' => $data
    );
          // print_r($result);exit();
     return  response()->json($result);

 }

 public function downloadExcel($type)
 {

    $data = Transaction::orderBy('id' , 'DESC')->with('user')->with('captian')->get();

    return Excel::create('Transaction Record', function($excel) use ($data) {
        $excel->sheet('user', function($sheet) use($data) {
            $excelData = [];
            $excelData[] = [
                'Id',
                'User Name',
                'Captain Name',
                'Amount',
                'Collect Amount',
                'Left Amount',
                'Commisions Percentage',
                'Commission Amount',
                'Date/time',
                
            ];

            foreach ($data as $key => $value) {
                $excelData[] = [
                    $value->id,
                    $value->user->fname . $value->user->lname,
                    $value->captian->fname . $value->captian->lname,
                    $value->amount .'.Rs',
                    $value->collect_amount .'.Rs',
                    $value->left_amount . '.Rs',
                    $value->commisions . '%',
                    $value->commisions_amount . '.Rs' ,
                    $value['created_at'],

                ];  

                $sheet->setTitle('Loader');

            }

        // Build the spreadsheet, passing in the payments array

        /////////////////////////////

            $sheet->fromArray($excelData, null, 'A1', true, false);

        });

    })->download($type);
}

}
