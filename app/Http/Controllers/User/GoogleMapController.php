<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleMapController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:web');
    }
    
    public function index()
    {   
        return view('user.maps_vector');
    }
   
}
