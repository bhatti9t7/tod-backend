<?php

namespace App\Http\Controllers\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Ride;
use App\RidesCompleted;
use Auth;

class RidesController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:web']);
    }
    
        public function index()
        {
            $ride = Ride::where('user_id' , Auth::guard('web')->id())->where('status', 0 )->with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->orderBy('id', 'DESC')->paginate(10);
            $rides =Ride::count();
            return view('user.rides' , compact([ 'ride', 'rides']));
        }

        public function pendingride()
        {
        $ride = Ride::where('user_id' , Auth::guard('web')->id())->where('status','<>',0)->with("user")->with("captain")->with("booking")->with("advancebookings")->paginate(10);
        $rides =Ride::count();
        return view('user.pendingrides' , compact([ 'ride', 'rides']));
        }

        public function show($id){
            $ride_complete = RidesCompleted::where('ride_id' ,$id)->with('BookingDestination')->first();
            return view('user.rideproof' , compact([ 'ride_complete']));
        }
}
