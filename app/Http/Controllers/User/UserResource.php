<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\UserWallet;
use App\file;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Booking;

class UserResource extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    
    public function index()
    {
        $user= User::orderBy('id', 'DESC')->with('wallet')->where('id' , Auth::guard('web')->id())->paginate();
        
        $users =User::count();

        return view('user.user.userRosource' , compact(['user', 'users']));
    }

    
    public function create()
    {
       return view('user.user.page_user_profile');
    }

    
    public function store(Request $request)
    { 
        $validator = Validator::make($request->all(),
    [
     'fname' => 'required',
     'lname' => 'required',
     'email' => 'required|email|unique:users',
     'phone' => 'required',
     'password' => 'required|min:6|confirmed',
     
   
   ]);

       if($validator->fails())
       {
            return Redirect::back()->withErrors($validator);
       }
   else {
       $obj = new User;

        $profile = $request->file('images')->getClientOriginalName();

        if($request->hasfile('images')){
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
          $profile = 'user'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/user/profile/', $profile);
      }
       
      $obj->fname = request('fname');
      $obj->lname = request('lname');
      $obj->phone = request('phone');
      $obj->email = request('email');
      $obj->password =  Hash::make($request->password);
      $obj->images = $profile;
      $obj->api_token = str_random(60);
      $obj->save();
      return redirect()->route('userResources.index');
   }
}

    public function edit($id)
    {
        $User = User::find($id);
    
        return view('user.user.page_user_profile_edit', compact('User'));
    }

    
    public function update(Request $request, $id)
    {


         $User = User::findOrFail($id);

        $input = $request->all();

        if(empty($input['password']))
            unset($input['password']);

          if($request->hasfile('images')){
          $profiles = $request->file('images')->getClientOriginalName();
          $Extension_profile = $request->file('images')->getClientOriginalExtension();

          $profile = 'profiles'.'_'.date('YmdHis').'.'.$Extension_profile;
          
          $request->file('images')->move('images/user/profile/', $profile);
          $input['images'] =$profile;
          }
        $User->update($input);
        session()->flash('message', 'Successfully updated the admin');
        return redirect()->route('userResources.index');
    }

    
    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        return redirect()->route('userResources.index');
    }

     public function show($id)
    {

     $admin = User::find($id);


      return view('user.user.profile', compact('admin'));

    }
    public function walletShow($id)
    {
        //\DB::enableQueryLog();
        $wallet = UserWallet::with("user")->find($id);
        //dd(\DB::getQueryLog());
        
        return view('admin.user.userswallets' , compact('wallet'));
    }

    public function bookingdetail($id)
    {

      $users = Ride::where('booking_id' ,$id)->with('captain')->with('RideCompleted')->orderBy('id', 'DESC')->get();
   
      return view('user.user.historydetail', compact('users'));

    }

}
