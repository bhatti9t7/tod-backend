<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;


use App\Vehical;


class VehicalTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
    }
    
    public function index()
     { 

        $vehicals = Vehical::orderBy('id', 'DESC')->paginate(5);
        //$vehicals =vehicals::count();
       
        return view('user.vehicaltype', compact(['vehicals']));


     }
    public function create()
    {
        
    }

    
    public function store(Request $request)
    {
        Vehical::create([
            'vehical_name'  => $request['vehical_name'],
            'type_id'       => $request['type_id'],
            ]);
        return redirect('vehical');
    }

    
    public function show(vehicalType $vehicalType)
    {
        //
    }

    
    public function edit($id)
    {

         $vehicl = Vehical::find($id);
        return view( 'user.vehicalsEdit', compact('vehicl'));
       
    }

      
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $vehical = Vehical::findOrFail($id);
        

        $vehical->update($input);
        return redirect()->route('vehical.index');
    }

    
    public function destroy($id)
    {
        $vehical = Vehical::find($id);
    
     
       
        $vehical->delete();
        return Redirect::route('vehical.index');
    }
}
