<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Captain;
use App\Ride;
use App\Booking;
use App\file;
use DB;
use PDF;
use App\Transaction;

class UserReportController extends Controller
{
    public function __construct()
    {
       $this->middleware(['auth:admin']);
    }
  
    public function report( $id )
    {

          $captains = Ride::where('user_id' , $id)->with('captain')->with('booking')->with('user')->with('vehical')->get();
          $total_ride = Ride::where('user)id' , $id)->count();
          $detail = User::where('id', $id)->first();       
         $amount = Booking::whereHas('Rides',function($query) use ($id){
            $query->where('user_id', $id);
         })->sum('amount');
        
        $pdf = PDF::loadView('admin.report.userreportpdf', compact('captains' , 'total_ride' , 'amount' ,'detail'));
         return $pdf->download('userdetail.pdf');
 
    }
    public function index()
    {
        $captain = Captain::all();
        $user = User::all();
        return view('reports.userReport' , compact('user' , 'captain'));
    }

    public function filteruser(request $request)
    {
        $get = $request->todate;
        $user = Transaction::whereDate('created_at',  $get)->with('captian')->get();
// dd($user);exit();
        $pdf = PDF::loadView('reports.userpdf', compact('user'));
         return $pdf->download('userdetail.pdf');	
    }

     public function betwenreport(request $request)
    {

        $fromDate = $request['start_date'];
        $toDate = $request['last_date'];
         $user = Ride::whereBetween('created_at', ['2018-05-14', '2018-07-14'])->with('captain')->with('booking')->with('user')->with('vehical')->get();

        $pdf = PDF::loadView('reports.userpdf', compact('user'));
         return $pdf->download('userdetail.pdf');
      
    }
    
}
