<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\VehicalCategorie;
use App\VehicleSubCategorie;
use Session;
class VehicalCategoriesController extends Controller
{
  public function __construct()
  {
    $this->middleware(['auth:admin','clearance']);
  }

  public function index()
  {
         // $categories = VehicalCategorie::orderBy('id', 'DESC')->paginate(5);
         // $scategories = VehicleSubCategorie::orderBy('id', 'DESC')->with('vehiclecategorie')->paginate(5);
         // // dd($scategories);exit;
    return view('admin.VehicalCategorie');

  }


  public function create()
  {
    return view('admin.vehicleCategoriesCreate');
  }


  public function store(Request $request)
  {

    $validator = Validator::make($request->all(),
     [
      'name' => 'required|max:40||unique:vehical_categories',
      'weight' => 'required' ,
      'volume' => 'required' ,
      'image' => 'required',
    ]);

    if($validator->fails())
    {
     $message = $validator->errors()->first();
     Session::flash('info', " $message");
     return Redirect::back()->withErrors($validator);
   } 
   else
   {

    if($request->hasfile('image')){
      $image = $request->file('image')->getClientOriginalName();
      $Extension_profile = $request->file('image')->getClientOriginalExtension();
      $image = 'vehicle_categories'.'_'.date('YmdHis').'.'.$Extension_profile;
      $request->file('image')->move('images/vehicle_categories/', $image);
    }

    $vehical_affected = VehicalCategorie::create([
      'name' => $request->name,
      'weight' => $request->weight,
      'volume' => $request->volume,
      'base_fare' => $request->base_fare,
      'amount_per_km' => $request->amount_per_km,
      'destination_charge' => $request->destination_charge,
      'free_time' => $request->free_time,
      'loading_waiting_price' => $request->loading_waiting_price,
      'unloading_waiting_price' => $request->unloading_waiting_price,
      'image' => $image,

    ]);

    if($vehical_affected)
      session()->flash('success', 'Record has been added. successfully.');
    else
      session()->flash('danger', 'Fail to update the record.');

    return redirect()->route('vehical-categories.index');
  }

}
public function show(datatype $datatype)
{

}


public function edit($id)
{
 $Datatypes = VehicalCategorie::find($id);

 return view('admin.vehicleCategoriesEdit', compact('Datatypes'));

}


public function update(Request $request, $id)
{
  $Datatypes = VehicalCategorie::findOrFail($id);
  $input = $request->all();

  if($request->hasfile('image')){
    $profiles = $request->file('image')->getClientOriginalName();
    $Extension_profile = $request->file('image')->getClientOriginalExtension();
    $profile = 'vehicle_categories'.'_'.date('YmdHis').'.'.$Extension_profile;
    $request->file('image')->move('images/vehicle_categories/', $profile);
    $input['image'] =$profile;
  }

  $vehical_updateed = $Datatypes->update($input);

  if($vehical_updateed)
    session()->flash('success', 'Record has been updated. successfully.');
  else
    session()->flash('danger', 'Fail to update the record.');
  return redirect()->route('vehical-categories.index');
}


public function delete_record($id)
{
  $datatyp = VehicalCategorie::find($id);
  $vehical_destroy = $datatyp->delete();

  if($vehical_destroy){
    $response['status'] = true;
    session()->flash('success_message', 'Record has been Delete. successfully.');
    $response['message'] = "Record has been delete Successfully.";
  }else{
    $response['status'] = false; 
    $response['message'] = "Fail to delete Record.";
  }
  return redirect()->route('vehical-categories.index');

}   
public function get_cat_vehical(Request $request)
{


  $datatable = ! empty( $request->datatable ) ? $request->datatable : array();
  $datatable = array_merge( array( 'pagination' => array(), 'sort' => array(), 'query' => array() ), $datatable );

  $sort  = ! empty( $datatable[ 'sort' ][ 'sort' ] ) ? $datatable[ 'sort' ][ 'sort' ] : 'desc';
  $field = ! empty( $datatable[ 'sort' ][ 'field' ] ) ? $datatable[ 'sort' ][ 'field' ] : 'id';

  $page    = ! empty( $datatable[ 'pagination' ][ 'page' ] ) ? (int)$datatable[ 'pagination' ][ 'page' ] : 1;
  $perpage = ! empty( $datatable[ 'pagination' ][ 'perpage' ] ) ? (int)$datatable[ 'pagination' ][ 'perpage' ] : -1;

  $filter = isset( $datatable[ 'query' ][ 'generalSearch' ] ) && is_string( $datatable[ 'query' ][ 'generalSearch' ] ) ? $datatable[ 'query' ][ 'generalSearch' ] : '';

  $pages = 1;
  $total = VehicalCategorie::count();

        // $perpage 0; get all data
  if ( $perpage > 0 ) {
            $pages  = ceil( $total / $perpage ); // calculate total pages
            $page   = max( $page, 1 ); // get 1 page when $_REQUEST['page'] <= 0
            $page   = min( $page, $pages ); // get last page when $_REQUEST['page'] > $totalPages
            $offset = ( $page - 1 ) * $perpage;
            if ( $offset < 0 ) {
              $offset = 0;
            }

          }
          $users = VehicalCategorie::offset($offset)->limit($perpage)->orderBy($field,$sort);

          if ( ! empty( $filter ) ) {
            $user->where('name', 'like', "%{$filter}%")
            ->orWhere('weight','like',"%{$filter}%")
            ->orWhere('volume','like',"%{$filter}%")
            ->orWhere('free_time','like',"%{$filter}%")
            ->orWhere('loading_waiting_price','like',"%{$filter}%")
            ->orWhere('unloading_waiting_price','like',"%{$filter}%")
            ->orWhere('status','like',"%{$filter}%");

            unset( $datatable[ 'query' ][ 'generalSearch' ] );

          }

          $data=$users->get();


          $meta = array(
            'page'    => $page,
            'pages'   => $pages,
            'perpage' => $perpage,
            'total'   => $total,
          );

          $result = array(
            'meta' => $meta + array(
              'sort'  => $sort,
              'field' => $field,
            ),
            'data' => $data
          );
          // print_r($result);exit();
          return  response()->json($result);

        }

        public function categories_change_status(Request $request)
        {
          $id = $request->input('user_id');
          $status = $request->input('activity');
          $user = VehicalCategorie::find($id) ;
          $user->status  = $status;
          $effected=$user->save();

          $response = array();
          if($effected){
            $response['activity'] = true;
            $response['message']=" Status has been changed Successfully.";
          }else{
            $response['activity'] = false;
            $response['message']="Fail to change the user status.";
          }
          return response()->json($response, 200);


        }



      }






