<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\VehicalCategorie;
use App\VehicleSubCategorie;
use App\Vehical;
use Session;


class VehicalTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin']);
    }
    
    public function index()
     { 

      
        
       //  $vehicals = Vehical::orderBy('id', 'DESC')->with('categories')->paginate(5);
       //  //$vehicals =vehicals::count();
       // $categories = VehicalCategorie::orderBy('id', 'DESC')->paginate(5);
        return view('admin.vehicaltype', compact(['vehicals' ,'categories']));


     }
    public function create()
    {
        $categories = VehicleSubCategorie::orderBy('id', 'DESC')->get();
        
        return view( 'admin.vehicalsCreate', compact('categories'));
    }

    
    public function store(Request $request)
    {
  
         $validator = Validator::make($request->all(),
        [
        'vehical_name' => 'required',
        'vehical_sub_categorie_id' => 'required',
        'amount_per_km' => 'required',
        'destination_charge' => 'required',
   
        ]);

       if($validator->fails())
       {
          $message = $validator->errors()->first();
          Session::flash('info', " $message") ;
          return Redirect::back()->withErrors($validator);
       } 
       else
       {
        $vehical_added = Vehical::create([
            'vehical_name'  => $request->vehical_name,
            'vehical_sub_categorie_id' => $request->vehical_sub_categorie_id,
            'amount_per_km'  => $request->amount_per_km,
            'destination_charge' => $request->destination_charge,
            ]);

        if($vehical_added)
        session()->flash('success', 'Record has been added. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');

      return redirect()->route('vehicals.index');

        }
    }
    
    public function show(vehicalType $vehicalType)
    {
        //
    }

    
    public function edit($id)
    {

         $vehicl = Vehical::find($id);
         $categories = VehicleSubCategorie::orderBy('id', 'DESC')->get();
        return view( 'admin.vehicalsEdit', compact('vehicl' , 'categories'));
       
    }

      
    public function update(Request $request, $id)
    {
        $input = $request->all();
        // dd($input);exit;
        $vehical = Vehical::findOrFail($id);
        

       $vehical_updated = $vehical->update($input);

        if($vehical_updated)
        session()->flash('success', 'Record has been updated. successfully.');
        else
        session()->flash('danger', 'Fail to update the record.');
        return redirect()->route('vehicals.index');
    }

    
    public function destroy($id)
    {
        $vehical = Vehical::find($id);
  
        $vehical_destory = $vehical->delete();

        if($vehical_destory){
        $response['status'] = true;
        $response['message'] = "Record has been delete Successfully.";
        }else{
        $response['status'] = false; 
        $response['message'] = "Fail to delete Record.";
        }
        return response()->json($response);
        
    }

     public function get_vehical(Request $request)
    {

    
    $limit = $request->input('length');
    $start = $request->input('start');
    
    $start = $start?$start+1:$start;

    $search = $request->input('search.value');


    $order_column_no = $request->input('order.0.column');
    $order_dir = $request->input('order.0.dir');
    $order_column_name = $request->input("columns.$order_column_no.data");


      $user = Vehical::orderby('id' ,'DESC')->with('categories')->offset($start)->limit($limit)->orderBy($order_column_name,$order_dir);



      if(!empty($search)){

               $user->where('vehical_name', 'like', "%{$search}%")
               ->orWhere('amount_per_km','like',"%{$search}%");

              $user->orWhereHas('categories', function ($query) use ($search){
            $query->where('name', 'like', "%{$search}%");
                });

      }

      $data = $user->get();

      $totalFiltered = Vehical::count();

      $json_data = array(
      "draw"      => intval($request->input('draw')),
      "recordsTotal"  => count($data),
      "recordsFiltered" => intval($totalFiltered),
      "data"      => $data
    );
  
       return response()->json($json_data, 200);

    }

    public function vehical_change_status(Request $request)
    {
        $id = $request->input('user_id');
        $status = $request->input('status');
        $user = Vehical::find($id) ;
        $user->status  = $status;
        $effected=$user->save();
        
        $response = array();
        if($effected){
        $response['status'] = true;
        $response['message']="vehical sub categorie status has been changed Successfully.";
          }else{
        $response['status'] = false;
        $response['message']="Fail to change the vehical sub categorie status.";
          }
        return response()->json($response, 200);


    }



}




