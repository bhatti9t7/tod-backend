<?php

namespace App\Http\Controllers\captain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\Booking;
use Illuminate\Support\Facades\Input;

class BookingController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:captain');
    }
    public function index()
    {

        $book = Booking::orderBy('id', 'DESC')->with("user")->with("PaymentMethod")->with("good")->paginate(5);
       
        $books =Booking::count();
        return view('captain.bookings' , compact([ 'book', 'books']));
    }
}
