<?php

namespace App\Http\Controllers\captain;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Captain;
use App\Vehical;
use App\CaptainWallet;
use App\file;
use Auth;
use Illuminate\Support\Facades\Input;

class CaptainResource extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:captain');
    }

    public function index()
    {
        $captain = Captain::orderBy('id', 'DESC')->with('wallet')->where('id' , Auth::guard('captain')->id())->get();
        $captains =Captain::count();
        return view('captain.captain.captainRosource' , compact(['$captain', 'captain','$captains', 'captains']));
    }

    
    public function create()
    {
        return view('captain.captain.page_captain_profile');
    }

    
    public function store(Request $request)
     { 
        $validator = Validator::make($request->all(),[
     'fname' => 'required',
     'lname' => 'required',
     'phone' => 'required',
     
     'password' => 'required|min:6|confirmed',
   
   ]);
       if($validator->fails())
       {
            echo 'error';
       } else {
       $obj = new captain;
       ///captain image
      //  $Nam = base64_decode($request->file(['images'])->getClientOriginalName());
      //  $Name = $_FILES['images']['name'];
      //  $Tmp = $_FILES['images']['tmp_name'];
      //  // Driving License Image
      //   $drive = base64_decode($request->file(['driving_licenes_images'])->getClientOriginalName());
      //  $drive = $_FILES['driving_licenes_images']['name'];
      //  $Tmp = $_FILES['driving_licenes_images']['tmp_name'];
      //  ////CNIC Front Image
      // $cnic_front = base64_decode($request->file(['cnic_front_side_img'])->getClientOriginalName());
      //  $cnic = $_FILES['cnic_img']['name'];
      //  $Tmp = $_FILES['cnic_img']['tmp_name'];
      //  ///////CNIC Back Image
      
        
        $obj->fname = $request->fname;
        $obj->lname = input::post('lname');
        $obj->phone = request('phone');
        $obj->email = request('email');
        $obj->vehicle_registration = request('vehicle_registration');
        $obj->licence_no = request('licence_no');
        $obj->vehicle_no = request('vehicle_no');
        $obj->vehical_id = request('vehical_id');
        $obj->owner_city = request('owner_city');
        $obj->owner_name = request('owner_name');
        $obj->owner_fname = request('owner_fname');
        $obj->chesi_no = request('chesi_no');
        $obj->engin_no = request('engin_no');
      
       $obj->password = Hash::make('password');
       $obj->api_token = str_random(60);

       // // 
       // $obj->images = $Name;
       // // ////driving_licenes_images
       
       // $obj->driving_licenes_images = $drive_licenes;
       // // ////cnic_front_side_img
       //  move_uploaded_file($Tmp, 'images/captain/Cnic_front'.base64_encode($cnic_front));
       //  $obj->cnic_front_side_img = $cnicFrontImg;
       // // /////cnic_back_side_img
       //  move_uploaded_file($Tmp, 'images/captain/Cnic_back'.base64_encode($cnic_back));
       //  $obj->cnic_back_side_img = $cnicBackImg;
      
       $obj->save();
        return redirect('captainResource');
   }
}

   public function edit($id)
    {
        $vehicals = Vehical::all();
        $captain = Captain::find($id);
    
        return view('captain.captain.captain_edit', compact('captain' , 'vehicals'));
    }

    
    public function update(Request $request, $id)
    {

        $data = Captain::findOrFail($id);
        $input = $request->all();
       // $data->update($input);
       
        

        
        $Get_Licence = $request->file('driving_licenes_images');

        $Licence_SET_String = 0;
        $CNIC_SET_String = 0;
        $Rename_CNIC = array();
        $Rename_License = array();

     if ($Get_Doc = $request->file('images'))
    {
          $Get_Docs = $Get_Doc->getClientOriginalName();                        
          $Extension_profile = $request->file('images')->getClientOriginalExtension();
           $Get_Doc = 'profile'.'_'.date('YmdHis').'.'.$Extension_profile;
          $request->file('images')->move('images/captain/profiles/', $Get_Doc);
                           
     }  
     if($cnic_images = $request->file('cnic_images')){
         foreach ($cnic_images as $key => $value){
           $Extension[] = $value->getClientOriginalExtension();
           $Rename_CNIC[] = 'CNIC'.'_'.date('YmdHis').'_'.$key.'.'.$Extension[$key];    
           $value->move('images/captain/captain cnic', $Rename_CNIC[$key]);
     }
         $CNIC_SET_String = implode(',', $Rename_CNIC);
        
 }      
  if($request->hasfile('driving_licenes_images')){
         foreach ($Get_Licence as $key => $value) {
           $Extension[] = $value->getClientOriginalExtension();
           $Rename_License[] = 'license'.'_'.date('YmdHis').'_'.$key.'.'.$Extension[$key];
           $value->move('images/captain/captain license', $Rename_License[$key]);
         }

         $Licence_SET_String = implode(',', $Rename_License);
      
       }

       $input['cnic_images'] = $CNIC_SET_String;
       $input['images'] =$Get_Doc;
       $input['driving_licenes_images'] =$Licence_SET_String;

       $data->update($input); 
      session()->flash('message', 'Successfully updated the post');
      return redirect()->route('captain-Resources.index');

        
    }

    
    public function destroy($id)
    {
        $captains = Captain::find($id);
        $captains->delete();
        return Redirect::route('captain-Resources.index');
    }
    public function searchbar(Request $request)
    {

     $response = array();
     $response['status'] = false;
     $response['message']= "No, Record Found";
     $searching = $request['search'];

     $captains=Captain::where('name', 'like', '%'.$searching.'%')
                       ->orWhere('email', 'like', '%'.$searching.'%')
                       ->get();

     if($captains){
     $response['status'] = true;
     $response['message']= "Record Found";
     $response['captains']= $captains;
     }             
     return response()->json($response);    

    }

    public function show($id)
    {

      $admin = Captain::find($id);


      return view('captain.captain.profile', compact('admin'));

    }

    public function walletShow($id)
    {
        //DB::enableQueryLog();
        $wallet = CaptainWallet::with("captain")->find($id);
        //dd(\DB::getQueryLog());
        
        return view('captain.captain.captainwallets' , compact('wallet'));
    }

     public function get_captains_ajax()
    {
          $captains = Captain::orderBy('id', 'DESC');
          return datatables()->of($captains)->make(true);
       
    }

   
     
}
