<?php

namespace App\Http\Controllers\captain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GoogleMapController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:captain');
    }
    
    public function index()
    {   
        return view('captain.maps_vector');
    }
   
}
