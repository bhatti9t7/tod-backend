<?php

namespace App\Http\Controllers\captain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Ride;
use App\RidesCompleted;
use Auth;

class RidesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:captain');
    }
    
     public function index()
        {
            $ride = Ride::where('user_id' , Auth::guard('captain')->id())->where('status', 0 )->with("user")->with("captain")->with("booking")->with('BookingDestination')->with("RideCompleted")->orderBy('id', 'DESC')->paginate(10);
            $rides =Ride::count();
            return view('captain.rides' , compact([ 'ride', 'rides']));
        }

        public function pendingride()
        {
        $ride = Ride::where('user_id' , Auth::guard('captain')->id())->where('status','<>',0)->with("user")->with("captain")->with("booking")->with("advancebookings")->paginate(10);
        $rides =Ride::count();
        return view('captain.pendingrides' , compact([ 'ride', 'rides']));
        }

        public function show($id){
            $ride_complete = RidesCompleted::where('ride_id' ,$id)->with('BookingDestination')->first();
            return view('captain.rideproof' , compact([ 'ride_complete']));
        }
}
