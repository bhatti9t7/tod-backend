<?php

namespace App\Http\Controllers\captain;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\UserWallet;
use App\file;
use Illuminate\Support\Facades\Input;
use Auth;

class UserResource extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:captain');
    }
    
    public function index()
    {
        $user= User::with('wallet')->where('id' , Auth::guard('web')->id())->get();
        
        $users =User::count();

        return view('captain.captain.userRosource' , compact(['user', 'users']));
    }

    
    public function create()
    {
       return view('captain.captain.page_user_profile');
    }

    
    public function store(Request $request)
    { 
        $validator = Validator::make($request->all(),
    [
     'fname' => 'required',
     'lname' => 'required',
     'email' => 'required|email|unique:users',
     'phone' => 'required',
     'password' => 'required|min:6|confirmed',
   
   ]);

       if($validator->fails())
       {
            echo 'error';
       }
   else {
       $obj = new User;
       $Nam = base64_decode($request->file(['images'])->getClientOriginalName());
       $Name = base64_encode($Nam);
       $Tmp = $_FILES['images']['tmp_name'];
       $obj->fname = request('fname');
        $obj->lname = request('lname');
        $obj->phone = request('phone');
       $obj->email = request('email');
       $obj->password = Hash::make('password');
       move_uploaded_file($Tmp, 'images/user/'.base64_encode($Nam));
       $obj->api_token = str_random(60);
       $obj->images = $Name;
       $obj->save();
        return redirect('userResources');
   }
}

    public function edit($id)
    {
        $User = User::find($id);
    
        return view('captain.captain.page_user_profile_edit', compact('User'));
    }

    
    public function update(Request $request, $id)
    {


         $User = User::findOrFail($id);

        $input = $request->all();

        if(empty($input['password']))
            unset($input['password']);
        $User->update($input);
        return redirect()->route('userResources.index');
    }

    
    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        return Redirect::route('userResources');
    }

     public function show($id)
    {

      $users = User::find($id);


      return view('captain.captain.user_profile_Byid', compact('users'));

    }

    public function walletShow($id)
    {
        //\DB::enableQueryLog();
        $wallet = UserWallet::with("user")->find($id);
        //dd(\DB::getQueryLog());
        
        return view('captain.captain.userswallets' , compact('wallet'));
    }

    public function profile(request $request)
   {

    $user= User::where('id' , Auth::guard('web')->id())->first();

    return view('captain.userprofile' , compact(['user']));
   }

}
