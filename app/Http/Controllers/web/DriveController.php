<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriveController extends Controller
{
    public function index(){
    	return view('public.drive');
    }
    public function drivapp(){
    	return view('public.drive_app');
    }
    public function drive_requirement(){
    	return view('public.drive_requirement');
    }
    public function drive_safety(){
    	return view('public.drive_safety');
    }
}
