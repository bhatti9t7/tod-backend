<?php

namespace App\Http\Controllers\web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RideController extends Controller
{
    public function howitwork(){
    	return view('public.howitwork');
    }

    public function ride_safety(){
    	return view('public.ride_safety');
    }
}
