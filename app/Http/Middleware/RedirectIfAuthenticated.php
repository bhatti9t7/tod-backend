<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->ajax() || $request->wantsJson()) {
            return response(['status'=>false,'message'=>'Unauthorized.'], 401);
        }
        
        switch ($guard) 
        {
            case 'admin':
            if(Auth::guard($guard)->check()){
                return redirect()->route('admin.dashboard');
            }
            break;

            case 'web':
            if(Auth::guard($guard)->check()){
                return redirect()->route('home');
            }
            break;
            
            case 'captain':
            if(Auth::guard($guard)->check()){
                return redirect()->route('captain.dashboard');
            }
            break;

            case 'api':
            if(Auth::guard($guard)->check()){
                return response()->json(['status'=>true,'message' => 'User has been successfully login.'], 200);
            }
            break;

            case 'captainapi':
            if(Auth::guard($guard)->check()){
                return response()->json(['status'=>true,'message' => 'Captain has been successfully login.'], 200);
            }
            break;
            
            default:
            if (Auth::guard($guard)->check()) {
                return redirect()->route('home');
            }
            break;
        }
        return $next($request);
    }
}
