<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken  extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'admin/ride-data' ,'admin/pending_data' ,'admin/booking_data' ,'admin/pending_booking_data', 'admin/user_data', 'admin/user_status' ,'admin/captain_getsss','admin/change_status','admin/admin_data',
        'admin/admin_status' ,'admin/get_country', 'admin/country_status' , 'admin/get_citi' , 'admin/citi_status' ,'admin/get_branch' ,'admin/branch_status' , 'admin/get_franchise' ,'admin/franchise_status' , 'admin/get-activity-log' ,'admin/traction' ,'admin/outflow_data' , 'admin/get_offer' , 'admin/offer_change_status' ,'admin/get_sub_vehical', 'admin/categories_change_status' ,'admin/get_good' ,'admin/get_cat_vehical' ,'admin/get_emergencies' ,'admin/get_reasons' , 'admin/password/email' ,
    ];
}
