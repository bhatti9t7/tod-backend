<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BookingDestinationCollection extends ResourceCollection
{
    
    public function toArray($request)
    {
        return BookingDestinationResource::collection($this->collection);    
    }
}