<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
class BookingResource extends JsonResource
{

    public function toArray($request)
    {
        return [
                'id' => $this->id,
                'status' => $this->status!=null?$this->status:'',
                'pickup_place' => $this->pickup_place!=null?$this->pickup_place:'',
                'booking_date' => $this->booking_date!=null?$this->booking_date:'',
                'vehical_sub_id' => $this->vehical_sub_id!=null?$this->vehical_sub_id:'',
                'vehical_cat_id' => $this->vehical_cat_id!=null?$this->vehical_cat_id:'',
                'amount' => $this->amount!=null?$this->amount:'',
                'distance' => $this->distance!=null?$this->distance:'',  
                'created_at' => $this->created_at?$this->created_at:'',
                'toll_plaza' => $this->toll_plaza!=null?$this->toll_plaza:0,
                'serge' => $this->serge!=null?$this->serge:0,
                'serge_charge' => $this->serge_charge!=null?$this->serge_charge:'',
                'loading_charge' => $this->loading_charge!=null?$this->loading_charge:'',
                'unloading_charge' => $this->unloading_charge!=null?$this->unloading_charge:'',
                'PaymentMethod' => $this->PaymentMethod!=null?$this->PaymentMethod:'Cash',
                'distance_charge' => $this->distance_charge!=null?$this->distance_charge:0,

                'reject_booking_image' => URL('images/reject_booking_image/').($this->reject_booking_image!=null?'/'.$this->reject_booking_image:'/no-image.png') ,
                'booking_destination' =>BookingDestinationResource::collection($this->BookingDestination),
                'vehicleSubCategorie'=>new VehicleSubCategoriesResource($this->vehicleSubCategorie),
                'good'=>new GoodResource($this->Good),
                'rides' => RideBookingResource::collection($this->Rides),
                
                     

            ];

    }
}
