<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CaptainRateCollection extends ResourceCollection
{
    
    public function toArray($request)
    {
         return CaptainRateResource::collection($this->collection);
    }
}
