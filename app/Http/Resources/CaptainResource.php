<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
class CaptainResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_name' => $this->fname.' '.$this->lname,
            'images' => URL('images/captain/profiles/').($this->images!=null?'/'.$this->images:'/no-image.png') ,
            'vehicle_image' => URL('images/captain/vehicle_image/').($this->vehicle_image!=null?'/'.$this->vehicle_image:'/no-image.png') ,
            'vehicle_registration_image'=>$this->vehicle_registration_image?$this->vehicle_registration_image:URL('images/captain/vehicle_image/no-image.png'),
            'phone' => $this->phone!=null?$this->phone:'',
            'vehicle_name' => $this->vehicle_name!=null?$this->vehicle_name:'',
            'api_token' => $this->api_token!=null?$this->api_token:'',
            'longitude' => $this->longitude!=null?$this->longitude:'',
            'latitude' => $this->latitude!=null?$this->latitude:'',
            'is_status' => $this->isStatus(),
            'vehicle_no' => $this->vehicle_no!=null?$this->vehicle_no:'',
            'CaptainStats'=>new CaptainStateResource($this->CaptainStats),
            
        ];
       // return parent::toArray($request);
    }

     public function with($request)
    {
        return [
            'status' => true,
            'message' => "Your Captain Records"
        ];
    }
}
