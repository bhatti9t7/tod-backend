<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DataCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return DataResource::collection($this->collection);    
    }
}
