<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DatashowCollection extends ResourceCollection
{
   public function toArray($request)
    {
        return DatashowResource::collection($this->collection);    
    }
}
