<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DatashowResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
                
                'id' => $this->id,
                'booking_id' => $this->booking_id!=null?$this->booking_id:'',
                'total_ride_time' => $this->total_ride_time!=null?$this->total_ride_time:'',
                'loading_image' => URL('images/loading_image/').($this->loading_image!=null?'/'.$this->loading_image:'/no-image.png') ,
                'status' => $this->status!=null?$this->status:'',

                'RideCompleted' =>RidesCompletedResource::collection($this->RideCompleted),  
                'captain'=>new CaptainResource($this->Captain),
                'booking'=>new DataResource($this->Booking),

            ];

    }
}
