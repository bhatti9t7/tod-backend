<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GoodResource extends JsonResource
{
    
    public function toArray($request)
    {
      
        return [
            'id' => $this->id,
            'good_name' => $this->good_name!=null?$this->good_name:'' ,
            
        ];
    }
}
