<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MapBookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
                
                'id' => $this->id,
                'pickup_place' => $this->pickup_place!=null?$this->pickup_place:'',
                'status' => $this->status,
                'amount' => $this->amount,
                'distance' => $this->distance,
                'pickup_longitude' => $this->pickup_longitude!=null?$this->pickup_longitude:0.00,
                'pickup_latitude' => $this->pickup_latitude!=null?$this->pickup_latitude:0.00,               
                'vehical_sub_id' => $this->vehical_sub_id!=null?$this->vehical_sub_id:'',
                'vehical_cat_id' => $this->vehical_cat_id!=null?$this->vehical_cat_id:'',
                'vehicleSubCategorie'=>new VehicleSubCategoriesResource($this->vehicleSubCategorie),
                'booking_destination' =>BookingDestinationResource::collection($this->BookingDestination),
                'good'=>new GoodResource($this->Good),
               
            ];
    }
}
