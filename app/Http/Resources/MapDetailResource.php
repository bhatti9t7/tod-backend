<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MapDetailResource extends JsonResource
{
    public function toArray($request)
    {
        return [
                
                'id' => $this->id,
                'booking_id' => $this->booking_id!=null?$this->booking_id:'',
                'status' => $this->status,
                'captain'=>new CaptainResource($this->Captain),
                'booking'=>new MapBookingResource($this->Booking),
               
            ];
    }
}
