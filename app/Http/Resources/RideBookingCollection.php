<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RideBookingCollection extends ResourceCollection
{
    
    public function toArray($request)
    {
    	 
         return RideBookingResource::collection($this->collection); 
    }
}
