<?php

namespace App\Http\Resources;
//use App\Ride;
use Illuminate\Http\Resources\Json\JsonResource;

class RideBookingResource extends JsonResource
{
    
    public function toArray($request)
    {
       return [
                
                'id' => $this->id,
                'booking_id' => $this->booking_id,
                'status' => $this->status,
                'total_ride_time' => $this->total_ride_time!=null?$this->total_ride_time:0,
                'loading_image' => URL('images/loading_image/').($this->loading_image!=null?'/'.$this->loading_image:'/no-image.png') ,
                'captain'=>new CaptainResource($this->Captain),
                'user'=>new UserResource($this->User),
                'RideCompleted' =>RidesCompletedResource::collection($this->RideCompleted),
                'rating' => new UserRateResource($this->user_rate),
                'transaction' => new TransactionResource($this->transaction),
               
                
            ];
    }
}
