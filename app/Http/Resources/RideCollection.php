<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class RideCollection extends ResourceCollection
{
    
    public function toArray($request)
    {
        return RideResource::collection($this->collection);    
    }
}


