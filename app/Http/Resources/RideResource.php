<?php

namespace App\Http\Resources;
use Illuminate\Http\Resources\Json\JsonResource;

class RideResource extends JsonResource
{
        public function toArray($request)
    {
        return [
                
                'id' => $this->id,
                'booking_id' => $this->booking_id,
                'status' => $this->status,
                'total_ride_time' => $this->total_ride_time!=null?$this->total_ride_time:0,
                'RideCompleted' =>RidesCompletedResource::collection($this->RideCompleted),
                'captain'=>new CaptainResource($this->Captain),
                'user'=>new UserResource($this->User),
                'booking'=>new BookingRideResource($this->Booking),
                'rating' =>new CaptainRateResource($this->captain_rate),
                'transaction' => new TransactionResource($this->transaction),
             

            ];

    }
    
}