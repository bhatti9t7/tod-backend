<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RidesCompletedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {    

        return [
            'ride_id' => $this->ride_id,
            'destination_id'=>$this->destination_id!=null?$this->destination_id:'',
            'image' => URL('images/ride_complete/').($this->image!=null?'/'.$this->image:'/no-image.png') ,
            'map_image' => URL('images/ride_complete/').($this->map_image!=null?'/'.$this->map_image:'/no-image.png') ,
            'estimate_time' => $this->estimate_time!=null?$this->estimate_time:'' ,
            'complete_at' => $this->complete_at!=null?$this->complete_at:'',
            'unloading_waiting_time' => $this->unloading_waiting_time!=null?$this->unloading_waiting_time:0,

                       
        ];
    }
}
