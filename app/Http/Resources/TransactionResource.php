<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
          
            'amount' => $this->amount!=null?$this->amount:0 ,
            'collect_amount' => $this->collect_amount!=null?$this->collect_amount:0 ,
            'left_amount' => $this->left_amount!=null?$this->left_amount:0 ,
            'commisions' => $this->commisions!=null?$this->commisions:0 ,
            'commisions_amount' => $this->commisions_amount!=null?$this->commisions_amount:0 ,
            
            
        ];
    }
}
