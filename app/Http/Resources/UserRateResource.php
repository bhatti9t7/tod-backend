<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserRateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => $this->user_id,
            'ride_id' => $this->ride_id,
            'captain_id' => $this->captain_id,
            'rate' => $this->rate!=null?$this->rate:'',
            'feedback' => $this->feedback!=null?$this->feedback:'',
            
        ];
    }
}
