<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
   
    public function toArray($request)
    {
        return [
        	'id' => $this->id,
        	'full_name' => $this->fname.' '.$this->lname,
            'email' => $this->email!=null?$this->email:'' ,
            'images' => URL('images/user/profile/').($this->images!=null?'/'.$this->images:'/no-image.png') ,
            
        ];
    }
}
