<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserRideRejectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return [
                
                'id' => $this->id,
                'booking_id' => $this->booking_id,
                'ride_id' => $this->ride_id,
                'booking'=>new BookingRideResource($this->booking),
            ];
    }
}
