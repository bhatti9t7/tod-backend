<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VehicalResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
    

    return [
            'vehical_name' => $this->vehical_name!=null?$this->vehical_name:'' ,
            'booking_charge' => $this->booking_charge==null?0,
            
            
        ];
    }
}
