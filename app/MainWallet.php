<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainWallet extends Model
{
   protected $fillable = ['amount','collect_commission','pending_commission' ];
    protected $hidden = ['status'];
}
