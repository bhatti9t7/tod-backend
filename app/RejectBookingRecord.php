<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RejectBookingRecord extends Model
{
     protected $fillable = ['captain_id','booking_id', 'ride_id','status'];
}
