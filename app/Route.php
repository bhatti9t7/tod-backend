<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;
class Route extends Model
{
    protected $fillable = ['url','permission_id','method'];
    protected $hidden = ['status'];

    public function permission(){
        
        return $this->belongsTo(Permission::class);
    }
}
