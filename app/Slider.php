<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
class Slider extends Model
{
	 use LogsActivity;
    protected $fillable = ['slide_image' , 'slide_content'];
 	protected $hidden = [ 'status' ];

 	    protected static $logAttributes = ['slide_image' , 'slide_content'];
    protected static $logOnlyDirty = true;
}
