<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupBookingDestination extends Model
{
      protected $fillable = [
    	'sup_booking_id', 'longitude', 'latitude' ,'drop_place'
    ];
}
