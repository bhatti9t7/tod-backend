<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
class Transaction extends Model
{
      use LogsActivity;

    protected $fillable = ['statement' , 'user_id', 'captain_id' , 'collect_amount' , 'left_amount', 'amount' ,'type' ,'commisions','commisions_amount', 'ride_id'];
    protected $hidden = [ 'status'];

     protected static $logAttributes = ['statement','ride_id' , 'user_id', 'captain_id' , 'collect_amount' , 'left_amount', 'amount' ,'type' ,'commisions','commisions_amount'];
    protected static $logOnlyDirty = true;

	public function captian()
    {
        return $this->belongsTo(Captain::class, 'captain_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function ride(){
        return $this->belongsTo(Ride::class);
    }

    
}
