<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
	use Notifiable;
    use LogsActivity;

	
     protected $fillable = [
        'fname', 'lname','email', 'phone' , 'images','longitude', 'latitude','api_token','device_token','password',
        'phone_verified','email_verified','status' ,'timeout', 'timein'];

    protected static $logAttributes = ['fname', 'lname','email', 'phone' , 'images','status'];
    protected static $logOnlyDirty = true;

   
    protected $hidden = [
        'fb_token',
    ];

    public function wallet()
    {
    	return $this->hasOne(UserWallet::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }

    public function AdvanceBookings()
    {
        return $this->hasMany(AdvanceBooking::class);
    }

    public function Rides()
    {
        return $this->hasMany(Ride::class);
    }



    
}
