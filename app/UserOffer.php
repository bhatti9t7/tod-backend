<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOffer extends Model
{
     protected $fillable = ['user_id','offer_id' , 'amount' ,'offer_code' , 'isused'  , 'status' ];
}
