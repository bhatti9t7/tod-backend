<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRate extends Model
{
    protected $fillable = [
        'rate' ,'user_id' , 'captain_id', 'feedback', 'ride_id'
        ];

       public function rides(){
       	return $this->belongsTo(Ride::class , 'ride_id');
       }

}
