<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRejectRide extends Model
{
    protected $fillable = ['user_id','booking_id', 'ride_id','status'];

    public function booking(){
    	return $this->belongsTo(Booking::class , 'booking_id');
    }
    public function ride(){
    	return $this->belongsTo(Ride::class , 'ride_id');
    }
}
