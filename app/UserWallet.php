<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model
{
    protected $fillable = ['amount' , 'debit_amount' , 'credit_amount','user_id'];

    protected $hidden = ['user_status','user_id'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
