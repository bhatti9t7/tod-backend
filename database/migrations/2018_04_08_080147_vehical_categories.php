<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VehicalCategories extends Migration
{
   
    public function up()
    {
        Schema::create('vehical_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('weight')->nullable();
            $table->string('volume')->nullable();
            $table->boolean('status')->default(1);
            $table->string('base_fare')->nullable();

            $table->string('amount_per_km')->nullable();
            $table->double('destination_charge')->nullable();
            $table->string('image')->nullable();
            $table->string('free_time')->nullable();
            $table->string('loading_waiting_price')->nullable();
            $table->string('unloading_waiting_price')->nullable();

            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('vehical_categories');
        Schema::enableForeignKeyConstraints();
    }
}
