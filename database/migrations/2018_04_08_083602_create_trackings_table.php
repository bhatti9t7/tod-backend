<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('vehical_sub_id')->unsigned()->nullable();
            $table->foreign('vehical_sub_id')->references('id')->on('vehicle_sub_categories')->onDelete('set null')->onUpdate('cascade');

            $table->string('longitude');
            $table->string('latitude');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('trackings');
        Schema::enableForeignKeyConstraints();
    }
}

