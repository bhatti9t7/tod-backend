<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_bookings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');

            $table->integer('good_id')->unsigned()->nullable();
            $table->foreign('good_id')->references('id')->on('goods')->onDelete('set null')->onUpdate('cascade');

            $table->integer('vehical_sub_id')->unsigned()->nullable();
            $table->foreign('vehical_sub_id')->references('id')->on('vehicle_sub_categories')->onDelete('set null')->onUpdate('cascade');

            $table->string('pickup_longitude');
            $table->string('pickup_latitude');
            $table->string('pickup_place')->nullable();
            
            $table->boolean('status')->default(1);
            $table->datetime('advance_boooking_date');
            
            $table->double('amount')->nullable();
            $table->string('distance')->nullable();
            $table->string('total_captain')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('advance_bookings');
        Schema::enableForeignKeyConstraints();
    }
}
