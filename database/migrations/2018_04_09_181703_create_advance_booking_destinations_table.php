<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceBookingDestinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_booking_destinations', function (Blueprint $table) {
            $table->increments('id');

             $table->integer('advance_booking_id')->unsigned();
            $table->foreign('advance_booking_id')->references('id')->on('advance_bookings')->onDelete('cascade')->onUpdate('cascade');

            $table->string('longitude');
            $table->string('latitude');
            $table->string('drop_place')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('advance_booking_destinations');
        Schema::enableForeignKeyConstraints();
    }
}
