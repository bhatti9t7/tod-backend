<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaptainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('captains', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('captain_cnic')->nullable();
            $table->string('password')->nullable();
            $table->string('remarks_captain')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();
            $table->integer('activity')->default(1);
            $table->string('images')->default('no-image.png')->nullable();
            $table->string('driving_licenes_images')->nullable();
            $table->string('vehicle_registration')->nullable();
            $table->string('cnic_images')->nullable();
            $table->string('licence_no')->nullable();
            $table->string('vehicle_no')->nullable();
            $table->integer('vehical_cat_id')->unsigned()->nullable();
            $table->foreign('vehical_cat_id')->references('id')->on('vehical_categories')->onDelete('set null')->onUpdate('cascade');

            $table->integer('vehical_sub_id')->unsigned()->nullable();
            $table->foreign('vehical_sub_id')->references('id')->on('vehicle_sub_categories')->onDelete('set null')->onUpdate('cascade');
            
            $table->string('vehicle_image')->nullable();
            $table->string('owner_cnic')->nullable();
            $table->string('owner_image')->nullable();
            $table->string('bank')->nullable();
            $table->string('branch_code')->nullable();
            $table->string('account_no')->nullable();
            $table->string('remarks_vehicle')->nullable();
            $table->string('owner_city')->nullable();
            $table->string('owner_name')->nullable();
            $table->string('owner_fname')->nullable();
            $table->string('chesi_no')->nullable();
            $table->string('engin_no')->nullable();
            $table->string('api_token')->unique()->nullable();
            $table->string('device_token')->nullable();
            $table->string('phone_verified')->nullable()->default(0);
            $table->string('email_verified')->nullable()->default(0);
            $table->boolean('status')->default(0);
            $table->string('city')->nullable();
            $table->string('remarks_owner')->nullable();
            $table->string('fb_token')->nullable();
            $table->string('created_by')->nullable();
            $table->string('labour_unloader')->nullable()->default(0);
            $table->string('labour_loader')->nullable()->default(0);
            $table->string('vehicle_registration_image')->nullable();
            $table->string('description')->nullable();
            $table->string('vehicle_name')->nullable();
            $table->rememberToken();
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('captains');
        Schema::enableForeignKeyConstraints();
    }
}
