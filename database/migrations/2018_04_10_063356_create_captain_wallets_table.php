<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaptainWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('captain_wallets', function (Blueprint $table) {
            $table->increments('id');

             $table->integer('captain_id')->unsigned();
            $table->foreign('captain_id')->references('id')->on('captains')->onDelete('cascade')->onUpdate('cascade');
            $table->double('amount')->default(0);
            $table->double('debit_amount')->nullable();
            $table->double('credit_amount')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('captain_wallets');
        Schema::enableForeignKeyConstraints();
    }
}
