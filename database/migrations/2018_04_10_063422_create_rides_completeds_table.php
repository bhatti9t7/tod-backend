<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesCompletedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides_completeds', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('ride_id')->unsigned();
            $table->foreign('ride_id')->references('id')->on('rides')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('destination_id')->unsigned()->nullable();
            $table->foreign('destination_id')->references('id')->on('booking_destinations')->onDelete('cascade')->onUpdate('cascade');

             $table->integer('skip_reasons_id')->unsigned()->nullable();
            $table->foreign('skip_reasons_id')->references('id')->on('skip_reasons')->onDelete('cascade')->onUpdate('cascade');
            

            $table->string('image')->nullable();
            $table->string('map_image')->nullable();
            $table->string('estimate_time')->nullable();
            $table->date('complete_at');
            $table->boolean('status')->default(1);
             $table->string('unloading_waiting_time')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('rides_completeds');
        Schema::enableForeignKeyConstraints();
    }
}
