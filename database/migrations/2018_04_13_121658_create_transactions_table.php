<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('statement');
            $table->double('amount')->nullable();
            $table->integer('type')->nullable();
            

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('captain_id')->unsigned();
            $table->foreign('captain_id')->references('id')->on('captains')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('ride_id')->unsigned();
            $table->foreign('ride_id')->references('id')->on('rides')->onDelete('cascade')->onUpdate('cascade');

            $table->double('collect_amount')->nullable();
            $table->double('left_amount')->nullable();

            $table->double('commisions')->nullable();
            $table->double('commisions_amount')->nullable();

            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
