<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
CREATE TRIGGER `captain_stats_trigger`
AFTER
  INSERT ON `rates` FOR EACH ROW BEGIN 
   DECLARE _users integer;
   DECLARE _rating double;
   DECLARE _hasStats integer;
select
  rating,
  users INTO _rating,
  _users
from
  captain_stats
where
  captain_id = new.captain_id;
SELECT
  COUNT(*) into _hasStats
FROM
  captain_stats
where
  captain_id = new.captain_id;IF(_hasStats > 0) THEN
SET
  _users = _users + 1;
SET
  _rating = _rating + new.rate;
UPDATE
  captain_stats
set
  rating = _rating,
  users = _users
where
  captain_id = new.captain_id;ELSE
SET
  _users = 1;
SET
  _rating = new.rate;
INSERT INTO
  captain_stats(captain_id, rating, users)
VALUES(new.captain_id, _rating, _users);

END IF;
END;
'
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER captain_stats_trigger');
    }
}
