<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financials', function (Blueprint $table) {
            $table->increments('id');
       
            $table->string('commisions')->nullable();
            $table->double('wait_amount')->nullable();
            $table->double('Serg_fee')->nullable();
            $table->double('labour_fare')->nullable();
            $table->double('destination_charge')->nullable();
            $table->double('reject_booking_amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financials');
    }
}