<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHelpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('helps', function (Blueprint $table) {
            $table->increments('id');
              $table->string('complaint_type');
            $table->string('complaint_description')->nullable();
            $table->string('additional_detail')->nullable();
            $table->string('captain_refuse_desc')->nullable();
            $table->string('destination_desc')->nullable();
            $table->string('behavior_detail')->nullable();
            $table->string('vehical_detail')->nullable();
            $table->string('license_detail')->nullable();
            $table->string('person_detail')->nullable();
            $table->string('passanger_detail')->nullable();
            $table->string('profile_detail')->nullable();
            $table->string('mention_captain')->nullable();
            $table->string('addition_amount')->nullable();
            $table->string('parking_location')->nullable();
            $table->string('parking_amount')->nullable();
            $table->string('toll_charge')->nullable();
            $table->string('date_accident')->nullable();
            $table->string('time_accident')->nullable();
            $table->string('location_accident')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('helps');
    }
}
