<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersStatsTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         {
        DB::unprepared('
CREATE TRIGGER `users_stats_trigger`
AFTER
  INSERT ON `user_rates` FOR EACH ROW BEGIN 
   DECLARE _captains integer;
   DECLARE _rating double;
   DECLARE _hasStats integer;
select
  rating,
  captains INTO _rating,
  _captains
from
  user_stats
where
  user_id = new.user_id;
SELECT
  COUNT(*) into _hasStats
FROM
  user_stats
where
  user_id = new.user_id;IF(_hasStats > 0) THEN
SET
  _captains = _captains + 1;
SET
  _rating = _rating + new.rate;
UPDATE
  user_stats
set
  rating = _rating,
  captains = _captains
where
  user_id = new.user_id;ELSE
SET
  _captains = 1;
SET
  _rating = new.rate;
INSERT INTO
  user_stats(user_id, rating, captains)
VALUES(new.user_id, _rating, _captains);

END IF;
END;
'
        );
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER users_stats_trigger');
    }
}
