<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('v_type')->nullable();
            $table->string('v_no')->nullable();
            $table->string('recpno')->nullable();
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->string('amount')->nullable();
            $table->string('amt_type')->nullable();
            $table->string('goes_to')->nullable();
            $table->string('chqno')->nullable();
            $table->string('created_by')->nullable();
            $table->string('edit_by')->nullable();
            $table->string('posting_date')->nullable();
            $table->string('from_cd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masters');
    }
}
