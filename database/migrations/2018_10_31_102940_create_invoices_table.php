<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('date')->nullable();
            $table->string('v_no')->nullable();
            $table->string('recpno')->nullable();
            $table->string('code')->nullable();
            $table->string('description')->nullable();
            $table->string('payable_cd')->nullable();
            $table->string('payable_amount')->nullable();
            $table->string('reciveable_amount')->nullable();
            $table->string('chqno')->nullable();
            $table->string('created_by')->nullable();
            $table->string('edit_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
