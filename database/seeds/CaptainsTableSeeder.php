<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class CaptainsTableSeeder extends Seeder
{
   
    public function run()
    {
        DB::table('captains')->delete();
        DB::table('captains')->insert(array(
                'fname' => 'Marcus',
                'lname' => 'Finley',
                'email' => 'info@aktechzone.com',
                'phone' => '+923076140561',
                'api_token' => str_random(60),
                'vehicle_no' => 'LWL 4455',
                'password' => Hash::make('loader123'),
                'created_at' => now(),
            )
        );
        DB::table('captains')->insert(array(
                'fname' => 'naveed',
                'lname' => 'shahzad',
                'email' => 'naveed@shahzad.com',
                'phone' => '+923456140561',
                'api_token' => str_random(60),
                'vehicle_no' => 'LWU 4495',
                'password' => Hash::make('pakistan'),
                'created_at' => now(),
            )
        );
        DB::table('captains')->insert(array(
                'fname' => 'yousaf',
                'lname' => 'khaild',
                'email' => 'yousaf@khaild.com',
                'phone' => '+923156140561',
                'api_token' => str_random(60),
                'vehicle_no' => 'LWP 4055',
                'password' => Hash::make('punj@b'),
                'created_at' => now(),
            )
        );
         DB::table('captains')->insert(array(
                'fname' => 'kashif',
                'lname' => 'ali',
                'email' => 'kashif@gmail.com',
                'phone' => '+923086140571',
                'api_token' => str_random(60),
                'vehicle_no' => 'LIL 4485',
                'password' => Hash::make('h@lloo'),
                'created_at' => now(),
            )
        );
         DB::table('captains')->insert(array(
                'fname' => 'junaid',
                'lname' => 'yousaf',
                'email' => 'junaid@gmail.com',
                'phone' => '+923076140563',
                'api_token' => str_random(60),
                'vehicle_no' => 'LEL 4355',
                'password' => Hash::make('123asd'),
                'created_at' => now(),
            )
        );

    }
}
