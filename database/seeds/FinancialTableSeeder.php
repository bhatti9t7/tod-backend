<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class FinancialTableSeeder extends Seeder
{
    
    public function run()
    {
       	DB::table('financials')->delete();
        DB::table('financials')->insert(array(
                'commisions' => '20',
                'wait_amount' => '15',
                'Serg_fee' => '1.1',
                'labour_fare' => '45',
                'destination_charge' => '25',
            )
        );
    }
}
