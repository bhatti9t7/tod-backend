<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Good;
class GoodsTableSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('goods')->delete();
        Good::create(['good_name'=>'Furniture','status'=>1]);
        Good::create(['good_name'=>'Fruits','status'=>1]);
        Good::create(['good_name'=>'Inferior','status'=>1]);
        Good::create(['good_name'=>'Luxury','status'=>1]);
        Good::create(['good_name'=>'Normal','status'=>1]);
        Good::create(['good_name'=>'Vegitables','status'=>1]);

    }
}
