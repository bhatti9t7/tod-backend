<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use App\Route;
class PermisionsTableSeeder extends Seeder
{
    
    public function run()
    {
         DB::table('permissions')->delete();

         $permission = Permission::create(['name'=>'Good_Categories index','guard_name' =>'admin']);
         Route::create(['url'=>'admin/good-categories' , 'permission_id' =>$permission->id, 'method'=> 'get']);

         $permission = Permission::create(['name'=>'Good_Categories Add' ,'guard_name' =>'admin' ]);
         Route::create(['url'=>'admin/good-categories/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/good-categories/store','permission_id' =>$permission->id, 'method'=> 'post']);
         $permission = Permission::create(['name'=>'Good_Categories Edit' ,'guard_name' =>'admin']);
         Route::create(['url'=>'admin/good-categories/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
         $permission = Permission::create(['name'=>'Good_Categories update' , 'guard_name' =>'admin']);
         Route::create(['url'=>'admin/good-categories/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
         $permission = Permission::create(['name'=>'Good_Categories delete' ,'guard_name' =>'admin']);
         Route::create(['url'=>'admin/good-categories/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);



        $permission = Permission::create(['name'=>'Goods index','guard_name' =>'admin']);
        Route::create(['url'=>'admin/goods' , 'permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'Goods Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/goods/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
        Route::create(['url'=>'admin/goods/store','permission_id' =>$permission->id, 'method'=> 'post']);

        $permission = Permission::create(['name'=>'Goods Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/goods/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'Goods update','guard_name' =>'admin']);
        Route::create(['url'=>'admin/goods/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);

        $permission = Permission::create(['name'=>'Goods delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/goods/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);


        $permission = Permission::create(['name'=>'Vehicle_Categories index','guard_name' =>'admin']);
        Route::create(['url'=>'Vehicle_Categories' , 'permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'Vehicle_Categories Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/Vehicle_Categories/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/Vehicle_Categories/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Vehicle_Categories Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/Vehicle_Categories/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'Vehicle_Categories update','guard_name' =>'admin']);
        Route::create(['url'=>'admin/Vehicle_Categories/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Vehicle_Categories delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/Vehicle_Categories/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);


        $permission = Permission::create(['name'=>'Vehicle index','guard_name' =>'admin']);
        Route::create(['url'=>'Vehicle' , 'permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'Vehicle Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/Vehicle/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/Vehicle/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Vehicle Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/Vehicle/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'Vehicle update','guard_name' =>'admin']);
        Route::create(['url'=>'admin/Vehicle/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Vehicle delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/Vehicle/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);


        $permission = Permission::create(['name'=>'Captain index','guard_name' =>'admin']);
        Route::create(['url'=>'captainResource' , 'permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'Captain Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/captainResource/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/captainResource/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Captain Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/captainResource/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'Captain update','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/captainResource/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Captain delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/captainResource/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);


        $permission = Permission::create(['name'=>'User index','guard_name' =>'admin']);
        Route::create(['url'=>'userResource' , 'permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'User Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/userResource/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/userResource/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'User Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/userResource/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'User update','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/userResource/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'User delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/userResource/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);


        $permission = Permission::create(['name'=>'roles index','guard_name' =>'admin']);
        Route::create(['url'=>'roles' , 'permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'roles Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/roles/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/roles/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'roles Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/roles/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'roles update','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/roles/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'roles delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/userResource/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);


        $permission = Permission::create(['name'=>'permissions index','guard_name' =>'admin']);
        Route::create(['url'=>'permissions' , 'permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'permissions Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/permissions/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/permissions/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'permissions Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/permissions/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'permissions update','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/permissions/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'permissions delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/userResource/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);
        

        $permission = Permission::create(['name'=>'financial index','guard_name' =>'admin']);
        Route::create(['url'=>'financial' , 'permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'financial Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/financial/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/financial/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'financial Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/financial/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'financial update','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/financial/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'financial delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/userResource/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);

        
        $permission = Permission::create(['name'=>'Admin index','guard_name' =>'admin']);
        Route::create(['url'=>'adminResource' , 'permission_id' =>$permission->id, 'method'=> 'get']);

        $permission = Permission::create(['name'=>'Admin Add','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/adminResource/create' , 'permission_id' =>$permission->id, 'method'=> 'get']);
         Route::create(['url'=>'admin/adminResource/store','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Admin Edit','guard_name' =>'admin']);
        Route::create(['url'=>'admin/adminResource/*/edit','permission_id' =>$permission->id, 'method'=> 'get']);
        $permission = Permission::create(['name'=>'Admin update','guard_name' =>'admin' ]);
        Route::create(['url'=>'admin/adminResource/*/edit','permission_id' =>$permission->id, 'method'=> 'post']);
        $permission = Permission::create(['name'=>'Admin delete','guard_name' =>'admin']);
        Route::create(['url'=>'admin/adminResource/*/destory','permission_id' =>$permission->id, 'method'=> 'delete']);
    }
}
