<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert(array(
                'fname' => 'Zubair',
                'lname' => 'Finley',
                'email' => 'info@aktechzone.com',
                'phone' => '+923076140581',
                'api_token' => str_random(60),
                'password' => Hash::make('loader123'),
                'created_at' => now(),
            )
        );
        DB::table('users')->insert(array(
                'fname' => 'ahmad',
                'lname' => 'shahzad',
                'email' => 'naveed@ahmad.com',
                'phone' => '+923456140588',
                'api_token' => str_random(60),
                'password' => Hash::make('pakistan'),
                'created_at' => now(),
            )
        );
        DB::table('users')->insert(array(
                'fname' => 'khaild',
                'lname' => 'ahmad',
                'email' => 'khaild@hotmain.com',
                'phone' => '+923156140123',
                'api_token' => str_random(60),
                'password' => Hash::make('punj@b'),
                'created_at' => now(),
            )
        );
         DB::table('users')->insert(array(
                'fname' => 'kashi',
                'lname' => 'ali',
                'email' => 'kashi@gmail.com',
                'phone' => '+923086140571',
                'api_token' => str_random(60),
                'password' => Hash::make('h@loHole'),
                'created_at' => now(),
            )
        );
         DB::table('users')->insert(array(
                'fname' => 'irfan',
                'lname' => 'irafan',
                'email' => 'irfan@gmail.com',
                'phone' => '+923076112363',
                'api_token' => str_random(60),
                'password' => Hash::make('123asif'),
                'created_at' => now(),
            )
        );

    }
}
