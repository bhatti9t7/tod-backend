<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    
    public function run()
    {
    	DB::table('roles')->delete();
        DB::table('roles')->insert(array('name' => 'super admin', 'guard_name' => 'admin'));
        
    }
}
