$(document).ready(function(){

    bind_check_uncheck();
});

function bind_check_uncheck() {
    $("#check_all").click(function () {
        if ($("#check_all").is(':checked')) {
            $("input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
            });

        } else {
            $("input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
}

function bulk_delete(controller)
{
    var check_array = [];
    $('.sub_checks').each(function(index){
        if($(this).is(":checked"))
        {
            check_array[index] = $(this).val();
        }
    });
    if(check_array.length < 1 )
    {
        swal({
            title: 'Select '+controller.replace('_', ' ')+' to Active/Inactive.',
            type: 'warning',
            timer: 10000
        });
    }
    else
    {
        swal({
            title: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm',
            closeOnConfirm: false
        }, function() {
            $('.sub_checks').parents('form:first').submit();
        });
    }
}

function delete_record(obj)
{
    var url = $(obj).attr('href');
    swal({
      title: "Are you sure?",
     
      icon: "warning",
      buttons: true,
      dangerMode: true,
  })
    .then((willDelete) => {
      if (willDelete) {
         window.location.replace(url);
    } else {
        swal("Your record safe");
    }
});
}

function delete_record_dt(obj, controller)
{
    var url = $(obj).attr('href');
    swal({
        title: 'Are you sure?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Confirm',
        closeOnConfirm: false
    }, function() {
        window.location.replace(url);
    });
}

function ucwords(str) {
    return (str + '')
    .replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
        return $1.toUpperCase();
    });
}

function remove_spaces_from_html(html_str)
{
    return html_str.replace(/\s+/g, " ");
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}