
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">

	<!--[html-partial:include:{"file":"partials\/_header-base.html"}]/-->
@section('header-base')
	@include('_layouts.admin.partials._header-base')
@show
				<!-- begin::Body -->
	<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

		<!--[html-partial:include:{"file":"partials\/_aside-left.html"}]/-->
		@section('aside-left')
			@include('_layouts.admin.partials._aside-left')
		@show

		<div class="m-grid__item m-grid__item--fluid m-wrapper">

			<!--[html-partial:include:{"file":"partials\/_subheader-default.html"}]/-->
			@section('subheader-default')
				@include('_layouts.admin.partials._subheader-default')
			@show

			<div class="m-content">
				@yield('content')

			</div>
			@section('footer-default')
		@include('_layouts.admin.partials._footer-default')
	@show


		</div>
	</div>
	<!-- end:: Body -->				

	<!--[html-partial:include:{"file":"partials\/_footer-default.html"}]/-->
	

</div>
</div>
<!-- end:: Page -->    		        
<!--[html-partial:include:{"file":"partials\/_layout-quick-sidebar.html"}]/-->
@section('layout-quick-sidebar')
	@include('_layouts.admin.partials._layout-quick-sidebar')
@show

<!--[html-partial:include:{"file":"partials\/_layout-scroll-top.html"}]/-->
@section('layout-scroll-top')
	@include('_layouts.admin.partials._layout-scroll-top')
@show

<!--[html-partial:include:{"file":"partials\/_layout-tooltips.html"}]/-->

