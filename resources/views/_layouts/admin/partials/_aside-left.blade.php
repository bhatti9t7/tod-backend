<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
	<i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">
	<!-- BEGIN: Aside Menu -->
	<div 		id="m_ver_menu" 		class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light " 		m-menu-vertical="1"		 m-menu-scrollable="1" m-menu-dropdown-timeout="500"  		style="position: relative;">
		<ul class="m-menu__nav ">
			<li class="m-menu__item  {{ areActiveRoutes(['admin.dashboard'],'m-menu__item--active') }}" aria-haspopup="true" >
				<a  href="{{ route('admin.dashboard')}}" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-line-graph"></i>
					<span class="m-menu__link-title">
						<span class="m-menu__link-wrap">
							<span class="m-menu__link-text">
								Dashboard
							</span>

						</span>
					</span>
				</a>
			</li>



			<li class="m-menu__section ">
				<h4 class="m-menu__section-text">
					Content Mangement
				</h4>
				<i class="m-menu__section-icon flaticon-more-v3"></i>
			</li>
			<li class="m-menu__item {{ areActiveRoutes(['logo.index','logo.create','logo.edit' ,'public-home-page.index','public-home-page.create','public-home-page.edit' ],'m-menu__item--open m-menu__item--expanded') }}  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a  href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						CMS Mangement
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
							<span class="m-menu__link">
								<span class="m-menu__link-text">
									Base
								</span>
							</span>
						</li>
						
						<li class="m-menu__item {{ areActiveRoutes(['logo.index','logo.create','logo.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('logo.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Logo Setting
								</span>
							</a>
						</li>

						<li class="m-menu__item  {{ areActiveRoutes(['public-home-page.index','public-home-page.create','public-home-page.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('public-home-page.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Home Page
								</span>
							</a>
						</li>
						
					</ul>
				</div>
			</li>
			<li class="m-menu__section ">
				<h4 class="m-menu__section-text">
					Accounts 
				</h4>
				<i class="m-menu__section-icon flaticon-more-v3"></i>
			</li>
			<li class="m-menu__item {{ areActiveRoutes(['code.index','code.create','code.edit' ,'cash-reciept.index','cash-reciepte.create','cash-reciept.edit','bank-payment.index','bank-payment.create','bank-payment.edit' ,'bank-deposit.index','bank-deposit.create','bank-deposit.edit' ,'journal-voucher.index','journal-voucher.create','journal-voucher.edit' ,'receivable.index','receivable.create','receivable.edit' ,'ledger.index','ledger.create','ledger.edit' ,'cash-payment.index' ,'cash-payment.create' ,'cash-payment.edit','payable.index',
			'payable.edit', 'payable.create'],'m-menu__item--open m-menu__item--expanded') }}  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a  href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						Accounts
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
							<span class="m-menu__link">
								<span class="m-menu__link-text">
									General Accounts
								</span>
							</span>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['code.index','code.create','code.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('code.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									All Accounts
								</span>
							</a>
						</li>
						
						<li class="m-menu__item {{ areActiveRoutes(['cash-reciept.index','cash-reciept.create','cash-reciept.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('cash-reciept.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Cash Reciept
								</span>
							</a>
						</li>

						<li class="m-menu__item  {{ areActiveRoutes(['cash-payment.index','cash-payment.create','cash-payment.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('cash-payment.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Cash Payment
								</span>
							</a>
						</li>
						<li class="m-menu__item  {{ areActiveRoutes(['bank-payment.index','bank-payment.create','bank-payment.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('bank-payment.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Bank Payment
								</span>
							</a>
						</li>
						<li class="m-menu__item  {{ areActiveRoutes(['bank-deposit.index','bank-deposit.create','bank-deposit.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('bank-deposit.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Bank Deposit
								</span>
							</a>
						</li>
						<li class="m-menu__item  {{ areActiveRoutes(['journal-voucher.index','journal-voucher.create','journal-voucher.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('journal-voucher.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									General Voucher
								</span>
							</a>
						</li>
						<li class="m-menu__item  {{ areActiveRoutes(['receivable.index','receivable.create','receivable.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('receivable.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Reciveable Account
								</span>
							</a>
						</li>
						<li class="m-menu__item  {{ areActiveRoutes(['payable.index','payable.create','payable.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('payable.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Payable Account
								</span>
							</a>
						</li>
						<li class="m-menu__item  {{ areActiveRoutes(['financials.index','financials.create','financials.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('financials.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Financial
								</span>
							</a>
						</li>
						<li class="m-menu__item  {{ areActiveRoutes(['ledger.index','ledger.create','ledger.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('ledger.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Ledger
								</span>
							</a>
						</li>
						
					</ul>
				</div>
			</li>

			
			
			<li class="m-menu__section ">
				<h4 class="m-menu__section-text">
					Main Menu
				</h4>
				<i class="m-menu__section-icon flaticon-more-v3"></i>
			</li>
			<li class="m-menu__item {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit' ,'roles.index' ,'roles.create','roles.edit','goods.index','goods.create','goods.edit' ,'good-categories.index','good-categories.create','good-categories.edit', 'vehical-categories.index','vehical-categories.create','vehical-categories.edit','vehicle-sub-categories.index','vehicle-sub-categories.create','vehicle-sub-categories.edit', 'financial.index','financial.create','financial.edit' , 'skip-reasons.index','skip-reasons.create','skip-reasons.edit' , 'emergencies.index','emergencies.create','emergencies.edit' ],'m-menu__item--open m-menu__item--expanded') }}  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a  href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-layers"></i>
					<span class="m-menu__link-text">
						System Settings
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu ">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
							<span class="m-menu__link">
								<span class="m-menu__link-text">
									Base
								</span>
							</span>
						</li>
						<li class="m-menu__item {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('permissions.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Permissions
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['roles.index','roles.create','roles.edit']) }} " aria-haspopup="true" >
							<a  href="{{ route('roles.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Roles
								</span>
							</a>
						</li>
						<li class="m-menu__item {{ areActiveRoutes(['goods.index','goods.create','goods.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('goods.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Goods
								</span>
							</a>
						</li>
						<li class="m-menu__item {{ areActiveRoutes(['vehical-categories.index','vehical-categories.create','vehical-categories.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('vehical-categories.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Vehicle Types
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['vehicle-sub-categories.index','vehicle-sub-categories.create','vehicle-sub-categories.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('vehicle-sub-categories.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Vehicles Categories
								</span>
							</a>
						</li>
						<li class="m-menu__item {{ areActiveRoutes(['emergencies.index','emergencies.create','emergencies.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('emergencies.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Emergency caution
								</span>
							</a>
						</li>
						<li class="m-menu__item {{ areActiveRoutes(['skip-reasons.index','skip-reasons.create','skip-reasons.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('skip-reasons.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Skip Reasons
								</span>
							</a>
						</li>
						<li class="m-menu__item {{ areActiveRoutes(['financial.index','financial.create','financial.edit']) }} " aria-haspopup="true" >
							<a  href="{{route('financial.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Financial Setting
								</span>
							</a>
						</li>
					</ul>
				</div>
			</li>



			<li class="m-menu__item {{ areActiveRoutes(['report.index','report.create','report.edit' ,'user-report.index','user-report.create','user-report.edit', 'main-wallet-data', 'inflow.index', 'out-flow.index' , 'finance-report.index' ,'offers.index' , 'offers.create' , 'offers.edit'],'m-menu__item--open m-menu__item--expanded') }} m-menu__item--submenu"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						Financial Report
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>
				<div class="m-menu__submenu">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">
						<li class="m-menu__item {{ areActiveRoutes(['main-wallet-data']) }} " aria-haspopup="true">
							<a href="{{route('main-wallet-data')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Main Wallet
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['inflow.index']) }} " aria-haspopup="true">
							<a href="{{route('inflow.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Inflow
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['out-flow.index']) }} " aria-haspopup="true">
							<a href="{{route('out-flow.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Out flow
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['offers.index','offers.edit','offers.update']) }}  " aria-haspopup="true">
							<a href="{{route('offers.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Offer
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['report.index']) }} " aria-haspopup="true">
							<a href="{{route('report.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									captain report
								</span>
							</a>
						</li>
						<li class="m-menu__item {{ areActiveRoutes(['finance-report.index']) }} " aria-haspopup="true">
							<a href="{{route('finance-report.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Financial report
								</span>
							</a>
						</li>


					</ul>
				</div>

			</li>



			<li class="m-menu__item {{ areActiveRoutes(['adminResource.index','adminResource.edit','adminResource.create','adminResource.update' , 'country.index','country.create','country.edit','country.update' ,'cities.index','cities.create','cities.edit','cities.update' ,'branch.index','branch.create','branch.edit','branch.update' , 'franchise.index','franchise.create' ,'franchise.edit','franchise.update' ,],'m-menu__item--open m-menu__item--expanded') }} m-menu__item--submenu"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a href="javascript:;" class="m-menu__link m-menu__toggle">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						Staff Mangement
					</span>
					<i class="m-menu__ver-arrow la la-angle-right"></i>
				</a>

				<div class="m-menu__submenu">
					<span class="m-menu__arrow"></span>
					<ul class="m-menu__subnav">

						<li class="m-menu__item {{ areActiveRoutes(['adminResource.index','adminResource.create','adminResource.edit']) }} " aria-haspopup="true">
							<a href="{{ route('adminResource.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Admin
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['country.index','country.create','country.edit']) }} " aria-haspopup="true">
							<a href="{{ route('country.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Country
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['cities.index','cities.create','cities.edit']) }} " aria-haspopup="true">
							<a href="{{ route('cities.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									City
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['branch.index','branch.create','branch.edit']) }} " aria-haspopup="true">
							<a href="{{ route('branch.index') }}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Branches
								</span>
							</a>
						</li>

						<li class="m-menu__item {{ areActiveRoutes(['franchise.index','franchise.create','franchise.edit']) }} " aria-haspopup="true">
							<a href="{{ route('franchise.index')}}" class="m-menu__link ">
								<i class="m-menu__link-bullet m-menu__link-bullet--dot">
									<span></span>
								</i>
								<span class="m-menu__link-text">
									Franchise
								</span>
							</a>
						</li>


					</ul>
				</div>

			</li>
			
			<li class="m-menu__item {{ areActiveRoutes(['captainResource.index','captainResource.create','captainResource.edit']) }}"  aria-haspopup="true" >
				<a href="{{ route('captainResource.index')}}" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						Captain
					</span>
				</a>

			</li>
			
			<li class="m-menu__item {{ areActiveRoutes(['userResource.index','userResource.create' ,'userResource.edit']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a href="{{ route('userResource.index')}}" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						User
					</span>
					
				</a>

			</li>
			<li class="m-menu__item {{ areActiveRoutes(['bookings.index','bookings.create' ,'bookings.edit']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a href="{{ route('bookings.index')}}" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						Booking
					</span>
					
				</a>

			</li>
			<li class="m-menu__item {{ areActiveRoutes(['pending-booking.index','pending-booking.create' ,'pending-booking.edit']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a href="{{ route('pending-booking.index')}}" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						Bookings(Not Responseded)
					</span>
					
				</a>

			</li>
			<li class="m-menu__item {{ areActiveRoutes(['advance-bookings.index','advance-bookings.create' ,'advance-bookings.edit']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
				<a href="{{ route('advance-bookings.index')}}" class="m-menu__link ">
					<i class="m-menu__link-icon flaticon-share"></i>
					<span class="m-menu__link-text">
						Advance Bookings
						
					</a>

				</li>
				<li class="m-menu__item {{ areActiveRoutes(['pendingRide']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
					<a href="{{ route('pendingRide')}}" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-share"></i>
						<span class="m-menu__link-text">
							Current Rides
						</span>
						
					</a>

				</li>
				<li class="m-menu__item {{ areActiveRoutes(['Rides.index','Rides.create' ,'Rides.edit']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
					<a href="{{ route('Rides.index')}}" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-share"></i>
						<span class="m-menu__link-text">
							Completed Rides
						</span>
						
					</a>

				</li>
				<li class="m-menu__item {{ areActiveRoutes(['googlemaps.index','googlemaps.create']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
					<a href="{{ route('googlemaps.index')}}" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-share"></i>
						<span class="m-menu__link-text">
							Vehicle Tracking
						</span>
						
					</a>

				</li>
				<li class="m-menu__item {{ areActiveRoutes(['activity-logs.index','activity-logs.create']) }}"  aria-haspopup="true"  m-menu-submenu-toggle="hover">
					<a href="{{ route('activity-logs.index')}}" class="m-menu__link ">
						<i class="m-menu__link-icon flaticon-share"></i>
						<span class="m-menu__link-text">
							Activity Log
						</span>
						
					</a>

				</li>
			</ul>
		</div>
		<!-- END: Aside Menu -->
	</div>
	<!-- END: Left Aside -->
