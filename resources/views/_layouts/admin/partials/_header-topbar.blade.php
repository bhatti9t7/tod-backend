<!-- BEGIN: Topbar -->
<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">

	<!--[html-partial:include:{"file":"partials\/_topbar-search-default.html"}]/-->
	@section('topbar-search-default')
		@include('_layouts.admin.partials._topbar-search-default')
	@show

	<div class="m-stack__item m-topbar__nav-wrapper">
		<ul class="m-topbar__nav m-nav m-nav--inline">
			<!--[html-partial:include:{"file":"partials\/_topbar-notifications.html"}]/-->
		@section('topbar-notifications')
			@include('_layouts.admin.partials._topbar-notifications')
		@show

<!--[html-partial:include:{"file":"partials\/_topbar-quick-actions.html"}]/-->
		@section('topbar-quick-actions')
			@include('_layouts.admin.partials._topbar-quick-actions')
		@show

<!--[html-partial:include:{"file":"partials\/_topbar-user-profile.html"}]/-->
			@section('topbar-user-profile')
				@include('_layouts.admin.partials._topbar-user-profile')
			@show

			<!-- <li id="m_quick_sidebar_toggle" class="m-nav__item">
				<a href="#" class="m-nav__link m-dropdown__toggle">
					<span class="m-nav__link-icon">
						<i class="flaticon-grid-menu"></i>
					</span>
				</a>
			</li> -->
		</ul>
	</div>
</div>
<!-- END: Topbar -->
