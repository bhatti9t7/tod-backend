<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')

@section('content')
<div class="col-sm-10 col-md-11 main">
          <div class="row" >
            <div class="col-sm-12 col-md-12" style="padding-left:0px;">
                <h1 class="page-header"><a href="{{route('code.index')}}"><i class="icon-arrow-left-3"></i></a>Edit Account </h1>
            </div>
          </div>
          <div class="row">
          
            <div class="col-sm-5 col-md-5">
              {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['code.update',$code->id], 'class'=>'form-horizontal']) !!}
              
                @csrf
            
                  
                <div class="form-group">
                  <label for="code">Code</label>
                 
                  <input type="text" id="code" class="form-control" name="code" value="{{ $code->code}}">
                 
                      @if ($errors->has('code'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('code')}}
                        </div>
                      @endif
                </div>
                
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" name="title" value="{{ $code->title}}">
                    @if ($errors->has('title'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('title')}}
                        </div>
                    @endif
                </div>
                
               
                <div class="form-group">
                  <label for="c_balance">Amount</label>
                  <input type="text" class="form-control" name="c_balance" value="{{ $code->c_balance}}">
                    @if ($errors->has('c_balance'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('c_balance')}}
                        </div>
                    @endif
                </div>
              <button type="submit" class="btn btn-success" value="submit"><span class="icon-checkmark"></span> Submit</button>
              {!! Form::close() !!}
            </div>
          </div>
         </div>


@endsection
