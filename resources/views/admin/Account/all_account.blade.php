<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

<style type="text/css">
  .table-responsive{
    padding: 20px 20px;
  }
</style>
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
              <h3 class="m-portlet__head-text">
                 General Accounts
              </h3>
            </div>
        </div>
      </div>
        @component('_components.alerts-default')
        @endcomponent
          <div class="row">
            <div class="table-responsive">
              <table  class="table table-striped table-hover table-bordered" id="accounts">
                <thead>
                  <tr>
                    <th></th>
                    <th>Code</th>
                    <th>Title</th>
                    <th>Account Categories</th>
                    <th>Current Balance</th>
                    <th>Created at</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                
              </table>
            </div>
          </div>
    
 @endsection
  @push('post-styles')
  <link href="{{ asset('assets/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  @endpush

  @push('post-scripts')
  <script src="{{ asset('assets/datatables/datatables.min.js') }}" type="text/javascript"></script>
@if(session('success_message')) 

<script type="text/javascript">

   swal({
      title: "Good job!",
      text: "You record delete sucessfully",
      icon: "success",
      button: "Aww yiss!"
  });
</script>
@endif 
 
          <script>
            
              $('#accounts').DataTable( {
                "processing": true,
                "serverSide": true,
                Length:10,
                ajax: {
                    "url":"<?= route('get_account') ?>",
                    "dataType":"json",
                    "type":"POST"
                },
                columns:[
                    {"data":"id","render":function(id){
                       return  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"> <input type="checkbox" class="checkboxes" value="'+id+'" />  <span></span></label>'; 
                    },"searchable":false,"orderable":false},
                    {"data":"code"},
                    {"data":"title"},
                    {"data":"account_categories.account_cat_name"},
                    {"data":"c_balance"}, 
                    {"data":"created_at"},
                   
                    {"data":"id","render":function(id){
                        var edit_route = '{{ route("code.edit", ":id") }}';
                            edit_route = edit_route.replace(':id', id);

                     
                        var buttons = '<a href="'+edit_route+'" class="btn btn-warning btn-xs">Edit</a>';

                        
                        return buttons;
                    },"searchable":false,"orderable":false}
                    ]
            } );
        
            
            
        function change_status(obj,id,status) {
          $.ajax({
            url: "", 
            method:"POST",
            data:{'user_id':id,'status':status},
            success: function(response){
                if(response.status){
                  if(status)
                  $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',0)" class="btn btn-success"> Active </a>');
                  else
                  $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',1)" class="btn btn-danger"> Deactive </a>');
                   toastr.success(response.message);
                }else{
                 toastr.error(response.message);

                }
           }});
        }
          var action_url ;
          var user_id ;
          var row_obj;
          function delete_row(obj,id) {
                  action_url = $(obj).attr('data-action');
                  user_id = id;
                  row_obj = obj;
                 $(obj).on('confirmed.bs.confirmation', function () {
                   
                    $.ajax({
                      url: action_url, 
                      method:"DELETE",
                      data:{'id':user_id},
                      success: function(response){
                          if(response.status){
                              $(row_obj).parent().parent().remove();
                          }
                          else{
                              console.log(response.message);
                         }
                     }});
                  });
          }    
      </script>

      @endpush