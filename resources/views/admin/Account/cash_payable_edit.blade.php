<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">


        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     Edit Cash Payment Voucher
                    </h3>
                </div>
            </div>
        </div>
        @component('_components.alerts-default')
        @endcomponent

        <div class="m-portlet__body">
          
            <div class="col-sm-5 col-md-5">
              
             {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['cash-payment.update',$cash_payment->id], 'class'=>'form-horizontal']) !!}
                @csrf
                  <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                <div class="form-group">
                  <label for="code">Code</label>
                  <input type="text" id="code" class="form-control" onclick="open_accounts_model(this)" name="code" value="{{ $cash_payment->code }}">
                      @if ($errors->has('code'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('code')}}
                        </div>
                      @endif
                </div>
                <div class="form-group">
                  <label for="goes_to">Amount goes to </label>
                  <input type="text" class="form-control" onclick="open_accounts_model(this)" name="goes_to" value="{{ $cash_payment->goes_to }}">
                      @if ($errors->has('goes_to'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('goes_to')}}
                        </div>
                      @endif
                </div>
                <div class="form-group">
                  <label for="description">Description</label>
                  <input type="text" class="form-control" name="description" value="{{ $cash_payment->description }}">
                    @if ($errors->has('description'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('description')}}
                        </div>
                    @endif
                </div>
                
                <div class="form-group">
                  <label for="recpno">Reciept No#</label>
                  <input type="text" class="form-control" name="recpno" value="{{ $cash_payment->recpno }}">
                    @if ($errors->has('recpno'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('recpno')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                  <label for="amount">Amount</label>
                  <input type="text" class="form-control" name="amount" value="{{ $cash_payment->amount }}">
                    @if ($errors->has('amount'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('amount')}}
                        </div>
                    @endif
                </div>
                <div class="form-group">
                  <label for="chqno">Cheque No</label>
                  <input type="text" class="form-control" name="chqno" value="{{ $cash_payment->chqno }}">
                    @if ($errors->has('chqno'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('chqno')}}
                        </div>
                    @endif
                </div>
                
                <div class="form-group">
                  <label for="posting_date">Posting Date</label>
                  <div class="date" id='datepicker1'>
                    <input type="text" class="form-control" name="posting_date" placeholder="Date" data-date-format="DD-MM-YYYY" value="{{ $cash_payment->posting_date }}">
                  </div>
                    <script type="text/javascript">
                            $(function () {
                              $('#datepicker1').datepicker({
                                pickTime: false,
                                useCurrent: false
                              });
                            });
                          </script>
                    @if ($errors->has('posting_date'))
                        <div class="alert alert-danger" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                          </button>
                          <strong>Warning!</strong> {{$errors->first('posting_date')}}
                        </div>
                    @endif
                </div>
                
              <button type="submit" class="btn btn-success" value="submit"><span class="icon-checkmark"></span> Submit</button>
             {!! Form::close() !!}
            </div>
          </div>
<!-- model is start here -->

<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 1200px; margin-top: 200px; margin-left: -350px;">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">All Account</h4>
          </div>
          <div class="modal-body infinite-scroll" style="height: 521px;" >
            <div class="table-responsive">
                <table class="table table-striped table-hover table-bordered show_value" id="accounts">
                    <thead>
                        <tr>
                            <th align="left">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> Account Code  </th>
                            <th> Account Title  </th>
                            <th> Categories</th>
                            <th> Opening Balance</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
  </div>
</div>
</div>
<!-- End Modal -->

@endsection

@push('post-styles')
 <link href="{{ asset('assets/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@push('post-scripts')
<script src="{{ asset('assets/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script>
   $('#accounts').DataTable( {
        "processing": true,
        "serverSide": true,
        Length:10,
        ajax: {
            "url":"<?= route('get_account') ?>",
            "dataType":"json",
            "type":"POST"
        },
        columns:[
        {"data":"id","render":function(id){
         return  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"> <input type="checkbox" class="checkboxes" value="'+id+'" />  <span></span></label>'; 
     },"searchable":false,"orderable":false},
      {"data":"code" , "render":function(code,display,row){
        console.log(row);
        return '<div onclick="moveValue('+code+',\''+row.title+'\')">'+code+'</div>'
    }},
    {"data":"title"},
    {"data":"account_categories", "render":function(account_categories){
        return account_categories!=null?account_categories.account_cat_name:'';
    }},
    {"data":"c_balance"},   
]
} );

    var ref_obj;
    function open_accounts_model(obj){
        ref_obj = obj;
        $('#myModal1').modal('show');
    }
    function moveValue( code , title )
    {
        $(ref_obj).val(code);
        $('#title').val(title);
    // $('[name="descript[]"]').val(title);
    $("#myModal1").modal("hide");
    console.log(code);
    // console.log(title);
    }
</script>

@endpush