<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')

@section('content')

<div class="m-portlet m-portlet--mobile">


        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     Account
                    </h3>
                </div>
            </div>
        </div>
        @component('_components.alerts-default')
        @endcomponent

        <div class="m-portlet__body" style="overflow: hidden;">

        	<div class="row">
        		<div class="col-md-12">
        			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;">
						<div class="col-md-12">
							<h3 class="head_1_1">1-2-3-4</h3>
							<h3 class="head_1_1">All Accounts</h3>
						</div>
						<div class="col-md-12">
								<a href="http://127.0.0.1:8000/admin/accounts/detail">
									<input type="button" value="All Account" class=" st_1">
								</a>
						</div>		
				    </div>
					<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;">
						<div class="col-md-12">
							<h2 class="head_1_1"> 1 <br>Assets</h2>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="112">
								<input hidden="" name="class" value="Acc Receivable">
								<button type="submit" class="btn_st">New Customer</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="113">
								<button type="submit" class="btn_st">Agent Customer</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="111">
								<input hidden="" name="class" value="Gen. Asset">
                                 <button type="submit" class="btn_st">New Bank Account</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="114">
								<button type="submit" class="btn_st">RFD Recieveable Agent</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="115">
								<input hidden="" name="clss" value="Gen. Asset">
								<button type="submit" class="btn_st">Staff Advances</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="100">
								<input hidden="" name="class" value="Gen. Asset">
                                <button type="submit" class="btn_st">Fixed And Other Assets</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="116">
								<input hidden="" name="clss" value="Gen. Asset">
								<button type="submit" class="btn_st">Bank guarnty &amp; Security Deposit</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="117">
								<input hidden="" name="clss" value="Gen. Asset">
								<button type="submit" class="btn_st">Adv.Tax And Other Deposits</button>
							</form>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;"> 
						<div class="col-md-12">   
							<h2 class="head_1_1"> 2 <br>Liabilities</h2>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="231">
								<input hidden="" name="class" value="Acc. payable">
                                <button type="submit" class="btn_st">New Agent</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="234">
								<input hidden="" name="class" value="Acc. payable">
                                <button type="submit" class="btn_st">Other Payable</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="232">
								<input hidden="" name="class" value="Gen. liability">
								<button type="submit" class="btn_st">RFD Payable Customer</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="210">
								<input hidden="" name="class" value="Gen. liability">
								<button type="submit" class="btn_st">Equity &amp; Capital</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="220">
								<input hidden="" name="class" value="Gen. liability">
                                <button type="submit" class="btn_st">Profit And Loss A/C</button>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
								<input hidden="" name="code" value="235">
								<input hidden="" name="class" value="Gen. liability">
                                <button type="submit" class="btn_st">Taxes Payables &amp; Others</button>
							</form>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;">
						<div class="col-md-12">
							<h2 class="head_1_1">3 <br> Expenses</h2>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form action="javascript:;" method="post" class="ids">
									<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
									<input hidden="" name="code" value="340">
									<input hidden="" name="class" value="Operating Expense">
									<button type="submit" class="btn_st">New Expenses</button>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form action="javascript:;" method="post" class="ids">
									<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
									<input hidden="" name="code" value="311">
									<button type="submit" class="btn_st">Cost Of Revenue</button>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h2 class="head_1_1">4 <br> Revenue</h2>
								<div class="row">
									<div class="col-md-12">
										<form action="javascript:;" method="post" class="ids">
											<input type="hidden" name="_token" value="t2TjTSorBf5tYHpfVXdl3a84ZonaTAZjTG6j6nbv">
											<input hidden="" name="code" value="400">
											<input hidden="" name="class" value="Revenue">
                                            <button type="submit" class="btn_st">New Income</button>
										</form>
									</div>
								</div>
							</div>
				        </div>
					</div>
        		</div>
        	</div>

        	<br><br>
        	<div class="row">
        		<div class="col-md-12">
        			<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;">
						<div class="col-md-12">
							<h3  class="head_1_1">1-2-3-4</h3>
							<h3  class="head_1_1">All Accounts</h3>
						</div>
						<div class="col-md-12">
								<a href="{{route('code.all_account')}}" >
									<input type="button" value="All Account" class=" st_1" >
								</a>
						</div>		
				    </div>
					<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;">
						<div class="col-md-12">
							<h2  class="head_1_1"> 1 <br>Assets</h2>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="112">
								<input hidden  name="class" value="Acc Receivable">

								<input type="submit" value="New Customer" class="btn_st"/>
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="113">
								<input type="submit" value="Agent Customer" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="111">
								<input hidden  name="class" value="Gen. Asset">

								<input type="submit" value="New Bank Account"class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="114">
								<input type="submit" value="RFD Recieveable Agent" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="115">
								<input hidden  name="clss" value="Gen. Asset">
								<input type="submit" value="Staff Advances" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="100">
								<input hidden  name="class" value="Gen. Asset">

								<input type="submit" value="Fixed And Other Assets" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="116">
								<input hidden  name="clss" value="Gen. Asset">
								<input type="submit" value="Bank guarnty & Security Deposit" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post"  class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="117">
								<input hidden  name="clss" value="Gen. Asset">
								<input type="submit" value="Adv.Tax And Other Deposits" class="btn_st" >
							</form>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;"> 
						<div class="col-md-12">   
							<h2  class="head_1_1"> 2 <br>Liabilities</h2>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="231">
								<input hidden  name="class" value="Acc. payable">

								<input type="submit" value="New Agent" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="234">
								<input hidden  name="class" value="Acc. payable">

								<input type="submit" value="Other Payable" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="232">
								<input hidden  name="class" value="Gen. liability">
								<input type="submit" value="RFD Payable Customer"class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="210">
								<input hidden  name="class" value="Gen. liability">
								<input type="submit" value="Equity & Capital" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="220">
								<input hidden  name="class" value="Gen. liability">

								<input type="submit" value="Profit And Loss A/C" class="btn_st" >
							</form>
						</div>
						<div class="col-md-12">
							<form action="javascript:;" method="post" class="ids">
								{{ csrf_field() }}
								<input hidden  name="code" value="235">
								<input hidden  name="class" value="Gen. liability">


								<input type="submit" value="Taxes Payables & Others" class="btn_st" >
							</form>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-xs-12 btn_st21" style="float: left;">
						<div class="col-md-12">
							<h2  class="head_1_1">3 <br> Expenses</h2>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form action="javascript:;" method="post" class="ids">
									{{ csrf_field() }}
									<input hidden  name="code" value="340">
									<input hidden  name="class" value="Operating Expense">
									<input type="submit" value="New Expenses" class="btn_st" >

								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form action="javascript:;" method="post" class="ids">
									{{ csrf_field() }}
									<input hidden  name="code" value="311">
									<input type="submit" value="Cost Of Revenue" class="btn_st" >
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h2  class="head_1_1">4 <br> Revenue</h2>
								<div class="row">
									<div class="col-md-12">
										<form action="javascript:;" method="post" class="ids">
											{{ csrf_field() }}
											<input hidden  name="code" value="400">
											<input hidden  name="class" value="Revenue">

											<input type="submit" value="New Income" class="btn_st" >
										</form>
									</div>
								</div>
							</div>
				        </div>
					</div>
        		</div>
        	</div>
        	<div class="row">
        		<div class="col-md-12">
        			<div class="boxx_1" style="float: left; width: 100%;">
						<div class="row" style="margin-top: 20px;">
							<form action="{{route('code.store')}}" class="form-horizontal" method="POST">
								<div class="col-md-12">
									<div class="col-xl-2 col-lg-4 col-md-4" style="float: left;">
										<label style="font-weight: bold;">New Account</label>
											
												{{ csrf_field() }}
												<input type="" hidden name="account_cat_id" value="" id="account_id">
												<input hidden  name="code" value="">
												<input type="text" name="code" value="" name="code" readonly style="width: 100px;color:#fff; background-color: red;" id="account_code">
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4" style="float: left;">
										<label style="font-weight: bold;">Title Of Account</label><br>
										<input class="form-control" type="text" name="title" placeholder="Account Title" style="width:100%;">
									</div>
									<div class="col-xl-3 col-lg-4 col-md-4" style="float: left;">
										<label style="font-weight: bold;">Opening Balance</label><br>
										<input class="form-control" type="number" name="c_balance" placeholder="Account Balance" style="width:100%;">
									</div>
									<div class="col-xl-2 col-lg-4 col-md-4 marg_top" style="float: left;">
										<label style="font-weight: bold;">Admin</label><br>
										<select class="form-control" type="text" name="admin_id" placeholder=" Balance" style="width:100%;">
											<option value="">choose Admin</option>
											@php( $users = \App\Admin::all()) 
												@if($users->count() > 0)
													@foreach($users as $category)
														<option value="{{$category->id}}">
														{{$category->name}} 
														</option>
													@endforeach
												@endif 
											</select>
									</div>
									<br>
									<div class="col-xl-1 col-lg-2 col-md-2 col-sm-4 marg_top marg_top1" style="float: left;">
										<input type="button" value="save" class="btn btn blue"class="btn_st">
									</div>
									<div class="col-xl-1 col-lg-2 col-md-2 col-sm-4 marg_top marg_top1" style="float: left;">
										<input type="reset" value="Exit" class="btn btn blue" class="btn_st">
									</div>
						        </div>
						    </form>
					    </div>
	    			</div>
        		</div>
        	</div>
    </div>




@endsection

@push('post-styles')
<style type="text/css">
.btn_st{padding: 8px 0px!important;
	width: 100%!important;
	color: blue!important;
	background: linear-gradient(0deg, rgba(226,226,226,1) 43%, rgba(247,247,247,1) 52%);
}
.st_1{
	padding: 20px 30px!important;
	width: 100%!important;
	color: blue!important;
	background: linear-gradient(0deg, rgba(226,226,226,1) 43%, rgba(247,247,247,1) 52%);
}
.form3_9{border: 1px solid black;

}
.Example_E {
	-moz-box-shadow: 0 0 5px #888;
	-webkit-box-shadow: 0 0 5px#888;
	box-shadow: 0 0 5px #888;
}
.head_1{color: red; font-weight: bold; position: absolute; top: 0px; margin-left: 15px;
	background-color: #fff;
}
.md-editor_1{border: 1px solid gray;    box-shadow: inset 0 2px 4px 0 rgba(0,0,0,0.06);box-shadow: i
}
.md-editor_1 h2{ color: red; text-align: center; font-weight: bold; 
	font-size: 20px;
}
.btn_st21{ border-right: 1px solid gray; height: 450px;

	-webkit-box-shadow: 3px 0 5px -2px #888;
	box-shadow: 3px 0 5px -2px #888;
}
.head_1_1{ 
	font-size: 25px; color: solid black; font-weight: bold; text-align: center;
}
.main_head{ border:1px solid #ccc; margin-top: 5px; padding-right: 10px;}

@media only screen and (max-width: 1450px){
.btn_st{
	      min-width: 150px;
    font-size: 12px;
}
}
@media only screen and (max-width: 1200px){
.marg_top{
	margin-top: 20px;
}
.marg_top1{
	margin-top: 48px;
}	
}

</style>

<style type="text/css">
input{
	text-transform:uppercase;
}
</style>
@endpush

@push('post-scripts')
  <script type="text/javascript">
      $(document).ready(function(){
        $('.ids').on("submit",function(e){
            e.preventDefault();
            var form = $(this).find("[name='code']").val();
            $.ajax({
                url:"{{route('account.new_acc')}}",
                method : "post",
                data : {data:form},
                dataType : "json",
                success : function(data){
                    $("#account_code").val(data.codes);
                    $("#account_id").val(data.account.id);
                }
           });
        });
     });

    </script>
@endpush