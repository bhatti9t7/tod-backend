<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

<style type="text/css">
.sec_12{
  margin-top: 20px;
}
.m-portlet__body{
  overflow: hidden;
}
.sec_11{
  margin-top: 65px;
}
</style>
<div class="m-portlet m-portlet--mobile">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <h3 class="m-portlet__head-text">
          Ledger Record 
        </h3>
      </div>
    </div>
  </div>

  @component('_components.alerts-default')
  @endcomponent
  <div class="m-portlet__body">
    <div class="col-md-12">
      <div class="portlet-body">
        <div class="tab-content">
          <div class="sec_1 row">
            <div class="row">
              <div class="col-md-12">
                <form action="{{route('ledger_detail')}}" method="post">        
                  @csrf                 
               </div>
              <div class="sec_12">
                <div class="col-md-12">
                  <div class="col-md-2" style="float: left;">
                    <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Acc.Code</label>
                    <br>
                    <input id="select2"  name="code" class="js-example-data-ajax form-control account_code" placeholder="Select Account" onclick="open_accounts_model(this)" style="width: 200">
                  </div>
                    <div class="col-md-2" style="float: left;">
                    <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Acc. Head</label>
                    <br>
                    <input type="text" placeholder="khdkjfhjk " id="title" class="form-control">
                    </div>
                    <div class="col-md-2" style="float: left;">
                    <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">From Date</label>
                    <br>
                    <input type="date" id="to_date"  name="start_date" placeholder="" class="form-control">
                    </div>
                    <div class="col-md-2" style="float: left;">
                    <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">To Date</label>
                    <br>
                    <input type="date" id="from_date"  name="last_date" placeholder="" class="form-control">
                    </div>
                    <div class="col-md-2" style="float: left;">
                    <label></label>
                    <input class="btn form-control btn-info" type="submit"  name="submit" value="Show" style="margin-top: 8px;">
                    </div>
              </div>
              </div>
              </div>
            </div>
          </div>
          <br>
         
            <div class="portlet-body">
              <div class="table-responsive sec_11">
                <table id="example" class="table table-bordered">
                  <thead>
                    <tr>
                      <th align="left">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                          <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes">
                          <span></span>
                        </label>
                      </th>
                      <th> V.TP  </th>
                      <th> DATE </th>
                      <th> Tkt.#/ Recipt.No</th>
                      <th> DESCRIPTION</th>
                      <th> DEBIT</th>
                      <th> CREDIT</th>
                      <th> BALANCE</th>
                      <th> Account Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                    $dabit_amont=0;
                    $credit_total = 0;
                    // $total_balance=$dabit_amont - $credit_total;
                    ?>
                    @if(isset($val2) && !empty($val2))
                    @foreach ($val2 as $posts)

                    <tr  @if($posts->v_type)  @else style="color: orange;" @endif >
                      <td>
                        <input type="checkbox" name="">
                      </td>
                      <td @if($posts->v_type=='BD')onClick="window.location = '{{route('bank-deposit.edit',$posts->id)}}'"@elseif($posts->v_type=='BP')onClick="window.location = '{{route('bank-payment.edit',$posts->id)}}'"@elseif($posts->v_type=='CR')onClick="window.location = '{{route('cash-reciept.edit',$posts->id)}}'"@elseif($posts->v_type=='CP')onClick="window.location = '{{route('cash-payment.edit',$posts->id)}}'"@elseif($posts->v_type=='JV')onClick="window.location = '{{route('journal-voucher.edit',$posts->id)}}'"@endif"> @if($posts->v_type){{$posts->v_type}}@else{{'INV'}}@endif  </td>
                      <td> @if($posts->posting_date){{date('d-m-Y', strtotime($posts->posting_date))}}@else{{date('d-m-Y', strtotime($posts->date)) }}@endif  </td>
                      <td> @if($posts->recpno){{$posts->recpno}}@else{{$posts->TKTNO}} @endif</td>
                      <td> @if($posts->description){{$posts->description}}@else{{$posts->description}} @endif</td>
                      <td class="debit_amt"> 
                        @if($posts->amt_type == 'DB') 
                        <?php $dabit_amont +=$posts->amount; ?>{{$posts->amount}}

                        @elseif($posts->code == $code)
                        <?php $dabit_amont +=$posts->reciveable_amount; ?>{{$posts->reciveable_amount}}
                        @endif 

                      </td>
                      <td class="credit_amt"> 
                        @if($posts->amt_type == 'CR') 
                        <?php $credit_total += $posts->amount; ?> {{$posts->amount}}
                        @elseif($posts->payable_cd == $code)
                        <?php $credit_total += $posts->payable_amount; ?>{{$posts->payable_amount}} 
                        @elseif($posts->income_cd == $code)
                        <?php $credit_total += $posts->income; ?>{{$posts->income}}
                        @endif 

                      </td>
                      <td>
                        @if($dabit_amont > $credit_total) {{ $dabit_amont - $credit_total}} @else {{$credit_total - $dabit_amont}} @endif

                      </td>
                      <td> 
                        @if($dabit_amont > $credit_total) {{ 'DB' }} @elseif($dabit_amont == $credit_total){{''}} @else {{ 'CR' }} @endif 
                      </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                      <td colspan="10" class="alert alert-info">No Record Found..</td>
                    </tr>
                    @endif

                  </tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
        <br>
        <br>
        <div class="sec_1">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-6 pull-right">
                <div class="col-md-4" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                  <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Dabit:</label>
                  <br>
                  <input type="text" name="" value="{{$dabit_amont}}" placeholder="00.00" style="width: 75px;;background: #5bc0de;">
                </div>
                <div class="col-md-2" style="float: left;">
                  <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;margin-top: -4px;">Credit:</label>
                  <br>
                  <input type="text" name="" value="{{$credit_total}}" placeholder="00.00" style="width: 75px;;background: #5bc0de;">
                </div>
                <div class="col-md-2" style="float: left;">
                  <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Total:</label>
                  <br>
                  <input type="text" name="" value="@if($dabit_amont>$credit_total) {{$dabit_amont-$credit_total}}@else {{$credit_total -$dabit_amont}}@endif" placeholder="00.00" style="width: 75px;;background:blue;color: white;">
                </div>
                <div class="col-md-2" style="float: left;">
                  <br>
                  <input type="text" name="" value="@if($dabit_amont>$credit_total){{'DABIT'}}  @else {{'CREDIT'}}@endif" placeholder="00.00" style="width: 75px;;background: green;margin-top: 7px; color: white;">
                </div>
              </div>
              <div class="col-md-6">
                <div class="col-md-2" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                  <br>
                  
                </div>
                <div class="col-md-3" style="float: left;">
                 
                </div>
                <div class="col-md-1" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

  <!-- model is start here -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" style="width: 1200px; margin-left: -350px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">All Account</h4>
        </div>
        <div class="modal-body infinite-scroll">
          <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered show_value" id="accounts">
              <thead>
                <tr>
                  <th align="left">
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                      <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                      <span></span>
                    </label>
                  </th>
                  <th> Account Code  </th>
                  <th> Account Title  </th>
                  <th> Categories</th>
                  <th> Opening Balance</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- End Modal -->
  @endsection

  @push('post-styles')
   <link href="{{asset('js/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('js/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
  <style>
  .btn-info {
    color: #fff;
    background-color: #3a7e88;
    border-color: #3c3c3c;
  }
</style>
@endpush

@push('post-scripts')

<script src="{{asset('js/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/jszip.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>

   <script>
    $(document).ready(function() {
     //Default data table
      $('#default-datatable').DataTable();


      var table = $('#example').DataTable( {
       lengthChange: false,
       buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
     } );

    table.buttons().container()
       .appendTo( '#example_wrapper .col-md-6:eq(0)' );

     } );

   </script>
<script>
 $('#accounts').DataTable( {
  "processing": true,
  "serverSide": true,
  Length:10,
  ajax: {
    "url":"<?= route('get_account') ?>",
    "dataType":"json",
    "type":"POST"
  },
  columns:[
  {"data":"id","render":function(id){
   return  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"> <input type="checkbox" class="checkboxes" value="'+id+'" />  <span></span></label>'; 
 },"searchable":false,"orderable":false},
 {"data":"code" , "render":function(code,display,row){
  console.log(row);
  return '<div onclick="moveValue('+code+',\''+row.title+'\')">'+code+'</div>'
}},
{"data":"title"},
{"data":"account_categories", "render":function(account_categories){
  return account_categories!=null?account_categories.account_cat_name:'';
}},
{"data":"c_balance"},   
]
} );

 var ref_obj;
 function open_accounts_model(obj){
  ref_obj = obj;
  $('#myModal1').modal('show');
}
function moveValue( code , title )
{
  $(ref_obj).val(code);
  $('#title').val(title);
    // $('[name="descript[]"]').val(title);
    $("#myModal1").modal("hide");
    console.log(code);
    // console.log(title);
  }
</script>
<script type="text/javascript">
  var t_account_code='';
  var t_start_date='';
  var t_last_date='';

  var table = $('#ledger_ful').DataTable( {
    "processing": true,
    "serverSide": true,
    stateSave: true,
    Length:10,
    ajax: {
      "url":"<?= route('search_ledger_amount') ?>",
      "dataType":"json",
      "type":"POST",
      "data": function (post) {
        post.account_code = t_account_code,
        post.start_date = t_start_date,
        post.last_date = t_last_date
      }
    },

    columns:[
    {"data":"id","render":function(id){
     return  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"> <input type="checkbox" class="checkboxes" value="'+id+'" />  <span></span></label>'; 
   },"searchable":false,"orderable":false},
   {"data":"code"},
   {"data":"title"},
   {"data":"id","render":function(status, type,row){
    if(row.dabit>row.credit){
      return 'DB';
    }else{
      return "CR";
    }
  }},
  {"data":"opening_balance"},
  {"data":"dabit"},
  {"data":"credit"},
  {"data":"id","render":function(status, type,row){
    if(row.dabit>row.credit){
      return row.dabit-row.credit;
    }else{
     return row.credit-row.dabit;
   }
 }},
 {"data":"c_balance"},

 {"data":"id","render":function(status, type, row){
  return '';
}},
]
});

  $('#pass_data').on("submit",function(e){
    e.preventDefault();
    t_account_code = $('account_code').val() || '';
    t_start_date = $('#t_start_date').val() || '';
    t_last_date = $('#t_last_date').val() || '';
    //var query_string = 'account_code='+account_code+'&start_date='+t_start_date+'&last_date='+t_last_date;
    table.draw();
/*
    table
    .search(query_string)
    .draw();
    console.log(account_code,t_start_date,t_last_date);*/
  });
  function redirect(amont_type,id)
  {
    console.log(amont_type,id);
    
  }
</script> 
@endpush