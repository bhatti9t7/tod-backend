<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

<style type="text/css">
  .m-portlet__body{
  overflow: hidden;
}
</style>


<div class="m-portlet m-portlet--mobile">


        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     Payable Account
                    </h3>
                </div>
            </div>
        </div>
        @component('_components.alerts-default')
        @endcomponent

        <div class="m-portlet__body">
 
            <div class="table-responsive">
              <table id="example" class="table table-bordered">
               <thead>
                <tr>
                  <th align="left">
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                      <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes">
                      <span></span>
                    </label>
                  </th>
                  <th> Account</th>
                  <th> Title </th>
                  <th> Created Date</th>
                  <th> Payable Amount</th>                                                 
                </tr>
              </thead>
              <tbody>
                @foreach($user as $acc)
                <tr>
                  <td></td>
                  <td>{{$acc->code}}</td>
                  <td>{{$acc->title}}</td>
                  <td>{{$acc->created_at}}</td>
                  <td>{{$acc->c_balance}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
      
      <div class="sec_1">
        <div class="row">
          <div class="col-md-12">
                <div class="col-xl-6 col-lg-8 col-md-6 col-sm-6 pull-right">
                  <div class="col-md-4" style="float: left;">
                      <!-- <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Print Pattern:</label> <br>
                      <select id="account_code" name="code" class="js-example-data-ajax form-control" style="background: red;color: white;">
                        <option selected="selected" value="">Ledger Pattern</option>
                      </select> -->
                  </div>
                 
                <div class="col-xl-2 col-lg-4 col-md-4" style="float: left;">
                   <label for="exampleInputEmail2" style="font-weight: bold; color: #337ab7;">Total:</label>
                   <br>
                  <input type="text" name="" value="{{($payable_amount)}}" placeholder="00.00" style="width: 75px;;background:blue;margin-top: 4px; color: white;">
                </div>
                <div class="col-xl-2 col-lg-4 col-md-4" style="float: left;">
                  <br>
                  <input type="text" name="" value="{{'CREDIT'}}" placeholder="00.00" style="width: 75px;;background: green;margin-top: 10px; color: white;">
                </div>
              </div>
              <div class="col-xl-6 col-lg-8 col-md-6 col-sm-6 ">
                <div class="col-md-2" style="float: left;">
                 
                </div>
                <div class="col-md-2" style="float: left;">
                 
                </div>
                <div class="col-md-3" style="float: left;">
                  
                </div>
                <div class="col-md-1" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                  
                </div>
                <div class="col-md-2" style="float: left;">
                
                </div>
              </div>

          </div>
        </div>
      </div>
    </div>


@endsection
@push('post-styles')
   <link href="{{asset('js/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{asset('js/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
  @endpush

  @push('post-scripts')


<script src="{{asset('js/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/jszip.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
 <script src="{{asset('js/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>

   <script>
    $(document).ready(function() {
     //Default data table
      $('#default-datatable').DataTable();


      var table = $('#example').DataTable( {
       lengthChange: false,
       buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ]
     } );

    table.buttons().container()
       .appendTo( '#example_wrapper .col-md-6:eq(0)' );

     } );

   </script>
        

      @endpush