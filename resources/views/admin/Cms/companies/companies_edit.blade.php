<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

    <div class="page-content-wrapper">

      <div class="page-content" id="page_content_responsive">
                  <div class="row">   
                      <div class="col-md-12">
                          <div class="portlet light portlet-fit bordered">
                     <div class="portlet-title">
                            <div class="caption">
                        <h3><i class="fa fa-user" style="color: #fff;"></i> <b style=" color: #fff!important;text-transform: uppercase;
                        font-size: 16px;">Companies Section Create</b> </h3></div></div>

                         {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['companies-section.update',$Companies->id], 'class'=>'form-horizontal']) !!}
                
                          <div class="row">
                            <div class = "col-md-12">
                                <div class="col-md-4"  style="margin: 0 auto;">   
                              <div class="fileinput fileinput-new" data-provides="fileinput" >
                                <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;">
                                <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                                  <img src="{{ asset('companies/image/').'/'.$Companies->images}}" id="blah" height="250" width = "250">
                                  <!-- </div> -->
                                </div>
                              </div>                              
                              <div class="form-group">
                               <label for="images" class="btn btn-primary" style="margin-left:35px;">Upload Profile</label>
                               <input type="file" id="images" name="images" class="hide" style="opacity: 0"> 
                             </div>
                             </div>  
                             <div class="col-md-8"  style="margin: 0 auto; border: 1px solid #ccc;">   
                             <div class="form-group" >
                                <label class="control-label" >Email</label>
                                  <input name = "email" type="email" class="form-control" placeholder="email" value="{{$Companies->email}}" > </div>
                                                           
                              <div class="form-group">
                                <label class="control-label">phone</label>
                                  <input type="text" name = "phone" placeholder="phone" class="form-control" value="{{$Companies->phone}}"> </div>

                             <div class="form-group">
                                <label class="control-label">Address</label>
                                  <input type="text" name = "address" placeholder="phone" class="form-control" value="{{$Companies->address}}"> </div>
                        </div>
               
                       <div class="col-md-8" style="margin: 0 auto;">
                       <div class="form-group">
                       <textarea id="editor"  name="description" class="CKEDITOR" style="width:100%; height:200px;"> {{$Companies->description}}</textarea>
                     
                        </div>
    
                      </div>
                                      
                          </div>
                      
           <div class="col-sm-3 pull-right" style="margin: 0 auto;">  <button class="btn btn-success" type = "Submit" name = "submit">Save</button>
              </form>
        <a href="{{route('public-home-page.index')}}" class="btn btn-success"> Cancel</a> </div>    
             
             </div>
           </div>
          </div>
        </div>
       </div>


      
@endsection
@stack('post-styles')
       <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
       <link rel="stylesheet" href="{{ asset('js/app.js') }}">
       <style type="text/css">
       #page_content_responsive{
        min-height: 1279px!important; padding: 0px !important; width: 100%;
       }
         /*Media Queries*/
          @media (max-width: 1580px){
          #page_content_responsive{
                padding: 0px !important;
               
                overflow-x: hidden !important;
          }
        }
          @media (max-width: 1199px){
          #page_content_responsive{
      
            padding: 0px !important;
            width: 79%;
          }
        }
         @media (max-width: 991px){
          #page_content_responsive{
          margin-left: 0px !important;
          width: 100%;
          }
        }
            @media (max-width: 767px){
          #page_content_responsive{
          margin-left: 0px !important;
          width: 100%;
          }
        }
        @media (max-width: 580px){
          #page_content_responsive{
          margin-left: 0px !important;
          width: 100%;
          }
        }
      
       
       
       </style>
       @push('post-scripts')


       <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
           <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

       <script type="text/javascript">
        function readURL(input) {

          if (input.files) {
            var reader = new FileReader();
            var totalfiles = input.files.length;
            for(i = 0; i < totalfiles; i ++){
              reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
              }
              reader.readAsDataURL(input.files[i]);
            }
          }
        }
        $("#images").change(function() {
          readURL(this);
        });
      </script>
      <script type="text/javascript">
        $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "100" width = "100" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                        });
                      });
                    </script>
                    <script>
                                  CKEDITOR.replace( 'editor' );
                                 CKEDITOR.config.width = '100%';

                                </script>  


                    @endpush


