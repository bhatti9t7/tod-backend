<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
 <div class="page-content-wrapper">
      <div class="page-content" id="page_content_responsive">
           <div class="row">   
              <div class="col-md-12">
                  <div class="portlet light portlet-fit bordered">
                     <div class="portlet-title">
                       <div class="caption">
                        <h3><i class="fa fa-user" style="color: #fff;"></i> <b style=" color: #fff!important;text-transform: uppercase;
                        font-size: 16px;">Drive Section Edit</b> </h3></div></div>
                        {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['drive-section.update', $drive->id], 'class'=>'form-horizontal']) !!}
                        <div class="row">
                         <div class="col-md-12">
                          <div class = "col-md-4" style="margin: 0 auto;">
                            <div class="fileinput fileinput-new" data-provides="fileinput" >
                              <div class="fileinput-new thumbnail">
                                <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                                  <img src="{{asset('public/drive/' . $drive->drive_image)}}" id="blah" height="250" width = "250">
                                  <!-- </div> -->
                                </div>
                              </div>

                              <div class="form-group">
                               <label for="images" class="btn btn-primary" style="margin-left: 35px;">Upload Profile</label>
                               <input type="file" id="images" name="drive_image" class="hide" style="opacity: 0"> 
                             </div>
                           </div>     
                        </div>
                      </div>
                   <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-6" style="margin: 0 auto;">
                             <textarea id="editor" class="CKEDITOR"  name="drive_content" style="width: 80%; height: 250px;">{{$drive->drive_content}}</textarea>
                          </div>
                        </div>                                         
<div class="col-sm-3" style="margin: 0 auto;">  <button class="btn btn-success" type = "Submit" name = "submit" >Save</button> 
                         </form>
                          <a href="{{route('public-home-page.index')}}" class="btn btn-success "> Cancel</a>
                          </div> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
         

          @endsection
  @stack('post-styles')
          <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
          <link rel="stylesheet" href="{{ asset('js/app.js') }}">
  
      @push('post-scripts')

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

        <script type="text/javascript">
          function readURL(input) {

            if (input.files) {
              var reader = new FileReader();
              var totalfiles = input.files.length;
              for(i = 0; i < totalfiles; i ++){
                reader.onload = function(e) {
                  $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[i]);
              }
            }
          }
          $("#images").change(function() {
            readURL(this);
          });
        </script>
        <script type="text/javascript">
          $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "100" width = "100" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                      });
        });
      </script>
      <script>
        CKEDITOR.replace( 'editor' );
        CKEDITOR.config.width = '100%';

      </script>  


      @endpush


