<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')


<div class="page-content-wrapper">
    <div class="page-content" style="min-height: 617px;">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Logo Table</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('logo.create')}}" id="sample_editable_1_new" class="btn btn-success btn-sm"> Add New
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                           

                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered responsive_class" id="captain">
                                 <thead>
                                    <tr>
                                        <th align="left">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                <span></span>
                                            </label>
                                        </th>
                                        <th>#id</th>
                                        <th> image </th>
                             
                                        <th>Action </th>
                                    </tr>
                                </thead>

                                <tbody>

                                       @foreach ($logo as $i) 
                                    <tr>

                                        <td align="left">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="checkboxes" value="1" />
                                                <span></span>
                                            </label>
                                        </td>

                                        
                                        <td> {{ $i->id }}   </td>
                                        <td> <img src="{{asset('logo_image/admin/logo/' . $i->logo_image)}}" alt="admin image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >
                                         </td>
                                        
                                   
                                

                                        <td class="pull-right"> 

                                            <a href="{{ route('logo.edit', $i['id']) }}"><button class="btn edit btn-warning btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip" title="edit"><span class="glyphicon glyphicon-pencil">
                                                Edit
                                            </span></button></a>

                                            <span  style="display: inline-block;">
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['logo.destroy', $i->id]]) }}
                                                {{ Form::hidden('id', $i->id) }}
                                                <button class="btn btn-danger btn-xs" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash">Delete</span></button>
                                                {!! Form::close() !!}
                                            </span>

                                        </td>
                                    </tr>
                                    @endforeach

                                 </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-sm-5">                           
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="dataTables_paginate paging_bootstrap_number" id="sample_editable_1_paginate">
                                    <ul class="pagination" style="visibility: visible; float: right; ">
                                        
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
  
       <div class="page-content">
           <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">slider section  </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a href="{{ route('slide.create')}}" id="sample_editable_1_new" class="btn btn-success btn-sm"> Add New
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>

                            </div>
                            

                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered responsive_class" id="captain">
                                 <thead>
                                    <tr>
                                        <th align="left">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                <span></span>
                                            </label>
                                        </th>
                                        <th>#id</th>
                                        <th> image </th>
                                        <th> Description </th>
                                        <th>Action </th>
                                    </tr>
                                </thead>

                                <tbody>

                                       @foreach ($slid as $i) 
                                    <tr>

                                        <td align="left">
                                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                <input type="checkbox" class="checkboxes" value="1" />
                                                <span></span>
                                            </label>
                                        </td>

                                        
                                        <td> {{ $i->id }}   </td>
                                        <td> <img src="{{asset('slider/image/' . $i->slide_image)}}" alt="admin image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >
                                         </td>
                                        <td> {{ $i->slide_content }} </td>


                                        <td class="pull-right"> 
                                            <a href="{{ route('slide.edit', $i['id']) }}"><button class="btn edit btn-warning btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip" title="edit"><span class="glyphicon glyphicon-pencil">Edit</span></button></a>

                                            <span  style="display: inline-block;">
                                                {{ Form::open(['method' => 'DELETE', 'route' => ['slide.destroy', $i->id]]) }}
                                                {{ Form::hidden('id', $i->id) }}
                                                <button class="btn btn-danger btn-xs" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash">Delete</span></button>
                                                {!! Form::close() !!}
                                            </span>

                                        </td>
                                    </tr>
                                    @endforeach

                                 </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
</div>

@endsection
@stack('post-styles')
      <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />


