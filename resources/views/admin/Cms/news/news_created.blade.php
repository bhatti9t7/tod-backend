<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-newspaper" ></i>
              <span class="caption-subject font-red sbold uppercase" >News Section Create</span>
            </div>

          </div>
          <div class="box-shadow4">
          <form method="POST" action="{{ route('news-section.store') }}" enctype = "multipart/form-data" id="upload_new_form">
            {{ csrf_field() }}
            @component('_components.alerts-default')
        @endcomponent
                  <div class="fileinput fileinput-new" data-provides="fileinput" style="width: 100%;" >
                   <div class="fileinput-new thumbnail" style="width: 250px; height: 250px; margin: 0 auto;">
                      <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                        <img src="{{asset('images/captain/profiles/no-image.png')}}" id="blah" height="250" width = "250">
                        <!-- </div> -->
                      </div>
                    </div>
                    <div class="img_btn" style="width: 100%; margin-top: 20px; margin-bottom: 20px;">
                      <div class="form-group" style="width: 130px; margin: 0 auto; ">
                       <label for="images" class="btn btn-primary">Upload Profile</label>
                       <input type="file" id="images" name="news_image" class="hide" style="opacity: 0">
                       
                
               
                  @if ($errors->has('news_image'))
                  <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('news_image')}}
                  </div>
                  @endif 
                </div> 
              </div>  

                        <div class="col-md-8 bordered_1" style="margin: 0 auto;">
                  <div class="form-group">

                   <textarea id="editor" class="CKEDITOR" name="news_content">
                     
                   </textarea>
                   @if ($errors->has('news_content'))
                   <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('news_content')}}
                  </div>
                  @endif
                </div>

              </div> 

                
                 <div class="col-md-3 col-sm-6 col-xs-12"> </div> 
                   <div class="col-md-12">
    

                <div class="col-md-4 col-xs-12 pull-right">
                  <button type="submit" class="btn btn-success green">Submit</button> 
                     </form>
                   <a href="{{route('public-home-page.index')}}" class="btn btn-success"> Cancel</a>  
              </div> 
              <br>
              <br>    
              <br> 
               </div> 
             </div>
      
      </div>
    </div>
  </div>
</div>
</div>




@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
<link rel="stylesheet" href="{{ asset('js/app.js') }}">
<style type="text/css">
.form-actions {
  padding: 20px 20px;
  margin: 0;
  background-color: #f5f5f5;
  border-top: 1px solid #e7ecf1;
}
.box-shadow4{
  box-shadow: 0px 0px 15px #ccc;
}
</style>
@push('post-scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>


<script type="text/javascript">
  function readURL(input) {

    if (input.files) {
      var reader = new FileReader();
      var totalfiles = input.files.length;
      for(i = 0; i < totalfiles; i ++){
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
  }
  $("#images").change(function() {
    readURL(this);
  });
</script>
<script type="text/javascript">
  $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "100" width = "100" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                      });
});
</script>
<script>
  CKEDITOR.replace( 'editor' );
  CKEDITOR.config.width = '100%';

</script>  


@endpush


