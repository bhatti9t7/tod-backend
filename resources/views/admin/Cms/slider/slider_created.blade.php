<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-cog" style="color: #fff;"></i>
              <span class="caption-subject font-red sbold uppercase" style="color: #fff !important;">  Slider section Create</span>
            </div>            
          </div>
           <div class="image_shadow3">
              <div class="image_shadow1">   
          <form method="POST" action="{{ route('slide.store') }}" enctype = "multipart/form-data" id="upload_new_form">
           {{ csrf_field() }}
           @component('_components.alerts-default')
           @endcomponent
        
         
                  <div class="fileinput fileinput-new" data-provides="fileinput"  style="width: 100%;" >
                   <div class="fileinput-new thumbnail" style="width: 250px; height: 250px; margin: 0 auto;">
                    <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                      <img src="{{asset('images/captain/profiles/no-image.png')}}" id="blah" height="250" width = "250">
                      <!-- </div> -->
                    </div>
                  </div>

                  <div class="img_btn" style="width: 100%; margin-top: 20px;" >
                    <div class="form-group" style="width: 130px; margin: 0 auto; ">

                     <label for="images" class="btn btn-primary">Upload Profile</label>
                     <input type="file" id="images" name="slide_image" class="hide" style="opacity: 0;"> 
                   </div>
                 </div>


           <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
              <div class="form-group">

               <textarea id="editor" class="CKEDITOR" name="slide_content"></textarea>
             </div>

           </div> 

           <div class="col-md-3 col-sm-3"> </div> 

         </div>

       </div>
         <div class="jam_1">
         <div class="pull-right">      
        
         <button type="submit" class="btn btn-primary green" name="submit">Submit</button>
         </form>
         <a href="{{route('logo.index')}}" class="btn btn-success"> Cancel</a>
     </div> </div>
     </div>
   </div>
 </div>
</div>
</div>
@endsection
@stack('post-styles')
<style type="text/css">
.form-actions {
  padding: 20px 20px;
  margin: 0;
  background-color: #f5f5f5;
  border-top: 1px solid #e7ecf1;
}
</style>

<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
<link rel="stylesheet" href="{{ asset('js/app.js') }}">


@push('post-scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'editor' );
</script>


<script type="text/javascript">
  function readURL(input) {

    if (input.files) {
      var reader = new FileReader();
      var totalfiles = input.files.length;
      for(i = 0; i < totalfiles; i ++){
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
  }
  $("#images").change(function() {
    readURL(this);
  });
</script>
<script type="text/javascript">
  $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "100" width = "100" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                      });
});
</script>
<script>
  CKEDITOR.replace( 'editor' );
  CKEDITOR.config.width = '100%';

</script>  


@endpush


