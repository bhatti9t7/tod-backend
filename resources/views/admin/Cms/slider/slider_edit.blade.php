<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
     <div class="page-content-wrapper">
        <div class="page-content">
           <div class="row">
             <div class="col-md-12">
               <div class="portlet light portlet-fit bordered">
                  <div class="portlet-title">
                            <div class="caption">
                              <i class="fa fa-cog" style="color: #fff;"></i>
                              <span class="caption-subject font-red sbold uppercase" style="color: #fff !important;">  Slider Edit section</span>
                            </div>

                          </div>
                      <div class="image_shadow3">
                        <div class="image_shadow1">   
                          {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['slide.update', $slide->id], 'class'=>'form-horizontal']) !!}
    
                                  <div class="fileinput fileinput-new" data-provides="fileinput" >
                                  <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;">
                                      <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                                        <img src="{{asset('slider/image/' . $slide->slide_image)}}" id="blah" height="250" width = "250">
                                        <!-- </div> -->
                                      </div>
                                    </div>
                                    <div class="form-group" style="margin-left: 35px;"> 
                                     <label for="images" class="btn btn-primary" >Upload Profile</label>
                          <input type="file" id="images" name="slide_image" class="hide" style="opacity: 0"> 
                                   </div>
                               
                                  <textarea id="editor"  name="slide_content" >{{$slide->slide_content}}</textarea>
                                </div>
                            
                                  <div class="jam_1">
                                <div class="pull-right">                               
                              <button class="btn green" type = "Submit" name = "submit">Save</button>
                              {!! Form::close() !!}
                              <a href="{{route('logo.index')}}" class="btn btn-success"> Cancel</a>
                              </div>
                             </div>
                            </div>
                           </div>
                          </div>
                         </div>
                        </div> 
                      

              @endsection
              @stack('post-styles')
              <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
              <link rel="stylesheet" href="{{ asset('js/app.js') }}">
              <style type="text/css">
              #page_content_responsive{
                min-height: 1279px; margin-left: 105px !important;padding: 0px !important; width: 100%;
              }
              /*Media Queries*/
              @media (max-width: 1580px){
                #page_content_responsive{
                  margin-left: 232px !important;
                  padding: 0px !important;
                  width: 80%;
                  overflow-x: hidden !important;
                }
              }
              @media (max-width: 1199px){
                #page_content_responsive{
                  margin-left: 225px !important;
                  padding: 0px !important;
                  width: 79%;
                }
              }
              @media (max-width: 991px){
                #page_content_responsive{
                  margin-left: 0px !important;
                  width: 100%;
                }
              }
              @media (max-width: 767px){
                #page_content_responsive{
                  margin-left: 0px !important;
                  width: 100%;
                }
              }
              @media (max-width: 580px){
                #page_content_responsive{
                  margin-left: 0px !important;
                  width: 100%;
                }
              }



            </style>

            @push('post-scripts')

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

            <script type="text/javascript">
              function readURL(input) {

                if (input.files) {
                  var reader = new FileReader();
                  var totalfiles = input.files.length;
                  for(i = 0; i < totalfiles; i ++){
                    reader.onload = function(e) {
                      $('#blah').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[i]);
                  }
                }
              }
              $("#images").change(function() {
                readURL(this);
              });
            </script>
            <script type="text/javascript">
              $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "100" width = "100" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                      });
            });
          </script>
          <script>
            CKEDITOR.replace( 'editor' );
            CKEDITOR.config.width = '100%';

          </script>  


          @endpush



