
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

             <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">       
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tab-content">
                                     <div class="portlet light portlet-fit bordered"> 
                                     <div class="portlet-title">
                                     <div class="caption">
                                     <i class="icon-settings font-red"></i>
                                      <span class="caption-subject font-red sbold uppercase">Admin Resource Edit</span>
                                      </div>                     
                                     </div>

                                        {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['adminResource.update',$admin->id], 'class'=>'form-horizontal']) !!}
                                        <div class="row">
                                          
                                          <div class = "col-md-4" style="margin-left: 10px;">
                                             <div class="fileinput fileinput-new" data-provides="fileinput" >
                                              <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;">
                                                    <img src="{{ asset('images/admin/profile/').'/'.$admin->images}}" id="blah" height="250px" width = "250px">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                               <label for="images" class="btn btn-primary" style="left: 35px;">Upload Profile</label>
                                               <input type="file" id="images" name="images" class="hide"  style="opacity: 0;">
                                            </div>
                                        </div>

                                        <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="control-label"> Name</label>
                                            <input name = "name" type="text" placeholder="John" class="form-control" value="{{$admin->name}}"> </div>

                                            <div class="form-group">
                                                <label class="control-label">Email</label>
                                                <input name = "email" type="email" class="form-control" placeholder="email" value="{{$admin->email}}">
                                                    @if ($errors->has('email'))
                                                       <div class="alert alert-danger" role="alert">
                                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            <span class="sr-only">Close</span>
                                                          </button>
                                                          <strong>Warning!</strong> {{$errors->first('email')}}
                                                        </div>
                                                    @endif
                                            </div>

                                                <div class="form-group">
                                                    <label class="control-label">phone</label>
                                                    <input type="text" name = "phone" placeholder="phone" class="form-control" value="{{$admin->phone}}">
                                                    @if ($errors->has('phone'))
                                                       <div class="alert alert-danger" role="alert">
                                                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            <span class="sr-only">Close</span>
                                                          </button>
                                                          <strong>Warning!</strong> {{$errors->first('phone')}}
                                                        </div>
                                                    @endif 
                                                </div>

                                                    <div class="form-group">
                                                        <label class="control-label">Password</label>
                                                        <input type="password" class="form-control" name = "password" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Confirm Password</label>
                                                        <input name = "password_confirmation" type="password" class="form-control"> 
                                                    </div>
                                                </div>
                                       
                                                  <div class="col-md-11" style="margin: 0 auto;">
                                                    <div class="form-group">
                                                        <div class="col-md-6"><label class="control-label">Roles</label></div>
                                                       
                                                            <select multiple="multiple" class="multi-select" id="role_select" name="roles[]">
                                                                @foreach($roles as $role)
                                                                <option @if($admin->hasRole($role)) selected @endif value="{{$role->id}}">{{$role->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                       &nbsp;
                                                   </div>


                                                   <div class="col-md-11" style="margin: 0 auto;">
                                                   <div class="form-group">
                                                    <div class="col-md-3"><label class="control-label">Permissions</label></div>
                                                   
                                                        <select multiple="multiple" class="multi-select" id="permission_select" name="permissions[]">
                                                            @foreach($permissions as $permission)
                                                            <option @if($admin->hasDirectPermission($permission)) selected @endif  value="{{$permission->id}}">{{$permission->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                               <div class="col-md-11">
                                                    <div class=" pull-right">
                                                     <button class="btn btn-info" type = "Submit" name = "submit">Save</button>
                                                                {!! Form::close() !!}
                                                     <a href="{{ route('adminResource.index')}}" class="btn btn-success" type="reset">Cancel</button></a>
                                                 </div>
                                             </div>
                                        
                                         </div>
                              
                                     </div>
                                 </div>
                             </div>
                             @endsection


                             @stack('post-styles')
                             <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />

                             <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
                             <link href="{{asset('assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
                             <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
                             <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />



                             @push('post-scripts')

                             <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                             <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
                             <script src="{{asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}" type="text/javascript"></script>
                             <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

                             <script type="text/javascript">

                                 $('#role_select').multiSelect();
                                 $('#permission_select').multiSelect();
                             </script>

                             <script type="text/javascript">
                                function readURL(input) {

                                    if (input.files) {
                                        var reader = new FileReader();
                                        var totalfiles = input.files.length;
                                        for(i = 0; i < totalfiles; i ++){
                                            reader.onload = function(e) {
                                                $('#blah').attr('src', e.target.result);
                                            }
                                            reader.readAsDataURL(input.files[i]);
                                        }
                                    }
                                }
                                $("#images").change(function() {
                                    readURL(this);
                                });
                            </script>
                            <script type="text/javascript">
                                $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                            if (input.files) {
                                var filesAmount = input.files.length;

                                for (i = 0; i < filesAmount; i++) {
                                    var reader = new FileReader();

                                    reader.onload = function(event) {
                                        $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    }

                                    reader.readAsDataURL(input.files[i]);
                                }
                            }

                        };

                        
                    });
                </script>


                @endpush 