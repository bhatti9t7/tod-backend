<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')


<div class="m-portlet m-portlet--mobile">


        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Booking Info
                    </h3>
                </div>
            </div>
        </div>
        @component('_components.alerts-default')
        @endcomponent
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit bordered" style="border: 1px solid orange !important;">

           
            <!-- BEGIN FORM-->
            <div class="container" style="background: white;">
                <div class="row">
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="col-md-6">
                           <div class="form-group">
                            <label>User Name</label>
                            <div class="input-icon right">
                                <input type="text" class="form-control input-circle" value="{{$ride->user->fname}} {{$ride->user->lname}}" placeholder="User Name"> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6"> 
                        <div class="form-group">
                            <label>Captain Name</label>
                            <div class="input-icon right">
                                <input type="text" class="form-control input-circle" value="{{$ride->captain->fname}} {{$ride->captain->lname}}" >
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="col-md-6">
                       <div class="form-group">
                        <label>Vehicle </label>
                        <div class="input-icon right">
                            <input type="text" class="form-control input-circle" value="{{ $Booking->vehicleSubCategorie->vehiclecategorie->name}}" placeholder="Vehicle"> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label>Goods Name</label>
                        <div class="input-icon right">
                            <input type="text" class="form-control input-circle" value="{{ $ride->booking->good->good_name}}" placeholder="Goods Name"> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="col-md-6"> 
                    <div class="form-group">
                        <label>Pickup Place</label>
                        <div class="input-icon right">
                            <input type="text" class="form-control input-circle" value="{{ $ride->booking->pickup_place}}" placeholder="Pickup Place"> 
                        </div>
                    </div>
                </div>
                <div class="col-md-6"> </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
                <div class="adres_heading" style="background: #FFB226;padding-top: 2px; padding-bottom: 4px !important;"> <h2 style="text-align: center; color: #fff;"> Address</h2>
                </div>
            </div>
           
            <div class="col-md-6"> 
                <div class="form-group">
                    <label>Labour loading</label>
                    <div class="input-icon right">
                        <input type="text" class="form-control input-circle" value="{{ $ride->booking->labour_unloader}}" placeholder="Payment"> 
                    </div>
                </div>
            </div>
            <div class="col-md-6"> 
                <div class="form-group">
                    <label>Labour Unloading</label>
                    <div class="input-icon right">
                        <input type="text" class="form-control input-circle" value="{{ $ride->booking->labour_unloader}}" placeholder="Payment"> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
               <div class="form-group">
                <label>Distance </label>
                <div class="input-icon right">
                    <input type="text" class="form-control input-circle" value="{{ $ride->booking->distance}}" placeholder="Distance"> 
                </div>
            </div>
        </div>
        <div class="col-md-6"> 
            <div class="form-group">
                <label>Amount</label>
                <div class="input-icon right">
                    <input type="text" class="form-control input-circle"  value="{{ $ride->booking->amount}}" placeholder="Amount"> 
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
       <div class="col-md-6"> 
                <div class="form-group">
                    <label>Payment</label>
                    <div class="input-icon right">
                        <input type="text" class="form-control input-circle" value="{{ $ride->booking->PaymentMethod}}" placeholder="Payment"> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
            <div class="col-md-12">
                <div class="adres_heading" style="background: #FFB226;padding-top: 8px; padding-left: 25px; padding-bottom: 10px !important;"> <h2 style="text-align: center; color: #fff;"> Destination </h2>
                </div>
            </div>
           
            <div class="col-md-6"> 
                <div class="form-group">
                    <label>drop Off</label>
                    <div class="input-icon right">
                        @foreach ($destination as $drop)
                        <input type="text" class="form-control input-circle" value="{{ $drop['drop_place'] }}" >
                        @endforeach 
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>


@endsection
