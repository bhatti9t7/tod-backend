<?php
/**
 * Project: loader.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">


    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                   Booking Record 
               </h3>
           </div>
       </div>
   </div>
   @component('_components.alerts-default')
   @endcomponent

   <div class="m-portlet__body">

    <!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
            <div class="col-xl-12 order-2 order-xl-1">
                <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-4">
                         <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                <span>
                                    <i class="la la-search"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="btn-group" style="margin-right: 5px; margin-bottom:5px ">
                            <a href="{{ URL::to('admin/bookingExcel/xls') }}" style="margin-right: 5px;"><button class="btn btn-success"> Excel xls</button></a>
                            <a href="{{ URL::to('admin/bookingExcel/xlsx') }}"  style="margin-right: 5px;"><button class="btn btn-success"> Excel xlsx</button></a>
                            <a href="{{ URL::to('admin/bookingExcel/csv') }}"><button class="btn btn-success"> CSV</button></a>
                          
                        </div>
                        
                </div>
            </div>
        </div>

    </div>
</div>
<!--end: Search Form -->

<div class="m_datatable" id="child_data_ajax"></div>
</div>


</div>


@endsection


@push('post-scripts')

<script type="application/javascript">

    var datatable = $('.m_datatable').mDatatable({

    // datasource definition
    data: {
        type: 'remote',


        source: {
            read: {
                url: BASE_URL+'/admin/booking_data'
            }
        },
    pageSize: 10, // display 20 records per page
    saveState: {
        cookie: false,
        webstorage: false
    },
    serverPaging: true,
    serverFiltering: true,
    serverSorting: true
},

    // layout definition
    layout: {
        theme: 'default',
        scroll: false,
        height: null,
        footer: false
    },

    // column sorting
    sortable: true,
    dom: 'lBfrtip',
    buttons: [
    {
        extend: 'collection',
        text: 'Export',
        buttons: [
        'copy',
        'excel',
        'csv',
        'pdf',
        'print'
        ]
    }
    ],
    pagination: true,

    detail: false,

    search: {
        input: $('#generalSearch')
    },
    dom: 'Bfrtip',
    buttons: [
    'colvis',
    'excel',
    'print'
    ],
    

    // columns definition
    columns: [ 
    {
        field: "id",
        title: "#Id ",
    // responsive: {hidden: 'md'}

},
{
    field: "user_id",
    title: "User Name",
    template: function (row) {
        return row.user.fname+" "+row.user.lname;
    }
},

{
    field: "good_id",
    title: "Good Name ",
    template: function (row) {
        return row.good.good_name;
    }
},
{
    field: "created_at",
    title: "Date/time ",

},
{
    field: "pickup_place",
    title: "PickUp Place",

},
{
    field: "amount",
    title: "Amount ",
},
{
    field: "distance",
    title: "Distance ",

},

{
    field: "status",
    title: "Status",
    template: function (status) {
        return status!==1?'Accepted':'not accepted';
    }
},
{
    field: "Actions",
    width: 110,
    title: "Actions",
    sortable: false,
    overflow: 'visible',
    template: function (row) {


        return '\
        <a href="admin/bookings/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="show details">\
        <i class="la la-edit"></i>\
        </a>\
        ';
    }
}
]
});

</script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('.m_datatable').Datatable( {
            dom: 'Bfrtip',
            buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
            ]
        } );
    } );
</script>


@endpush
