
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

<div class="page-content-wrapper">
  <div class="page-content">
    <div class="portlet light">
      <div class="portlet-title">
        <div class="caption">
          <span class="caption-subject font-green-sharp bold uppercase">
            <!--   Add new -->
          </span>
        </div>
      </div><!--portlet-title ends-->
      {!! Form::open(['method' => 'PATCH','files'=>true, 'route' => ['captainResource.update',$captain->id], 'class'=>'form-horizontal']) !!}
      {{ csrf_field() }}
      <div class="form-body">
        <div class="panel-group">
          <div class="panel panel-primary basic_info_panel">
            <div class="panel-heading" style="padding: 13px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4      style="width:100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
            text-transform: uppercase;">  Basic Info</b></h4>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-lg-3 col-md-3 col-sm-12 pull-left">
                <div class="fileinput fileinput-new" data-provides="fileinput" >
                  <div class="fileinput-new thumbnail" style="width: 250px; height: 250px; border-radius:">
                    <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                      @if(!empty($captain->images))
                      <img src="{{asset('images/captain/profiles/' . $captain->images)}}" id="profile-img-tag" height="250" width = "250">
                      @else                              
                      <img src="{{asset('images/captain/profiles/no-image.png')}}" id="ownrprofile" height="150" width="150">
                      @endif
                      <!-- </div> -->
                    </div>
                  </div>
                  <div class="form-group col-md-3">
                   <label for="images" class="btn btn-primary">Upload Profile</label>
                   <input type="file" id="images" name="images" class="hide" style="opacity: 0;">
                 </div>
                 <div class="col-md-4">
                  <div class="form-group" style="padding: 0px; ">
                    <div class = "gallery"></div>
                  </div>
                </div>
              </div>
              <div class="col-md-offset-1"></div>
              <div class="col-lg-8 col-md-8 col-sm-12 pull-right">
                <div class="row">
                  <div class="col-md-6" style="float: left;">
                   <label class="control-label">First Name</label>
                   <input name="fname" type="text" value="{{ $captain->fname }}" placeholder="First Name" class="form-control">
                   @if ($errors->has('fname'))
                   <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('fname')}}
                  </div>
                  @endif
                </div>
                <div class="col-md-6" style="float: left;">
                 <label class="control-label">Last Name</label>
                 <input type="text" name="lname" value="{{ $captain->lname }}" placeholder="Last Name" class="form-control">
                 @if ($errors->has('lname'))
                 <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                  </button>
                  <strong>Warning!</strong> {{$errors->first('lname')}}
                </div>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-6" style="float: left;">
                <label class="control-label">Email</label>
                <input name="email" type="email" value="{{ $captain->email }}" class="form-control" placeholder="abc123@example.com">
                @if ($errors->has('email'))
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                  </button>
                  <strong>Warning!</strong> {{$errors->first('email')}}
                </div>
                @endif
              </div>
              <div class="col-md-6" style="float: left;">
                <label class="control-label">phone</label>
                <input type="text" name="phone" value="{{ $captain->phone }}" placeholder="Mobile No" class="form-control">
                @if ($errors->has('phone'))
                <div class="alert alert-danger" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                  </button>
                  <strong>Warning!</strong> {{$errors->first('phone')}}
                </div>
                @endif

              </div>
            </div> 
            <div class="row">
              <div class="col-md-6" style="float: left;">
                <label class="control-label">Password</label>
                <input type="password" placeholder="******" class="form-control" name="password">
              </div>
              <div class="col-md-6" style="float: left;">
                <label class="control-label">Confirm Password</label>
                <input name="password_confirmation" type="password" placeholder="******" class="form-control">
              </div>
            </div> 
            <div class="row">
              <div class="col-md-6" style="float: left;">
               <label class="control-label">City</label>
               <input name="city" type="text" value="{{ $captain->city }}" placeholder="City" class="form-control">
             </div>
             <div class="col-md-6" style="float: left;">
               <label class="control-label">Captain Cnic</label>
               <input name="captain_cnic" value="{{ $captain->captain_cnic }}" type="text" placeholder="captain_cnic" class="form-control">
             </div>
           </div> 
           <div class="row">
            <div class="col-md-6" style="float: left;">
              <label class="control-label">Remarks of Captain</label>
              <input name="remarks_captain" value="{{ $captain->remarks_captain }}" type="text" placeholder="mole on neck" class="form-control">
            </div>
            <div class="col-md-6"></div>
          </div> 
        </div>
      </div>

    </div>
  </div>




  <div class="panel panel-primary basic_info_panel">
    <div class="panel-heading" style="padding: 13px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="     width: 100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
    text-transform: uppercase;">  Vehicle Info</b></h4>
  </div>
  <div class="panel-body">
   <div class="col-md-12">
     <div class="row">
       <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">Vehicle Registration No</label>
        <input name="vehicle_registration" type="text" value="{{$captain->vehicle_registration}}" class="form-control" placeholder="Registration No">
      </div>  
      <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">Vehicle Name</label>
        <select class="form-control" name="vehical_cat_id" value="{{ $captain->vehical_cat_id }} " style="width: 100%;">
          @foreach ($vehicals as $i)
          <option value="{{$i->id}}" @if($i->id==$captain->vehical_cat_id) selected='selected' @endif>{{$i->name}} </option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">Vehicle Categories</label>
        <select class="form-control" name="vehical_sub_id" >
          <option value="{{$captain->vehical_sub_id}}" >@isset($captain->VehicalSubCategorie){{$captain->VehicalSubCategorie->name}}@endisset </option>
        </select> 
      </div>
    </div>    
    <div class="row">                                     
      <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">License No</label>
        <input type="text" name="licence_no" placeholder="License No" value="{{$captain->licence_no}}"  class="form-control">
      </div> 
      <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">Vehicle No</label>
        <input type="text" name="vehicle_no" placeholder="Vehicle No" value="{{$captain->vehicle_no}}"  class="form-control">
      </div>                         
      <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">Chesis No</label>
        <input type="text" placeholder="Chesis No" class="form-control" value="{{$captain->chesi_no}}"  name="chesi_no">
      </div>
    </div>
    <div class="row">
      <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">Engine No</label>
        <input name="engin_no" type="text" placeholder="Engine No" value="{{$captain->engin_no}}"  class="form-control"> 
      </div>
      <div class="form-group col-md-4" style="float: left;">
        <label class="control-label">Remarks Vehicle</label>
        <input name="remarks_vehicle" type="text" placeholder="white color" value="{{$captain->remarks_vehicle}}"  class="form-control"> 
      </div>
    </div>
  </div>       
</div> 
</div>


<div class="panel panel-primary basic_info_panel">
  <div class="panel-heading" style="padding: 13px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
  text-transform: uppercase;">Owner Info</b></h4></div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-4 pull-left">
        <div class="col-md-3" style="margin-left: 10px;">
         <div class="fileinput fileinput-new" data-provides="fileinput">
          <div class="fileinput-new thumbnail" style="width: 200px; height:180px;">
            <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
              @if(!@empty($captain->owner_image)):
              <img src="{{asset('owner_image/captain/owner_image/' . $captain->owner_image)}}" id="ownrprofile" height="150" width="150">
              @else:
              <img src="{{asset('images/captain/profiles/no-image.png')}}" id="ownrprofile" height="150" width="150">
              @endif;
              <!-- </div> -->
            </div>
          </div>
          <div class="form-group col-md-4" >
           <label for="owner" class="btn btn-primary">Upload Profile</label>
           <input type="file" id="owner"  name="owner_image" class="hide" style="opacity: 0;">
         </div>
         <!-- cnic image upload here -->
         <div class="col-md-4">
          <div class="form-group" style="padding: 0px; ">
            <div class="owner_image"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group col-md-8 pull-right">
      <div class="row">                                
        <div class="form-group col-md-6" style="float: left;">
          <label class="control-label">Owner Name</label>
          <input name="owner_name" type="text" placeholder="Vehicle Name" value="{{$captain->owner_name}}"  class="form-control">
        </div>
        <div class="form-group col-md-6" style="float: left;">
         <label class="control-label">Owner Father Name</label>
         <input name="owner_fname" type="text" placeholder="Vehicle Name" value="{{$captain->owner_fname  }}"  class="form-control">
       </div> 
     </div>        
     <div class="row">                         
      <div class="form-group col-md-6" style="float: left;">
        <label class="control-label">CNIC No#</label>
        <input name="owner_cnic" type="text" placeholder="35101-65088345-0" value="{{$captain->owner_cnic }}"  class="form-control">
      </div>
      <div class="form-group col-md-6" style="float: left;">
        <label class="control-label">Bank </label>
        <input name="bank" type="text" placeholder="bank" value="{{$captain->bank }}"  class="form-control">
      </div>
    </div>
    <div class="row">         
      <div class="form-group col-md-6" style="float: left;">
        <label class="control-label">Branch Code </label>
        <input name="branch_code" type="text" placeholder="branch_code " value="{{$captain->branch_code }}"  class="form-control">
      </div>
      <div class="form-group col-md-6" style="float: left;">
        <label class="control-label">Account No </label>
        <input name="account_no" type="text" placeholder="account no " value="{{$captain->account_no}}"  class="form-control"> 
      </div>
    </div>
    <div class="row">         
      <div class="form-group col-md-6" style="float: left;">
        <label class="control-label">Remarks Owner</label>
        <input name="remarks_owner" type="text" placeholder="mole on chin" value="{{$captain->remarks_owner}}"  class="form-control"> 
      </div>
    </div>
  </div>
</div>
</div>
</div>       


<div class="panel panel-primary basic_info_panel">
  <div class="panel-heading" style="padding: 13px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey   ;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
  text-transform: uppercase;">All Documents</b></h4>
</div>
<div class="panel-body">
  <div class="row"">
    <div class="col-md-4" style="float: left;">
      <div class="clsbox-1" runat="server">
        <div class="dropzone clsbox" id="license_file_dropzone">
          <div class="dz-message" data-dz-message><span>Click here or drag/drop License to upload.</span></div>
        </div>
      </div>
    </div>
    <div class="col-md-4" style="float: left;">
      <div class="clsbox-1" runat="server">
        <div class="dropzone clsbox" id="cnic_file_dropzone">
          <div class="dz-message" data-dz-message><span>Click here or drag/drop CNIC to upload.</span></div>
        </div>
      </div>
    </div>
    <div class="col-md-4" style="float: left;">
      <div class="clsbox-1" runat="server">
        <div class="dropzone clsbox" id="vehicle_reg_file_dropzone">
          <div class="dz-message" data-dz-message><span>Click here or drag/drop Vehicle Registration to upload.</span></div>
        </div>
      </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4" style="float: left;"></div>
    <div class="col-md-4 text-center" style="float: left;margin-top: 20px;margin-bottom: 20px;">
      <div class="clsbox-1" runat="server">
        <div class="dropzone clsbox" id="vehicle_file_dropzone">
          <div class="dz-message" data-dz-message><span>Click here or drag/drop Vehicle to upload.</span></div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="panel panel-primary basic_info_panel">
  <div class="panel-heading" style="padding: 13px 0px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey;"><h4 style="width: 100%;margin-left: 10px;"> <i class="fa fa-car" style="color: #fff;"> </i> <b style="color: #fff!important;
  text-transform: uppercase;">Status</b></h4></div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-6" style="padding-top: 8px;float: left;">
        <label class="control-label" style="padding-top: 0px !important">Status: <span class="req">*</span></label>
        <div style="padding-top: 20px">
          <label style="display:inline; padding-right: 35px">
            <input name="status" id="facility_used_for_active"  class="icheck facility_status" type="radio" value="1"  @if($captain->status==1 )checked @endif> Active </label>
            <label style="display:inline;">
              <input name="status" id="facility_used_for_inactive" class="icheck facility_status" type="radio" value="0"  @if($captain->status==0 )checked @endif>  Inactive </label>
            </div>
          </div>

          <div class="col-md-6" style="float: left;">
            <label class="control-label">Description: <span class="req"></span></label>
            <textarea  class="form-control" name="description" id="document_description" maxlength="150">{{$captain->description }}</textarea>
            <br>
          </div><!--form-group ends-->
        </div>
      </div><!-- end of panel group -->
    </div>
    <div class="form-actions">
      <div class="row">
        <div class="col-md-offset-3 col-md-9">

         <button class="btn btn-primary" type="Submit" name="submit">Save</button>
       </form>
       <a href="{{ route('captainResource.index')}}"class="btn  btn-primary" type="reset">Cancel</a>
     </div>
   </div>
 </div><!--form-actions ends-->
</div><!--form-body ends-->
</div>
@endsection
@stack('post-styles')

<style type="text/css">
.form-actions {
  padding: 20px 20px;
  margin: 0;
  background-color: #f5f5f5;
  border-top: 1px solid #e7ecf1;
}
.panel-group .panel{
  overflow: hidden !important;
}
</style>
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />

@push('post-scripts')



<script type="text/javascript">





  <?php
  function get_mockfiles($folder_name="",$_files=""){
    $mockFiles = array();
    if(!empty($_files)){
      $files = explode(',', $_files);
      foreach ($files as $file) {
       $mockfile = array();
       if(file_exists("images/captain/".$folder_name."/".$file))
         $filesize= filesize("images/captain/".$folder_name."/".$file);
       else
        $filesize= 0;
      $ext_parts = explode('.',$file);
      $ext=end($ext_parts);
      $mockfile['name']= $file;
      $mockfile['size']= $filesize;
      $mockfile['isMock']= true;
      $mockfile['dataURL']= asset("images/captain/".$folder_name."/".$file);
      $mockFiles[] = $mockfile;
    }
  }
  return $mockFiles; 
}
?>


var captain_id = <?php echo $captain->id ?>;
var l_mock_Files = <?php 
if(!empty($captain->driving_licenes_images))
  echo json_encode(get_mockfiles("licenses",$captain->driving_licenes_images));
else
  echo json_encode(array());
?>;
CustomDropZone("license_file_dropzone","licenses","driving_licenes_images",l_mock_Files,captain_id);
var d_mock_Files = <?php 
if(!empty($captain->cnic_images))
  echo json_encode(get_mockfiles("cnics",$captain->cnic_images)); 
else
  echo json_encode(array());
?>;
CustomDropZone("cnic_file_dropzone","cnics","driving_cnic_images",d_mock_Files,captain_id);
var vr_mock_Files = <?php 
if(!empty($captain->vehicle_registration_image))
  echo json_encode(get_mockfiles("registration_image",$captain->vehicle_registration_image)); 
else
  echo json_encode(array());
?>;
CustomDropZone("vehicle_reg_file_dropzone","registration_image","vehicle_registration_image",vr_mock_Files,captain_id);
var vi_mock_Files = <?php 
if(isset($captain->vehicle_image) && !empty($captain->vehicle_image))
  echo json_encode(get_mockfiles("vehicle_image",$captain->vehicle_image));
else
  echo json_encode(array());
?>;
CustomDropZone("vehicle_file_dropzone","vehicle_image","vehicle_image",vi_mock_Files,captain_id);


function  CustomDropZone(drop_zone_id,folder_name,field_name,mockFiles,captain_id) {
  Dropzone.autoDiscover = false;
  var temp_files=[];
  var uploaded_files = [];
  var myDropzone = new Dropzone('#'+drop_zone_id, {
    url: "{{ route('upload_document_file') }}",
    acceptedFiles: "image/*,application/pdf",
    maxFilesize: 50,
    uploadMultiple: true,
    createImageThumbnails: true,
    addRemoveLinks: true,
    maxFiles: null,
    processQueue: true,
    autoProcessQueue: true,
    removedfile: function(file){
     var file_name = file.previewElement.querySelector("[data-dz-name]").innerHTML;
     //var file_name = file.name;
     $.ajax({
       type: 'POST',
       url: "{{ route('remove_document_file') }}",
       data: {'captain_id':captain_id,'folder_name':folder_name,file: file_name,'_token': $('meta[name="csrf-token"]').attr('content')},
       success: function(response){
        file.previewElement.remove();
        uploaded_files.pop(file_name);
        //$('#'+field_name).val(uploaded_files);
        //$('#license_file').val('');
      }
    });

   },
   success: function(file,response){
    if(response.status){
      temp_files.push(file);
      if(temp_files.length==response.files_names.length){
        temp_files.forEach(function(row,index){
          var fileuploded = row.previewElement.querySelector("[data-dz-name]");
          fileuploded.innerHTML = response.files_names[index];
          uploaded_files.push(response.files_names[index]);
          //$('#'+field_name).val(uploaded_files);
        });
      }
      //file.name = response.files_name;
      //var fileuploded = file.previewElement.querySelector("[data-dz-name]");
      //fileuploded.innerHTML = response.files_name;
      //console.log(file,response);
    }
  },
  init: function () {
    var my_dropzone = this;
    if(mockFiles!=undefined  && mockFiles!=null && mockFiles.length){
     mockFiles.forEach(function(mockfile){ 
       my_dropzone.files.push(mockfile);
       my_dropzone.emit("addedfile", mockfile);
       my_dropzone.createThumbnailFromUrl(mockfile,
        my_dropzone.options.thumbnailWidth, 
        my_dropzone.options.thumbnailHeight,
        my_dropzone.options.thumbnailMethod, true, function (thumbnail) 
        {
          my_dropzone.emit('thumbnail', mockfile, thumbnail);
        });
       my_dropzone.emit('complete', mockfile);  


       var a = document.createElement('a');
       a.setAttribute('href',mockfile.dataURL);
       a.setAttribute('target',"_blank");
       a.setAttribute('class', 'dz-remove');
       a.innerHTML = "<br>Download";
       mockfile.previewTemplate.appendChild(a);
       my_dropzone.removeEventListeners();

     });

   } 

   this.on('addedfile', function(file) {
    var ext = file.name.split('.').pop();
    if (ext == "pdf") {
      $(file.previewElement).find(".dz-image img").attr("src", "{{asset('images/pdf.png')}}");
      $('#file_ext').hide();
    }
    else if(ext == "doc" || ext == "docx") {
      $(file.previewElement).find(".dz-image img").attr("src", "{{asset('images/docx.png')}}");
      $('#file_ext').hide();
    }
    else{
      $('#file_ext').show();
    } 
  });

   this.on("thumbnail", function(file, dataUrl) {
    $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
  });
   this.on('sending', function(file, xhr, formData){
     temp_files = [];
     formData.append('folder_name',folder_name);
     formData.append('captain_id',captain_id);
     formData.append('_token',$('meta[name="csrf-token"]').attr('content'));
   });
 }

});

  return myDropzone;
}


</script>


<script type="text/javascript">
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#profile-img-tag').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#images").change(function(){
    readURL(this);
  }); 

</script>


<script type="text/javascript">
  $('#owner').change(function(){
    var curElement = $(this).parent().parent().find('#ownrprofile');
    console.log(curElement);
    var reader = new FileReader();

    reader.onload = function (e) {
        // get loaded data and render thumbnail.
        curElement.attr('src', e.target.result);
      };

    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });
</script>
<script type="text/javascript">
  $("[name='vehical_cat_id']").on("change",function(){
    // $("#city").show();
    $("[name='vehical_sub_id']").html(` <option > Choose the Category  </option>`);
    var vehicle  = $("#vehicle").val();

    $.ajax({
      method:"get",
      url:"{{route('vehicle-categories_data')}}",
      data : {id:vehicle},
      dataType:"json",
      success:function(data){
                            // console.log(data);
                            data.forEach(function(val,ind){

                             var id = val.id;
                             var name = val.name;
                             var option = `<option value="${id}">${name}</option>`;

                             $("[name='vehical_sub_id']").append(option);
                           });



                          }
                        });

  })
</script>

@endpush  