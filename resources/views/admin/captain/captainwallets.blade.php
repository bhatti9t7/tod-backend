<?php
/**
 * Project: loader.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

<?php $total_amount = (($wallet->credit_amount) - ($wallet->debit_amount));  ?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">

                            <span class="caption-subject font-red sbold uppercase">{{ $wallet->captain->fname }} {{ $wallet->captain->lname }}</span> <small style="margin-left: 5px; font-size: 12px;"> Be Your Own Bank</small> <br>

                        </div>
                        <div class="actions">

                            <h3 style="margin: 0px;"> Total Amount: {{ $wallet->amount }} </h3> 
                            <p style="float: right"> Current Amount: {{ number_format($total_amount, 2) }}</p>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a  href="{{route('captain-wallet.edit', $wallet['id'])}}"   class="btn green"  > Cash Withdraw
                                            <i class="fa fa-cash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">

                                    </div>
                                </div>
                            </div>

                        </div>
                        <table class="table table-striped table-hover table-bordered" id="captain-table">
                            <thead>
                                <tr>
                                   <th align="left">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                        <span></span>
                                        </label>
                                    </th>

                                    <th> name </th>
                                    <th> transection </th> 
                                    <th> date </th>
                                    <th> Status </th>
                                    <th> Commission Amount </th>
                                    <th> Credit Amount </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="checkboxes" value="1" />
                                            <span></span>
                                        </label>
                                    </td>

                                    <td> {{ $wallet->captain->fname }}</td>
                                    <td> withdraw </td>
                                    <td> {{ $wallet->updated_at }} </td>

                                    @if($wallet->status === 0 )
                                    <td>Deactive</td>
                                    @else
                                    <td> Active </td>
                                    @endif


                                    <td> {{ $wallet->debit_amount }} </td>
                                    <td> {{ $wallet->credit_amount }} </td>
                                </tr>
                            </tbody>
                        </table>

            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="dataTables_info" id="sample_editable_1_info" role="status" aria-live="polite"></div>
                </div>
                <div class="col-md-7 col-sm-7">
                    <div class="dataTables_paginate paging_bootstrap_number" id="sample_editable_1_paginate">
                        <ul class="pagination" style="visibility: visible; float: right;">

                        </ul>
                    </div>
                </div>
            </div>
            <!-- here Ride table is start .............. -->

            <!-- here Ride table is end .............. -->
        </div>
    </div>

            
       


</div>
</div>
  <!-- END EXAMPLE TABLE PORTLET-->
</div>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-head">

            <div class="page-title">

            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light portlet-fit bordered" style="margin-top:-265px;">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Captain Ride Detail</span>
                        </div>
                        <div class="actions">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                 <div class="col-md-6 col-sm-6">
                                    <div id="lets_search" class="dataTables_filter">
                                        <label for="search" >Search:<input type="search" class="form-control input-sm input-small input-inline" placeholder="" onkeyup="search" id="search" aria-controls="search"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="btn-group pull-right">

                                </div>
                            </div>
                        </div> -->

                    </div>
                    <table class="table table-striped table-hover table-bordered" id="booking-table">
                       <thead>
                        <tr>
                         <th align="left">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                <span></span>
                            </label>
                        </th>
                         <th> #id </th>
                        <th> user Name </th>
                        <th> Pickup Place </th> 
                        <th> Booking date/time </th>
                        <th> amount </th>
                        <th> distance </th>
                        <th> payment  </th>
                        <th> Status </th>                        
                        <th> Drop off </th>
                    </tr>
                </thead>
                <tbody>

                 @foreach ($captain as $i) 
                 <tr>

                  <td align="center">
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="1" />
                        <span></span>
                    </label>
                </td>
                <td> {{ $i->id }} </td>
                <td> {{ $i->user->fname }} </td>
                <td> {{ $i->booking->pickup_place }}  </td>
                <td>{{ $i->booking->created_at }}</td>
                <td>{{ $i->booking->amount }}</td>
                <td>{{ $i->booking->distance }}</td>
                <td>{{ $i->booking->PaymentMethod }}</td>
                @if($i->status == 0 )
                <td>complete</td>
                @else
                <td>pending</td>
                @endif

                
                 <td>
                     <a href="{{route('Rides.show', $i['id'])}}"><button class="btn btn-primary btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" ><span class="glyphicon glyphicon-eye-open"></span>show</button></a>
                </td> 



            </tr>
            @endforeach

        </tbody>
    </table>

    <div class="row">
        <div class="col-md-5 col-sm-5">
            <div class="dataTables_info" id="sample_editable_1_info" role="status" aria-live="polite"></div>
        </div>
        <div class="col-md-7 col-sm-7">
            <div class="dataTables_paginate paging_bootstrap_number" id="sample_editable_1_paginate">
                <ul class="pagination" style="visibility: visible; float: right;">
                  
                </ul>
            </div>
        </div>
    </div>
</div>
</div>

</div>


<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Account</h4>
        </div>
        <div class="modal-body">
            <div class="tab-pane active" id="tab_0">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                        <i class="fa fa-gift"></i>Cash Withdraw  
                        </div>
                        <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>

                        </div>
                    </div>
                    <div class="portlet-body form">

                        <input type="hidden" name="id" value="{{$wallet->id}}">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Total Amount</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control input-circle" placeholder="Total Amount" name="good_name" >
                                </div>
                            </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Withdraw Amount</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-circle" placeholder="Total Amount" name="good_name" >

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Current Amount</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control input-circle" placeholder="Total Amount" name="good_name" readonly >

                            </div>
                        </div>
                    </div> 
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-circle green">Submit</button>
                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      </div>
  </div>
</div>




@endsection       <!-- BEGIN FOOTER -->


@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />



 @push('post-scripts')


<script src="{{ asset('assets/apps/scripts/table-datatables-custom-managed.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/apps/scripts/table-datatables-custom-managed.js') }}" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript"></script>

    @endpush 

