@extends('layout.admin.headerAdmin')
@section('content')
  <div class="page-content-wrapper">
                <div class="page-content" style="min-height: 1279px;">
                    <div class="page-head">
                        <div class="page-title">
                            <h1>Add User</h1>
                        </div>
                        <div class="page-toolbar">
                            <div class="btn-group btn-theme-panel">
                                <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                                </a>
                                <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <h3>HEADER</h3>
                                            <ul class="theme-colors">
                                                <li class="theme-color theme-color-default active" data-theme="default">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Dark Header</span>
                                                </li>
                                                <li class="theme-color theme-color-light " data-theme="light">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Light Header</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12 seperator">
                                            <h3>LAYOUT</h3>
                                            <ul class="theme-settings">
                                                <li> Theme Style
                                                    <select class="layout-style-option form-control input-small input-sm">
                                                        <option value="square">Square corners</option>
                                                        <option value="rounded" selected="selected">Rounded corners</option>
                                                    </select>
                                                </li>
                                                <li> Layout
                                                    <select class="layout-option form-control input-small input-sm">
                                                        <option value="fluid" selected="selected">Fluid</option>
                                                        <option value="boxed">Boxed</option>
                                                    </select>
                                                </li>
                                                <li> Header
                                                    <select class="page-header-option form-control input-small input-sm">
                                                        <option value="fixed" selected="selected">Fixed</option>
                                                        <option value="default">Default</option>
                                                    </select>
                                                </li>
                                                <li> Top Dropdowns
                                                    <select class="page-header-top-dropdown-style-option form-control input-small input-sm">
                                                        <option value="light">Light</option>
                                                        <option value="dark" selected="selected">Dark</option>
                                                    </select>
                                                </li>
                                                <li> Sidebar Mode
                                                    <select class="sidebar-option form-control input-small input-sm">
                                                        <option value="fixed">Fixed</option>
                                                        <option value="default" selected="selected">Default</option>
                                                    </select>
                                                </li>
                                                <li> Sidebar Menu
                                                    <select class="sidebar-menu-option form-control input-small input-sm">
                                                        <option value="accordion" selected="selected">Accordion</option>
                                                        <option value="hover">Hover</option>
                                                    </select>
                                                </li>
                                                <li> Sidebar Position
                                                    <select class="sidebar-pos-option form-control input-small input-sm">
                                                        <option value="left" selected="selected">Left</option>
                                                        <option value="right">Right</option>
                                                    </select>
                                                </li>
                                                <li> Footer
                                                    <select class="page-footer-option form-control input-small input-sm">
                                                        <option value="fixed">Fixed</option>
                                                        <option value="default" selected="selected">Default</option>
                                                    </select>
                                                </li>   
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="captain_form_wrapper">
            <div class="container">
                <div class="row ">
    
                    <div class="col-md-12 add_user_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae;padding-bottom: 5px;"> Captain Form</h1>
                         <div class="tab-pane active">
                               {!! Form::open(['method' => 'PATCH', 'route' => ['captainResource.update',$captain->id], 'class'=>'form-horizontal']) !!}
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="fname" value="{{$captain->fname}}"> 
                                            
                                            <span class="help-block">Please write your firts name here </span>
                                            
                                        </div>
                                    </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="lname" value="{{$captain->lname}}">
                        
                                            <span class="help-block">Please write your last name here </span>
                                            
                                        </div>
                                     </div>

                                      <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="email" class="form-control" name="email" value="{{$captain->email}}">
                            
                                            <span class="help-block">Please write your Email Adress here </span>
                                            
                                        </div>
                                     </div>

                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="phone" value="{{$captain->phone}}">
                              
                                            <span class="help-block">+92313******* </span>
                                           
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="password" class="form-control" name="password">
                           
                                            <span class="help-block"> Please enter your password here </span>
                                           
                                        </div>
                                     </div>
                                      <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="password" class="form-control" name="password_confirmation">
                                   
                                            <span class="help-block"> Please confirm your password here </span>
                                           
                                        </div>
                                     </div>

                                         <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="city" value="{{$captain->city}}">
                        
                                            <span class="help-block"> Write your city name here</span>
                                           
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6" id="input_color">
                          
                                        <input type="file" id="exampleInputFile1" name="images" value="{{$captain->images}}">
                                        <p class="help-block"> some help text here. </p>
                                    </div>

                                
                            </div>
                    </div>
                </div>
       

                <div class="row ">
                    <div class="col-md-12 captain_form_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae; padding-bottom: 5px;"> Vehical Form</h1>
                         <div class="tab-pane active">
                                
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="vehical_id" value="{{$captain->vehical_id}}">
                            
                                            <span class="help-block">Please enter your vehicle id here </span>
                                          
                                        </div>
                                    </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="vehicle_no" value="{{$captain->vehicle_no}}" >
                                            <span class="help-block">Please enter your vehicle number here </span>
                                           
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="vehicle_registration" value="{{$captain->vehicle_registration}}">
                                            <span class="help-block">Please enter your vehicle registration number here </span>
                                            
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="licence_no" value="{{$captain->licence_no }}">
                                            <span class="help-block">Please enter your license number here </span>
                                        </div>
                                     </div>


                                     <div class="form-group form-md-line-input has-success form-md-
                                        <input type="file" id="exampleInputFile1" name="driving_licenes_images" value="{{$captain->driving_licenes_images}}">
                                        <p class="help-block">Please upload your driving license image here </p>
                                    </div>
                                    <div class="form-group form-md-line-input has-success 
                                        <input type="file" id="exampleInputFile1" name="cnic_front_side_img" value="{{$captain->cnic_front_side_img}}">
                                        <p class="help-block"> Please upload your CNIC Front side image here </p>
                                    </div>
                                    <div class="form-group form-md-line-input has-success 
                                        <input type="file" id="exampleInputFile1" name="cnic_back_side_img" value="{{$captain->cnic_back_side_img}}">
                                        <p class="help-block">Please upload your CNIC Back side image here  </p>
                                    </div>

                                
                            </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12 captain_form_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae; padding-bottom: 5px;"> Owner Form</h1>
                         <div class="tab-pane active">
                               
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="owner_name" value="{{$captain->owner_name}}">
                                            
                                            <span class="help-block">Please write Owner name here </span>
                                         </div>
                                    </div>
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="owner_fname" value="{{$captain->owner_fname}}">
                        
                                            <span class="help-block">Please write Owner Father name here </span>
                                         </div>
                                    </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="owner_city" value="{{$captain->owner_city}}">
                    
                                            <span class="help-block">Please write Owner city name here </span>
                                         </div>
                                    </div>
                                    
                                
                            </div>
                    </div>
                </div>
                    <div class="row ">
                    <div class="col-md-12 captain_form_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae; padding-bottom: 5px;"> Owner Form</h1>
                         <div class="tab-pane active">
                                
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="chesi_no" value="{{$captain->chesi_no}}">
                    
                                            <span class="help-block">Please write Chesi Number here </span>
                                         </div>
                                    </div>
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="engin_no" value="{{$captain->engin_no}}">
                    
                                            <span class="help-block">Please write Engine here </span>
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                         <div class="margiv-top-10 save_btn_display">
                                            <button class="btn green" type="submit" name="submit"> Save </button> 
                                            <button class="btn default" type="reset"> Cancel </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('styles')
@parent
        <link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
@endsection