<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

</head>
<body>

        <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-head">
                        <div class="page-title">
                            <h1>Ride
                                <small></small>
                            </h1>
                        </div>
                    </div>
                    <div class="invoice">
                        <div class="row invoice-logo">
                            <div class="col-xs-6 invoice-logo-space">
                                <!-- <img src="{{ asset('assets/pages/img/loader.png')}}" class="img-responsive" alt="" /> --> </div>
                            <div class="col-xs-6">
                                <p> <strong>Joining Date </strong>  {{ $detail->created_at}}
                                    <span class="muted"> Consectetuer adipiscing elit </span>
                                </p>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-xs-4">
                                <h3>Captain:</h3>
                                <ul class="list-unstyled">
                                    <li> {{ $detail->fname}} {{ $detail->lname}} </li>
                                    <li> {{ $detail->phone}} </li>
                                    <li>{{ $detail->vehical_id}} </li>
                                    <li> {{ $detail->vehicle_no}} </li>
                                    
                                    <li> {{ $detail->city   }} </li>
                                    
                                </ul>
                            </div>
                            <div class="col-xs-4">
                                <h3>Owner:</h3>
                                <ul class="list-unstyled">
                                    <li> {{ $detail->owner_name}}  </li>
                                    <li> {{ $detail->owner_fname}}  </li>
                                    <li> {{ $detail->owner_city}}  </li>
                                    
                                </ul>
                            </div>
                            <div class="col-xs-4 invoice-payment">
                                <h3>Vehicle Details:</h3>
                                <ul class="list-unstyled">
                                    <li>
                                        <strong>licence No#:</strong> {{ $detail->licence_no}} </li>
                                    <li>
                                        <strong>chesi No#:</strong> {{ $detail->chesi_no}} </li>
                                    <li>
                                        <strong>Engin No#:</strong> {{ $detail->engin_no}} </li>
                                </ul>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th> Ride id#</th>
                                            <th> User </th>
                                            <th> pickup_place </th>
                                            <th> drop_place </th>
                                            <th> distance </th>
                                            <th> amount </th>
                                            <th> status </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($captains as $ride)
                                        <tr>
                                            <td> {{ $ride->id}} </td>
                                            <td> {{ $ride->user->fname}} {{ $ride->user->lname}} </td>
                                            <td> {{ $ride->booking['pickup_place']}} </td>
                                            <td>  {{ $ride->booking_destinations['drop_place']}} </td>
                                            <td> {{ $ride->booking['distance']}}Km </td>
                                            <td> {{ $ride->booking->amount}} </td>
                                            <td> {{ $ride->status}} </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="well">
                                    <address>
                                        <strong>Loader, Inc.</strong>
                                        <br/> Fiaz Road,near Wadahat Road Lahore
                                        <br/> 03003413778
                                        <br/>
                                        <abbr title="Phone">P:</abbr> (300) 4277528 </address>
                                    <address>
                                        <strong>Full Name</strong>
                                        <br/>
                                        <a href="mailto:#"> loader.aktechzone@email.com </a>
                                    </address>
                                </div>
                            </div>
                            <div class="col-xs-8 invoice-block">
                                <ul class="list-unstyled amounts">
                                    <li>
                                        <strong>Sub - Total amount:</strong>  </li>
                                    <li>
                                        <strong>Discount:</strong> 12.9% </li>
                                    <li>
                                        <strong>VAT:</strong> ----- </li>
                                    <li>
                                        <strong>Grand Total:</strong> $12489 </li>
                                </ul>
                                <br/>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</body>
</html>