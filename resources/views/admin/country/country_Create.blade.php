<style>
  .box_shadow_form{
    margin: 0 auto;
    padding: 8px;
    box-shadow: 0px 2px 10px #ccc;
  }
</style>
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

 <div class="m-portlet__head">
  <div class="m-portlet__head-caption head_21">
    <div class="m-portlet__head-title ">
      <h3 class="m-portlet__head-text sbold uppercase">
       <i class="fa fa-car" style="color: #fff;"></i> Add new Country  
     </h3>
   </div>
 </div>
</div>

<div class="page-content-wrapper">
  <div class="page-content">
    <div class="tab-content">
      <div class="portlet-title">
        <div class="caption">
        </div>
        <div class="portlet-body form">
          <form action="{{ route('country.store') }}" class="form-horizontal" method="POST">
            {{ csrf_field() }}
<div class="box_shadow_form">
            <div class="form-body">
              <div class="form-group">
                <label class="col-md-3 control-label">Country Name</label>
                <div class="col-md-12">
                  <input type="text" class="form-control input-circle"  name="country_name" >
                    @if ($errors->has('country_name'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('country_name')}}
                    </div>
                    @endif

                </div>
              </div>
            
            <!-- END FORM-->
          
          <div class="form-actions" style="margin: 15px;">
          <div class="col-md-12"> 
              <div class=" pull-right">
                <button type="submit" class="btn btn-success green">Submit</button>
                   </form>
                <a href="{{ route('country.index')}}" class="btn btn-success">Cancel</a> 
              </div>
              </div>
              </div>
            </div>
            <br>
          </div>
      </div>
    </div>
  </div>
</div>


@endsection

