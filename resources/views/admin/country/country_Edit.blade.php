<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

 <div class="m-portlet__head">
  <div class="m-portlet__head-caption head_21">
    <div class="m-portlet__head-title ">
      <h3 class="m-portlet__head-text sbold uppercase">
       <i class="fa fa-car" style="color: #fff;"></i> Edit Country  
     </h3>
   </div>
 </div>
</div>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">


        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                <div class="portlet box green">
                    <div class="portlet-title">
                        

                        </div>
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->


                            {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['country.update',$country_data->id], 'class'=>'form-horizontal']) !!}

                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Country Name</label>
                                    <div class="col-md-4">
                                        <input type="text" value="{{$country_data->country_name}}" class="form-control input-circle"  name="country_name" >

                                    </div>
                                </div>

                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn btn-circle green">Submit</button>
                                           </form>
                                        <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                    </div>
                                </div>
                            </div>

                     
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

@endsection

