<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered box_shadow_form">
                    <div class="portlet-title head_21">
                        <div class="caption">
                            <i class="fa fa-user-md" style="color: #fff !important"></i>
                            <span class="caption-subject font-red sbold uppercase" style="color: #fff !important">Emergency Section</span>
                        </div>
                    </div>
                      <div class = "col-md-11" style="margin: 0 auto;">
                     <form action="{{ route('emergencies.store') }}" class="form-horizontal" method="POST">
                                                        {{ csrf_field() }}
                                                
                    <div class="portlet-body">
             
                    
                         <div class="form-group">
                                <label class="control-label">Emergency Name</label>
                                  <input type="text" name = "emergencies_name" placeholder="Emergency Name" class="form-control" > 
                                 @if ($errors->has('emergencies_name'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('emergencies_name')}}
                      </div>
                      @endif</div>
                           </div>  
                     
                            <div class=" col-md-6">
                              <textarea id="editor"  name="description" rows="9" cols="40" style="resize: none;"></textarea>
                           @if ($errors->has('description'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('description')}}
                      </div>
                      @endif</div>
                         
                         
                   
                      </div>
                      <div class="form-actions">
                  
                          <div class="  pull-right">
            <button type="submit" class="btn btn-warning" style="background-color: #f26727;">Submit</button>
              </form>  
        <a href="{{route('emergencies.index')}}" class="btn btn-success ">Cancel</button>
                          </a>
                     
                      </div>  
                      <br>   
                       <br>        
                        </div> 
             </div>
        </div>
        </div>
       </div>
     </div>

<style type="text/css">
.form-actions {
  padding: 20px 20px;
  margin-top: 15px;
  margin-bottom: 0px !important;
  background-color: #f5f5f5;
  border-top: 1px solid #e7ecf1;
}
.portlet.light.bordered{
  border: 1px solid #ffb226!important;
}
.head_21{ box-shadow: 0px 4px 15px dimgrey;
          background-color: #f26727!important;
}
</style>
       @endsection
       @section('styles')
       @parent
       <link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
       <link rel="stylesheet" href="{{ asset('js/app.js') }}">
       <style type="text/css">
       #page_content_responsive{
        min-height: 1279px; margin-left: 105px !important;padding: 0px !important; width: 100%;
       }
         /*Media Queries*/
          @media (max-width: 1580px){
          #page_content_responsive{
                margin-left: 232px !important;
                padding: 0px !important;
                width: 80%;
                overflow-x: hidden !important;
          }
        }
          @media (max-width: 1199px){
          #page_content_responsive{
            margin-left: 225px !important;
            padding: 0px !important;
            width: 79%;
          }
        }
         @media (max-width: 991px){
          #page_content_responsive{
          margin-left: 0px !important;
          width: 100%;
          }
        }
            @media (max-width: 767px){
          #page_content_responsive{
          margin-left: 0px !important;
          width: 100%;
          }
        }
        @media (max-width: 580px){
          #page_content_responsive{
          margin-left: 0px !important;
          width: 100%;
          }
        }
      #btn_11{margin-left: 30%;
        padding-left: 3%;
        padding-right: 3%;

      }
       .{

       }
       
       </style>
       @endsection
       @section('javascripts')


                    <script>
                                  CKEDITOR.replace( 'editor' );
                                 CKEDITOR.config.width = '100%';

                                </script>  


                    @endsection


