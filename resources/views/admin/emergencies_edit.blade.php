<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">

                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings" style="color: #fff !important"></i>
                            <span class="caption-subject sbold uppercase" style="color: #fff !important">Emergencies Section</span>
                        </div>
                    </div>
                    {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['emergencies.update',$emergencies->id], 'class'=>'form-horizontal']) !!}
                    
                                                
                    <div class="portlet-body">
                        <div class="table-toolbar">
                 
                     <div class="col-md-12">
                  
              
                         <div class="form-group">
                                <label class="control-label">Emergencies Name</label>
                                  <input type="text" name = "emergencies_name" placeholder="Emergencies Name" value="{{$emergencies->emergencies_name}}" class="form-control" > </div>
                           </div>     
                            <div class="col-sm--12 col-md-12">
                              <textarea id="editor"  name="description" class="CKEDITOR"  rows="8" cols="52" style="resize: none;">{{$emergencies->description}}</textarea>
                            </div>
                                <div class="col-md-offset-5 col-md-4" style="margin-top: 10px;">
                              <button class="btn btn-info" type = "Submit" name = "submit">Save</button>
                               <button class="btn btn-success" type = "Submit" name = "submit">cancel</button>
                            </div>               
                        </div>
                      </div>
                </div>
              </form>


             </div></div>


             </div>
        </div>
        </div>
       </div>
     </div>
 </div>
       @endsection
       @section('styles')
       @parent
       <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
       <link rel="stylesheet" href="{{ asset('js/app.js') }}">

   
       @endsection
       @section('javascripts')
        <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>

                    <script>
                                  CKEDITOR.replace( 'editor' );
                                 CKEDITOR.config.width = '100%';

                                </script>  


                    @endsection


