<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>
@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

 <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text" >
             <i class="fa fa-car" style="color: #fff;"></i>Financial setting 
         </h3>
     </div>
 </div>
</div>

<div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-head">
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_0">
            <div class="portlet box green">
                <div class="portlet-title">
                         <div class="col-md-11" style="margin: 0 auto;">
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form method="POST" action="{{ route('financial.store') }}" enctype = "multipart/form-data" id="upload_new_form">
                          {{ csrf_field() }}



                      @foreach ($Financial as $Datatypes) 
                      <div class="form-body">
                        <input type="hidden" value="{{ $Datatypes->id }}" class="form-control input-circle" name="id" >
                    </div>

                    <div class="form-body" style="margin-bottom: 10px;">
                       <div class="form-group">
                        <label class="col-md-3 control-label">Commisions%</label>
                        <div class="col-md-12">
                            <input type="number" step="any"   min="0" max="100" class="form-control input-circle"   name="commisions" 
                            value="{{ $Datatypes->commisions }}">
                            @if ($errors->has('commisions'))
                            <div class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                  <span class="sr-only">Close</span>
                              </button>
                              <strong>Warning!</strong> {{$errors->first('commisions')}}
                          </div>
                          @endif 


                      </div>
                  </div>
              </div>

              <div class="form-body" style="margin-bottom: 10px;">
               <div class="form-group">
                <label class="col-md-3 control-label">Serg Charge%</label>
                <div class="col-md-12">
                    <input type="number" step="any"   min="1.1" max="3.0" class="form-control input-circle"   name="Serg_fee" 
                    value="{{ $Datatypes->Serg_fee }}">
                    @if ($errors->has('Serg_fee'))
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('Serg_fee')}}
                  </div>
                  @endif 

              </div>
          </div>
      </div>
      <div class="form-body" style="margin-bottom: 10px;">
       <div class="form-group">
        <label class="col-md-3 control-label">Reject Ride Amount </label>
        <div class="col-md-12">
            <input type="number" min="1" class="form-control input-circle"   name="reject_booking_amount" 
            value="{{ $Datatypes->reject_booking_amount }}">
            @if ($errors->has('reject_booking_amount'))
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">×</span>
                  <span class="sr-only">Close</span>
              </button>
              <strong>Warning!</strong> {{$errors->first('reject_booking_amount')}}
          </div>
          @endif 

      </div>
  </div>
</div>
<div class="form-body" style="margin-bottom: 10px;">
   <div class="form-group">
    <label class="col-md-3 control-label">Labour Charge Rs/.</label>
    <div class="col-md-12">
        <input type="number"  step="any"  min="0" max="100"  class="form-control input-circle"   name="labour_fare" 
        value="{{ $Datatypes->labour_fare }}">
        @if ($errors->has('labour_fare'))
        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
          </button>
          <strong>Warning!</strong> {{$errors->first('labour_fare')}}
      </div>
      @endif 

  </div>
</div>
</div>
<div class="form-actions" >
    <div class="row">
        <div class="col-md-offset-4 col-md-8">
            <button type="submit" class="btn btn-success green" style="margin: 15px;">Update</button>
            </form>
            <a href="{{route('financial.index')}}" class="btn btn-info red"> Cancel</a>
        </div>
    </div>
</div>
@endforeach
</div>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection

