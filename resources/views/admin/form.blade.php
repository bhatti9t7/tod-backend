<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')

<div class="page-content-wrapper">
  <div class="page-content">
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-user" style="color: #fff;"></i>
              <span class="caption-subject font-red sbold uppercase" > Admin form</span>
            </div>

          </div>
      

          <div class="row">
            <div class="col-md-12">
              <form method="POST" action="{{ route('adminResource.store') }}" enctype = "multipart/form-data" id="upload_new_form">
                {{ csrf_field() }}

                <div class = "col-md-4 col-sm-4"  style="margin-left: 55px;">
                 <div class="fileinput fileinput-new" data-provides="fileinput" >
                  <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;">
                    <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                      <img src="{{ asset('images/admin/profile/no-image.png')}}" id="blah" height="250px" width = "250px">
                      <!-- </div> -->
                    </div>
                  </div>
                  <div class="form-group">
                   <label for="images" class="btn btn-primary" style="position: relative;left: 60px;top: 20px;">Upload Profile</label>
                  <input type="file" id="images" name="images" class="hide" style="opacity: 0;"> 
                        @if ($errors->has('images'))
                         <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              <span class="sr-only">Close</span>
                            </button>
                            <strong>Warning!</strong> {{$errors->first('images')}}
                          </div>
                        @endif
                 </div>
               </div>

               <div class="col-md-6 col-sm-8">
                <div class="form-group">
                  <label class="control-label"> Name</label>
                  <input name = "name" type="text" placeholder="John" class="form-control">
                        @if ($errors->has('name'))
                         <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              <span class="sr-only">Close</span>
                            </button>
                            <strong>Warning!</strong> {{$errors->first('name')}}
                          </div>
                        @endif
                </div>

                  <div class="form-group">
                    <label class="control-label">Email</label>
                    <input name = "email" type="email" class="form-control" placeholder="email"> 
                        @if ($errors->has('email'))
                         <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              <span class="sr-only">Close</span>
                            </button>
                            <strong>Warning!</strong> {{$errors->first('email')}}
                          </div>
                        @endif
                  </div>

                    <div class="form-group">
                      <label class="control-label">phone</label>
                      <input type="text" name = "phone" placeholder="phone" class="form-control"> 
                        @if ($errors->has('phone'))
                         <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              <span class="sr-only">Close</span>
                            </button>
                            <strong>Warning!</strong> {{$errors->first('phone')}}
                          </div>
                        @endif
                    </div>
                      <div class="form-group">
                        <label class="control-label">Password</label>
                        <input type="password" class="form-control" name = "password">
                          @if ($errors->has('password'))
                           <div class="alert alert-danger" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                              </button>
                              <strong>Warning!</strong> {{$errors->first('password')}}
                            </div>
                          @endif
                      </div>
                      <div class="form-group">
                        <label class="control-label">Confirm Password</label>
                        <input name = "password_confirmation" type="password" class="form-control"> 
                        @if ($errors->has('password_confirmation'))
                           <div class="alert alert-danger" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                              </button>
                              <strong>Warning!</strong> {{$errors->first('password_confirmation')}}
                            </div>
                          @endif
                      </div>


                      <div class="form-group" >
                        <label class="control-label">Country Name</label>
                        <select class="form-control" name="country_id" id="country">
                          <option > Choose the Country  </option>
                          @foreach ($Country as $categorie)
                          <option value="{{ $categorie->id }}"> {{ $categorie->country_name}} </option>
                          @endforeach                                        
                        </select>
                      </div>

                      <div class="form-group" style="display:none;"  id="city">
                        <label class="control-label" >City Name</label>
                        <select class="form-control" name="city_id">
                         
                        <!--   @foreach ($City as $categorie)
                          <option value="{{ $categorie->id }}"> {{ $categorie->name}} </option>
                          @endforeach   -->                                      
                        </select>
                      </div>
                      <div class="form-group" style="display:none;" id="branch">
                        <label class="control-label">Branch Name</label>
                        <select class="form-control" name="branch_id">
                         <!--  <option > Choose the Branch  </option>
                          @foreach ($Branch as $categorie)
                          <option value="{{ $categorie->id }}"> {{ $categorie->name}} </option>
                          @endforeach                                           -->
                        </select>
                      </div>

                      <div class="form-group" style="visibility: hidden;">
                        <label class="control-label">Franchise Name</label>
                        <select class="form-control" name="franchise_id">
                        <!--   <option > Choose the Franchise  </option>
                          @foreach ($Franchise as $categorie)
                          <option value="{{ $categorie->id }}"> {{ $categorie->name}} </option>
                          @endforeach   -->                                        
                        </select>
                      </div>

                      <div class="form-group overflow_sc">
                        <div class="col-md-3 col-sm-3"><label class="control-label">Roles</label></div>
                        <div class="col-md-9 col-sm-9">
                          <select multiple="multiple" class="multi-select" id="role_select" name="roles[]">
                            @foreach($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                       &nbsp;
                     </div>



                     <div class="form-group overflow_sc">
                      <div class="col-md-3 col-sm-3"><label class="control-label">Permissions</label></div>
                      <div class="col-md-9 col-sm-9" style="margin-bottom: 20px;">
                        <select multiple="multiple" class="multi-select" id="permission_select" name="permissions[]">
                          @foreach($permissions as $permission)
                          <option value="{{$permission->id}}">{{$permission->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                  </div>
                </div>

              </div>
              <div class="col-md-12 form-actions" style="padding: 30px;">
                <div class="col-md-5">
                  <div class="margiv-top-10 pull-right">
                   <button class="btn green" type = "Submit" name = "submit" style="margin-top: -23px;">Save</button>
                   <button class="btn red" type="reset" style="margin-top: -23px;">Cancel</button>
                 </div>
               </div>
             </div>
           </form>
         </div>
       </div>


   
    @endsection
      @stack('post-styles')

 
    <link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />

    <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <style type="text/css">
       .form-actions {
        padding: 20px 20px;
        margin: 0;
        background-color: #f5f5f5;
        border-top: 1px solid #e7ecf1;
      }
      @media (max-width: 580px){
        .overflow_sc{
          overflow-x: scroll !important;
        }
      }
      @media (min-width: 992px) and (max-width: 1160px){
        .overflow_sc{
          overflow-x: scroll !important;

        }
      }

    </style>


       @push('post-scripts')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript">

     $('#role_select').multiSelect();
     $('#permission_select').multiSelect();
   </script>

   <script type="text/javascript">
    function readURL(input) {

      if (input.files) {
        var reader = new FileReader();
        var totalfiles = input.files.length;
        for(i = 0; i < totalfiles; i ++){
          reader.onload = function(e) {
            $('#blah').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[i]);
        }
      }
    }
    $("#images").change(function() {
      readURL(this);
    });
  </script>
  <script type="text/javascript">
    $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                      });
                    </script>


                    <script type="text/javascript">
                      $("[name='country_id']").on("change",function(){
                        $("#city").show();
                         $("[name='city_id']").html(` <option > Choose the City  </option>`);
                        var country  = $("#country").val();

                        $.ajax({
                          method:"get",
                          url:"{{route('city_data')}}",
                          data : {id:country},
                          dataType:"json",
                          success:function(data){
                            // console.log(data);

                            data.forEach(function(val,ind){

                             var id = val.id;
                             var name = val.name;
                             var option = `<option value="${id}">${name}</option>`;

                             $("[name='city_id']").append(option);
                           });



                          }
                        });

                      })
                    </script>
                   

                     <script type="text/javascript">
                      $("[name='city_id']").on("change",function(){
                        $("#branch").show();
                         $("[name='branch_id']").html(` <option > Choose the Branch  </option>`);
                        var branches  = $("[name='city_id']").val();
                        // console.log(branches);return false;

                        $.ajax({
                          method:"get",
                          url:"{{route('branch_data')}}",
                          data : {id:branches},
                          dataType:"json",
                          success:function(data){
                            // console.log(data);return false;

                            data.forEach(function(val,ind){

                             var id = val.id;
                             var name = val.name;
                             var option = `<option value="${id}">${name}</option>`;

                             $("[name='branch_id']").append(option);
                           });



                          }
                        });

                      })
                    </script>


                       @endpush 