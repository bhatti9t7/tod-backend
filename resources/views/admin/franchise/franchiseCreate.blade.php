<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

   <div class="m-portlet__head">
    <div class="m-portlet__head-caption head_21">
        <div class="m-portlet__head-title ">
            <h3 class="m-portlet__head-text sbold uppercase">
              <i class="fa fa-car" style="color: #fff;"></i>Add New Franchise 
          </h3>
      </div>
  </div>
</div>
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">

        </div>

        <div class="tab-content">


            <div class="caption">
            </div>

        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->

            <form action="{{route('franchise.store') }}" class="form-horizontal" method="POST">
                {{ csrf_field() }}

                <div class="form-body">

                    <div class="form-group">
                        <label class="col-md-3 control-label">Franchise Name</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control input-circle"  name="name" >
                                    @if ($errors->has('name'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                              <span class="sr-only">Close</span>
                                          </button>
                                          <strong>Warning!</strong> {{$errors->first('name')}}
                                      </div>
                                    @endif

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Address</label>
                        <div class="col-md-12">
                            <input type="text" class="form-control input-circle"  name="address" >
                                    @if ($errors->has('address'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                              <span class="sr-only">Close</span>
                                          </button>
                                          <strong>Warning!</strong> {{$errors->first('address')}}
                                      </div>
                                    @endif

                        </div>
                    </div>


                    <div class="form-body">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="font-size: 13px;"> City Name</label>
                            <div class="col-md-12">
                                <select   class="form-control input-circle" name="branch_id" >

                                 @foreach ($branch_data as $categorie)

                                 <option  value="{{ $categorie->id }}"> {{ $categorie->name}}
                                 </option>
                                 @endforeach
                             </select>
                         </div>

                     </div>
                 </div>

             </div>

             <div class="form-actions">
           <div class=" col-md-12">
                  <div class="pull-right">
                        <button type="submit" class="btn btn-warning ">Submit</button>
                            </form>
                    <a href="{{ route('franchise.index')}}" type="button" class="btn btn-success btn-outline">Cancel</a>
                    </div>
                </div>
                <br>
            </div>
<br>
<br>
    
        <!-- END FORM-->
</div>
</div>
</div>
</div>
</div>
</div>


@endsection

