@extends('layout.admin.headerAdmin')
@section('content')
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                     
                    </div>
                    
                    
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Categories Edit  </div>
                                                
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                
                                             {!! Form::open(['method' => 'PATCH', 'route' => ['good-categories.update',$Datatypes->id], 'class'=>'form-horizontal']) !!}
                                                
                                                    <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Goods Name</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle"  name="name" value="{{ $Datatypes->name }}">
                                                                
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                               
                                              {!! Form::close() !!}
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

                            