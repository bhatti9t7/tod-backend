<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content">
 <div class="row">
  <div class="col-md-12">
    <div class="portlet light portlet-fit bordered">
      <div class="portlet-title">
        <div class="caption">     
         <div class="caption head_color sbold uppercase" style="color: #fff;">
           <i class="fa fa-gift" style="color: #fff;"></i>Add new Goods  
         </div>
       </div>
     </div>
     <div class="col-md-11 box_shadow_form">
       <div class="col-md-12">
        <div class="portlet-body form">
          <!-- BEGIN FORM-->
          
          <form action="{{ route('goods.store') }}" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            
            <div class="form-body">

              <div class="form-group">
                <label class="col-md-3 control-label">Goods Name</label>
                <div class="col-md-12">
                  <input type="text" class="form-control input-circle"  name="good_name" >
                  @if ($errors->has('good_name'))
                  <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">×</span>
                      <span class="sr-only">Close</span>
                    </button>
                    <strong>Warning!</strong> {{$errors->first('good_name')}}
                  </div>
                  @endif
                  
                </div>
              </div>
              
              <!-- END FORM-->
            </div>
            <div class="form-actions">
             <div class="row" style="margin: 0 auto; background-color: #f6f6f6; padding: 10px;">
               <div class="col-md-12" style="margin: 0 auto; background-color: #f6f6f6; padding: 10px;">
                
                 <div class=" pull-right">
                   <button type="Submit" class="btn btn-info">Submit</button>
                 </form>
                 <a href="{{route('goods.index')}}" class="btn btn-success btn-outline">Cancel</a>
                 
               </div>
             </div>
           </div>
         </div>
       </div>

     </div>

   </div>
   <br><br>
 </div>
</div>
</div>
</div>
</div>
</div>
@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css"/>

@push('post-scripts')
@endpush  
