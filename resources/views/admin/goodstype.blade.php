<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">


    <div class="m-portlet__head head_21">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title ">
                <h3 class="m-portlet__head-text head_color sbold uppercase">
                  Goods Type
              </h3>
          </div>
      </div>
  </div>
  @component('_components.alerts-default')
  @endcomponent

  <div class="m-portlet__body">
    <!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
            <div class="col-xl-8 order-2 order-xl-1">
                <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-4">
                        <div class="btn-group">
                            <a href="{{route('goods.create')}}" id="sample_editable_1_new" class="btn btn-success" style="position: relative;top: -10px;"> Add New
                            </i>
                        </a>

                    </div>
                    <div class="m-input-icon m-input-icon--left">
                        <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                        <span class="m-input-icon__icon m-input-icon__icon--left">
                            <span>
                                <i class="la la-search"></i>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!--end: Search Form -->

<div class="m_datatable" id="child_data_ajax"></div>
</div>


</div>


@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
<style>
.head_21{padding: 20px 10px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey; border-bottom: 1px solid #f26727!important;
} 
.head_color{background-color:#f26727; padding: 10px; font-size: 20px!important; font-weight: bold; color: #fff!important; width: 95%;

}
</style>

@push('post-scripts')
<@if(session('success_message')) 
<script type="text/javascript">
   swal({
      title: "Good job!",
      text: "You record delete sucessfully",
      icon: "success",
      button: "Aww yiss!"
  });
</script>
@endif 

<script type="application/javascript">

    var datatable = $('.m_datatable').mDatatable({
    // datasource definition
    data: {
        type: 'remote',
        source: {
            read: {
                url: BASE_URL+'/admin/get_good'
            }
        },
    pageSize: 10, // display 20 records per page
    saveState: {
        cookie: false,
        webstorage: false
    },
    serverPaging: true,
    serverFiltering: true,
    serverSorting: true
},

    // layout definition
    layout: {
        theme: 'default',
        scroll: false,
        height: null,
        footer: false
    },

    // column sorting
    sortable: true,

    pagination: true,

    detail: false,

    search: {
        input: $('#generalSearch')
    },

    // columns definition
    columns: [ 
    
    {
        field: "good_name",
        title: "Good Name",
        
    },
    
    {
        field: "status",
        title: "Status",
        template: function (row) {
            // return row.status;
            return row.status?`<a href="javascript:;" onclick="change_status(this,${row.id},0)" class="btn btn-success">Active</a>`:`<a href="javascript:;" onclick="change_status(this,${row.id},1)" class="btn btn-danger">Deactive</a>`;
        }
    },
    {
        field: "Actions",
        
        title: "Actions",
        sortable: false,
        overflow: 'visible',
        template: function (row) {

            
            return '\
            <a href="admin/goods/'+row.id+'/edit" class="btn btn-warning btn-xs" style="padding:5px;font-size:12px;background-color:#ffb822;border-color: #ffb822;">\
            edit</i>\
            </a>\
            <a href="admin/good_delete_record/'+row.id+'" class="btn btn-danger btn-xs" title="show details" style="padding:5px;font-size:12px;" onclick="delete_record(this); return false;">\
            delete\
            </a>\
            ';
        }
    }
    ]
});

</script>
<script type="text/javascript">

  function change_status(obj,id,activity) {
    console.log(id,activity);
    $.ajax({
        url: "{{ route('good-change-status') }}", 
        method:"POST",
        data:{'user_id':id,'activity':activity},
        // dataType:"text",
        success: function(response){
            // console.log(response);return false;
            if(response.activity){
                if(activity)
                    $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',0)" class="btn btn-success"> Active </a>');
                else
                    $(obj).replaceWith('<a href="javascript:;" onclick="change_status(this,'+id+',1)" class="btn btn-danger"> Deactive </a>');

                toastr.success(response.message);
                console.log(activity);
            }else{
               toastr.error(response.message);

           }
       }});

}

</script>



@endpush
