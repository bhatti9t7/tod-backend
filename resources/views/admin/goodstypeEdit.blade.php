<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">

                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fas fa-truck" style="color: #fff;"></i>  
                <span class="caption-subject sbold uppercase" style="color: #fff !important">Goods Edit</span>
                        </div></div>
                        <div class="portlet-body form">
                            {!! Form::open(['method' => 'PATCH', 'route' => ['goods.update',$Datatypes->id], 'class'=>'form-horizontal']) !!}
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Goods Name</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control input-circle"  name="good_name" value="{{ $Datatypes->good_name }}">
                                        @if ($errors->has('good_name'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                              <span aria-hidden="true">×</span>
                                              <span class="sr-only">Close</span>
                                          </button>
                                          <strong>Warning!</strong> {{$errors->first('good_name')}}
                                      </div>
                                      @endif

                                  </div>
                              </div>
                          </div>
                          <div class="form-actions">
                           <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-info green">Submit</button>
                                           {!! Form::close() !!}
                                    <a href="{{route('goods.index')}}" class="btn btn-success">Cancel</a>
                                </div>
                                </div>
                            </div>
                        </div>

                 
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
@push('post-scripts')
@endpush  

