<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

 <div class="m-portlet__head">
  <div class="m-portlet__head-caption head_21">
    <div class="m-portlet__head-title ">
      <h3 class="m-portlet__head-text sbold uppercase">
       <i class="fa fa-money" aria-hidden="true" style="color: #fff;"></i>Main Wallet

     </div>
   </div>
 </div>

 <div class="page-content-wrapper">

  <div class="page-content">

    <div class="caption">
    </div>

    <?php  $pending = round(abs($wallet->pending_commission),2)  ?>

    <div class="portlet-body form">
      <!-- BEGIN FORM-->
      <form method="POST" action="{{ route('out-flow.store') }}" enctype = "multipart/form-data" id="upload_new_form">
        {{ csrf_field() }}
          <div class="row">
  <div class="col-md-6">
        <div class="form-body">
          <input type="hidden" value="{{ $wallet->id }}" class="form-control input-circle" name="id" >
        </div>
        <div class="form-body" style="margin-bottom: 10px;">
         <div class="form-group">
          <label class="col-md-3 control-label">Pending Amount</label>
          <div class="col-md-12">
            <input type="text" class="form-control input-circle" name="pending_commission" value="{{ $pending }}">
          </div>
        </div>
      </div>

      <div class="form-body">
        <div class="form-group">
          <label class="col-md-3 control-label" style="font-size: 13px;"> Captain Name</label>
          <div class="col-md-12">
            <select id="select2" name="captain_id" class="js-example-data-ajax form-control">
              <option value="" selected="selected">select2/select2</option>
            </select>                                        
          </div>
          @if ($errors->has('captain_id'))
          <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('captain_id')}}
          </div>
          @endif   
        </div><br>
      </div>
    </div>
      <div class="col-md-6">
      <div class="form-body" style="margin-bottom: 10px;">
       <div class="form-group">
         <label class="col-md-3 control-label">Captain Name</label>
         <div class="col-md-12">
           <input type="text" id="title"   class="form-control input-circle" >
        </div>
      </div>
   

      <div class="form-body" style="margin-bottom: 10px;">
       <div class="form-group">
         <label class="col-md-3 control-label">Collect Amount</label>
         <div class="col-md-12">
           <input type="number" step="any"   class="form-control input-circle"   name="collect_commission" >
           @if ($errors->has('collect_commission'))
           <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <strong>Warning!</strong> {{$errors->first('collect_commission')}}
          </div>
          @endif     

        </div>
      </div>
    </div>
</div>
<br>
    <div class="form-actions" >
     
        <div class="col-md-offset-3 col-md-12">
          <button type="submit" class="btn btn-success pull-right">update</button>
        </div>
      
    </div>
      <br> <br>
  </form>

</div>
</div>

</div>
</div>

@endsection
  @stack('post-styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@push('post-scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
 $('#select2').select2({
  ajax: {
    url: "{{route('captain_account')}}",
    method:"post",
    dataType: 'json',
    processResults: function (_data, params) {
      
        var data1= $.map(_data, function (obj) {
        var newobj = {};
        newobj.id = obj.id;
        newobj.text= obj.fname+" "+obj.lname;
        newobj.balance= obj.wallet.credit_amount;
        return newobj;
      });
       //console.log(data1);return false;
      return { results:data1};
    }
  }
}).on('change', function() {

  console.log($(this).select2('data')[0].balance);
  var balance = $(this).select2('data')[0].balance;
  $('#title').val(balance);
});

</script>
@endpush  