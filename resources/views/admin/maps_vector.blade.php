<?php
/**
 * Project: hajjtrack.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row" >
                       


                        <!------------- google map here -------->

                        
                            <div id="map" ></div>
                        
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            
            
        </div>
    </div>
</div>

<!-- Modal -->
<div id="captain_model" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="captain_name">Captain Name</h4>
      </div>
      <div class="modal-body" id="captain_detail">
        Captain Detail
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

        
 @endsection
 @stack('post-styles')

   <link href="{{asset('assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" />
   <style>
    #map{
      height:900px;
      width:100%;
    }
   
/*#map {
    height: 100%;
    width:100%;
}

a {
    text-decoration: none;
}*/
  </style>


@push('post-scripts')

<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLZzq31lWF8Oo31xbFTqHchlmXIfzqeAI&callback=initMap">
</script>

<script type="text/javascript">
  
 var map;
var Markers = {};
 function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: new google.maps.LatLng(31.519115000000003, 74.32091333333332),
    mapTypeId: 'roadmap'
  });

  var iconBase = '{{ asset("assets/public/images/track.png")}}';

   var captains = <?php echo json_encode($captains); ?>
        //infowindow = new google.maps.InfoWindow();
        // Create markers.
        captains.forEach(function(captain,index) {
          var marker = new google.maps.Marker({
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(captain.latitude, captain.longitude),
            icon: iconBase,
            map: map
          });



          google.maps.event.addListener(marker, 'click', (function (marker, captain) {
          return function () {
                            //displayRoute(locations[0][6],locations[0][7],locations[0][8],locations[0][9]);
                           // infowindow.setContent(captain.type);
                            //infowindow.setOptions({maxWidth: 270});
                            //infowindow.open(map, marker);
                             $('#captain_name').html(captain.fname+" "+captain.lname);
                            var captain_detail = '<b>Phone: </b>'+captain.phone+'<br>';
                            captain_detail += '<b>Email: </b>'+captain.email+'<br>';
                            $('#captain_detail').html(captain_detail);
                            $('#captain_model').modal('show');
                          }
                        })(marker, captain));
          Markers[captain.id] = marker;

        });





}

</script>


    
</body>

        

 @endpush          