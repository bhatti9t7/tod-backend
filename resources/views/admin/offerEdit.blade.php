@extends('layout.admin.headerAdmin')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
        </div>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_0">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                         <i class="fa fa-gift" aria-hidden="true" style="color: #fff;"></i>Offer Add </div>

                     </div>
                     <div class="portlet-body form">
                        <!-- BEGIN FORM-->

                        <form action="{{ route('offers.store') }}" class="form-horizontal" method="POST">
                            {{ csrf_field() }}

                            {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['offers.update',$offers->id], 'class'=>'form-horizontal']) !!}

                            <div class="form-body">

                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12" style="margin-top: 30px;">
                                            <div class="col-md-4"> </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Offer Name</label>
                                                    <div class="input-icon right">
                                                        <input type="text" class="form-control " name="offer_name"  value="{{ $offers->offer_name }}" > </div>
                                                    </div>
                                                    <div class="col-md-9" style="padding: 0px 0px 10px 0px;">
                                                        <div class="form-group">
                                                            <label for="span_small" class="control-label">Time Period</label>
                                                            <select id="span_small" class="form-control select2 select2-hidden-accessible" tabindex="-1" aria-hidden="true" name="time_period" value="{{ $offers->time_period }}">
                                                                <option value="yearly">Yearly Offer</option>
                                                                <option value="monthly">Monthly Offer</option>
                                                                <option value="weakly">Weekly Offer</option>   
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3"> 
                                                        <button type="button" class="btn " data-toggle="collapse" data-target="#demo" style="margin-top: 27px;margin-left: 15px;background-color: #f36722; color: #fff;">Custom</button>
                                                    </div>
                                                    <div class="col-md-12" style="padding: 0px;">
                                                        <div id="demo" class="collapse">
                                                            <div class="form-group">
                                                                <label>Start Date</label>
                                                                <div class="input-icon right">
                                                                    <input type="text" class="form-control " name="start_time" placeholder="Start Date" value="{{ $offers->start_time }}" > </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>End Date</label>
                                                                    <div class="input-icon right">
                                                                        <input type="text" class="form-control " name="offer_expairy" placeholder="End Date" value="{{ $offers->time_period }}" > </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            


                                                            <div class="clearfix"> </div>
                                                            <div class="form-group">
                                                                <label>Ride</label>
                                                                <div class="input-icon right">
                                                                    <input type="text" class="form-control " placeholder="Ride" name="rides" value="{{ $offers->rides }}"> </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Amount</label>
                                                                    <div class="input-icon right">
                                                                        <input type="text" class="form-control " placeholder="Amount" name="amount" value="{{ $offers->amount }}"> </div>
                                                                    </div>
                                                                    <div class="col-md-12" style="padding: 0px 0px 10px 0px;">
                                                                        <div class="form-group">
                                                                            <label for="span_small" class="control-label">Offer Types</label>
                                                                            <select id="span_small" class="form-control select2 select2-hidden-accessible" name="offer_type" tabindex="-1" aria-hidden="true" value="{{ $offers->offer_type }}">

                                                                                <option value="user_offer">User Offer</option>
                                                                                <option value="captain_offer">Captain offer</option>


                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>Offer Expiry Date</label>
                                                                        <div class="input-icon right">
                                                                            <input type="text" class="form-control " placeholder="Offer Expiry Date" name="offer_expairy" value="{{ $offers->offer_expairy }}"> </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label>Description</label>
                                                                            <div class="input-icon right">
                                                                                <textarea type="" class="form-control " placeholder="description" name="description" style="resize: none;"> {{ $offers->description }}</textarea>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                </div>


                                                                
                                                                <div class="col-md-4"> </div>
                                                                

                                                            </div>



                                                        </div>

                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-4 col-md-8">
                                                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </form>                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endsection

