<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption head_21">
            <div class="m-portlet__head-title ">
                <h3 class="m-portlet__head-text sbold uppercase">
                   <i class="fa fa-gift" aria-hidden="true"></i>Offer Add

               </div>
           </div>
       </div>
       <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">

            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <form action="{{ route('offers.store') }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    <div class="form-body">
                       <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Offer Name</label>
                                <div class="input-icon right">
                                    <input type="text" class="form-control " name="offer_name" >
                                    @if ($errors->has('offer_name'))
                                    <div class="alert alert-danger" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                            <span class="sr-only">Close</span>
                                        </button>
                                        <strong>Warning!</strong> {{$errors->first('offer_name')}}
                                    </div>
                                    @endif  
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="span_small" class="control-label">Time Period</label>
                                <select id="span_small" class="form-control" tabindex="-1" aria-hidden="true" name="time_period">
                                    <option value="yearly">Yearly Offer</option>
                                    <option value="monthly">Monthly Offer</option>
                                    <option value="weakly">Weekly Offer</option>   
                                </select>

                            </div>
                        </div>
                     
                           <!--  <label class="control-label col-md-4"> For Custom Offer</label>
                             <div class="col-md-12"> 
                            <button type="button" class="btn arrow down " data-toggle="collapse" data-target="#demo" style="margin-top: 27px;margin-left: 8px;background-color: #f36722; color: #fff;">Custom </button>
                        </div> -->
                        <!-- <div class="col-md-12" style="padding: 0px;">
                            <div id="demo" class="collapse"> -->
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <div class="input-icon right">
                                        <input type="date" class="form-control " name="start_date" placeholder="Start Date" > 
                                        @if ($errors->has('start_date'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <strong>Warning!</strong> {{$errors->first('start_date')}}
                                        </div>
                                        @endif 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
                                    <div class="input-icon right">
                                        <input type="date" name="last_date" class="form-control " placeholder="End Date" > 
                                        @if ($errors->has('end_date'))
                                        <div class="alert alert-danger" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                                <span class="sr-only">Close</span>
                                            </button>
                                            <strong>Warning!</strong> {{$errors->first('end_date')}}
                                        </div>
                                        @endif 
                                    </div>

                                </div>
                            
                            <div class="col-md-12">                                      
                                <div class="form-group">
                                    <label>Ride</label>
                                    <div class="input-icon right">
                                        <input type="text" class="form-control " placeholder="Ride" name="rides"> </div>
                                    </div>
                                </div></div>
                                <div class="col-md-12"> 
                                    <div class="col-md-12"> 
                                        <div class="form-group">
                                            <label>Amount</label>
                                            <div class="input-icon right">
                                                <input type="text" class="form-control " placeholder="Amount" name="amount"> </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="span_small" class="control-label">Offer Types</label>
                                                <select id="span_small" class="form-control " name="offer_type" tabindex="-1" aria-hidden="true">
                                                    <option value="custom_offer">Custom Offer</option>
                                                    <option value="captain_offer">Captain offer</option>
                                                     <option value="user_offer">User Offer</option>
                                                    <option value="captain_offer">Captain offer</option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12">

                                            <div class="form-group">
                                                <label>Offer Expiry Date</label>
                                                <div class="input-icon right">
                                                    <input type="date" class="form-control " placeholder="Offer Expiry Date" name="offer_expairy"> </div>
                                                </div></div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Description</label>
                                                        <div class="input-icon right">
                                                            <textarea type="" class="form-control " placeholder="description" name="description" style="resize: none;"></textarea>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>

                                        <div class="form-actions">
                                            <div class="col-md-12">
                                                <div class=" pull-right" >
                                                    <button type="submit" class="btn btn-info green">Submit</button>
                                                </form>  
                                                <a href="{{route('offers.index')}}" type="button" class="btn btn-success grey-salsa btn-outline">Cancel</a>
                                            </div></div>
                                            <br><br><br>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                        </div>
                    </div>
                </div>

                @endsection


