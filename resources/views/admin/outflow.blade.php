<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">


    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                  Outflow Record 
              </h3>
          </div>
      </div>
  </div>
  @component('_components.alerts-default')
  @endcomponent

  <div class="m-portlet__body">

    <!--begin: Search Form -->
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
        <div class="row align-items-center">
             <div class="col-xl-8 order-2 order-xl-1">
                <div class="form-group m-form__group row align-items-center">
                    <div class="col-md-4">
                         <div class="m-input-icon m-input-icon--left">
                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                <span>
                                    <i class="la la-search"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="btn-group" style="margin-right: 5px; margin-bottom:5px ">
                            <a href="{{ URL::to('admin/captainCommisionExcel/xls') }}" style="margin-right: 5px;"><button class="btn btn-success"> Excel xls</button></a>
                            <a href="{{ URL::to('admin/captainCommisionExcel/xlsx') }}"  style="margin-right: 5px;"><button class="btn btn-success"> Excel xlsx</button></a>
                            <a href="{{ URL::to('admin/captainCommisionExcel/csv') }}"><button class="btn btn-success"> CSV</button></a>
                          
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!--end: Search Form -->
z
    <div class="m_datatable" id="child_data_ajax"></div>
</div>


</div>


@endsection

@push('post-scripts')
<script type="application/javascript">

    var datatable = $('.m_datatable').mDatatable({
    // datasource definition
    data: {
        type: 'remote',
        source: {
            read: {
                url: BASE_URL+'/admin/outflow_data'
            }
        },
    pageSize: 10, // display 20 records per page
    saveState: {
        cookie: false,
        webstorage: false
    },
    serverPaging: true,
    serverFiltering: true,
    serverSorting: true
},

    // layout definition
    layout: {
        theme: 'default',
        scroll: false,
        height: null,
        footer: false
    },

    // column sorting
    sortable: true,

    pagination: true,

    detail: false,

    search: {
        input: $('#generalSearch')
    },

    // columns definition
    columns: [ 
    
    {
        field: "id",
        title: "Captain Name",
        template:function(row){
            return row.captain.fname+' '+row.captain.lname;
        }
        
    },

    {
        field: "amount",
        title: "Amount",
    },
    {
        field: "credit_amount",
        title: "Commision ",
    },
    {
        field: "debit_amount",
        title: "Current Amount",
    },

    ]
});

</script>


@endpush
