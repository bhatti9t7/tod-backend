
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">
        </div>
        <div class="row">
            <div class="col-md-12" style="margin: 0 auto;">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->
                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                        </div>
                                    </div>
                                        <ul class="nav nav-tabs" style="margin-top: -20">
                                           <button class="btn_tabs"> <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab" style="color: #fff;">Personal Info</a>
                                            </li></button>
                                            <button class="btn_tabs"><li>
                                                <a href="#tab_1_2" data-toggle="tab" style="color: #fff;">Change Avatar</a>
                                            </li></button>
                                            <button class="btn_tabs"> <li>
                                                <a href="#tab_1_3" data-toggle="tab" style="color: #fff;"s>Change Password</a>
                                            </li></button>
                                           
                                                   <!--  <li>
                                                        <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                    </li> -->
                                                </ul>
                                           
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <!-- PERSONAL INFO TAB -->
                                                    <div class="tab-pane active" id="tab_1_1">
                                                           <div class="col-md-6">
                                                        {!! Form::open(['method' => 'PATCH', 'route' => ['adminResource.update',$admin->id], 'class'=>'form-horizontal']) !!}

                                                        <form role="form" action="{}" method="POST">
                                                           <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                            <div class="input-icon">
                                                                <input type="text" class="form-control" name="name" value="{{$admin->name}}">
                                                                <label for="form_control_1"> First Name</label>
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                            <div class="input-icon">
                                                                <input type="email" class="form-control" name="email" value="{{$admin->email}}">
                                                                <label type="email" for="form_control_1">  Email</label>
                                                                
                                                                <i class="glyphicon glyphicon-envelope"></i>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                            <div class="input-icon">
                                                                <input type="text" class="form-control" name="phone" value="{{$admin->phone}}">
                                                                <label for="form_control_1">Phone</label>
                                                                
                                                                <i class="fa fa-phone"></i>
                                                            </div>
                                                        </div> 
                                                        <div class="margiv-top-10">
                                                            <button type="submit " class="btn btn-primary"> Save Changes </button>
                                                            
                                                        </div>
                                                    </form>
                                                        {!! Form::close() !!}
                                                    </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_2">
                                                            <div class="col-md-6">
                                                        {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['adminResource.update',$admin->id], 'class'=>'form-horizontal']) !!}
                                                        <div class="form-group">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                     <img src="{{asset('images/admin/profile/' . $admin->images)}}" width="200px" height="150px" alt="" id="blah">
                                                                   
                                                                     </div>
                                                                    <div class="col-md-1">
                                                                       <label for="images" class="btn btn-primary">Upload Profile</label>
                                                                            
                                                                        <input type="file" id="images" name="images" class="hide" style="opacity: 0;">
                                                                        </div>
                                                                    </div>
                                                                      <div class="col-md-1">
                                                                 <div class="margin-top-10">
                                                                    <button type="submit" class="btn btn-primary"> Submit </button>
                                                                    
                                                                </div></div>
                                                                </div>
                                                               
                                                            </form>
                                                                {!! Form::close() !!}
                                                            </div></div>
                                                            <div class="tab-pane" id="tab_1_3">
                                                                     <div class="col-md-6" >
                                                             {!! Form::open(['method' => 'PATCH', 'route' => ['adminResource.update',$admin->id], 'class'=>'form-horizontal']) !!}
                                                             <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                                <div class="input-icon">
                                                                    <input type="password" readonly 
                                                                    class="form-control" >
                                                                    <label for="form_control_1"> Password</label>
                                                                    <span class="help-block">Write Your Password</span>
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                                <div class="input-icon">
                                                                    <input type="password" name="password" class="form-control">
                                                                    <label for="form_control_1"> New Password</label>
                                                                    <span class="help-block">Write Your New Password</span>
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                                <div class="input-icon">
                                                                    <input type="password" name="password_confirmation" class="form-control">
                                                                    <label for="form_control_1"> Re-type Password</label>
                                                                    <span class="help-block">Write Your Password</span>
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn btn-primary"> Change Password </button>
                                                               
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </form>
                                                        </div></div>
                                                        
                                                        <!-- END CHANGE PASSWORD TAB -->
                                                        <!-- PRIVACY SETTINGS TAB -->

                                                        <!-- END PRIVACY SETTINGS TAB  -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--   END PROFILE CONTENT -->
                            </div>
                        </div>
                        <!-- END PAGE BASE CONTENT -->
                    </div>
                    <!--    END CONTENT BODY -->
                </div>
                
                


        @endsection

        @stack('post-styles')
        <style>
           .btn_tabs{ color: #fff;
                     box-shadow: 0px 4px 10px darkgrey;
                  padding-right: 50px;   padding-left:50px;
                    margin: 5px;

                 background-color: #f26727;
           }          
        </style>


        <link href="{{ asset('assets/pages/css/profile-2.css') }}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />

       @push('post-scripts')
        
        <script src="{{asset('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/gmaps/gmaps.min.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

          <script type="text/javascript">
                          
                           $('#role_select').multiSelect();
                           $('#permission_select').multiSelect();
                      </script>

                <script type="text/javascript">
                    function readURL(input) {

                        if (input.files) {
                            var reader = new FileReader();
                            var totalfiles = input.files.length;
                            for(i = 0; i < totalfiles; i ++){
                                reader.onload = function(e) {
                                    $('#blah').attr('src', e.target.result);
                                }
                                reader.readAsDataURL(input.files[i]);
                            }
                        }
                    }
                    $("#images").change(function() {
                        readURL(this);
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                            if (input.files) {
                                var filesAmount = input.files.length;

                                for (i = 0; i < filesAmount; i++) {
                                    var reader = new FileReader();

                                    reader.onload = function(event) {
                                        $($.parseHTML('<img height = "200" width = "150" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    }

                                    reader.readAsDataURL(input.files[i]);
                                }
                            }

                        };

                        
                    });
                </script>

        @endpush  