<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      Rides Proof Record
                    </h3>
                </div>
            </div>
        </div>
        @component('_components.alerts-default')
        @endcomponent
            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-head">

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">

                   

                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase"></span>
                                    </div>
                                    <div class="actions">
                                        
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                          
                                        
                                    </div>
                                    <table class="table table-striped table-hover table-bordered" id="rides">
                                       <thead>
                                            <tr>
                                             <th align="left">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                               
                                                <th> Destination Name </th>
                                                <th> Delivery Proof </th>
                                                <th>Route Map image</th>  
                                                
                                            </tr>
                                        </thead>
                                        <tbody>                                            
                                                 @foreach ($ride_complete as $ride_completed)
                                                <tr>                                                   
                                                  <td align="center">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1" />
                                                        <span></span>
                                                    </label>
                                                </td>                                       
                                                    <td>{{ $ride_completed->BookingDestination['drop_place'] }} </td>
                                                    <td><img src="{{asset('images/ride_complete/' . $ride_completed->image)}}" alt=" image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" > </td>
                                                    <td> <img src="{{asset('images/ride_complete/' . $ride_completed->map_image)}}" alt="map image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >  </td>

                                                </tr>
                                               @endforeach()
                                             
                                        </tbody>
                                    </table>
                                    
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5">
                                            <div class="dataTables_info" id="sample_editable_1_info" role="status" aria-live="polite"></div>
                                        </div>
                                        <div class="col-md-7 col-sm-7">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>           
          </div> 
        <!-- END CONTAINER -->
 @endsection       <!-- BEGIN FOOTER -->





