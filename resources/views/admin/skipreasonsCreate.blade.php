<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">
   <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
                <i class="fa fa-car" style="color: #fff;"></i>Reasons Add  
            </h3>
        </div>
    </div>
</div>
             


            <div class="col-md-12">
                <div class="tab-pane active" id="tab_0">

                    <!-- BEGIN FORM-->

                    <form action="{{ route('skip-reasons.store') }}" class="form-horizontal" method="POST">
                        {{ csrf_field() }}

                        <div class="form-body">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Reasons Name</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control input-circle"  name="reasons" >
                                      @if ($errors->has('reasons'))
                       <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('reasons')}}
                      </div>
                      @endif
                                </div>
                            </div>

                        </div>



                   
                    <!-- END FORM-->

                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-success">Submit</button>
                             </form>
                            <a href="{{route('skip-reasons.index')}}" type="button" class="btn btn-success"> Cancel</a>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
        </div>

</div>
</div>
</div>
</div>
</div>

@endsection
@push('post-scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
