<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

     <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                      <i class="fa fa-car" style="color: #fff;"></i>Skip Reasons Edit  
                    </h3>
                </div>
            </div>
        </div>
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-toolbar">
                            <!-- BEGIN THEME PANEL -->

                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>                 
                    
                        <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                          
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                
                                             {!! Form::open(['method' => 'PATCH', 'route' => ['skip-reasons.update',$reasons->id], 'class'=>'form-horizontal']) !!}
                                                
                                                    <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Reasons Name</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle"  name="reasons" value="{{ $reasons->reasons }}">
                                                                
                                                            </div>
                                                        </div>

                                                        
                                                        
                                                        
                                                    </div>
                                                    

                                                    <div class="form-actions">
                                                        <div class="form-group">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn btn-info green">Submit</button>
                                                                      {!! Form::close() !!}
                                                                <a href="{{route('skip-reasons.index')}}" type="button" class="btn btn-success"> Cancel</a>
                                                        </div>
                                                        <br>
                                                    </div>
                                               
                                        
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

                            