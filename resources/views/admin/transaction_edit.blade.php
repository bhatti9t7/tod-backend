@extends('layout.admin.headerAdmin')
@section('content')
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-head">
                        <div class="page-toolbar">

                        </div>
                    </div>
                                    <div class="tab-pane" id="tab_1">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                
                                            </div>
                                            <div class="portlet-body form">
                                                {!! Form::open(['method' => 'PATCH', 'route' => ['inflow.update',$transaction->id], 'class'=>'form-horizontal']) !!}
                                                    <div class="form-body">
                                                        <h3 class="form-section">Transaction Info</h3>
                                                        <!-- <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">User Name</label>
                                                                    <input type="text" name="user_id" id="firstName" class="form-control" readonly value="{{$transaction->user['fname']}} {{$transaction->user['lname']}}">
                                                                    
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            <!-- <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Captain Name</label>
                                                                    <input type="text" id="firstName" readonly name="captain_id" class="form-control" value="{{$transaction->captian['fname']}} {{$transaction->captian['lname']}}">
                                                                    
                                                                </div>
                                                            </div>
                                                          
                                                        </div> --> 
                                                   
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Total Amount</label>
                                                                    <input type="text" class="form-control" name="amount" value="{{$transaction->amount}}"> </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Collect Amount</label>
                                                                    <input type="text" class="form-control" name="collect_amount" value="{{$transaction->collect_amount}}" > </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Left Amount</label>
                                                                    <input type="text" class="form-control" name="left_amount" value="{{$transaction->left_amount}}"> </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        <h3 class="form-section">Commission</h3>

                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Commission Percentage</label>
                                                                    <input type="text" class="form-control" name="commisions" value="{{$transaction->commisions}}"> </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Commisions Amount </label>
                                                                    <input type="text" class="form-control" name="commisions_amount" value="{{$transaction->commisions_amount    }}"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="form-actions right">
                                                        <button type="button" class="btn default">Cancel</button>
                                                        <button type="submit" class="btn green">
                                                            <i class="fa fa-check"></i> Save</button>
                                                    </div>
                                                {!! Form::close() !!}
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                        
       @endsection