<?php
/**
 * Project: loader.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="row">
      <div class="col-md-12">
        <div class="portlet light portlet-fit bordered">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-user" style="color: #fff;"></i>
              <span class="caption-subject font-red sbold uppercase"> users Form</span>
            </div>

          </div>
          <div class="row">
            <div class="col-md-12">

              <div class="tab-content">
                <div id="tab_1-1" class="tab-pane active">

                  <form method="POST" action="{{ route('userResource.store') }}" enctype = "multipart/form-data" id="upload_new_form">
                    {{ csrf_field() }}
                    <div class="form-group col-md-6">
                      <label class="control-label">First Name</label>
                      <input name = "fname" type="text" placeholder="John" class="form-control">
                      @if ($errors->has('fname'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('fname')}}
                      </div>
                      @endif
                    </div>
                    <div class="form-group col-md-6">
                      <label class="control-label">Last Name</label>
                      <input type="text" name = "lname" placeholder="Doe" class="form-control"> 
                      @if ($errors->has('lname'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('lname')}}
                      </div>
                      @endif
                    </div>
                    <div class="form-group col-md-6">
                      <label class="control-label">Email</label>
                      <input name = "email" type="email" class="form-control" placeholder="email"> 
                      @if ($errors->has('email'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('email')}}
                      </div>
                      @endif
                    </div>

                    <div class="form-group col-md-6">
                      <label class="control-label">phone</label>
                      <input type="text" name = "phone" placeholder="phone" class="form-control"> 
                      @if ($errors->has('phone'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('phone')}}
                      </div>
                      @endif
                    </div>

                    <div class="form-group col-md-6">
                      <label class="control-label">Password</label>
                      <input type="password" class="form-control" name = "password">
                      @if ($errors->has('password'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('password')}}
                      </div>
                      @endif
                    </div>
                    <div class="form-group col-md-6">
                      <label class="control-label">Confirm Password</label>
                      <input name = "password_confirmation" type="password" class="form-control"> 
                        @if ($errors->has('password_confirmation'))
                         <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              <span class="sr-only">Close</span>
                            </button>
                            <strong>Warning!</strong> {{$errors->first('password_confirmation')}}
                          </div>
                        @endif
                    </div>
                    <div  class="form-group col-md-6">
                      <div class = "col-md-4"  style="margin-left: 5px;">
                        <div class="fileinput fileinput-new" data-provides="fileinput" >
                          <div class="fileinput-new thumbnail" >

                            <img src="{{asset('images/user/profile/no-image.png')}}" id="output_image" style="max-width:250px;>

                          </div>
                          <div class="form-group">
                             <label for="images" class="btn btn-primary"  style="position: relative;top: 020px;left:40px;">Upload Profile</label>
                             <input type="file" id="images" name="images" class="hide" style="opacity: 0;">
                          </div>
                        </div>
                      </div>
                    </div>
                    @if ($errors->has('images'))
                         <div class="col-md-6">
                              <div class="alert alert-danger" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">×</span>
                              <span class="sr-only">Close</span>
                            </button>
                            <strong>Warning!</strong> {{$errors->first('images')}}
                          </div>
                         </div>
                        @endif
               <div class="row" >
                   <div class="col-md-12 form-actions" style="background-color:unset;border: 0;margin-top: 30px;">
                        <div class="col-md-3">
                          <div class="margiv-top-10 pull-right">
                             <button class="btn green" type = "Submit" name = "submit">Save</button>
                             <button class="btn red" type="reset">Cancel</button>
                          </div>
                        </div>
                      </div>
              </div>
             </form>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>



   <style type="text/css">
   .form-actions {
    padding: 20px 20px;
    margin: 0;
    background-color: #f5f5f5;
    border-top: 1px solid #e7ecf1;
  }

  .thumbnail img {

  </style>


  @endsection
  @stack('post-styles')

  <link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />

  <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />


  @push('post-scripts')



  <script type='text/javascript'>
    function preview_image(event) 
    {
     var reader = new FileReader();
     reader.onload = function()
     {
      var output = document.getElementById('output_image');
      output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
  }
</script>



@endpush