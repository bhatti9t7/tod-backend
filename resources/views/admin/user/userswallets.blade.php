<?php
/**
 * Project: loader.
 * User: naveed
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<!-- BEGIN CONTENT -->
<?php $user_wallet = $wallet->credit_amount;  ?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">

                        <div class="caption">

                            <span class="caption-subject font-red sbold uppercase">{{ $wallet->user->fname }} {{ $wallet->user->lname }}</span> <small style="margin-left: 5px; font-size: 12px;"> Be Your Own Bank</small> <br>

                        </div>
                        <div class="actions">
                            <h3 style="margin: 0px;"> Total Amount: {{ $wallet->amount }} </h3> 
                            <p style="float: right"> Current Amount: {{ number_format($user_wallet , 2) }}</p>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <button id="sample_editable_1_new" class="btn green" data-toggle="modal" data-target="#myModal"> Cash Withdraw
                                            <i class="fa fa-cash"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="btn-group pull-right">

                                    </div>
                                </div>
                            </div>

                        </div> -->
                        <table class="table table-striped table-hover table-bordered" id="captain-table">
                           <thead>
                            <tr>
                             <th align="left">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> name </th>
                            <th> transection </th> 
                            <th> date </th>
                            <th> Status </th>

                            <th>  total Amount </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>

                          <td align="center">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="checkboxes" value="1" />
                                <span></span>
                            </label>
                        </td>

                        <td> {{ $wallet->user->fname }}</td>
                        <td> withdraw </td>
                        <td> {{ $wallet->updated_at }} </td>

                        @if($wallet->status === 0 )
                        <td>Deactive</td>
                        @else
                        <td> Active </td>
                        @endif



                        <td> {{ number_format($wallet->credit_amount ,3) }} </td>


                    </tr>


                </tbody>
            </table>

            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <div class="dataTables_info" id="sample_editable_1_info" role="status" aria-live="polite"></div>
                </div>
                <div class="col-md-7 col-sm-7">
                    <div class="dataTables_paginate paging_bootstrap_number" id="sample_editable_1_paginate">
                        <ul class="pagination" style="visibility: visible; float: right;">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

</div>
<!-- END CONTENT BODY -->
</div>

</div>
</div>
@endsection       <!-- BEGIN FOOTER -->


@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />



@push('post-scripts')


<script src="{{ asset('assets/apps/scripts/table-datatables-custom-managed.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/apps/scripts/table-datatables-custom-managed.js') }}" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript"></script>

@endpush 

