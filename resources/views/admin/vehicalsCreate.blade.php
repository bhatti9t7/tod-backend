@extends('layout.admin.headerAdmin')
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_0">
                    <div class="portlet box green">
                        <div class="portlet-title head_21">
                            <div class="caption">
                                <i class="fa fa-truck sbold uppercase" style="color:#fff;"></i>Add Vehicles </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <form action="{{ route('vehicals.store') }}" class="form-horizontal" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Vehicals Name</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control input-circle"  name="vehical_name">
                                            </div>
                                        </div>
                                        <div class="form-body">

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Amount Per km</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control input-circle"  name="amount_per_km" >

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Destination Charge</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control input-circle"  name="destination_charge" >

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-3 control-label" style="font-size: 13px;"> Categorie Name</label>
                                        <div class="col-md-4">
                                            <select   class="form-control input-circle" name="vehical_sub_categorie_id">
                                                <option> choose the categories</option>
                                                @foreach ($categories as $categorie)
                                                <option value="{{ $categorie->id }}"> {{ $categorie->name}} </option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                            <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
<style>
.head_21{padding: 20px 10px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey; border-bottom: 1px solid #f26727!important;
} 
.head_color{background-color:#f26727; padding: 10px; font-size: 20px!important; font-weight: bold; color: #fff!important; width: 95%;

}
</style>                