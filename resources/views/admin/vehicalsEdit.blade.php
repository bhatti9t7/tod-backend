@extends('layout.admin.headerAdmin')
@section('content')
<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">

                    </div>
                    
                    
                    
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_0">
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Vehicals Edit  </div>
                                               
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                            {!! Form::open(['method' => 'PATCH', 'route' => ['vehicals.update',$vehicl->id], 'class'=>'form-horizontal']) !!}

                                        
                                                
                                                    <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Vehicals Name</label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle"  name="vehical_name" 
                                                                value="{{ $vehicl->vehical_name }}">
                                                                
                                                            </div>
                                                        </div>
                                                         <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"> Amount Per Km </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle"  name="amount_per_km" 
                                                                value="{{ $vehicl->amount_per_km }}">
                                                                
                                                            </div>
                                                        </div>

                                                         <div class="form-group">
                                                            <label class="col-md-3 control-label"> Destination Charge </label>
                                                            <div class="col-md-4">
                                                                <input type="text" class="form-control input-circle"  name="destination_charge" 
                                                                value="{{ $vehicl->destination_charge }}">
                                                                
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="form-body">
                                                            <div class="form-group">
                                                                <label class="col-md-3 control-label" style="font-size: 13px;"> Categorie Name</label>
                                                                <select   class="form-control input-circle" style="width: 31%;" name="vehical_sub_categorie_id">
                                                                    @foreach ($categories as $categorie)
<option value="{{ $categorie->id }}" @if($categorie->id==$vehicl->vehical_cat_id) selected='selected' @endif> {{ $categorie->name}} </option>
                                                                     @endforeach
                                                                </select>
                                                                           
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>

                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn btn-circle green">Submit</button>
                                                                   {!! Form::close() !!}
                                                                <a  href="{{route('vehicle-sub-categories.index')}}" class="btn btn-success  btn-outline">cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                               
                                          
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

                            