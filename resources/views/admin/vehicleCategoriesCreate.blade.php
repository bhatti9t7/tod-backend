<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
    <!-- BEGIN PAGE HEAD-->
     <div class="row">
          <div class="col-md-12">
             <div class="portlet light portlet-fit bordered">     
    <div class="portlet light box_shadow_form">

        <div class="tab-pane active" id="tab_0">
 
            <div class="portlet-title head_21">
                     
             <i class="fa fa-key" style="color: #fff;"></i>
             <span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 20px!important;"> Add Vehicle categories</span>
           </div></div>
            <div class="col-md-12 col-sm-10 col-xm-10 ">
          
           <div class="portlet-body form">
            <!-- BEGIN FORM-->

            <form action="{{ route('vehical-categories.store') }}" class="form-horizontal" method="POST" enctype = "multipart/form-data" id="upload_new_form">
              {{ csrf_field() }}

              <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="fileinput fileinput-new" data-provides="fileinput" >
                                  <div class="fileinput-new thumbnail" style="max-width:250px!important; height: 250px!important">
                                      <img src="{{asset('images/captain/profiles/no-image.png')}}" id="blah" height="250px" width = "250px">

                          </div>
                        </div>
                                      <div class="form-group" style="margin-top: 10px; margin-left: 25px;">
                                        <div style="margin-left: 35px;">
                                        <label for="image" class="btn btn-primary">Upload Profile</label>
                                        <input type="file" id="image" name="image" class="hide" style="opacity: 0;">
                                          </div>
                                          @if ($errors->has('image'))
                                              <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                  <span aria-hidden="true">×</span>
                                                  <span class="sr-only">Close</span>
                                                </button>
                                                <strong>Warning!</strong> {{$errors->first('image')}}
                                              </div>
                                            @endif
                                      </div>                                      
                       
                    </div>
                 <div class="col-md-7">
                <div class="form-group">
                  <label class=" col-md-4 control-label">Vehical Categories Name</label>
                  <div class="col-md-12">
                    <input type="text" class="form-control input-circle"  name="name" >

                    @if ($errors->has('name'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('name')}}
                    </div>
                    @endif

                  </div>
               </div>

                <div class="form-group">
                  <label class="control-label col-md-4"> weight</label>
                  <div class="col-md-12">
                    <input type="text" class="form-control input-circle"  name="weight" >
                    @if ($errors->has('weight'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('weight')}}
                    </div>
                    @endif

                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-4"> volume </label>
                  <div class="col-md-12">
                <input type="text" class="form-control input-circle"  name="volume" >
                    @if ($errors->has('volume'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('volume')}}
                    </div>
                    @endif

                  </div>
                </div></div>
                 </div>
        <div class="form-group">
                  <label class="col-md-3 control-label"> Base Fare</label>
                  <div class="col-md-12">
                    <input type="text" class="form-control input-circle"  name="base_fare" >
                    @if ($errors->has('base_fare'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('base_fare')}}
                    </div>
                    @endif

                </div>
              </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Amount /Km </label>
                    <div class="col-md-12">
                      <input type="text" class="form-control input-circle"  name="amount_per_km" >
                      @if ($errors->has('amount_per_kem'))
                      <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <strong>Warning!</strong> {{$errors->first('amount_per_kem')}}
                      </div>
                      @endif

                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label"> Destination Charge </label>
                    <div class="col-md-12">
                      <input type="text" class="form-control input-circle"  name="destination_charge" >
                        @if ($errors->has('destination_charge'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('destination_charge')}}
                    </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label"> Free Wait min</label>
                    <div class="col-md-12">
                      <input type="text" class="form-control input-circle"  name="free_time" >
                               @if ($errors->has('free_time'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('free_time')}}
                    </div>
                    @endif
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label"> loading place detention charge </label>
                    <div class="col-md-12">
                      <input type="text" class="form-control input-circle"  name="loading_waiting_price" >
                                @if ($errors->has('loading_waiting_price'))
                    <div class="alert alert-danger" role="alert">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <strong>Warning!</strong> {{$errors->first('loading_waiting_price')}}
                    </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label"> Unloading  place detention charge </label>
                    <div class="col-md-12">
                      <input type="text" class="form-control input-circle"  name="unloading_waiting_price">
                                 @if ($errors->has('unloading_waiting_price'))
                                              <div class="alert alert-danger" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                  <span aria-hidden="true">×</span>
                                                  <span class="sr-only">Close</span>
                                                </button>
                                                <strong>Warning!</strong> {{$errors->first('unloading_waiting_price')}}
                                              </div>
                                            @endif
                    </div>
                  </div>
             
                   
                  </div>
  
    <br>
    <br>
  
      <div class="form-actions">
      
         <div class="col-md-3 pull-right">
           <button type="submit" class="btn btn-info" style="background-color:#f26727; color: #fff;">Submit</button>
              {!! Form::close() !!}
          <a  href="{{route('vehical-categories.index')}}" class="btn btn-success btn-outline"> Cancel</a>
         </div>

     </div>
    
    </form>
  
   </div>
</div>
</div>
   @endsection
   @stack('post-styles')
   <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css"/>
   <style>
   .head_21{padding: 20px 10px;background-color:#f26727;margin-bottom: 20px;box-shadow: 0px 4px 15px dimgrey; border-bottom: 1px solid #f26727!important;
   } 
   .head_color{background-color:#f26727; padding: 10px; font-size: 20px!important; font-weight: bold; color: #fff!important; width: 95%;

   }
 </style>

 @push('post-scripts')

 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
 <script type="text/javascript">
  function readURL(input) {

    if (input.files) {
      var reader = new FileReader();
      var totalfiles = input.files.length;
      for(i = 0; i < totalfiles; i ++){
        reader.onload = function(e) {
          $('#blah').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[i]);
      }
    }
  }
  $("#image").change(function() {
    readURL(this);
  });
</script>
<script type="text/javascript">
  $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                          if (input.files) {
                            var filesAmount = input.files.length;

                            for (i = 0; i < filesAmount; i++) {
                              var reader = new FileReader();

                              reader.onload = function(event) {
                                $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                              }

                              reader.readAsDataURL(input.files[i]);
                            }
                          }

                        };

                        
                      });
                    </script>



                    @endpush




