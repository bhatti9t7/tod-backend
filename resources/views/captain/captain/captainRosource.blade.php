@extends('layout.captain.capheader')
@section('content')
 <!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">Captain </span>
                        </div>
                        <div class="actions">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    
                                </div>

                            </div>

                        </div>
                        <table class="table table-striped table-hover table-bordered" id="captain-table">
                           <thead>
                            <tr>
                             <th align="left">
                                <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                    <span></span>
                                </label>
                            </th>
                            <th> Image </th>
                            <th> Name </th>
                            <th> email </th>

                            <th> phone </th>
                            

                            <th>Action </th>
                        </tr>
                    </thead>
                    <tbody>

                     @foreach ($captain as $i) 
                     <tr>

                      <td align="center">
                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                            <input type="checkbox" class="checkboxes" value="1" />
                            <span></span>
                        </label>
                    </td>
                    <td> <img src="{{asset('images/captain/profiles/'.$i->images)}}" alt=" image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" />  </td>
                    <td> {{ $i->fname }}  {{$i->lname}} </td>
                    <td> {{ $i->email }} </td>
                    <td> {{ $i->phone }} </td>
                    

                    <td>
                     <a href="captain-wallet/{{$i['id']}}"><button class="btn btn-primary btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-briefcase"></span></button></a> 
                     <a href="{{ route('captain-Resources.edit', $i['id']) }}"><button class="btn btn-primary btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-pencil"></span></button></a>

                     <span style="display: inline-block;">
                        {{ Form::open(['method' => 'DELETE', 'route' => ['captain-Resources.destroy', $i->id]] 
                        ) }}
                        {{ Form::hidden('id', $i->id) }}
                        <button class="btn btn-danger btn-xs" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash"></span></button>
                        {!! Form::close() !!}
                    </span>


                </td>



            </tr>
            @endforeach

        </tbody>
    </table>

    
</div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>


<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->
</div>

<a href="javascript:;" class="page-quick-sidebar-toggler">
    <i class="icon-login"></i>
</a>
<div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
    <div class="page-quick-sidebar">

    </div>
</div>

</div>
</div>

</div>

@endsection       


@section('styles')
@parent
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/apps/css/stylebutton.css') }}" rel="stylesheet" type="text/css" />
<!-- <link href="{{ asset('assets/apps/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />
 -->@endsection

@section('javascripts')
@parent


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript"></script>

 <!-- <script src="{{ asset('assets/apps/scripts/table-datatables-custom-managed.js') }}" type="text/javascript"></script>  -->




<script type="text/javascript">
    $(document).ready(function() {
    $('#captain').DataTable( {
        dom: 'Bfrtip',

         "paging": false,
        "bInfo": false,
        "searching": false,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>



<script type="text/javascript">

    function delete_action() {
        var r = confirm("Are you sure you want to delete this!");
        if (r == true) {
           return true;
       } else {

           return false;
       }
   }
</script>

@endsection


