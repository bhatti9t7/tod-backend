@extends('layout.captain.capheader')
@section('content')
 <div class="container">
    <div class="row">
        <div class="page-content-wrapper">
            <div class="page-content" style="min-height: 1279px;">
                <div class="profile">
                    <div class="tabbable-line tabbable-full-width">
                        <div class="tab-content" style="background-color: #fff;">
                            <div class="tab-pane active" id="tab_1_3">
                                <div class="row">
                                    <div class="tab-content">        
                                        <div class="col-md-12">
                                            <h3 style="width: 100%;margin-left: 10px;"><b>Captain Info</b></h3><hr>

                                               {!! Form::open(['method' => 'PATCH','files'=>true, 'route' => ['captain-Resources.update',$captain->id], 'class'=>'form-horizontal']) !!}
                                              {{ csrf_field() }}
                                              <div class="row">

                                                  <div class = "col-md-4"  style="margin-left: 10px;">
                                               <div class="fileinput fileinput-new" data-provides="fileinput" >
                                                  <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;">
                                                    <!-- <div style="text-align: center;font-size:20px;margin:80px;">Profile<br>Photo -->
                                                        <img src="{{asset('images/captain/captain profile/' . $captain->images)}} " id="blah" height="250" width = "250"> 
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                             <label for="images" class="btn btn-primary">Upload Profile</label>
                                             <input type="file" id="images" name="images" class="hide" value="{{ $captain->images }}">
                                         </div>
                                     </div>
                                       <div class="form-group col-md-3" >
                                          <label class="control-label">First Name</label>
                                          <input name = "fname" type="text" placeholder="First Name" class="form-control" value="{{ $captain->fname }}">
                                      </div>
                                      <div class="form-group col-md-3">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" name = "lname" placeholder="Last Name" class="form-control" value="{{ $captain->lname }}">
                                    </div>


                                       <div class="form-group col-md-3">
                                        <label class="control-label">Email</label>
                                        <input name = "email" type="email" class="form-control" placeholder="abc123@example.com" value="{{ $captain->email }}">
                                    </div>                           
                                    <div class="form-group col-md-3">
                                        <label class="control-label">phone</label>
                                        <input type="text" name = "phone" placeholder="Mobile No" class="form-control" value="{{ $captain->phone }}">
                                    </div>                                   
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Password</label>
                                        <input type="password" placeholder="******" class="form-control" name = "password">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Confirm Password</label>
                                        <input name = "password_confirmation" type="password" placeholder="******" class="form-control"> 
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">City</label>
                                        <input name = "city" type="text" placeholder="City" class="form-control" value="{{ $captain->city }}"> 
                                    </div>
                                   

                                 </div>


                                    <div class="row">
                                         <div class="col-md-12">
                                        <hr><h3 style="width: 100%;margin-left: 10px;"><b>Vehicle Info</b></h3><hr>
                                        <div class="form-group col-md-3" >
                                          <label class="control-label">Vehicle Name</label>
                                          <select   class="form-control" name="vehical_id" value="{{ $captain->vehical_id }}">
                                                                    @foreach($vehicals as $i)
                                                                    <option value="{{ $i->id }}">{{ $i->vehical_name}} </option>
                                                                    @endforeach
                                                                       
                                                                      </select>
                                      </div>
                                      <div class="form-group col-md-3">
                                          <label class="control-label">Vehicle No</label>
                                          <input type="text" name = "vehicle_no" placeholder="Vehicle No" class="form-control" value="{{ $captain->vehicle_no }}">
                                      </div>
                                      <div class="form-group col-md-3">
                                          <label class="control-label">Vehicle Registration No</label>
                                          <input name = "vehicle_registration" type="text" class="form-control" placeholder="Registration No" value="{{ $captain->vehicle_registration }}">
                                      </div>                                           
                                      <div class="form-group col-md-3">
                                        <label class="control-label">License No</label>
                                        <input type="text" name = "licence_no" placeholder="License No" class="form-control" value="{{ $captain->licence_no }}">
                                    </div>                          
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Chesis No</label>
                                        <input type="text" placeholder="Chesis No" class="form-control" name = "chesi_no" value="{{ $captain->chesi_no }}">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Engine No</label>
                                        <input name = "engin_no" type="text" placeholder="Engine No" class="form-control" value="{{ $captain->engin_no }}"> 
                                    </div>
                                    </div>





                                    <div class = "col-md-12"  style="margin-left: 10px;">
                                         <div class="fileinput fileinput-new" data-provides="fileinput">
                                          <!-- <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;"> -->
                                            <!-- <div style="text-align: center;font-size:20px;margin:80px;">Upload<br> C.N.I.C Image</div> -->
                                        </div>
                                   </div>
                                    <div class="form-group" style="padding: 0px 20px;">
                                       <label for="cnic_images" class="btn btn-primary">Upload C.N.I.C Images</label>

                                       <img src="{{asset('images/captain/captain cnic/' . $captain->cnic_images)}} " id="blah" height="100" width = "100">
                                       <input type="file" id="cnic_images" name="cnic_images[]" class="hide" multiple>
                                       
                                   </div>
                                   <div class="col-md-12">
                                       <div class="form-group" style="padding: 0px 20px; ">
                                          <div class = "gallery"></div>
                                       </div>
                                   </div>
                                </div> 
                           <div class = "col-md-4"  style="margin-left: 10px;">
                                     <div class="fileinput fileinput-new" data-provides="fileinput">
                                      <!-- <div class="fileinput-new thumbnail" style="width: 250px; height: 250px;"> -->
                                        <!-- <div style="text-align: center;font-size:20px;margin:80px;">Upload<br> License Image</div> -->
                                    </div>
                                </div>
                                <div class="form-group" style="padding: 0px 20px;">
                                       <label for="licence_images" class="btn btn-primary">Upload License Images</label>

                                       <img src="{{asset('images/captain/captain license/' . $captain->driving_licenes_images)}} " id="blah" height="100" width = "100">
                                       <input type="file" id="licence_images" name="driving_licenes_images[]" class="hide" multiple>
                                       
                                   </div>
                                   <div class="col-md-12">
                                       <div class="form-group" style="padding: 0px 20px; ">
                                          <div class = "gallery2"></div>
                                       </div>
                                   </div>
                                </div> 
                           </div> 

                           <div class="form-group col-md-2">
                            <div class="fileinput fileinput-new" data-provides="fileinput"></div>
                            
                  </div>





                  <div class = "form-group col-md-12">
                    <hr><h3 style="width: 100%;margin-left: 10px;"><b>Owner Info</b></h3><hr> 

                    <div class="row" style="padding: 20px;">
                       <div class="form-group col-md-4">
                        <label class="control-label">Owner Name</label>
                        <input name="owner_name" type="text" placeholder="Vehicle Name" class="form-control" value="{{ $captain->owner_name }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label">Owner Father Name</label>
                        <input name="owner_fname" type="text" placeholder="Vehicle Name" class="form-control" value="{{ $captain->owner_fname }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label class="control-label">Owner City</label>
                        <input name="owner_city" type="text" placeholder="Vehicle Name" class="form-control" value="{{ $captain->owner_city }}">
                    </div>
                     <div class="form-group col-md-12">
                <div class="margiv-top-10 pull-right">
                 <button class="btn green" type = "Submit" name = "submit">Save</button>
                 <button class="btn red" type="reset">Cancel</button>
             </div>
         </div>
                </div>
            </div>
           
     </div>
 {!! Form::close() !!}
</div>
</div>
</div>
</div></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">
    function readURL(input) {

  if (input.files) {
    var reader = new FileReader();
    var totalfiles = input.files.length;
    for(i = 0; i < totalfiles; i ++){
    reader.onload = function(e) {
      $('#blah').attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[i]);
  }
}
}
$("#images").change(function() {
  readURL(this);
});
</script>
<script type="text/javascript">
$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img height = "100" width = "100" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $('#cnic_images').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
    $('#licence_images').on('change', function() {
        imagesPreview(this, 'div.gallery2');
    });
});
</script>

@endsection
@section('styles')
@parent
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
@endsection