@extends('layout.captain.capheader')
@section('content')
            <div class="page-content-wrapper">
                <div class="page-content" style="min-height: 1279px;">
                    <div class="page-head">
                        <div class="page-title">
                            <h1>Add User</h1>
                        </div>
                        <div class="page-toolbar">
                            <div class="btn-group btn-theme-panel">
                                <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                                </a>
                                <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <h3>HEADER</h3>
                                            <ul class="theme-colors">
                                                <li class="theme-color theme-color-default active" data-theme="default">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Dark Header</span>
                                                </li>
                                                <li class="theme-color theme-color-light " data-theme="light">
                                                    <span class="theme-color-view"></span>
                                                    <span class="theme-color-name">Light Header</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-12 seperator">
                                            <h3>LAYOUT</h3>
                                            <ul class="theme-settings">
                                                <li> Theme Style
                                                    <select class="layout-style-option form-control input-small input-sm">
                                                        <option value="square">Square corners</option>
                                                        <option value="rounded" selected="selected">Rounded corners</option>
                                                    </select>
                                                </li>
                                                <li> Layout
                                                    <select class="layout-option form-control input-small input-sm">
                                                        <option value="fluid" selected="selected">Fluid</option>
                                                        <option value="boxed">Boxed</option>
                                                    </select>
                                                </li>
                                                <li> Header
                                                    <select class="page-header-option form-control input-small input-sm">
                                                        <option value="fixed" selected="selected">Fixed</option>
                                                        <option value="default">Default</option>
                                                    </select>
                                                </li>
                                                <li> Top Dropdowns
                                                    <select class="page-header-top-dropdown-style-option form-control input-small input-sm">
                                                        <option value="light">Light</option>
                                                        <option value="dark" selected="selected">Dark</option>
                                                    </select>
                                                </li>
                                                <li> Sidebar Mode
                                                    <select class="sidebar-option form-control input-small input-sm">
                                                        <option value="fixed">Fixed</option>
                                                        <option value="default" selected="selected">Default</option>
                                                    </select>
                                                </li>
                                                <li> Sidebar Menu
                                                    <select class="sidebar-menu-option form-control input-small input-sm">
                                                        <option value="accordion" selected="selected">Accordion</option>
                                                        <option value="hover">Hover</option>
                                                    </select>
                                                </li>
                                                <li> Sidebar Position
                                                    <select class="sidebar-pos-option form-control input-small input-sm">
                                                        <option value="left" selected="selected">Left</option>
                                                        <option value="right">Right</option>
                                                    </select>
                                                </li>
                                                <li> Footer
                                                    <select class="page-footer-option form-control input-small input-sm">
                                                        <option value="fixed">Fixed</option>
                                                        <option value="default" selected="selected">Default</option>
                                                    </select>
                                                </li>   
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="captain_form_wrapper">
            <div class="container">
                <div class="row ">
    
                    <div class="col-md-12 add_user_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae;padding-bottom: 5px;"> Captain Form</h1>
                         <div class="tab-pane active">
                               <form method="POST" action="{{ route('captainResource.store') }}" enctype = "multipart/form-data" id="upload_new_form">
                                     {{ csrf_field() }}
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="fname" > 
                                            <label for="form_control_1">First Name </label>
                                            <span class="help-block">Please write your firts name here </span>
                                            
                                        </div>
                                    </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="lname">
                                            <label for="form_control_1">Last Name </label>
                                            <span class="help-block">Please write your last name here </span>
                                            
                                        </div>
                                     </div>

                                      <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="email" class="form-control" name="email">
                                            <label for="form_control_1"> Email Adress </label>
                                            <span class="help-block">Please write your Email Adress here </span>
                                            
                                        </div>
                                     </div>

                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="phone">
                                            <label for="form_control_1">Phone Number </label>
                                            <span class="help-block">+92313******* </span>
                                           
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="password" class="form-control" name="password">
                                            <label for="form_control_1">Password  </label>
                                            <span class="help-block"> Please enter your password here </span>
                                           
                                        </div>
                                     </div>
                                      <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="password" class="form-control" name="password_confirmation">
                                            <label for="form_control_1">Confirm Password  </label>
                                            <span class="help-block"> Please confirm your password here </span>
                                           
                                        </div>
                                     </div>

                                         <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="city">
                                            <label for="form_control_1"> City  </label>
                                            <span class="help-block"> Write your city name here</span>
                                           
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6" id="input_color">
                                        <label for="exampleInputFile1">File input</label>
                                        <input type="file" id="exampleInputFile1" name="images[]" multiple>
                                        <p class="help-block"> some help text here. </p>
                                    </div>

                                
                            </div>
                    </div>
                </div>
       

                <div class="row ">
                    <div class="col-md-12 captain_form_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae; padding-bottom: 5px;"> Vehical Form</h1>
                         <div class="tab-pane active">
                                
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="vehical_id">
                                            <label for="form_control_1">Vehicle Id </label>
                                            <span class="help-block">Please enter your vehicle id here </span>
                                          
                                        </div>
                                    </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="vehicle_no">
                                            <label for="form_control_1">Vehicle Number </label>
                                            <span class="help-block">Please enter your vehicle number here </span>
                                           
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="vehicle_registration">
                                            <label for="form_control_1">Vehicle Registration Number </label>
                                            <span class="help-block">Please enter your vehicle registration number here </span>
                                            
                                        </div>
                                     </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="licence_no">
                                            <label for="form_control_1">License Number </label>
                                            <span class="help-block">Please enter your license number here </span>
                                        </div>
                                     </div>


                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-4" id="input_color">
                                        <label for="exampleInputFile1" >Driving License Image</label>
                                        <input type="file" id="exampleInputFile1" name="driving_licenes_images[]" multiple>
                                        <p class="help-block">Please upload your driving license image here </p>
                                    </div>
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-4" id="input_color">
                                        <label for="exampleInputFile1">CNIC Images</label>
                                        <input type="file" id="exampleInputFile1" name="cnic_img[]" multiple>
                                        <p class="help-block"> Please upload your CNIC Front side image here </p>
                                    </div>
                                    

                                
                            </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-12 captain_form_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae; padding-bottom: 5px;"> Owner Form</h1>
                         <div class="tab-pane active">
                               
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="owner_name">
                                            <label for="form_control_1">Owner Name </label>
                                            <span class="help-block">Please write Owner name here </span>
                                         </div>
                                    </div>
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="owner_fname">
                                            <label for="form_control_1">Owner Father Name </label>
                                            <span class="help-block">Please write Owner Father name here </span>
                                         </div>
                                    </div>
                                     <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="owner_city">
                                            <label for="form_control_1">Owner City </label>
                                            <span class="help-block">Please write Owner city name here </span>
                                         </div>
                                    </div>
                                    
                                
                            </div>
                    </div>
                </div>
                    <div class="row ">
                    <div class="col-md-12 captain_form_inner">
                        <h1 style="text-align: center; color: #fff; font-weight: bold; background-color: #44b6ae; padding-bottom: 5px;"> Owner Form</h1>
                         <div class="tab-pane active">
                                
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="chesi_no">
                                            <label for="form_control_1">Chesi Number  </label>
                                            <span class="help-block">Please write Chesi Number here </span>
                                         </div>
                                    </div>
                                    <div class="form-group form-md-line-input has-success form-md-floating-label col-md-6">
                                        <div class="input-icon right" id="input_color">
                                            <input type="text" class="form-control" name="engin_no">
                                            <label for="form_control_1">Engine Number </label>
                                            <span class="help-block">Please write Engine here </span>
                                         </div>
                                    </div>
                                    <div class="col-md-12">
                                         <div class="margiv-top-10 save_btn_display">
                                            <button class="btn green" type="submit" name="submit"> Save </button> 
                                            <button class="btn default" type="reset"> Cancel </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('styles')
@parent
        <link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
@endsection