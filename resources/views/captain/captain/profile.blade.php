@extends('layout.captain.capheader')
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-head">

        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PROFILE SIDEBAR -->
                <div class="profile-sidebar">
                    <!-- PORTLET MAIN -->

                    <div class="profile-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_2" data-toggle="tab">Change Avatar</a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                            </li>
                                                   <!--  <li>
                                                        <a href="#tab_1_4" data-toggle="tab">Privacy Settings</a>
                                                    </li> -->
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="tab-content">
                                                    <!-- PERSONAL INFO TAB -->
                                                    <div class="tab-pane active" id="tab_1_1">

                                                        {!! Form::open(['method' => 'PATCH','files'=>true, 'route' => ['captain-Resources.update',$admin->id], 'class'=>'form-horizontal']) !!}

                                                        <form role="form" action="{}" method="POST">
                                                           <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                            <div class="input-icon">
                                                                <input type="text" class="form-control" name="fname" value="{{$admin->fname}}">
                                                                <label for="form_control_1"> First Name</label>
                                                                
                                                                <i class="fa fa-user"></i>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                            <div class="input-icon">
                                                                <input  class="form-control" name="lname" value="{{$admin->lname}}">
                                                                <label  for="form_control_1">  Last name</label>
                                                                
                                                                <i class="glyphicon glyphicon-envelope"></i>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                            <div class="input-icon">
                                                                <input type="email" class="form-control" name="email" value="{{$admin->email}}">
                                                                <label type="email" for="form_control_1">  Email</label>
                                                                
                                                                <i class="glyphicon glyphicon-envelope"></i>
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                            <div class="input-icon">
                                                                <input type="text" class="form-control" name="phone" value="{{$admin->phone}}">
                                                                <label for="form_control_1">  Phone</label>
                                                                
                                                                <i class="fa fa-phone"></i>
                                                            </div>
                                                        </div>


                                                        
                                                        <div class="margiv-top-10">
                                                            <button type="submit " class="btn green"> Save Changes </button>
                                                            
                                                        </div>
                                                        {!! Form::close() !!}
                                                    </div>
                                                    
                                                    <div class="tab-pane" id="tab_1_2">

                                                        {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['captain-Resources.update',$admin->id], 'class'=>'form-horizontal']) !!}
                                                        <div class="form-group">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="{{asset('images/captain/profiles/' . $admin->images)}}" alt="" />
                                                                    <img src="#" id="blah">
                                                                     </div>
                                                                    
                                                                    <div>
                                                                       <label for="images" class="btn btn-primary">Upload Profile</label>
                                                                            
                                                                        <input type="file" id="images" name="images" class="hide">
                                         
                                                                            
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <button type="submit" class="btn green"> Submit </button>
                                                                    
                                                                </div>
                                                                {!! Form::close() !!}
                                                            </div>
                                                            <div class="tab-pane" id="tab_1_3">
                                                             {!! Form::open(['method' => 'PATCH', 'route' => ['captain-Resources.update',$admin->id], 'class'=>'form-horizontal']) !!}
                                                             
                                                            <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                                <div class="input-icon">
                                                                    <input type="password" name="password" class="form-control">
                                                                    <label for="form_control_1"> New Password</label>
                                                                    <span class="help-block">Write Your New Password</span>
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-md-line-input has-success form-md-floating-label">
                                                                <div class="input-icon">
                                                                    <input type="password" name="password_confirmation" class="form-control">
                                                                    <label for="form_control_1"> Re-type Password</label>
                                                                    <span class="help-block">Write Your Password</span>
                                                                    <i class="fa fa-user"></i>
                                                                </div>
                                                            </div>
                                                            <div class="margin-top-10">
                                                                <button type="submit" class="btn green"> Change Password </button>
                                                               
                                                            </div>
                                                            {!! Form::close() !!}
                                                        </div>
                                                        <!-- END CHANGE PASSWORD TAB -->
                                                        <!-- PRIVACY SETTINGS TAB -->

                                                        <!-- END PRIVACY SETTINGS TAB  -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--   END PROFILE CONTENT -->
                            </div>
                        </div>
                        <!-- END PAGE BASE CONTENT -->
                    </div>
                    <!--    END CONTENT BODY -->
                </div>
                
                


        @endsection

        @section('styles')
        @parent

        <link href="{{ asset('assets/pages/css/profile-2.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{asset('assets/layouts/layout4/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/layouts/layout4/css/themes/default.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{asset('assets/layouts/layout4/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
        @endsection

        @section('javascripts')
        @parent
        <script src="{{asset('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{asset('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/gmaps/gmaps.min.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

          <script type="text/javascript">
                          
                           $('#role_select').multiSelect();
                           $('#permission_select').multiSelect();
                      </script>

                <script type="text/javascript">
                    function readURL(input) {

                        if (input.files) {
                            var reader = new FileReader();
                            var totalfiles = input.files.length;
                            for(i = 0; i < totalfiles; i ++){
                                reader.onload = function(e) {
                                    $('#blah').attr('src', e.target.result);
                                }
                                reader.readAsDataURL(input.files[i]);
                            }
                        }
                    }
                    $("#images").change(function() {
                        readURL(this);
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                            if (input.files) {
                                var filesAmount = input.files.length;

                                for (i = 0; i < filesAmount; i++) {
                                    var reader = new FileReader();

                                    reader.onload = function(event) {
                                        $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    }

                                    reader.readAsDataURL(input.files[i]);
                                }
                            }

                        };

                        
                    });
                </script>

        @endsection  