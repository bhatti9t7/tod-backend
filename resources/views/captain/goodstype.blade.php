@extends('layout.captain.capheader')
@section('content')
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Goods Type</span>
                                    </div>
                                    <div class="actions">
                                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                                            <label class="btn btn-transparent red btn-outline btn-circle btn-sm active">
                                                <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                            <label class="btn btn-transparent red btn-outline btn-circle btn-sm">
                                                <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                                <div class="btn-group">
                                                    <button id="sample_editable_1_new" class="btn green" data-toggle="modal" data-target="#myModal"> Add New
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">

                                                    

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div id="lets_search" class="dataTables_filter" style="padding: 15px ;">
                                                    <label for="search" >Search:<input type="search" class="form-control input-sm input-small input-inline" placeholder="" onkeyup="search" id="search" aria-controls="search"></label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <table class="table table-striped table-hover table-bordered" id="goods-table">
                                       <thead>
                                            <tr>
                                             <th align="left">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                                <th> #id </th>
                                                <th> Goods Name </th>
                                               
                                               
                                                <th> Status </th>
                                               <!--  <th> Edit </th>
                                                <th> Delete </th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                                 @foreach ($datatype as $i) 
                                                <tr>
                                                  
                                                  <td align="center">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1" />
                                                        <span></span>
                                                    </label>
                                                </td>
                                        
                                                    <td> {{ $i->id }} </td>
                                                    <td> {{ $i->good_name }} </td>
                                                    
                                                    
                                                    @if($i->status === 0 )
                                                            <td>Deactive</td>
                                                        @else
                                                            <td>Active</td>
                                                    @endif
                                                    
                                                    <!--  <td> 
                                                        <a href="{{ route('goods.edit', $i['id']) }}"><button class="btn btn-primary btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-pencil"></span></button></a></td>
                                                    <td>

                                                        {{ Form::open(['method' => 'DELETE', 'route' => ['goods.destroy', $i->id]]) }}
                                                        {{ Form::hidden('id', $i->id) }}
                                                             <button class="btn btn-danger btn-xs" onclick="return delete_action()"  data-title="Delete" data-toggle="modal" data-target="#delete" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-trash"></span></button>
                                                        {!! Form::close() !!}

                                                      
                                                    </td> -->
                                                    

                                                    
                                                </tr>
                                                @endforeach
                                                
                                        </tbody>
                                    </table>
                                    
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5">
                                            <div class="dataTables_info" id="sample_editable_1_info" role="status" aria-live="polite">Showing 1 to 5 of 5 entries</div>
                                        </div>
                                        <div class="col-md-7 col-sm-7">
                                            <div class="dataTables_paginate paging_bootstrap_number" id="sample_editable_1_paginate">
                                                <ul class="pagination" style="visibility: visible; float: right;">
                                                   {{ $datatype->links() }}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
               

                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
          
            <!-- END QUICK SIDEBAR -->

             
          




           
          </div> 


 
    
      
        <!-- END CONTAINER -->
 @endsection       <!-- BEGIN FOOTER -->


@section('styles')
    @parent
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

     <link href="{{ asset('assets/apps/css/stylebutton.css') }}" rel="stylesheet" type="text/css" />
     <link href="{{ asset('assets/apps/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('javascripts')
@parent

<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

<script src="{{ asset('assets/apps/scripts/table-datatables-custom-managed.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>

<script src="{{ asset('assets/apps/scripts/table-datatables-custom-managed.js') }}" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript"></script>






<script type="text/javascript">
    $(document).ready(function() {
    $('#goods-table').DataTable( {
        dom: 'Bfrtip',
         "paging": false,
        "bInfo": false,
        "searching": false,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>


<script type="text/javascript">
    
function delete_action() {
var r = confirm("Are you sure you want to delete this!");
if (r == true) {
   return true;
} else {
   
       return false;
}
    }
</script>

@endsection


