  <div class="page-sidebar-wrapper">

                <div class="page-sidebar navbar-collapse collapse">
                    
                    
                    <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start">
                            <a href="{{ route('admin.dashboard')}}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title" style="font-size: 20px; color: #fbc052; font-weight: bold;     text-shadow: 0px 1px 2px black;">Dashboard</span>
                                <span class="selected"></span>
                            </a>
                            
                        </li>
                        <!-- CMS content mangement tab -->
                        <li class="nav-item start {{ areActiveRoutes(['report.index','report.create','report.edit' ,'user-report.index','user-report.create','user-report.edit' ],'active open') }}  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <span class="title">CMS Mangement </span>
                                <span class="arrow"></span>
                            </a>
                       
                            <ul class="sub-menu">
                                <li class="nav-item {{ areActiveRoutes(['logo.index','logo.create','logo.edit']) }} ">
                                        <a href="{{route('logo.index')}}" class="nav-link ">
                                            <i class="fa fa-cog"></i>
                                            <span class="title">logo setting</span>
                                        </a>
                                </li>
                                <li class="nav-item  {{ areActiveRoutes(['public-home-page.index','public-home-page.create','public-home-page.edit']) }}">
                                        <a href="{{route('public-home-page.index')}}" class="nav-link ">
                                            <i class="fa fa-home"></i>
                                            <span class="title">Home Page</span>
                                        </a>
                                </li>
                           
                            </ul>
                        </li>
                     
                        <!-- CMS end tab here -->
                        
                        <li class="heading">
                             <i class="fa fa-file" style="display: inline-block; color: #fff; font-size: 19px;"></i>
                             <h3 class="uppercase" style="display: inline-block;" id="pages">Pages</h3>
                        </li>

                        

                            <li class="nav-item start {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit' ,'roles.index' ,'roles.create','roles.edit','goods.index','goods.create','goods.edit' ,'good-categories.index','good-categories.create','good-categories.edit', 'vehical-categories.index','vehical-categories.create','vehical-categories.edit','vehicle-sub-categories.index','vehicle-sub-categories.create','vehicle-sub-categories.edit', 'financial.index','financial.create','financial.edit' ],'active open') }} ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-desktop "></i>
                                <span class="title">System Definitions</span>
                                <span class="arrow {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit'],'active open') }}"></span>
                            </a>
                            <ul class="sub-menu {{ areActiveRoutes(['permissions.index','permissions.create'],'style=display:block;') }} ">
                            
                            @if(auth()->user()->can('permissions index'))
                            <li class="nav-item  {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit']) }}">
                                <a href="{{ route('permissions.index')}}" class="nav-link ">
                                    <i class="fa fa-key"></i>
                                    <span class="title">Permissions</span>
                                </a>
                            </li>
                            @endif
                           @if(auth()->user()->can('roles index'))
                                
                            <li class="nav-item {{ areActiveRoutes(['roles.index','roles.create','roles.edit']) }}  ">
                                <a href="{{ route('roles.index')}}" class="nav-link ">
                                    <i class="fa fa-tasks"></i>
                                    <span class="title">Roles</span>
                                </a>
                            </li>
                            @endif

                        
                           <!--  <li class="nav-item {{ areActiveRoutes(['good-categories.index','good-categories.create','good-categories.edit']) }}">
                                <a href="{{ route('good-categories.index')}}" class="nav-link ">
                                <i class="fa fa-truck"></i>
                                <span class="title">Goods Categories</span>
                                    </a>
                            </li> -->
                         
                           
                           @if(auth()->user()->can('Goods index'))
                            <li class="nav-item  {{ areActiveRoutes(['goods.index','goods.create','goods.edit']) }}">
                                <a href="{{ route('goods.index')}}" class="nav-link ">
                                <i class="fa fa-truck"></i>
                                <span class="title">Goods</span>
                                    </a>
                            </li>
                            @endif
                        
                          @if(auth()->user()->can('Vehicle_Categories index'))
                            <li class="nav-item  {{ areActiveRoutes(['vehical-categories.index','vehical-categories.create','vehical-categories.edit']) }}">
                                <a href="{{route('vehical-categories.index')}}" class="nav-link ">
                                <i class="fa fa-truck"></i>
                                <span class="title">Vehicle Types</span>
                                
                                </a>
                            </li>
                             @endif
                            @if(auth()->user()->can('Vehicle index'))
                            <li class="nav-item  {{ areActiveRoutes(['vehicle-sub-categories.index','vehicle-sub-categories.create','vehicle-sub-categories.edit']) }}">
                                <a href="{{route('vehicle-sub-categories.index')}}" class="nav-link ">
                                <i class="fa fa-truck"></i>
                                <span class="title">Vehicles Categories </span>
                                
                                </a>
                            </li>
                            @endif

                             <li class="nav-item  {{ areActiveRoutes(['emergencies.index','emergencies.create','emergencies.edit']) }}">
                                <a href="{{route('emergencies.index')}}" class="nav-link ">
                                <i class="fa fa-user-md"></i>
                                <span class="title">Emergency caution </span>
                                
                                </a>
                            </li>


                            <li class="nav-item  {{ areActiveRoutes(['skip-reasons.index','skip-reasons.create','skip-reasons.edit']) }}">
                                <a href="{{route('skip-reasons.index')}}" class="nav-link ">
                                <i class="fa fa-comment"></i>
                                <span class="title">Skip Reasons </span>
                                
                                </a>
                            </li>

                            @if(auth()->user()->can('financial index'))
                            <li class="nav-item  {{ areActiveRoutes(['financial.index','financial.create','financial.edit']) }}">
                                <a href="{{route('financial.index')}}" class="nav-link ">
                                <i class="fa fa-money"></i>
                                <span class="title">Financial Setting</span>
                                    </a>
                            </li>
                            @endif
                        </ul>

                        </li>
                        
                            

                        <li class="nav-item start {{ areActiveRoutes(['report.index','report.create','report.edit' ,'user-report.index','user-report.create','user-report.edit' ],'active open') }}  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">Financial Report</span>
                                <span class="arrow"></span>
                            </a>
                       
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="{{route('main-wallet-data')}}" class="nav-link ">
                                    <i class="fa fa-gift"></i>
                                    <span class="title">Main Wallet</span>
                                        </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{route('inflow.index')}}" class="nav-link ">
                                    <i class="fa fa-history"></i>
                                    <span class="title">Inflow</span>
                                        </a>
                                </li>
                               

                                <li class="nav-item  ">
                                    <a href="{{route('out-flow.index')}}" class="nav-link ">
                                    <i class="fa fa-history"></i>
                                    <span class="title">Out flow</span>
                                        </a>
                                </li>

                                <li class="nav-item  ">
                                    <a href="{{route('offers.index')}}" class="nav-link ">
                                    <i class="fa fa-gift"></i>
                                    <span class="title">Offer</span>
                                        </a>
                                </li>

                                <li class="nav-item  ">
                                    <a href="{{route('report.index')}}" class="nav-link ">
                                    <i class="fa fa-history"></i>
                                    <span class="title">captain report</span>
                                        </a>
                                </li>

                                <li class="nav-item  ">
                                    <a href="{{route('finance-report.index')}}" class="nav-link ">
                                    <i class="fa fa-history"></i>
                                    <span class="title">Financial report</span>
                                        </a>
                                </li>


                                
                            </ul>
                        </li>

                         
                        <li class="nav-item  {{ areActiveRoutes(['adminResource.index','adminResource.create']) }}">
                            <a href="{{ route('adminResource.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Admin </span>
                                
                            </a>
                        </li>

                         <li class="nav-item  {{ areActiveRoutes(['country.index','country.create']) }}">
                            <a href="{{ route('country.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title"> Country  </span>
                                
                            </a>
                        </li>

                        <li class="nav-item  {{ areActiveRoutes(['cities.index','cities.create']) }}">
                            <a href="{{ route('cities.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title"> City  </span>
                                
                            </a>
                        </li>

                        <li class="nav-item  {{ areActiveRoutes(['branch.index','branch.create']) }}">
                            <a href="{{ route('branch.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title"> Branches  </span>
                                
                            </a>
                        </li>

                        <li class="nav-item  {{ areActiveRoutes(['franchise.index','franchise.create']) }}">
                            <a href="{{ route('franchise.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title"> Franchise  </span>
                                
                            </a>
                        </li>
              
                            @if(auth()->user()->can('Captain index'))
                        <li class="m-menu__item  {{ areActiveRoutes(['captainResource.index','captainResource.create','captainResource.edit']) }} " aria-haspopup="true">
                            <a href="{{ route('captainResource.index')}}" class="m-menu__link ">
                                <i class="icon-user"></i>
                                <span class="title">Captain </span>
                                
                            </a>
                        </li>
                            @endif
                            @if(auth()->user()->can('User index'))
                        <li class="nav-item  {{ areActiveRoutes(['userResource.index','userResource.create' ,'userResource.edit']) }}">
                            <a href="{{ route('userResource.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">User </span>
                                
                            </a>
                        </li>
                        @endif
                           
                    
                        <li class="nav-item {{ areActiveRoutes(['bookings.index','bookings.create' ,'bookings.edit']) }} ">
                            <a href="{{ route('bookings.index')}}" class="nav-link ">
                                <i class="fa fa-ticket"></i>
                                <span class="title">Bookings</span>
                            </a>
                        </li>

                        <li class="nav-item {{ areActiveRoutes(['pending-booking.index','pending-booking.create' ,'pending-booking.edit']) }} ">
                            <a href="{{ route('pending-booking.index')}}" class="nav-link ">
                                <i class="icon-briefcase"></i>
                                <span class="title">Bookings(Not Responseded)</span>
                            </a>
                        </li>
                       
                 
                        <li class="nav-item  {{ areActiveRoutes(['advance-bookings.index','advance-bookings.create' ,'advance-bookings.edit']) }}">
                            <a href="{{ route('advance-bookings.index')}}" class="nav-link ">
                                <i class="fa fa-ticket" ></i>
                                <span class="title">Advance Bookings</span>
                            </a>
                        </li>


                         <li class="nav-item  {{ areActiveRoutes(['pendingRide']) }}">
                            <a href="{{ route('pendingRide')}}" class="nav-link ">
                                 <i class="fa fa-truck" aria-hidden="true" ></i>
                                <span class="title">Current Rides</span>
                            </a>
                        </li>
                        
                        <li class="nav-item  {{ areActiveRoutes(['Rides.index','Rides.create' ,'Rides.edit']) }}">
                            <a href="{{ route('Rides.index')}}" class="nav-link ">
                                 <i class="fa fa-truck" aria-hidden="true" ></i>
                                <span class="title">Completed Rides</span>
                            </a>
                        </li>
                       

                        <li class="nav-item  {{ areActiveRoutes(['googlemaps.index','googlemaps.create']) }}">
                            <a href="{{ route('googlemaps.index')}}" class="nav-link ">
                                <i class="icon-pointer"></i>
                                <span class="title">Vehicle Tracking</span>
                            </a>
                        </li>
                        
                       
                            
                        <li class="nav-item  {{ areActiveRoutes(['activity-logs.index','activity-logs.create']) }}">
                            <a href="{{ route('activity-logs.index')}}" class="nav-link ">
                                <i class="fa fa-history"></i>
                                <span class="title">Activity Log</span>
                            </a>
                        </li>
                    </li>
                </ul>
                    <!-- END SIDEBAR MENU -->
            </div>
                <!-- END SIDEBAR -->
        </div>



