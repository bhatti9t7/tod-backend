  <div class="page-sidebar-wrapper">

                <div class="page-sidebar navbar-collapse collapse">
                    
                    
                    <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start">
                            <a href="{{ route('admin.dashboard')}}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                            </a>
                            
                        </li>
                        
                        <li class="heading">
                            <h3 class="uppercase">Pages</h3>
                        </li>

                            <li class="nav-item start {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit' ,'roles.index' ,'roles.create','roles.edit','goods.index','goods.create','goods.edit' ,'good-categories.index','good-categories.create','good-categories.edit', 'vehical-categories.index','vehical-categories.create','vehical-categories.edit','vehicals.index','vehicals.create','vehicals.edit', 'financial.index','financial.create','financial.edit' ],'active open') }} ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">System Definitions</span>
                                <span class="arrow {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit'],'active open') }}"></span>
                            </a>
                            <ul class="sub-menu" {{ areActiveRoutes(['permissions.index','permissions.create'],'style=display:block;') }} >
                            
                        
                            <li class="nav-item  {{ areActiveRoutes(['permissions.index','permissions.create','permissions.edit']) }}">
                                <a href="{{ route('permissions.index')}}" class="nav-link ">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Permissions</span>
                                </a>
                            </li>
                         
                                
                            <li class="nav-item {{ areActiveRoutes(['roles.index','roles.create','roles.edit']) }}  ">
                                <a href="{{ route('roles.index')}}" class="nav-link ">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Roles</span>
                                </a>
                            </li>
                           
                            <li class="nav-item {{ areActiveRoutes(['good-categories.index','good-categories.create','good-categories.edit']) }}">
                                <a href="{{ route('good-categories.index')}}" class="nav-link ">
                                <i class="icon-diamond"></i>
                                <span class="title">Goods Categories</span>
                                    </a>
                            </li>
                            
                            <li class="nav-item  {{ areActiveRoutes(['goods.index','goods.create','goods.edit']) }}">
                                <a href="{{ route('goods.index')}}" class="nav-link ">
                                <i class="icon-diamond"></i>
                                <span class="title">Goods</span>
                                    </a>
                            </li>
                            
                            <li class="nav-item  {{ areActiveRoutes(['vehical-categories.index','vehical-categories.create','vehical-categories.edit']) }}">
                                <a href="{{route('vehical-categories.index')}}" class="nav-link ">
                                <i class="fa fa-car"></i>
                                <span class="title">Vehicle Categories</span>
                                
                                </a>
                            </li>
                          
                            <li class="nav-item  {{ areActiveRoutes(['vehicals.index','vehicals.create','vehicals.edit']) }}">
                                <a href="{{route('vehicals.index')}}" class="nav-link ">
                                <i class="fa fa-car"></i>
                                <span class="title">Vehicles </span>
                                
                                </a>
                            </li>
                        
                            <li class="nav-item  {{ areActiveRoutes(['financial.index','financial.create','financial.edit']) }}">
                                <a href="{{route('financial.index')}}" class="nav-link ">
                                <i class="fa fa-money"></i>
                                <span class="title">Financial Setting</span>
                                    </a>
                            </li>
                            
                          

                            

                            

                        
                        </ul>

                        </li>
                        
                            

                          <li class="nav-item start {{ areActiveRoutes(['report.index','report.create','report.edit' ,'user-report.index','user-report.create','user-report.edit' ],'active open') }}  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-money"></i>
                                <span class="title">Financial Report</span>
                                <span class="arrow"></span>
                            </a>
                       
                            <ul class="sub-menu">
                            
                     
                            <li class="nav-item {{ areActiveRoutes(['report.index','report.create','report.edit']) }} ">
                                <a href="{{route('report.index')}}" class="nav-link ">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Captain Report</span>
                                </a>
                            </li>
                            
                            <li class="nav-item  {{ areActiveRoutes(['user-report.index','user-report.create','user-report.edit']) }}">
                                <a href="{{route('user-report.index')}}" class="nav-link ">
                                    <i class="icon-diamond"></i>
                                    <span class="title">User Report</span>
                                </a>
                            </li>
                           

                            <li class="nav-item  ">
                                <a href="{{route('inflow.index')}}" class="nav-link ">
                                <i class="icon-diamond"></i>
                                <span class="title">Inflow</span>
                                    </a>
                            </li>
                           

                            <li class="nav-item  ">
                                <a href="" class="nav-link ">
                                <i class="icon-diamond"></i>
                                <span class="title">Out flow</span>
                                    </a>
                            </li>
                            
                          

                        </ul>
                        </li>
                       
                         
                        <li class="nav-item  {{ areActiveRoutes(['adminResource.index','adminResource.create']) }}">
                            <a href="{{ route('adminResource.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Admin </span>
                                
                            </a>
                        </li>
              
                            
                        <li class="nav-item  {{ areActiveRoutes(['captainResource.index','captainResource.create','captainResource.edit']) }} ">
                            <a href="{{ route('captainResource.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Captain </span>
                                
                            </a>
                        </li>
                          
                        <li class="nav-item  {{ areActiveRoutes(['userResource.index','userResource.create' ,'userResource.edit']) }}">
                            <a href="{{ route('userResource.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">User </span>
                                
                            </a>
                        </li>
                     
                           
                    
                        <li class="nav-item {{ areActiveRoutes(['bookings.index','bookings.create' ,'bookings.edit']) }} ">
                            <a href="{{ route('bookings.index')}}" class="nav-link ">
                                <i class="icon-briefcase"></i>
                                <span class="title">Bookings</span>
                            </a>
                        </li>
                       
                 
                        <li class="nav-item  {{ areActiveRoutes(['advance-bookings.index','advance-bookings.create' ,'advance-bookings.edit']) }}">
                            <a href="{{ route('advance-bookings.index')}}" class="nav-link ">
                                <i class="icon-briefcase"></i>
                                <span class="title">Advance Bookings</span>
                            </a>
                        </li>
                        
                        
                        <li class="nav-item  {{ areActiveRoutes(['Rides.index','Rides.create' ,'Rides.edit']) }}">
                            <a href="{{ route('Rides.index')}}" class="nav-link ">
                                 <i class="icon-puzzle"></i>
                                <span class="title">Completed Rides</span>
                            </a>
                        </li>
                       
                        
                        <li class="nav-item  {{ areActiveRoutes(['pendingRide.index','pendingRide.create','pendingRide.edit']) }}">
                            <a href="{{ route('pendingRide')}}" class="nav-link ">
                                 <i class="icon-puzzle"></i>
                                <span class="title">Pending Rides</span>
                            </a>
                        </li>
                      
                       
                        <li class="nav-item  {{ areActiveRoutes(['googlemaps.index','googlemaps.create']) }}">
                            <a href="{{ route('googlemaps.index')}}" class="nav-link ">
                                <i class="icon-pointer"></i>
                                <span class="title">Vehicle Tracking</span>
                            </a>
                        </li>
                        
                            
                        

                        
                        
                                


                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>



