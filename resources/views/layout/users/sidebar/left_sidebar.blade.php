  <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start active open">
                            <a href="{{ route('home')}}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            
                        </li>
                        
                        <li class="heading">
                            <h3 class="uppercase">Pages</h3>
                        </li>
                        
                        
                        <li class="nav-item  ">
                            <a href="{{ route('userResources.index')}}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">User </span>
                                
                            </a>
                        </li>
                        
                           
                        
                        
                        <li class="nav-item  ">
                            <a href="{{ route('booking.index')}}" class="nav-link ">
                                <i class="icon-briefcase"></i>
                                <span class="title">Bookings</span>
                            </a>
                        </li>

                        <li class="nav-item  ">
                            <a href="{{ route('advance-booking.index')}}" class="nav-link ">
                                <i class="icon-briefcase"></i>
                                <span class="title">Advance Bookings</span>
                            </a>
                        </li>

                        <li class="nav-item  ">
                            <a href="{{ route('Ride.index')}}" class="nav-link ">
                                 <i class="icon-puzzle"></i>
                                <span class="title">Completed Rides</span>
                            </a>
                        </li>

                        <li class="nav-item  ">
                            <a href="{{ route('pending-Rides')}}" class="nav-link ">
                                 <i class="icon-puzzle"></i>
                                <span class="title">Pending Rides</span>
                            </a>
                        </li>

                        <li class="nav-item  ">
                            <a href="{{ route('googlemap.index')}}" class="nav-link ">
                                <i class="icon-pointer"></i>
                                <span class="title">Vehicals Tracking</span>
                            </a>
                        </li>
                        
                                


                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>



