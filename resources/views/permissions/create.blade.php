
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')


<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
              <div class="portlet light portlet-fit bordered box_shadow_form">
               <div class="portlet-title">
                  <div class="caption">
                    <i class="fa fa-key" style="color: #fff;"></i>
                    <span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 20px!important;"> Available Permissions</span>
                </div>
                <br> </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class='col-lg-12 col-md-12 col-lg-offset-4' style="margin: 0 auto;">

                          @component('_components.alerts-default')
                          @endcomponent
                          {{ Form::open(array('url' => 'admin/permissions')) }}

                          <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', '', array('class' => 'form-control')) }}
                        </div><br>
                        @if(!$roles->isEmpty())
                        <hr>
                        <h4>Assign Permission to Roles</h4>
                        <hr>
                        @foreach ($roles as $role) 
                        {{ Form::checkbox('roles[]',  $role->id ) }}
                        {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                        @endforeach
                        @endif
                        <br>
                        <div class="form-group"> 
                            {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                            {{ Form::close() }}
                            <a href="{{route('permissions.index')}}" class="btn btn-success"> Cancel</a>

                        </div>
                    </div>
                    <br>
                </div>
            </div>
            @endsection
            @stack('post-styles')
            <style>
            .box_shadow_form{padding: 8px;
                box-shadow: 1px 1px 6px #ccc;

            }
            
        </style>
        <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />


        @push('post-scripts')


        

        @endpush
