
<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')


<div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
              <div class="row">
            <div class="col-md-12">
             <div class="portlet light portlet-fit bordered">
             <div class="portlet-title">                               
           
    <h1><i style="color: #fff;" class='fa fa-key'></i><span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 18px!important;"> Permission Edit: {{$permission->name}}</span></h1>     
          </div>
        
                <div class="col-sm-11 box_shadow_form">
    {{ Form::model($permission, array('route' => array('permissions.update', $permission->id), 'method' => 'PUT')) }}{{-- Form model binding to automatically populate our fields with permission data --}}

    <div class="form-group">
        {{ Form::label('name', 'Permission Name') }}
        {{ Form::text('name', null, array('class' => 'form-control')) }}
    </div>
    <br>
         <div class="form-group"> 
    {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}
     <a href="{{route('permissions.index')}}" class="btn btn-success"> Cancel</a>
  </div></div>
  <br>
</div>
</div>

@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
<style>
  .box_shadow_form{
    margin: 0 auto; border: 1px solid #ccc; box-shadow: 0px 1px 8px #ccc;
  }
</style>
@push('post-scripts')            

 @endpush