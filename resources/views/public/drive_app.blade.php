@extends('layout.public.publicheader')
@section('content')
<div class="_style_4aX1Qk" data-reactid="260"><div class="gutter" data-reactid="261"><div class="dotcom-body app-body" data-reactid="262"><div><div><div class=""><div class="pull-app-gutter--sides soft-app-gutter--left bg-primary-layer-color position--relative z-20"><div class="bg-primary-layer-color soft-app-gutter--right position--relative
                         portable-pull-layout-gutter--left portable-soft-layout-gutter--left
                         soft-huge--bottom"><div class="desk-wrapper"><div class="soft-huge--right soft-huge--ends lap-hard--right partner-app__hero-copy"><div></div><div class="soft-tiny--bottom five-sixths"><h3 class="flush"><div>Putting drivers first</div><div class="primary-font--thin">An experience built around you</div></h3></div><p class="lead flush">You're in control with Loader. The app for drivers is designed with features that help you decide how, where, and when you earn.</p></div></div></div><div class="split-pane-image partner-app__hero-image split-pane-image--right
                           split-pane-image--elevated video-image " style="transform: scaleX(1);"></div><div class="video-play-button--split-pane-image position--absolute display--inline-block"></div><div class="content-cta__pattern position--absolute right top"><div class="theme-pattern"><div aria-label="Decorative pattern" aria-hidden="false" class="full-height"><div class="isvg loaded full-height"><svg xmlns="http://www.w3.org/2000/svg"><title>Decorative pattern</title><desc>Decorative Illustration</desc>
    <defs>
        <pattern id="a___-1414862764" width="60" height="60" patternUnits="userSpaceOnUse">
            <path class="pattern-stroke" d="M11.5 39.8L0 51.2 8.8 60h12.4l8.8-8.8-11.5-11.4c-1.9-2-5.1-2-7 0z"></path>
            <path class="pattern-stroke" d="M30 38.8L18.5 50.3c-2 2-5.1 2-7.1 0L0 38.8 8.8 30h12.4l8.8 8.8zm11.5 1L30 51.2l8.8 8.8h12.4l8.8-8.8-11.5-11.4c-1.9-2-5.1-2-7 0z"></path>
            <path class="pattern-stroke" d="M60 38.8L48.5 50.3c-2 2-5.1 2-7.1 0L30 38.8l8.8-8.8h12.4l8.8 8.8zm-48.5-29L0 21.2 8.8 30h12.4l8.8-8.8L18.5 9.8c-1.9-2-5.1-2-7 0z"></path>
            <path class="pattern-stroke" d="M30 8.8L18.5 20.3c-2 2-5.1 2-7.1 0L0 8.8 8.8 0h12.4L30 8.8zm11.5 1L30 21.2l8.8 8.8h12.4l8.8-8.8L48.5 9.8c-1.9-2-5.1-2-7 0z"></path>
            <path class="pattern-stroke" d="M60 8.8L48.5 20.3c-2 2-5.1 2-7.1 0L30 8.8 38.8 0h12.4L60 8.8z"></path>
        </pattern>
    </defs>
    <rect fill="url(#a___-1414862764)" height="100%" width="100%"></rect>
</svg>
</div></div></div></div></div></div></div><!-- react-empty: 1763 --></div>

                     <section class="home-section1">
 <div class="slider slick-initialized slick-slider">
   
       <div class="slick-list draggable" tabindex="0">
        <div class="slick-track">

            <div class="slick-slide slide slide--has-caption" style="background-image: url({{asset('assets/public/images/Investing-Online-Made-Easier.jpg')}};    background-size:100%;
    height: 600px;
    background-repeat: no-repeat;">
    <div style="display: table;height: 100%;">
      <div style="display: table-cell;vertical-align: middle; padding: 0px 59px;">
    <h1 style="color: #000;">Going online</h1>
    <h3 style="
    width: 66%;
   
    color: #000;    font-size: 20px;
    line-height: 30px;">With expertise in healthcare, education, employment and more. Asian Human Services helps tens of thousands of people every year, from 55 different countries and in 25 different languages to fully participate, prosper and thrive in our community</h3>
 </div>
</div>
               
                <div class="slide-caption">
                   <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 680" height="436" width="384"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <path d="M265 0H45A45 45 0 0 0 0 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M265 0H45A45 45 0 0 0 0 45v590a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path fill="#f1f1f1" d="M43.19 60H310v520H43.19z"></path>
    <path fill="#f1f1f1" d="M47.09 95.95h257.68v485H47.09z"></path>
    <path fill="#010101" d="M42.16 525.58h266.81v54.8H42.16z"></path>
    <path d="M310 581.38H41.16v-56.8H310v56.8zm-266.81-2H308v-52.8H43.16v52.8z" fill="#d6d6d5"></path>
    <path fill="#fff" d="M104.28 551.98h139.36v2H104.28z"></path>
    <path fill="#d6d6d5" d="M42.19 60.65h265.87v212.98H42.19z"></path>
    <path d="M56.56 71.39h22.35v2H56.56zm0 6.53h22.35v2H56.56zm0 6.53h22.35v2H56.56z"></path>
    <path d="M285 0H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45zm15 580H50a5 5 0 0 1-5-5V65a5 5 0 0 1 5-5h250a5 5 0 0 1 5 5v510a5 5 0 0 1-5 5z" fill="#fff"></path>
    <path d="M285 2a43 43 0 0 1 43 43v550a43 43 0 0 1-43 43H65a43 43 0 0 1-43-43V45A43 43 0 0 1 65 2h220m0-2H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M182 624h-14a6 6 0 0 1-6-6v-13.89a6 6 0 0 1 6-6h14a6 6 0 0 1 6 6V618a6 6 0 0 1-6 6zm-14-23.89a4 4 0 0 0-4 4V618a4 4 0 0 0 4 4h14a4 4 0 0 0 4-4v-13.89a4 4 0 0 0-4-4h-14z" fill="#d6d6d5"></path>
    <path d="M217.93 102.94h-84a20.62 20.62 0 1 0 0 41.24h84a20.62 20.62 0 1 0 0-41.24z" fill="#5d75b9"></path>
    <path fill="#f1f1f1" d="M139.06 122.44h69v2h-69z"></path>
    <path d="M124.16 188.07l4.34 13.36a2.08 2.08 0 0 0 1.85 1.35h14.05c3.3 0 3.82 1.59 1.15 3.53l-11.37 8.26a2.08 2.08 0 0 0-.71 2.18l4.34 13.36c1 3.14-.33 4.12-3 2.18L123.45 224a2.08 2.08 0 0 0-2.29 0l-11.37 8.26c-2.67 1.94-4 1-3-2.18l4.34-13.36a2.08 2.08 0 0 0-.71-2.18l-11.37-8.24c-2.67-1.94-2.16-3.53 1.15-3.53h14.05a2.08 2.08 0 0 0 1.85-1.35l4.34-13.36c1.03-3.13 2.7-3.13 3.72.01z" fill="#8ed0bc"></path>
    <path d="M181.27 220.25v6.48h-2.19v-6.48h-13.14l-.22-1.59L178.3 199h3v19.14h3.85l-.15 2.11h-3.74zm-2-16.33l.11-3h-.08l-1.59 2.59-7.71 12.08-1.58 2.41v.11h10.66zm12.43 23.18a1.52 1.52 0 0 1-1.78-1.74 1.55 1.55 0 0 1 1.78-1.7 1.51 1.51 0 0 1 1.7 1.7 1.54 1.54 0 0 1-1.7 1.74zm12.81-15.1v-.11c-2.63-1.07-4.59-2.78-4.59-6.33 0-4.3 2.41-6.89 8.37-6.89 5.7 0 8.37 2.37 8.37 6.89 0 3-1.7 5.33-4.78 6.66v.07c3.15 1.07 5.81 2.78 5.81 7 0 5.22-3.22 7.77-9.4 7.77-6.55 0-9.48-2.55-9.48-7.77 0-3.52 2.37-5.96 5.7-7.29zm2.29.81c-3.18 1.33-5.74 3.18-5.74 6.48 0 3.78 2 5.74 7.29 5.74 4.29 0 7.07-1.7 7.07-5.63.01-4.48-4.42-5.26-8.61-6.59zm2.78-1.26c3.48-1.44 5-3.22 5-5.81 0-3.33-2-5.07-6.29-5.07s-6.18 1.78-6.18 5c-.04 3.7 3.33 4.7 7.47 5.88zM232.54 227c-6.18 0-9.48-3.33-9.48-13.22 0-10.44 3.92-15.14 11.22-15.14a14.84 14.84 0 0 1 6 1.11l-.28 2.18a13.83 13.83 0 0 0-5.44-1c-5.85 0-9 3.44-9.25 11.25l.11.07a11.49 11.49 0 0 1 7.7-3c6 0 8.59 2.59 8.59 8-.03 6.45-3.1 9.75-9.17 9.75zm.15-15.85a10.29 10.29 0 0 0-7.26 3.22c.11 7.66 2.22 10.51 7.11 10.51 4.52 0 6.81-2.59 6.81-7.66 0-4.15-2.04-6.04-6.67-6.04z" fill="#010101"></path>
    <path fill="#d6d6d5" d="M78.92 323.63h109.3v2H78.92zm0 14h89v2h-89z"></path>
    <path fill="#8ed0bc" d="M78.92 384.87h187.67v5.95H78.92z"></path>
    <path fill="#d6d6d5" d="M78.92 403.6h181.67v5.95H78.92zm0 50.25h109.3v2H78.92zm0 14h89v2h-89z"></path>
</svg>
                </div>
            </div>
      
           <div class="slick-slide slide slide--has-caption" style="background-image: url(./images/Investing-Online-Made-Easier.jpg);    background-size:100%;
    height: 600px;
    background-repeat: no-repeat;">
    <div style="display: table;height: 100%;">
      <div style="display: table-cell;vertical-align: middle; padding: 0px 59px;">
    <h1 style="color: #000;">Accepting trip requests</h1>
    <h3 style="
    width: 66%;
   
    color: #000;    font-size: 20px;
    line-height: 30px;">With expertise in healthcare, education, employment and more. Asian Human Services helps tens of thousands of people every year, from 55 different countries and in 25 different languages to fully participate, prosper and thrive in our community</h3>
 </div>
</div>
               
                <div class="slide-caption">
                  <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 680" height="436" width="384"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <path d="M265 0H45A45 45 0 0 0 0 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M265 0H45A45 45 0 0 0 0 45v590a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path fill="#f1f1f1" d="M43.19 60H310v520H43.19z"></path>
    <path fill="#f1f1f1" d="M47.76 95.95h257.68v485H47.76z"></path>
    <path fill="#f1f1f1" d="M45 492.53h260.44v88.42H45z"></path>
    <path fill="#d6d6d5" d="M42.19 60.65h265.87v36.54H42.19z"></path>
    <path d="M56.56 71.39h22.35v2H56.56zm0 6.53h22.35v2H56.56zm0 6.53h22.35v2H56.56z"></path>
    <path d="M285 0H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45zm15 580H50a5 5 0 0 1-5-5V65a5 5 0 0 1 5-5h250a5 5 0 0 1 5 5v510a5 5 0 0 1-5 5z" fill="#fff"></path>
    <path d="M285 2a43 43 0 0 1 43 43v550a43 43 0 0 1-43 43H65a43 43 0 0 1-43-43V45A43 43 0 0 1 65 2h220m0-2H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M182 624h-14a6 6 0 0 1-6-6v-13.89a6 6 0 0 1 6-6h14a6 6 0 0 1 6 6V618a6 6 0 0 1-6 6zm-14-23.89a4 4 0 0 0-4 4V618a4 4 0 0 0 4 4h14a4 4 0 0 0 4-4v-13.89a4 4 0 0 0-4-4h-14z" fill="#d6d6d5"></path>
    <path d="M176.6 366.24A108.21 108.21 0 1 1 284.8 258a108.33 108.33 0 0 1-108.2 108.24zm0-214.41A106.21 106.21 0 1 0 282.8 258a106.33 106.33 0 0 0-106.2-106.17z" fill="#5d75b9"></path>
    <path d="M124.08 174.7a99 99 0 0 0-22.15 19.1h22.15v-19.1zm-24.3 115.96v-21.39h-21a97.9 97.9 0 0 0 4.9 21.39h16.1zm26.3-63.72h49.02v40.34h-49.02zm-24.3 0h22.3v40.34h-22.3zm-2 0H83.12a98.38 98.38 0 0 0-5 31.1c0 3.12.15 6.2.43 9.24h21.23v-40.34zm26.3 42.33h49.02v21.39h-49.02zm-24.3-73.46h22.3v29.13h-22.3zm-2 .59a98.32 98.32 0 0 0-16 28.54h16V196.4zm75.32 28.54v-57.26h-37.74a98.17 98.17 0 0 0-11.28 5.8v51.46h49zm-90.73 67.72a98.35 98.35 0 0 0 15.41 27v-27H84.37zm92.73 0v46.38l46.38-46.38H177.1zm73.66-27.28l23.07-23.07a97.82 97.82 0 0 0-3.76-15.37h-19.31v38.44zm18.62-40.44a98.4 98.4 0 0 0-18.62-31.71v31.71h18.62zm-22.51 44.33H177.1v21.39h48.38l21.39-21.39zm-81.01 86.67a98.78 98.78 0 0 0 21.85 0L176.81 345zm63.57-63.57l25.44 25.44a98.24 98.24 0 0 0 15-28l-18.92-18.92zm-51.21 51.2l12 12a98.38 98.38 0 0 0 63.41-36.18L228 293.78zm92.33-55.92a98.44 98.44 0 0 0 4.54-29.65 99.55 99.55 0 0 0-.59-10.74l-22.15 22.15zM204.9 163.68a98.46 98.46 0 0 0-27.8-4.13v4.13h27.8zm-27.8 63.26h71.66v40.34H177.1zm-75.32 42.33h22.3v21.39h-22.3zm0 23.39v29.42a99 99 0 0 0 22.3 19.28v-48.7h-22.3zm73.32-133.1a98.41 98.41 0 0 0-26.8 4.12h26.8v-4.12zm2 65.38h71.66V191a98.71 98.71 0 0 0-32.92-23.34H177.1v57.26zm-2 67.72h-49v49.93a97.85 97.85 0 0 0 34.79 12.68L175.1 341v-48.34z" fill="#f1f1f1"></path>
    <path d="M99.78 292.66v27q1 1.22 2 2.41v-29.41h-2zm0-23.39h2v21.39h-2zm2-44.33v-29.13h-1.53l-.47.59v28.54h2zm-2 2h2v40.34h-2zm25.3-33.13v2h-1v29.13h2v-51.47q-1 .6-2 1.23v19.1h1zm-1 33.13h2v40.34h-2zm0 42.33h2v21.39h-2zm0 23.39v48.7q1 .63 2 1.23v-49.93h-2zm126.68-24.45l.17-.17 1.41 1.41 22.15-22.15q-.27-2.51-.67-5l-23.07 23.07v2.83zm-75.37 75.36l.71-.71h-1V341l-14.23 14.23q2.47.4 5 .67l10.94-10.9zm50.09-52.91h1.83v1l.7-.71 1.42 1.42 21.5-21.51-1.41-1.41.17-.18h-2.82l-21.39 21.39zm2.53 3.12l-1.12-1.12h-3.41l-46.38 46.38v3.41l1.12 1.12 49.79-49.79zm-126.23-1.12h73.32v-2H83.65q.35 1 .73 2h17.41z" fill="#d6d6d5"></path>
    <path fill="#d6d6d5" d="M226.89 292.66l-.29-.29.71-.71v-1H177.1v2h49.79zm23.87-67.72v-31.71q-1-1.12-2-2.21v33.92h2zm-148.98 44.33h73.32v-2H78.54q.09 1 .21 2h23zm147.91 0l1.07-1.06v-41.27h-2v40.33H177.1v2h72.59zm-147.91-42.33h73.32v-2H83.81q-.36 1-.69 2h18.66zm146.98-2H177.1v2h93q-.33-1-.69-2h-20.65zm-73.66-61.26h-26.8a97.75 97.75 0 0 0-10.94 4h37.74v-4zm2 4h38.74a97.75 97.75 0 0 0-10.94-4h-27.8v4zm-2 0v175.19h1l.71-.71.29.29v-182.9h-2v8.12z"></path>
    <path d="M228 291l-.71.71-.71.71.29.29 1.12 1.12 25.62 25.62q.63-.79 1.24-1.59l-25.44-25.44zm-50.9 51.45l-.29-.29-.71.71-.71.71 1.41 1.41 10.91 10.91q1.26-.14 2.51-.32l-12-12zM250.93 268l-.17.17-1.07 1.07-.17.17 1.41 1.41 18.92 18.92q.36-1.06.7-2.13l-18.2-18.2zm-126.85-72.19h1v-2h-23.15q-.85 1-1.67 2h23.82z" fill="#d6d6d5"></path>
    <circle cx="176.6" cy="248.89" r="9.19"></circle>
    <path d="M167.65 251l6.69 21a2.36 2.36 0 0 0 4.5 0l6.69-21h-17.88z"></path>
    <circle cx="176.6" cy="248.89" r="3.54" fill="#fff"></circle>
    <path fill="#d6d6d5" d="M69.39 512.21H283.8v51H69.39z"></path>
    <path fill="#231f20" d="M175.03 536.17h52.59v2h-52.59zm-49.46 0h14.33v2h-14.33z"></path>
    <path d="M155.62 531.68l1.11 3.42a.53.53 0 0 0 .48.35h3.6c.85 0 1 .41.29.9l-2.91 2.12a.53.53 0 0 0-.18.56l1.11 3.42c.26.8-.08 1.06-.77.56l-2.91-2.12a.53.53 0 0 0-.59 0l-2.91 2.11c-.68.5-1 .25-.77-.56l1.11-3.42a.53.53 0 0 0-.18-.56l-2.91-2.12c-.68-.5-.55-.9.29-.9h3.6a.53.53 0 0 0 .48-.35l1.11-3.42c.26-.79.69-.79.95.01z" fill="#5d75b9"></path>
    <path fill="#d6d6d5" d="M69.39 397.21H283.8v106.67H69.39z"></path>
    <path fill="#231f20" d="M161.76 423.54h52.59v2h-52.59zm-45.91 39h86.57v2h-86.57zm93.35 0h23.59v2H209.2zm-112.91-8.66h83.28v2H96.29zm88.21 0h70.78v2H184.5z"></path>
    <circle cx="139.9" cy="424.54" r="15.55" fill="#8ed0bc"></circle>
    <path fill="#787878" d="M144.69 430.74l-5.79-5.78v-13.45h2v12.62l5.2 5.2-1.41 1.41z"></path>
</svg>
                </div>
            </div>
          <div class="slick-slide slide slide--has-caption" style="background-image: url({{asset('assets/public/images/Investing-Online-Made-Easier.jpg')}};    background-size:100%;
    height: 600px;
    background-repeat: no-repeat;">
    <div style="display: table;height: 100%;">
      <div style="display: table-cell;vertical-align: middle; padding: 0px 59px;">
    <h1 style="color: #000;">Turn-by-turn directions
</h1>
    <h3 style="
    width: 66%;
   
    color: #000;    font-size: 20px;
    line-height: 30px;">With expertise in healthcare, education, employment and more. Asian Human Services helps tens of thousands of people every year, from 55 different countries and in 25 different languages to fully participate, prosper and thrive in our community</h3>
 </div>
</div>
               
                <div class="slide-caption">
                <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 680" height="436" width="384"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <path d="M265 0H45A45 45 0 0 0 0 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M265 0H45A45 45 0 0 0 0 45v590a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path fill="#f1f1f1" d="M43.19 60H310v520H43.19z"></path>
    <path fill="#010101" d="M43.19 525.2H310V580H43.19z"></path>
    <path d="M311 581H42.19v-56.8H311V581zm-266.81-2H309v-52.8H44.19V579z" fill="#d6d6d5"></path>
    <path fill="#fff" d="M105.32 551.6h139.36v2H105.32z"></path>
    <path fill="none" d="M60.55 240.99h12.43v14.31H60.55zm14.44 34.79h28.31v23.15H74.99zm0-34.79h28.31v32.8H74.99zm155.22-25.49h-20.37v9.78h10.59l9.78-9.78zm-2.6 189.81l23.77 23.76 12.27-12.13-23.77-23.77-12.27 12.14zM74.99 215.5h28.31v21.48H74.99zm-14.44 41.79h12.43v16.49H60.55zm0 57.54v70.33l12.44-12.43v-57.9H60.55zm14.44 0v55.9l28.31-28.31v-27.59H74.99zm-14.44-39.05h12.43v23.15H60.55zm-7.51 154.93l22.42 21.93L98.9 429.2l-22.17-22.17-23.69 23.68zm21.95-129.78h28.31v11.89H74.99zm-14.44 0h12.43v11.89H60.55zm76.286 13.602l12.205-12.204 14.78 14.778-12.202 12.205zm73.414-73.415l13.138-13.138 14.778 14.774-13.138 13.138zm-59.798 59.803l24.013-24.012 14.78 14.778L165.23 315.7zm59.388-73.64v8.59l8.59-8.59h-8.59zm-33.957 48.205l32.95-32.95 14.78 14.777-32.95 32.952zM122.62 361.14l29.005-29.005L162.19 342.7l-29.007 29.005zm141.963-94.784l20.64-20.64 24.403 24.402-20.64 20.64zm-48.92 48.91l32.953-32.95 24.402 24.402-32.95 32.95zm25.33-72.51l20.64-20.64 22.175 22.175-20.64 20.64zm-16.19-16.192l20.64-20.64 14.78 14.778-20.643 20.64zm1.637 30.746l13.138-13.14 22.175 22.175-13.138 13.138zm23.59 23.588l13.14-13.138 24.4 24.403-13.137 13.138zm49.99 49.992l13.21-13.06-24.25-24.25-13.14 13.14 24.18 24.17zm-83.97 35.62l23.84 23.84 24.15-23.88-23.98-23.97-24.01 24.01zm5.48 110.89a69.25 69.25 0 0 0-16.47 26.14l.18.18 58.48-59.48-4.51-4.51zm-49.52-66.85l23.59 23.59 29.17-28.84-23.75-23.76-29.01 29.01z"></path>
    <path d="M222.2 460.74l.1.1-1.41 1.41-.11-.11-28.72 28.4 9.89 9.89a73.29 73.29 0 0 1 16.75-25.86l37.67-37.67-5-5zm-80.16-20.22l23.43 23.43 28.71-28.4-23.58-23.59-28.56 28.56zm60.39-60.39l23.77 23.76 12.27-12.13-23.84-23.83-12.2 12.2zm62.7 65.53l-28.76 29.25 21.07 21.07 28.76-29.25-21.07-21.07zM106.416 344.954l29.005-29.006 14.78 14.78-29.003 29.004zM102.99 479.57l23.46 22.96 23.02-22.76-23.34-23.34-23.14 23.14zm94.03-44.01l23.77 23.76 29.17-28.84-23.77-23.77-29.17 28.85zm-44.72 44.22l23.77 23.77 13.16-13.02-23.77-23.76-13.16 13.01zm89-88.01l23.77 23.76 24.15-23.88-23.77-23.76-24.15 23.88zm-51.067-51.067l24.014-24.014 24.402 24.4-24.017 24.012zM166.88 465.36l23.77 23.77 28.71-28.4-23.76-23.77-28.72 28.4zm-39.34-10.34l23.35 23.34 13.15-13.01-23.41-23.41-13.09 13.08zm-13.31-13.31l13.08-13.08 11.9 11.9-13.08 13.082zm14.503-14.49l28.553-28.554 11.9 11.9-28.552 28.554zm47.885-72.906l12.204-12.205 24.403 24.4-12.205 12.203zm-30.42 30.42l29.005-29.004 24.402 24.402-29.005 29.006zM89.53 466.41l12.03 11.77 23.16-23.16-11.9-11.9-23.29 23.29zm21.87-24.7l-11.09-11.09-23.42 23.42 11.21 10.97 23.3-23.3zm-59.8 37.62l24.67 24.14 23.88-23.88-24.67-24.13-23.88 23.87zm189.88-138.24l23.98 23.98 33.13-32.77-24.16-24.17-32.95 32.96zm-125.254 73.615l28.553-28.553 11.083 11.087-28.553 28.55zm-14.494 14.505l13.08-13.08 11.088 11.087-13.08 13.082zM60.55 215.5h12.43v21.48H60.55zm1.398 173.92l13.08-13.083 14.78 14.78-13.082 13.08zM149.05 215.5h58.79v9.78h-58.79zm0 11.78h58.79v9.7h-58.79zM78.138 405.61l13.082-13.08 22.175 22.174-13.082 13.08zm-1.686-30.683l28.553-28.553 14.78 14.778-28.554 28.553zM105.29 227.28h41.76v9.7h-41.76zm0-11.78h41.76v9.78h-41.76zM92.643 391.118l28.553-28.553 22.174 22.175-28.553 28.553zm54.417-92.458v-22.88H105.3v23.16h41.48l.28-.28zm-41.76 16.17v25.59l25.59-25.59H105.3zm39.48-13.89H105.3v11.89h27.59l11.89-11.89zm4.28-25.16v20.88l20.87-20.88h-20.87zm-14.473 97.345l29.005-29.006 10.197 10.193-29.01 29.005zM105.29 240.99h41.76v32.8h-41.76zm47.737 89.734l12.204-12.205 22.18 22.17-12.205 12.206zm13.615-13.61l24.014-24.014 22.175 22.174-24.01 24.014zm25.432-25.437l32.95-32.95L247.2 280.9l-32.952 32.952zM149.06 240.99v32.79h22.87l32.8-32.79h-55.67z" fill="none"></path>
    <path fill="#d6d6d5" d="M58.55 300.93h2v11.89h-2zm0-25.15h2v23.15h-2zm2 39.05h-2v72.33l2-2v-70.33zm-2-73.84h2v14.31h-2zm0 16.3h2v16.49h-2zm0-41.79h2v21.48h-2zm0-35.94h2v33.94h-2zm14.44 61.43v14.3h1v2h-1v16.49h2v-32.79h-2zm0-61.43h2v33.94h-2zm0 96.22h2v23.15h-2zm0-60.28h2v21.48h-2zm0 99.33v57.9l2-2v-55.9h-2zm0-13.9h2v11.89h-2zm59.9 11.9h1.83v1l.7-.71 1.42 1.41 12.2-12.2-1.4-1.39h-2.86l-11.89 11.89zm14.17-13.89v-.28l-.28.28h.28zm85.45-83.44h-2.3l-9.78 9.78h2.67v.16l.29-.28 1.41 1.41 20.64-20.64-1.41-1.41 1.41-1.42 1.41 1.42 24.24-24.24-2.83-2.82-24 24v2.78h-2v-.78l-10.05 10.04h.3v2zm-22.26 25.62l13.14-13.14-.7-.7h-4.26l-8.59 8.59v3.12h-1v.73l1.41 1.4zm-74.83 74.83l-1.12-1.12h-3.41l-25.59 25.59v3.41l1.12 1.12 29-29z"></path>
    <path fill="#d6d6d5" d="M60.53 388.01l1.42 1.41 13.08-13.08-1.41-1.41 1.41-1.42 1.42 1.42L105 346.37l-1.41-1.42.71-.7h-1v-1.83l-28.31 28.31-2 2-12.44 12.43-2 2-17.09 17.1v5.66l19.07-19.08-1.41-1.42 1.41-1.41zm111.4-114.23h1.83v1l.71-.7 1.41 1.41 32.95-32.95-1.41-1.41.14-.14h-2.83l-32.8 32.79zm-22.87 22.88v2.86l1.39 1.4 24.02-24.02-1.12-1.12h-3.42l-20.87 20.88zm16.172 19.04l24.013-24.013 1.415 1.414-24.014 24.018zm-74.01 74.002l28.553-28.553 1.414 1.41-28.556 28.553zM225.03 255.906l13.138-13.138 1.414 1.414-13.138 13.138zM76.728 404.208l13.082-13.08 1.414 1.413-13.082 13.085zm113.925-113.934l32.95-32.95 1.416 1.413-32.956 32.95zm48.93-48.92l20.64-20.64 1.414 1.413-20.64 20.64zm22.044-22.054l25.477-25.478 1.414 1.414-25.477 25.477zM41.46 439.47v2.83l10.17-10.17-1.43-1.4-8.74 8.74zm35.27-32.44l-1.42-1.41-23.7 23.69 1.43 1.4 23.69-23.68zm74.894-77.716l12.204-12.204 1.414 1.414-12.204 12.205zm-30.428 30.43l29.006-29.007 1.414 1.415-29.006 29.005zM72.99 314.83h30.31v-2H41.46v2h31.53zm61.31 0l-.29-.3.71-.7v-1H105.3v2h29zm12.75-99.33h2v9.78h-2zm-74.06 85.44h30.31v-2H41.46v2h31.53zm74.06-59.95h2v32.8h-2zm0-61.43h2v33.94h-2zm0 47.72h2v9.7h-2zm.59 73.66l-.01-.02 1.41-1.42.02.02v-23.74h-2v23.16H105.3v2h42.34zm-74.65-25.16h30.31v-2H41.46v2h31.53z"></path>
    <path fill="#d6d6d5" d="M149.06 275.78h24.29l-.3-.29.71-.71v-1H105.3v2h43.76zm-76.07-34.79h30.31v-4H41.46v4h31.53zm76.07 0h58.5l1.27-1.28.01.01v-.73h-1v-2H105.3v4h43.76zm-2-25.49h60.78v-2H105.3v2h41.76zm-74.07 0h30.31v-2H41.46v2h31.53zm157.22 0h2.3v-2h-22.67v2h20.37zM103.3 236.99v107.26h1l.7-.71.3.29V227.28h-1v-2h1v-45.72h-2v57.43zm97.72 144.55l23.75 23.76 1.43-1.41-23.77-23.76-1.41 1.41zm62.7 62.7l1.4-1.42-4.5-4.51-1.42 1.42 4.52 4.51zm22.48 22.49l1.4-1.43-21.07-21.07-1.4 1.43 21.07 21.07zm-60.01-60.02l23.77 23.77 1.42-1.41-23.77-23.76-1.42 1.4zm30.19 30.19l1.41-1.41-5-5-1.42 1.4 5.01 5.01zm-81.177-81.17l1.414-1.415 24.402 24.402-1.418 1.415zm-23.583-26.42l-14.78-14.78-1.42-1.41-.7.71-.71.7.29.3 1.12 1.12 14.78 14.78 1.42 1.41 10.56 10.57.71-.71 1.42 1.42-.71.7 10.19 10.2 1.42-1.42-22.18-22.17-1.41-1.42zm70.68 131.53l-.1-.1-1.42 1.4.11.11 1.41-1.41zm-26.7-23.88l23.76 23.77 1.43-1.41-23.77-23.76-1.42 1.4zm-25-25l23.58 23.59 1.42-1.41-23.59-23.59-1.41 1.41zm-25.82-25.81l11.09 11.09.71-.71 1.41 1.41-.71.71 11.9 11.9 1.42-1.41-24.4-24.4-1.42 1.41z"></path>
    <path fill="#d6d6d5" d="M133.88 373.84l-1.41-1.42.71-.71-10.57-10.56-1.42-1.42-14.77-14.78-1.12-1.12-.3-.29-.7.71-.71.7 1.41 1.42 14.78 14.78 1.41 1.41 22.18 22.18 1.41-1.42-10.19-10.19-.71.71zm70.83 130.77c-.12.35-.24.7-.35 1.06l19.54 19.53h2.83l-20.07-20.07-.7.71zm-39.25-37.84l23.77 23.76 1.42-1.4-23.77-23.77-1.42 1.41zm39.42 37.31l.36-.36-.18-.18c-.06.18-.11.36-.18.54zm-90.065-87.955l1.414-1.415 11.084 11.088-1.414 1.414zm12.498 12.5l1.414-1.415 11.9 11.9-1.414 1.415zm64.747 61.915l-1.42 1.41 10.55 10.55c.24-.7.5-1.38.76-2.07zm-51.43-48.6l23.41 23.41 1.43-1.4-23.43-23.43-1.41 1.42zM91.22 389.7l-14.77-14.77-1.42-1.42-1.41 1.42 1.41 1.41 14.78 14.78 1.41 1.41 22.18 22.18 1.41-1.42-22.17-22.17-1.42-1.42zm85.2 117.02l1.41-1.41-.35-.35-1.42 1.41.36.35zM98.9 429.2l1.41-1.41-22.17-22.17-1.41-1.42-14.78-14.78-1.42-1.41-1.41 1.41 1.41 1.42 14.78 14.78 1.42 1.41L98.9 429.2zm53.4 50.58l-1.42 1.4 23.77 23.77 1.42-1.4-23.77-23.77zm-51.978-49.166l1.414-1.414 11.088 11.087-1.414 1.414zm12.49 12.506l1.415-1.414 11.9 11.9-1.414 1.415zm13.318 13.31l23.34 23.34 1.42-1.41-23.35-23.34-1.41 1.41zm-37.9 11.27l-1.41-1.41 1.28-1.28-11.21-10.97-1.41 1.42 24.67 24.13 1.41-1.41-12.03-11.77-1.3 1.29zm58.54 57.5h2.86l-21.75-21.27-1.42 1.4 20.31 19.87zm-43.78-45.63l-1.41 1.42 23.45 22.94 1.42-1.4-23.46-22.96zm-49.95-48.86l-1.43-1.4-10.15-9.93v2.8l8.74 8.55 1.43 1.4 22.42 21.93 1.41-1.42-22.42-21.93zm42.59 94.49h2.86L77.7 504.87l-1.42 1.41 19.35 18.92zm-54.17-55.79v2.8l7.3 7.14 1.41-1.41-8.71-8.53zm10.14 9.92l-1.41 1.42 24.66 24.13 1.42-1.41-24.67-24.14zm188.28-86.16l23.77 23.77 1.42-1.41-23.77-23.76-1.42 1.4zm-51.057-51.062l1.414-1.414 24.402 24.402-1.418 1.414zm25.807 25.822l23.84 23.83 1.42-1.41-23.84-23.84-1.42 1.42zm-49.4-52.23l-14.78-14.78-1.39-1.4-.02-.02-1.41 1.42.01.02 1.4 1.39 14.78 14.78 1.41 1.41 22.18 22.18 1.41-1.41-22.17-22.18-1.42-1.41zm74.83 26.8l23.98 23.97 1.42-1.4-23.98-23.98-1.42 1.41zm49.16 49.15l1.42-1.4-23.77-23.77-1.42 1.41 23.77 23.76zm-74.97-74.97l1.414-1.413 24.402 24.402-1.414 1.41zm-23.59-26.41l-14.78-14.78-1.41-1.41-.71.7-.71.71.3.29 1.12 1.12 14.78 14.78 1.41 1.42 22.17 22.17 1.42-1.41-22.18-22.18-1.41-1.41z"></path>
    <path fill="#d6d6d5" d="M75.48 455.46l1.41-1.42 23.42-23.42 1.42-1.42 13.08-13.08 1.42-1.41 28.55-28.56 1.42-1.41 29.01-29.01 1.41-1.41 12.2-12.21 1.42-1.41 24.01-24.01 1.41-1.42 32.96-32.95-1.42-1.41-32.95 32.95-1.42 1.41-24.01 24.02-1.41 1.41-12.2 12.2-1.42 1.42-29.01 29-1.41 1.42-28.56 28.55-1.41 1.42-13.09 13.08-1.41 1.41-23.44 23.44-1.41 1.42-23.88 23.88-1.41 1.41-7.3 7.3v2.83l8.73-8.73 1.41-1.42 23.88-23.87zm173.13-175.964l13.14-13.138 1.413 1.414-13.138 13.138zm36.615-36.623l4.27-4.27 1.415 1.413-4.27 4.27zm-22.06 22.063l20.64-20.64 1.416 1.414-20.64 20.64zM204.53 504.44l.35-.36.18-.54a69.25 69.25 0 0 1 16.47-26.14l37.67-37.67 1.41-1.41L315 383.93v-5.66l-57.21 57.21-1.41 1.41-37.68 37.68a73.29 73.29 0 0 0-16.75 25.86c-.26.69-.52 1.38-.76 2.07a73.51 73.51 0 0 0-4 22.7h4a69.61 69.61 0 0 1 3.16-19.53c.11-.35.23-.7.35-1.06zm-78.4-48.01l1.41-1.41 13.09-13.08 1.41-1.42 28.56-28.56 1.41-1.41 29.01-29.01 1.41-1.41 12.2-12.2 1.42-1.42 24.01-24.01 1.42-1.41 32.95-32.96-1.42-1.41-32.95 32.95-1.41 1.42-24.02 24.01-1.41 1.41-12.2 12.21-1.41 1.41-29.01 29.01-1.42 1.41-28.55 28.56-1.42 1.41-13.08 13.09-1.41 1.41-23.16 23.16-1.41 1.41-23.88 23.88-1.42 1.41-20.32 20.32h2.83l18.92-18.92 1.42-1.41 23.88-23.88 1.41-1.42 23.14-23.14zm162.86-165.684l20.642-20.64 1.414 1.414-20.64 20.64zm-14.568 14.56l13.138-13.138 1.414 1.414-13.138 13.138zM315 267.56v-2.82l-3.96 3.96 1.41 1.41 2.55-2.55zm-14.98 63.33l1.41 1.42 13.21-13.07-1.41-1.41-13.21 13.06zM165.46 466.77l1.42-1.41 28.72-28.4 1.42-1.4 29.17-28.85 1.42-1.4 12.27-12.14 1.42-1.4 24.15-23.88 1.42-1.41 33.14-32.77-1.42-1.41-33.13 32.77-1.42 1.4-24.15 23.88-1.42 1.41-12.27 12.13-1.43 1.41-29.17 28.84-1.42 1.41-28.71 28.4-1.43 1.4-13.15 13.01-1.42 1.41-23.02 22.76-1.42 1.4-21.51 21.27h2.85l20.09-19.87 1.42-1.4 23-22.75 1.42-1.4 13.16-13.01zM315 316.07l-.35.35.35.35v-.7zm-94.22 146.07l1.42-1.4 29.17-28.85 1.42-1.4L315 368.97v-2.81l-24.36 24.09-1.42 1.4-24.15 23.88-1.42 1.41-12.27 12.13-1.42 1.41-29.17 28.84-1.43 1.41-28.71 28.4-1.42 1.4-13.16 13.02-1.42 1.4-20.48 20.25h2.84l19.05-18.83 1.42-1.41 13.16-13.01 1.42-1.41 28.72-28.4z"></path>
    <path fill="#d6d6d5" d="M235.92 474.46l.45.45 28.76-29.25 1.4-1.43 48.47-49.3v-2.85l-49.88 50.74-1.4 1.42-58.48 59.48-.36.36-.35.36.18.17 1.25 1.23.69-.71 28.32-28.8-.46-.45 1.41-1.42zm-7.21 50.74h2.8l25.94-26.38-1.41-1.41-27.33 27.79zm28.73-29.22l1.42 1.41 56.14-57.1v-2.85l-27.4 27.86-1.4 1.43-28.76 29.25z"></path>
    <path fill="#d6d6d5" d="M286.66 525.2l-27.8-27.81-1.42-1.41-21.07-21.07-.45-.45-1.41 1.42.46.45 21.07 21.08 1.41 1.41 26.39 26.38h2.82zm-10.82-218.48l-1.41-1.41-24.4-24.4-1.41-1.42-22.18-22.17-1.41-1.42-14.78-14.78-1.41-1.4-.01-.01-1.27 1.28-.14.14 1.41 1.41 14.78 14.78 1.42 1.41 22.17 22.18 1.42 1.41 24.39 24.4 1.42 1.41 24.16 24.17 1.42 1.41L315 348.7v-2.82l-13.57-13.57-1.41-1.42-24.18-24.17zm-11.26-40.37l-1.41-1.41-22.18-22.18-1.41-1.41-14.78-14.78-1.41-1.41-.29.28v1.84h-.41l.7.7 14.78 14.78 1.41 1.42 22.17 22.17 1.42 1.42 24.4 24.4 1.41 1.41 24.25 24.25 1.41 1.41.36.36v-2.83l-.35-.35-24.25-24.25-1.42-1.42-24.4-24.4zm-20.55-61.83l1.41 1.41 14.78 14.78 1.41 1.41 22.18 22.18 1.41 1.41 24.4 24.4 1.42 1.42 3.96 3.96v-2.83l-2.55-2.55-1.41-1.41-24.4-24.4-1.42-1.42-22.17-22.17-1.42-1.42-14.78-14.77-1.41-1.42-1.41 1.42z"></path>
    <path fill="#d6d6d5" d="M222.69 227.28h.41v-2h-13.26v2h12.85zm-73.63 0h58.78v-2H104.3v2h44.76zm-76.07 30.01h1v-2H41.46v2h31.53zm59.48 115.13l1.41 1.42.71-.71 29.01-29.01.71-.7-1.42-1.42-.71.71-29 29-.71.71zm-6.57 54.79l-13.08 13.08-1.42 1.42-23.3 23.3-1.28 1.28 1.41 1.41 1.3-1.29 23.29-23.29 1.41-1.41 13.08-13.09 1.42-1.41 28.55-28.56.71-.71-1.41-1.41-.71.71-28.56 28.55-1.41 1.42zm81.94-201.93v13.71h2v-60.33h-2v46.62zm36.42-21.04v-25.37h-2v25.37h2zm63.67-143.96H42.07v36.54h265.86V60.28zM78.79 86.08H56.44v-2h22.35v2zm0-6.53H56.44v-2h22.35v2zm0-6.53H56.44V71h22.35v2z"></path>
    <path d="M56.44 71.02h22.35v2H56.44zm0 6.53h22.35v2H56.44zm0 6.53h22.35v2H56.44z"></path>
    <path d="M114.8 413.72a2 2 0 0 1-1.41-.59l-40.47-40.5a2 2 0 0 1 0-2.83l30-30 2.83 2.83-28.6 28.59 37.65 37.67 98.05-98.05a2 2 0 0 1 1.41-.59 2 2 0 0 1 1.41.59l51.75 51.79-2.83 2.83-50.33-50.37-98.05 98.05a2 2 0 0 1-1.41.58z" fill="#5d75b9"></path>
    <circle cx="104.29" cy="341.25" r="4.65" fill="#5d75b9"></circle>
    <circle cx="104.29" cy="316.59" r="9.19"></circle>
    <path d="M95.35 318.67l6.69 21a2.36 2.36 0 0 0 4.5 0l6.69-21H95.35z"></path>
    <circle cx="104.29" cy="316.59" r="3.54" fill="#fff"></circle>
    <circle cx="266.01" cy="364.04" r="5.06" fill="#8ed0bc"></circle>
    <path d="M285 0H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45zm15 580H50a5 5 0 0 1-5-5V65a5 5 0 0 1 5-5h250a5 5 0 0 1 5 5v510a5 5 0 0 1-5 5z" fill="#fff"></path>
    <path d="M285 2a43 43 0 0 1 43 43v550a43 43 0 0 1-43 43H65a43 43 0 0 1-43-43V45A43 43 0 0 1 65 2h220m0-2H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M182 624h-14a6 6 0 0 1-6-6v-13.89a6 6 0 0 1 6-6h14a6 6 0 0 1 6 6V618a6 6 0 0 1-6 6zm-14-23.89a4 4 0 0 0-4 4V618a4 4 0 0 0 4 4h14a4 4 0 0 0 4-4v-13.89a4 4 0 0 0-4-4h-14zM45 177.87h259.67v2H45zm81.99-69.05h2v57h-2z" fill="#d6d6d5"></path>
    <path d="M150 129.32h109.3v2H150zm0 14h89v2h-89z"></path>
    <path fill="#8ed0bc" d="M87.89 143.13l-13.56 8.44 13.56-28.5 13.56 28.5-13.56-8.44z"></path>
</svg>
                </div>
            </div>
            <div class="slick-slide slide slide--has-caption" style="background-image: url({{asset('assets/public/images/Investing-Online-Made-Easier.jpg')}};    background-size:100%;
    height: 600px;
    background-repeat: no-repeat;">
    <div style="display: table;height: 100%;">
      <div style="display: table-cell;vertical-align: middle; padding: 0px 59px;">
    <h1 style="color: #000;">Track your earnings</h1>
    <h3 style="
    width: 66%;
   
    color: #000;    font-size: 20px;
    line-height: 30px;">With expertise in healthcare, education, employment and more. Asian Human Services helps tens of thousands of people every year, from 55 different countries and in 25 different languages to fully participate, prosper and thrive in our community</h3>
 </div>
</div>
               
                <div class="slide-caption">
                   <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 680" height="436" width="384"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <path d="M265 0H45A45 45 0 0 0 0 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M265 0H45A45 45 0 0 0 0 45v590a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path fill="#f1f1f1" d="M43.19 60H310v520H43.19z"></path>
    <path fill="#f1f1f1" d="M47.09 95.95h257.68v485H47.09z"></path>
    <path fill="#010101" d="M42.16 525.58h266.81v54.8H42.16z"></path>
    <path d="M310 581.38H41.16v-56.8H310v56.8zm-266.81-2H308v-52.8H43.16v52.8z" fill="#d6d6d5"></path>
    <path fill="#fff" d="M104.28 551.98h139.36v2H104.28z"></path>
    <path fill="#d6d6d5" d="M42.19 60.65h265.87v212.98H42.19z"></path>
    <path d="M56.56 71.39h22.35v2H56.56zm0 6.53h22.35v2H56.56zm0 6.53h22.35v2H56.56z"></path>
    <path d="M285 0H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45zm15 580H50a5 5 0 0 1-5-5V65a5 5 0 0 1 5-5h250a5 5 0 0 1 5 5v510a5 5 0 0 1-5 5z" fill="#fff"></path>
    <path d="M285 2a43 43 0 0 1 43 43v550a43 43 0 0 1-43 43H65a43 43 0 0 1-43-43V45A43 43 0 0 1 65 2h220m0-2H65a45 45 0 0 0-45 45v550a45 45 0 0 0 45 45h220a45 45 0 0 0 45-45V45a45 45 0 0 0-45-45z" fill="#d6d6d5"></path>
    <path d="M182 624h-14a6 6 0 0 1-6-6v-13.89a6 6 0 0 1 6-6h14a6 6 0 0 1 6 6V618a6 6 0 0 1-6 6zm-14-23.89a4 4 0 0 0-4 4V618a4 4 0 0 0 4 4h14a4 4 0 0 0 4-4v-13.89a4 4 0 0 0-4-4h-14z" fill="#d6d6d5"></path>
    <path d="M217.93 102.94h-84a20.62 20.62 0 1 0 0 41.24h84a20.62 20.62 0 1 0 0-41.24z" fill="#5d75b9"></path>
    <path fill="#f1f1f1" d="M139.06 122.44h69v2h-69z"></path>
    <path d="M124.16 188.07l4.34 13.36a2.08 2.08 0 0 0 1.85 1.35h14.05c3.3 0 3.82 1.59 1.15 3.53l-11.37 8.26a2.08 2.08 0 0 0-.71 2.18l4.34 13.36c1 3.14-.33 4.12-3 2.18L123.45 224a2.08 2.08 0 0 0-2.29 0l-11.37 8.26c-2.67 1.94-4 1-3-2.18l4.34-13.36a2.08 2.08 0 0 0-.71-2.18l-11.37-8.24c-2.67-1.94-2.16-3.53 1.15-3.53h14.05a2.08 2.08 0 0 0 1.85-1.35l4.34-13.36c1.03-3.13 2.7-3.13 3.72.01z" fill="#8ed0bc"></path>
    <path d="M181.27 220.25v6.48h-2.19v-6.48h-13.14l-.22-1.59L178.3 199h3v19.14h3.85l-.15 2.11h-3.74zm-2-16.33l.11-3h-.08l-1.59 2.59-7.71 12.08-1.58 2.41v.11h10.66zm12.43 23.18a1.52 1.52 0 0 1-1.78-1.74 1.55 1.55 0 0 1 1.78-1.7 1.51 1.51 0 0 1 1.7 1.7 1.54 1.54 0 0 1-1.7 1.74zm12.81-15.1v-.11c-2.63-1.07-4.59-2.78-4.59-6.33 0-4.3 2.41-6.89 8.37-6.89 5.7 0 8.37 2.37 8.37 6.89 0 3-1.7 5.33-4.78 6.66v.07c3.15 1.07 5.81 2.78 5.81 7 0 5.22-3.22 7.77-9.4 7.77-6.55 0-9.48-2.55-9.48-7.77 0-3.52 2.37-5.96 5.7-7.29zm2.29.81c-3.18 1.33-5.74 3.18-5.74 6.48 0 3.78 2 5.74 7.29 5.74 4.29 0 7.07-1.7 7.07-5.63.01-4.48-4.42-5.26-8.61-6.59zm2.78-1.26c3.48-1.44 5-3.22 5-5.81 0-3.33-2-5.07-6.29-5.07s-6.18 1.78-6.18 5c-.04 3.7 3.33 4.7 7.47 5.88zM232.54 227c-6.18 0-9.48-3.33-9.48-13.22 0-10.44 3.92-15.14 11.22-15.14a14.84 14.84 0 0 1 6 1.11l-.28 2.18a13.83 13.83 0 0 0-5.44-1c-5.85 0-9 3.44-9.25 11.25l.11.07a11.49 11.49 0 0 1 7.7-3c6 0 8.59 2.59 8.59 8-.03 6.45-3.1 9.75-9.17 9.75zm.15-15.85a10.29 10.29 0 0 0-7.26 3.22c.11 7.66 2.22 10.51 7.11 10.51 4.52 0 6.81-2.59 6.81-7.66 0-4.15-2.04-6.04-6.67-6.04z" fill="#010101"></path>
    <path fill="#d6d6d5" d="M78.92 323.63h109.3v2H78.92zm0 14h89v2h-89z"></path>
    <path fill="#8ed0bc" d="M78.92 384.87h187.67v5.95H78.92z"></path>
    <path fill="#d6d6d5" d="M78.92 403.6h181.67v5.95H78.92zm0 50.25h109.3v2H78.92zm0 14h89v2h-89z"></path>
</svg>
                </div>
            </div>
        </div>
    </div>
  </div>
   </section><div id="earnings" class="position--relative z-20 pull-app-gutter--sides soft-app-gutter--right pull-large--bottom"><div class="bg-primary-layer-color soft-app-gutter--left soft--right soft-huge--ends"><div class="desk-wrapper soft-huge--ends portable-hard--ends"><h3 class=""><div class="eyebrow">Earnings</div><div>Get the most from driving</div></h3><div class="layout"><div class="layout__item one-half palm-one-whole soft-huge--right palm-hard--right
                             soft--bottom palm-soft-huge--bottom"><div class="soft-huge--right portable-hard--right"><div class="fixed-ratio-image fixed-ratio-image--partner-app-summaries soft-small--bottom" style="transform: scaleX(1);"></div><p class="primary-font--semibold flush">Follow your earnings</p><p>You'll see exact fares after every trip and get detailed reports of weekly earnings so you know what you're taking home and when you hit your goals.</p></div></div><div class="layout__item one-half palm-one-whole soft-huge--right palm-hard--right
                             palm-soft-huge--bottom"><div class="soft-huge--right portable-hard--right"><div class="fixed-ratio-image fixed-ratio-image--partner-app-predictions soft-small--bottom" style="transform: scaleX(1);"></div><p class="primary-font--semibold flush">Profitable hotspots</p><p>Make sure you're in the right place at the right time with a live map showing areas where riders need drivers most.</p></div></div><div class="layout__item one-half palm-one-whole soft-huge--right palm-hard--right
                             palm-soft-huge--bottom"><div class="soft-huge--right portable-hard--right"><div class="fixed-ratio-image fixed-ratio-image--partner-app-upcoming soft-small--bottom" style="transform: scaleX(1);"></div><p class="primary-font--semibold flush">Minimize downtime</p><p class="desk-and-up-flush lap-flush">The driver app will often send you new trip requests when you approach your rider's destination to help you make the most of your time behind the wheel.</p></div></div><div class="layout__item one-half palm-one-whole soft-huge--right palm-hard--right"><div class="soft-huge--right portable-hard--right"><div class="fixed-ratio-image fixed-ratio-image--partner-app-instant soft-small--bottom" style="transform: scaleX(1);"></div><p class="primary-font--semibold flush">Money when you need it</p><p class="flush">Payments happen electronically with Loader. By providing your banking account details, you'll receive payments on a weekly basis. You can access your payment statements at any time by visiting your partner dashboard.</p></div></div></div></div></div></div><div><div class=""><div class="pull-app-gutter--sides position--relative bg-secondary-layer-color
                     position--relative block-context pull-large--bottom"><div class="soft-app-gutter--sides"><div class="soft-huge--ends desk-wrapper push-large--ends"><div class="partner-app__convenience-copy"><div class="soft-huge--ends portable-hard--ends"><h3 class=""><div class="eyebrow">Convenience</div><div>Let the app be your guide</div></h3><p class="primary-font--semibold flush">Turn-by-turn navigation</p><p>The app makes it easy to find your way. Get turn-by-turn directions to your rider's pickup location and destination so all you have to focus on is the road ahead.</p><p class="primary-font--semibold flush">Convenient pit stops</p><p>The map shows you the nearest gas stations with the lowest prices as well as public restrooms for when you need to take a break.</p></div></div></div></div><div class="split-pane-image split-pane-image--left partner-app__convenience-image" style="transform: scaleX(1);"></div></div></div><!-- react-empty: 1848 --></div><div class="position--relative z-20 pull-app-gutter--sides soft-app-gutter--left palm-hard--sides"><div class="bg-primary-layer-color soft-app-gutter--right palm-soft-app-gutter--sides
                       lap-soft-layout-gutter--left soft-huge--ends"><div class="desk-wrapper soft-huge--ends portable-hard--ends"><h3 class=""><div class="eyebrow">Communication</div><div>Feedback for success</div></h3><div class="layout"><div class="layout__item one-half palm-one-whole soft-huge--right palm-hard--right"><div class="soft-huge--right portable-hard--right"><div class="fixed-ratio-image fixed-ratio-image--partner-app-notifications soft-tiny--bottom" style="transform: scaleX(1);"></div><p class="primary-font--semibold flush">Stay informed</p><p class="desk-and-up-flush lap-flush">Get customized updates about local events and promotions in your city, and tips on how to deliver the best Loader experience.</p></div></div><div class="layout__item one-half palm-one-whole soft-huge--right palm-hard--right"><div class="soft-huge--right portable-hard--right"><div class="fixed-ratio-image fixed-ratio-image--partner-app-comments soft-tiny--bottom" style="transform: scaleX(1);"></div><p class="primary-font--semibold flush">Rider comments</p><p class="flush">Use the ratings section to hear what people love about their 5-star trips and find out why riders give low ratings so you can decide how to adjust accordingly.</p></div></div></div></div></div></div><div class="bg-secondary-layer-color pull-app-gutter--sides soft-app-gutter--right
                     soft-huge--top position--relative z-10"><div class="soft-app-gutter--left bg-secondary-layer-color"><div class="desk-wrapper desk-and-up-one-half soft--right"><div class="push--bottom"><h3 class="push-tiny--bottom"><div class="eyebrow">Sign up</div><div>Sign up</div></h3><p>Drive with Loader and earn on your own terms.</p><a href="#"><button direction="right" kind="primary" class="_style_1s9rPr" style="background-color: rgb(26, 26, 26); border-color: rgb(26, 26, 26); line-height: 1.7em; padding: 12px 20px; text-align: left; width: auto;"><!-- react-text: 1876 -->Sign up to drive<!-- /react-text --><svg viewBox="0 0 64 64" width="16px" height="16px" class="_style_1gCyK0 _style_4wJp4e"><path fill-rule="evenodd" clip-rule="evenodd" d="M59.927 31.985l.073.076-16.233 17.072-3.247-3.593L51.324 34H4v-4h47.394L40.52 18.407l3.247-3.494L60 31.946l-.073.039z"></path></svg></button></a></div><div class="push--bottom"><hr></div><h4>Already signed up?</h4><p>Invite your friends to try Loader and you'll earn extra when they hit the road.</p><a href="#" class="btn btn--link hard--bottom   _style_Zlcmm borderless--left"><!-- react-text: 1884 -->Invite friends<!-- /react-text --><svg viewBox="0 0 64 64" width="16px" height="16px" class="_style_QJAbj _style_4wJp4e"><path d="M23.1611652,53.2132034l-4.2426414-4.2426414c-0.3908424-0.3908424-0.3908424-1.0233688,0-1.4142113L34.4748726,32 L18.9185238,16.4436512c-0.3908424-0.3908424-0.3908424-1.0233717,0-1.4142141l4.2426414-4.2426405 c0.3908424-0.3908424,1.0233707-0.3908424,1.4142132,0l16.2634544,16.2634563l4.2426414,4.2426395 c0.3908424,0.3908424,0.3908424,1.0233727,0,1.4142151l-4.2426414,4.2426414L24.5753784,53.2132034 C24.184536,53.6040459,23.5520077,53.6040459,23.1611652,53.2132034z"></path></svg></a></div></div></div><div class="soft-huge--top bg-secondary-layer-color
                     pull-app-gutter--sides soft-app-gutter--right position--relative z-10"><div class="soft-huge--top palm-hard--top soft-app-gutter--left"><div class="desk-wrapper"><p class="legal flush">The information provided on this web page is intended for informational purposes only and may not be applicable in your country, region, or city. It is subject to change and may be updated without notice.</p></div></div></div><div class="partner-app__modal position--relative"><span></span></div></div></div></div></div>
                     @endsection
                      @section('javascripts')
                        @parent
                        <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="https://raw.githubusercontent.com/pegsbenedict/demos/master/slick/slick.min.js"></script>

<script>
   $(document).ready(function(){

    $('.slick-track').slick();

    });
/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

  Author: Ken Wheeler
 Website: https://kenwheeler.github.io
    Docs: https://kenwheeler.github.io/slick
    Repo: https://github.com/kenwheeler/slick
  Issues: https://github.com/kenwheeler/slick/issues

 */

/* global window, document, define, jQuery, setInterval, clearInterval */

(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        factory(jQuery);
    }

}(function($) {
    'use strict';
    var Slick = window.Slick || {};

    Slick = (function() {

        var instanceUid = 0;

        function Slick(element, settings) {

            var _ = this,
                responsiveSettings, breakpoint;

            _.defaults = {
                accessibility: true,
                arrows: true,
                autoplay: false,
                autoplaySpeed: 3000,
                centerMode: false,
                centerPadding: '50px',
                cssEase: 'ease',
                customPaging: function(slider, i) {
                    return '<button type="button">' + (i + 1) + '</button>';
                },
                dots: false,
                draggable: true,
                easing: 'linear',
                fade: false,
                infinite: true,
                lazyLoad: 'ondemand',
                onBeforeChange: null,
                onAfterChange: null,
                onInit: null,
                onReInit: null,
                pauseOnHover: true,
                responsive: null,
                slide: 'div',
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 300,
                swipe: true,
                touchMove: true,
                touchThreshold: 5,
                vertical: false
            };

            _.initials = {
                animating: false,
                autoPlayTimer: null,
                currentSlide: 0,
                currentLeft: null,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: false,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: false
            };

            $.extend(_, _.initials);

            _.activeBreakpoint = null;
            _.animType = null;
            _.animProp = null;
            _.breakpoints = [];
            _.breakpointSettings = [];
            _.cssTransitions = false;
            _.paused = false;
            _.positionProp = null;
            _.$slider = $(element);
            _.$slidesCache = null;
            _.transformType = null;
            _.transitionType = null;
            _.windowWidth = 0;
            _.windowTimer = null;

            _.options = $.extend({}, _.defaults, settings);

            _.originalSettings = _.options;
            responsiveSettings = _.options.responsive || null;

            if (responsiveSettings && responsiveSettings.length > -1) {
                for (breakpoint in responsiveSettings) {
                    if (responsiveSettings.hasOwnProperty(breakpoint)) {
                        _.breakpoints.push(responsiveSettings[
                            breakpoint].breakpoint);
                        _.breakpointSettings[responsiveSettings[
                            breakpoint].breakpoint] =
                            responsiveSettings[breakpoint].settings;
                    }
                }
                _.breakpoints.sort(function(a, b) {
                    return b - a;
                });
            }

            _.autoPlay = $.proxy(_.autoPlay, _);
            _.autoPlayClear = $.proxy(_.autoPlayClear, _);
            _.changeSlide = $.proxy(_.changeSlide, _);
            _.setPosition = $.proxy(_.setPosition, _);
            _.swipeHandler = $.proxy(_.swipeHandler, _);
            _.dragHandler = $.proxy(_.dragHandler, _);
            _.keyHandler = $.proxy(_.keyHandler, _);
            _.autoPlayIterator = $.proxy(_.autoPlayIterator, _);

            _.instanceUid = instanceUid++;

            _.init();

        }

        return Slick;

    }());

    Slick.prototype.addSlide = function(markup, index, addBefore) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            addBefore = index;
            index = null;
        } else if (index < 0 || (index >= _.slideCount)) {
            return false;
        }

        _.unload();

        if (typeof(index) === 'number') {
            if (index === 0 && _.$slides.length === 0) {
                $(markup).appendTo(_.$slideTrack);
            } else if (addBefore) {
                $(markup).insertBefore(_.$slides.eq(index));
            } else {
                $(markup).insertAfter(_.$slides.eq(index));
            }
        } else {
            if (addBefore === true) {
                $(markup).prependTo(_.$slideTrack);
            } else {
                $(markup).appendTo(_.$slideTrack);
            }
        }

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).remove();

        _.$slideTrack.append(_.$slides);

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.animateSlide = function(targetLeft,
        callback) {

        var animProps = {}, _ = this;

        if (_.transformsEnabled === false) {
            if (_.options.vertical === false) {
                _.$slideTrack.animate({
                    left: targetLeft
                }, _.options.speed, _.options.easing, callback);
            } else {
                _.$slideTrack.animate({
                    top: targetLeft
                }, _.options.speed, _.options.easing, callback);
            }

        } else {

            if (_.cssTransitions === false) {

                $({
                    animStart: _.currentLeft
                }).animate({
                    animStart: targetLeft
                }, {
                    duration: _.options.speed,
                    easing: _.options.easing,
                    step: function(now) {
                        if (_.options.vertical === false) {
                            animProps[_.animType] = 'translate(' +
                                now + 'px, 0px)';
                            _.$slideTrack.css(animProps);
                        } else {
                            animProps[_.animType] = 'translate(0px,' +
                                now + 'px)';
                            _.$slideTrack.css(animProps);
                        }
                    },
                    complete: function() {
                        if (callback) {
                            callback.call();
                        }
                    }
                });

            } else {

                _.applyTransition();

                if (_.options.vertical === false) {
                    animProps[_.animType] = 'translate3d(' + targetLeft + 'px, 0px, 0px)';
                } else {
                    animProps[_.animType] = 'translate3d(0px,' + targetLeft + 'px, 0px)';
                }
                _.$slideTrack.css(animProps);

                if (callback) {
                    setTimeout(function() {

                        _.disableTransition();

                        callback.call();
                    }, _.options.speed);
                }

            }

        }

    };

    Slick.prototype.applyTransition = function(slide) {

        var _ = this,
            transition = {};

        if (_.options.fade === false) {
            transition[_.transitionType] = _.transformType + ' ' + _.options.speed + 'ms ' + _.options.cssEase;
        } else {
            transition[_.transitionType] = 'opacity ' + _.options.speed + 'ms ' + _.options.cssEase;
        }

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.autoPlay = function() {

        var _ = this;

        if (_.autoPlayTimer) {
            clearInterval(_.autoPlayTimer);
        }

        if (_.slideCount > _.options.slidesToShow && _.paused !== true) {
            _.autoPlayTimer = setInterval(_.autoPlayIterator,
                _.options.autoplaySpeed);
        }

    };

    Slick.prototype.autoPlayClear = function() {

        var _ = this;

        if (_.autoPlayTimer) {
            clearInterval(_.autoPlayTimer);
        }

    };

    Slick.prototype.autoPlayIterator = function() {

        var _ = this;

        if (_.options.infinite === false) {

            if (_.direction === 1) {

                if ((_.currentSlide + 1) === _.slideCount -
                    1) {
                    _.direction = 0;
                }

                _.slideHandler(_.currentSlide + _.options
                    .slidesToScroll);

            } else {

                if ((_.currentSlide - 1 === 0)) {

                    _.direction = 1;

                }

                _.slideHandler(_.currentSlide - _.options
                    .slidesToScroll);

            }

        } else {

            _.slideHandler(_.currentSlide + _.options.slidesToScroll);

        }

    };

    Slick.prototype.buildArrows = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow = $(
                '<button type="button" class="slick-prev">Previous</button>').appendTo(
                _.$slider);
            _.$nextArrow = $(
                '<button type="button" class="slick-next">Next</button>').appendTo(
                _.$slider);

            if (_.options.infinite !== true) {
                _.$prevArrow.addClass('slick-disabled');
            }

        }

    };

    Slick.prototype.buildDots = function() {

        var _ = this,
            i, dotString;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            dotString = '<ul class="slick-dots">';

            for (i = 0; i <= _.getDotCount(); i += 1) {
                dotString += '<li>' + _.options.customPaging.call(this, _, i) + '</li>';
            }

            dotString += '</ul>';

            _.$dots = $(dotString).appendTo(
                _.$slider);

            _.$dots.find('li').first().addClass(
                'slick-active');

        }

    };

    Slick.prototype.buildOut = function() {

        var _ = this;

        _.$slides = _.$slider.children(_.options.slide +
            ':not(.slick-cloned)').addClass(
            'slick-slide');
        _.slideCount = _.$slides.length;
        _.$slidesCache = _.$slides;

        _.$slider.addClass('slick-slider');

        _.$slideTrack = (_.slideCount === 0) ?
            $('<div class="slick-track"/>').appendTo(_.$slider) :
            _.$slides.wrapAll('<div class="slick-track"/>').parent();

        _.$list = _.$slideTrack.wrap(
            '<div class="slick-list"/>').parent();
        _.$slideTrack.css('opacity', 0);

        if (_.options.centerMode === true) {
            _.options.infinite = true;
            _.options.slidesToScroll = 1;
            if (_.options.slidesToShow % 2 === 0) {
                _.options.slidesToShow = 3;
            }
        }

        $('img[data-lazy]', _.$slider).not('[src]').addClass('slick-loading');

        _.setupInfinite();

        _.buildArrows();

        _.buildDots();

        if (_.options.accessibility === true) {
            _.$list.prop('tabIndex', 0);
        }

        _.setSlideClasses(0);

        if (_.options.draggable === true) {
            _.$list.addClass('draggable');
        }

    };

    Slick.prototype.checkResponsive = function() {

        var _ = this,
            breakpoint, targetBreakpoint;

        if (_.originalSettings.responsive && _.originalSettings
            .responsive.length > -1 && _.originalSettings.responsive !== null) {

            targetBreakpoint = null;

            for (breakpoint in _.breakpoints) {
                if (_.breakpoints.hasOwnProperty(breakpoint)) {
                    if ($(window).width() < _.breakpoints[
                        breakpoint]) {
                        targetBreakpoint = _.breakpoints[
                            breakpoint];
                    }
                }
            }

            if (targetBreakpoint !== null) {
                if (_.activeBreakpoint !== null) {
                    if (targetBreakpoint !== _.activeBreakpoint) {
                        _.activeBreakpoint =
                            targetBreakpoint;
                        _.options = $.extend({}, _.defaults,
                            _.breakpointSettings[
                                targetBreakpoint]);
                        _.refresh();
                    }
                } else {
                    _.activeBreakpoint = targetBreakpoint;
                    _.options = $.extend({}, _.defaults,
                        _.breakpointSettings[
                            targetBreakpoint]);
                    _.refresh();
                }
            } else {
                if (_.activeBreakpoint !== null) {
                    _.activeBreakpoint = null;
                    _.options = $.extend({}, _.defaults,
                        _.originalSettings);
                    _.refresh();
                }
            }

        }

    };

    Slick.prototype.changeSlide = function(event) {

        var _ = this;

        switch (event.data.message) {

            case 'previous':
                _.slideHandler(_.currentSlide - _.options
                    .slidesToScroll);
                break;

            case 'next':
                _.slideHandler(_.currentSlide + _.options
                    .slidesToScroll);
                break;

            case 'index':
                _.slideHandler($(event.target).parent().index() * _.options.slidesToScroll);
                break;

            default:
                return false;
        }

    };

    Slick.prototype.destroy = function() {

        var _ = this;

        _.autoPlayClear();

        _.touchObject = {};

        $('.slick-cloned', _.$slider).remove();
        if (_.$dots) {
            _.$dots.remove();
        }
        if (_.$prevArrow) {
            _.$prevArrow.remove();
            _.$nextArrow.remove();
        }
        _.$slides.unwrap().unwrap();
        _.$slides.removeClass(
            'slick-slide slick-active slick-visible').removeAttr('style');
        _.$slider.removeClass('slick-slider');
        _.$slider.removeClass('slick-initialized');

        _.$list.off('.slick');
        $(window).off('.slick-' + _.instanceUid);
    };

    Slick.prototype.disableTransition = function(slide) {

        var _ = this,
            transition = {};

        transition[_.transitionType] = "";

        if (_.options.fade === false) {
            _.$slideTrack.css(transition);
        } else {
            _.$slides.eq(slide).css(transition);
        }

    };

    Slick.prototype.fadeSlide = function(slideIndex, callback) {

        var _ = this;

        if (_.cssTransitions === false) {

            _.$slides.eq(slideIndex).css({
                zIndex: 1000
            });

            _.$slides.eq(slideIndex).animate({
                opacity: 1
            }, _.options.speed, _.options.easing, callback);

        } else {

            _.applyTransition(slideIndex);

            _.$slides.eq(slideIndex).css({
                opacity: 1,
                zIndex: 1000
            });

            if (callback) {
                setTimeout(function() {

                    _.disableTransition(slideIndex);

                    callback.call();
                }, _.options.speed);
            }

        }

    };

    Slick.prototype.filterSlides = function(filter) {

        var _ = this;

        if (filter !== null) {

            _.unload();

            _.$slideTrack.children(this.options.slide).remove();

            _.$slidesCache.filter(filter).appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.getDotCount = function() {

        var _ = this,
            breaker = 0,
            dotCounter = 0,
            dotCount = 0,
            dotLimit;

        dotLimit = _.options.infinite === true ? _.slideCount + _.options.slidesToShow - _.options.slidesToScroll : _.slideCount;

        while (breaker < dotLimit) {
            dotCount++;
            dotCounter += _.options.slidesToScroll;
            breaker = dotCounter + _.options.slidesToShow;
        }

        return dotCount;

    };

    Slick.prototype.getLeft = function(slideIndex) {

        var _ = this,
            targetLeft;

        _.slideOffset = 0;

        if (_.options.infinite === true) {
            if (_.slideCount > _.options.slidesToShow) {
                _.slideOffset = (_.slideWidth * _.options.slidesToShow) * -1;
            }
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
                    _.slideOffset = ((_.slideCount % _.options.slidesToShow) * _.slideWidth) * -1;
                }
            }
        } else {
            if (_.slideCount % _.options.slidesToShow !== 0) {
                if (slideIndex + _.options.slidesToScroll > _.slideCount && _.slideCount > _.options.slidesToShow) {
                    _.slideOffset = ((_.slideCount % _.options.slidesToShow) * _.slideWidth);
                }
            }
        }

        if (_.options.centerMode === true) {
            _.slideOffset += _.slideWidth * Math.floor(_.options.slidesToShow / 2) - _.slideWidth;
        }

        if (_.options.vertical === false) {
            targetLeft = ((slideIndex * _.slideWidth) * -1) + _.slideOffset;
        } else {
            _.listHeight = _.$list.height();
            if (_.options.infinite === true) {
                targetLeft = ((slideIndex * _.listHeight) * -1) - _.listHeight;
            } else {
                targetLeft = ((slideIndex * _.listHeight) * -1);
            }
        }

        return targetLeft;

    };

    Slick.prototype.init = function() {

        var _ = this;

        if (!$(_.$slider).hasClass('slick-initialized')) {

            $(_.$slider).addClass('slick-initialized');
            _.buildOut();
            _.setProps();
            _.startLoad();
            _.loadSlider();
            _.initializeEvents();
            _.checkResponsive();
        }

        if (_.options.onInit !== null) {
            _.options.onInit.call(this, _);
        }

    };

    Slick.prototype.initArrowEvents = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow.on('click.slick', {
                message: 'previous'
            }, _.changeSlide);
            _.$nextArrow.on('click.slick', {
                message: 'next'
            }, _.changeSlide);
        }

    };

    Slick.prototype.initDotEvents = function() {

        var _ = this;

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {
            $('li', _.$dots).on('click.slick', {
                message: 'index'
            }, _.changeSlide);
        }

    };

    Slick.prototype.initializeEvents = function() {

        var _ = this;

        _.initArrowEvents();

        _.initDotEvents();

        _.$list.on('touchstart.slick mousedown.slick', {
            action: 'start'
        }, _.swipeHandler);
        _.$list.on('touchmove.slick mousemove.slick', {
            action: 'move'
        }, _.swipeHandler);
        _.$list.on('touchend.slick mouseup.slick', {
            action: 'end'
        }, _.swipeHandler);
        _.$list.on('touchcancel.slick mouseleave.slick', {
            action: 'end'
        }, _.swipeHandler);

        if (_.options.pauseOnHover === true && _.options.autoplay === true) {
            _.$list.on('mouseenter.slick', _.autoPlayClear);
            _.$list.on('mouseleave.slick', _.autoPlay);
        }

        if(_.options.accessibility === true) {
            _.$list.on('keydown.slick', _.keyHandler); 
        }

        $(window).on('orientationchange.slick.slick-' + _.instanceUid, function() {
            _.checkResponsive();
            _.setPosition();
        });

        $(window).on('resize.slick.slick-' + _.instanceUid, function() {
            if ($(window).width !== _.windowWidth) {
                clearTimeout(_.windowDelay);
                _.windowDelay = window.setTimeout(function() {
                    _.windowWidth = $(window).width();
                    _.checkResponsive();
                    _.setPosition();
                }, 50);
            }
        });

        $(window).on('load.slick.slick-' + _.instanceUid, _.setPosition);

    };

    Slick.prototype.initUI = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.show();
            _.$nextArrow.show();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.show();

        }

        if (_.options.autoplay === true) {

            _.autoPlay();

        }

    };

    Slick.prototype.keyHandler = function(event) {

        var _ = this;

        if (event.keyCode === 37) {
            _.changeSlide({
                data: {
                    message: 'previous'
                }
            });
        } else if (event.keyCode === 39) {
            _.changeSlide({
                data: {
                    message: 'next'
                }
            });
        }

    };

    Slick.prototype.lazyLoad = function() {

        var _ = this,
            loadRange, cloneRange, rangeStart, rangeEnd;

        if (_.options.centerMode === true) {
            rangeStart = _.options.slidesToShow + _.currentSlide - 1;
            rangeEnd = rangeStart + _.options.slidesToShow + 2;
        } else {
            rangeStart = _.options.infinite ? _.options.slidesToShow + _.currentSlide : _.currentSlide;
            rangeEnd = rangeStart + _.options.slidesToShow;
        }

        loadRange = _.$slider.find('.slick-slide').slice(rangeStart, rangeEnd);

        $('img[data-lazy]', loadRange).not('[src]').each(function() {
            $(this).css({opacity: 0}).attr('src', $(this).attr('data-lazy')).removeClass('slick-loading').load(function(){
                $(this).animate({ opacity: 1 }, 200);
            });
        });

        if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
            cloneRange = _.$slider.find('.slick-cloned').slice(0, _.options.slidesToShow);
            $('img[data-lazy]', cloneRange).not('[src]').each(function() {
                $(this).css({opacity: 0}).attr('src', $(this).attr('data-lazy')).removeClass('slick-loading').load(function(){
                    $(this).animate({ opacity: 1 }, 200);
                });
            });
        } else if (_.currentSlide === 0) {
            cloneRange = _.$slider.find('.slick-cloned').slice(_.options.slidesToShow * -1);
            $('img[data-lazy]', cloneRange).not('[src]').each(function() {
                $(this).css({opacity: 0}).attr('src', $(this).attr('data-lazy')).removeClass('slick-loading').load(function(){
                    $(this).animate({ opacity: 1 }, 200);
                });
            });
        }

    };

    Slick.prototype.loadSlider = function() {

        var _ = this;

        _.setPosition();

        _.$slideTrack.css({
            opacity: 1
        });

        _.$slider.removeClass('slick-loading');

        _.initUI();

        if (_.options.lazyLoad === 'progressive') {
            _.progressiveLazyLoad();
        }

    };

    Slick.prototype.postSlide = function(index) {

        var _ = this;

        if (_.options.onAfterChange !== null) {
            _.options.onAfterChange.call(this, _, index);
        }

        _.animating = false;

        _.setPosition();

        _.swipeLeft = null;

        if (_.options.autoplay === true && _.paused === false) {
            _.autoPlay();
        }

        _.setSlideClasses(_.currentSlide);

    };

    Slick.prototype.progressiveLazyLoad = function() {

        var _ = this,
            imgCount, targetImage;

        imgCount = $('img[data-lazy]').not('[src]').length;

        if (imgCount > 0) {
            targetImage = $($('img[data-lazy]', _.$slider).not('[src]').get(0));
            targetImage.attr('src', targetImage.attr('data-lazy')).removeClass('slick-loading').load(function() {
                _.progressiveLazyLoad();
            });
        }

    };

    Slick.prototype.refresh = function() {

        var _ = this;

        _.destroy();

        $.extend(_, _.initials);

        _.init();

    };

    Slick.prototype.reinit = function() {

        var _ = this;

        _.$slides = $(_.options.slide +
            ':not(.slick-cloned)', _.$slideTrack).addClass(
            'slick-slide');

        _.slideCount = _.$slides.length;

        if (_.currentSlide >= _.slideCount && _.currentSlide !== 0) {
            _.currentSlide = _.currentSlide - _.options.slidesToScroll;
        }

        _.setProps();

        _.setupInfinite();

        _.buildArrows();

        _.updateArrows();

        _.initArrowEvents();

        _.buildDots();

        _.updateDots();

        _.initDotEvents();

        _.setSlideClasses(0);

        _.setPosition();

        if (_.options.onReInit !== null) {
            _.options.onReInit.call(this, _);
        }

    };

    Slick.prototype.removeSlide = function(index, removeBefore) {

        var _ = this;

        if (typeof(index) === 'boolean') {
            removeBefore = index;
            index = removeBefore === true ? 0 : _.slideCount - 1;
        } else {
            index = removeBefore === true ? --index : index;
        }

        if (_.slideCount < 1 || index < 0 || index > _.slideCount - 1) {
            return false;
        }

        _.unload();

        _.$slideTrack.children(this.options.slide).eq(index).remove();

        _.$slides = _.$slideTrack.children(this.options.slide);

        _.$slideTrack.children(this.options.slide).remove();

        _.$slideTrack.append(_.$slides);

        _.$slidesCache = _.$slides;

        _.reinit();

    };

    Slick.prototype.setCSS = function(position) {

        var _ = this,
            positionProps = {}, x, y;

        x = _.positionProp == 'left' ? position + 'px' : '0px';
        y = _.positionProp == 'top' ? position + 'px' : '0px';

        positionProps[_.positionProp] = position;

        if (_.transformsEnabled === false) {
            _.$slideTrack.css(positionProps);
        } else {
            positionProps = {};
            if (_.cssTransitions === false) {
                positionProps[_.animType] = 'translate(' + x + ', ' + y + ')';
                _.$slideTrack.css(positionProps);
            } else {
                positionProps[_.animType] = 'translate3d(' + x + ', ' + y + ', 0px)';
                _.$slideTrack.css(positionProps);
            }
        }

    };

    Slick.prototype.setDimensions = function() {

        var _ = this;

        if (_.options.centerMode === true) {
            _.$slideTrack.children('.slick-slide').width(_.slideWidth);
        } else {
            _.$slideTrack.children('.slick-slide').width(_.slideWidth);
        }


        if (_.options.vertical === false) {
            _.$slideTrack.width(Math.ceil((_.slideWidth * _
                .$slideTrack.children('.slick-slide').length)));
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: ('0px ' + _.options.centerPadding)
                });
            }
        } else {
            _.$list.height(_.$slides.first().outerHeight());
            _.$slideTrack.height(Math.ceil((_.listHeight * _
                .$slideTrack.children('.slick-slide').length)));
            if (_.options.centerMode === true) {
                _.$list.css({
                    padding: (_.options.centerPadding + ' 0px')
                });
            }
        }

    };

    Slick.prototype.setFade = function() {

        var _ = this,
            targetLeft;

        _.$slides.each(function(index, element) {
            targetLeft = (_.slideWidth * index) * -1;
            $(element).css({
                position: 'relative',
                left: targetLeft,
                top: 0,
                zIndex: 800,
                opacity: 0
            });
        });

        _.$slides.eq(_.currentSlide).css({
            zIndex: 900,
            opacity: 1
        });

    };

    Slick.prototype.setPosition = function() {

        var _ = this;

        _.setValues();
        _.setDimensions();

        if (_.options.fade === false) {
            _.setCSS(_.getLeft(_.currentSlide));
        } else {
            _.setFade();
        }

    };

    Slick.prototype.setProps = function() {

        var _ = this;

        _.positionProp = _.options.vertical === true ? 'top' : 'left';

        if (_.positionProp === 'top') {
            _.$slider.addClass('slick-vertical');
        } else {
            _.$slider.removeClass('slick-vertical');
        }

        if (document.body.style.WebkitTransition !== undefined ||
            document.body.style.MozTransition !== undefined ||
            document.body.style.msTransition !== undefined) {
            _.cssTransitions = true;
        }

        if (document.body.style.MozTransform !== undefined) {
            _.animType = 'MozTransform';
            _.transformType = "-moz-transform";
            _.transitionType = 'MozTransition';
        }
        if (document.body.style.webkitTransform !== undefined) {
            _.animType = 'webkitTransform';
            _.transformType = "-webkit-transform";
            _.transitionType = 'webkitTransition';
        }
        if (document.body.style.msTransform !== undefined) {
            _.animType = 'transform';
            _.transformType = "transform";
            _.transitionType = 'transition';
        }

        _.transformsEnabled = (_.animType !== null);

    };

    Slick.prototype.setValues = function() {

        var _ = this;

        _.listWidth = _.$list.width();
        _.listHeight = _.$list.height();
        _.slideWidth = Math.ceil(_.listWidth / _.options
            .slidesToShow);

    };

    Slick.prototype.setSlideClasses = function(index) {

        var _ = this,
            centerOffset, allSlides, indexOffset;

        _.$slider.find('.slick-slide').removeClass('slick-active').removeClass('slick-center');
        allSlides = _.$slider.find('.slick-slide');

        if (_.options.centerMode === true) {

            centerOffset = Math.floor(_.options.slidesToShow / 2);

            if (index >= centerOffset && index <= (_.slideCount - 1) - centerOffset) {
                _.$slides.slice(index - centerOffset, index + centerOffset + 1).addClass('slick-active');
            } else {
                indexOffset = _.options.slidesToShow + index;
                allSlides.slice(indexOffset - centerOffset + 1, indexOffset + centerOffset + 2).addClass('slick-active');
            }

            if (index === 0) {
                allSlides.eq(allSlides.length - 1 - _.options.slidesToShow).addClass('slick-center');
            } else if (index === _.slideCount - 1) {
                allSlides.eq(_.options.slidesToShow).addClass('slick-center');
            }

            _.$slides.eq(index).addClass('slick-center');

        } else {

            if (index > 0 && index < (_.slideCount - _.options.slidesToShow)) {
                _.$slides.slice(index, index + _.options.slidesToShow).addClass('slick-active');
            } else {
                indexOffset = _.options.slidesToShow + index;
                allSlides.slice(indexOffset, indexOffset + _.options.slidesToShow).addClass('slick-active');
            }

        }

        if (_.options.lazyLoad === 'ondemand') {
            _.lazyLoad();
        }

    };

    Slick.prototype.setupInfinite = function() {

        var _ = this,
            i, slideIndex, infiniteCount;

        if (_.options.fade === true || _.options.vertical === true) {
            _.options.slidesToShow = 1;
            _.options.slidesToScroll = 1;
            _.options.centerMode = false;
        }

        if (_.options.infinite === true && _.options.fade === false) {

            slideIndex = null;

            if (_.slideCount > _.options.slidesToShow) {

                if (_.options.centerMode === true) {
                    infiniteCount = _.options.slidesToShow + 1;
                } else {
                    infiniteCount = _.options.slidesToShow;
                }

                for (i = _.slideCount; i > (_.slideCount -
                    infiniteCount); i -= 1) {
                    slideIndex = i - 1;
                    $(_.$slides[slideIndex]).clone().attr('id', '').prependTo(
                        _.$slideTrack).addClass('slick-cloned');
                }
                for (i = 0; i < infiniteCount; i += 1) {
                    slideIndex = i;
                    $(_.$slides[slideIndex]).clone().attr('id', '').appendTo(
                        _.$slideTrack).addClass('slick-cloned');
                }
                _.$slideTrack.find('.slick-cloned').find('[id]').each(function() {
                    $(this).attr('id', '');
                });

            }

        }

    };

    Slick.prototype.slideHandler = function(index) {

        var targetSlide, animSlide, slideLeft, unevenOffset, targetLeft = null,
            _ = this;

        if (_.animating === true) {
            return false;
        }

        targetSlide = index;
        targetLeft = _.getLeft(targetSlide);
        slideLeft = _.getLeft(_.currentSlide);

        unevenOffset = _.slideCount % _.options.slidesToScroll !== 0 ? _.options.slidesToScroll : 0;

        if (_.options.infinite === false && (index < 0 || index > (_.slideCount - _.options.slidesToShow + unevenOffset))) {
            targetSlide = _.currentSlide;
            _.animateSlide(slideLeft, function() {
                _.postSlide(targetSlide);
            });
            return false;
        }

        if (_.options.autoplay === true) {
            clearInterval(_.autoPlayTimer);
        }

        _.currentLeft = _.swipeLeft === null ? slideLeft : _.swipeLeft;

        if (targetSlide < 0) {
            if (_.slideCount % _.options.slidesToScroll !== 0) {
                animSlide = _.slideCount - (_.slideCount % _.options.slidesToScroll);
            } else {
                animSlide = _.slideCount - _.options.slidesToScroll;
            }
        } else if (targetSlide > (_.slideCount - 1)) {
            animSlide = 0;
        } else {
            animSlide = targetSlide;
        }

        _.animating = true;

        if (_.options.onBeforeChange !== null && index !== _.currentSlide) {
            _.options.onBeforeChange.call(this, _, _.currentSlide, animSlide);
        }

        _.currentSlide = animSlide;
        _.updateDots();
        _.updateArrows();

        if (_.options.fade === true) {
            _.fadeSlide(animSlide, function() {
                _.postSlide(animSlide);
            });
            return false;
        }

        _.animateSlide(targetLeft, function() {
            _.postSlide(animSlide);
        });

    };

    Slick.prototype.startLoad = function() {

        var _ = this;

        if (_.options.arrows === true && _.slideCount > _.options.slidesToShow) {

            _.$prevArrow.hide();
            _.$nextArrow.hide();

        }

        if (_.options.dots === true && _.slideCount > _.options.slidesToShow) {

            _.$dots.hide();

        }

        _.$slider.addClass('slick-loading');

    };

    Slick.prototype.swipeDirection = function() {

        var xDist, yDist, r, swipeAngle, _ = this;

        xDist = _.touchObject.startX - _.touchObject.curX;
        yDist = _.touchObject.startY - _.touchObject.curY;
        r = Math.atan2(yDist, xDist);

        swipeAngle = Math.round(r * 180 / Math.PI);
        if (swipeAngle < 0) {
            swipeAngle = 360 - Math.abs(swipeAngle);
        }

        if ((swipeAngle <= 45) && (swipeAngle >= 0)) {
            return 'left';
        }
        if ((swipeAngle <= 360) && (swipeAngle >= 315)) {
            return 'left';
        }
        if ((swipeAngle >= 135) && (swipeAngle <= 225)) {
            return 'right';
        }

        return 'vertical';

    };

    Slick.prototype.swipeEnd = function(event) {

        var _ = this;

        _.$list.removeClass('dragging');

        if (_.touchObject.curX === undefined) {
            return false;
        }

        if (_.touchObject.swipeLength >= _.touchObject.minSwipe) {
            $(event.target).on('click.slick', function(event) {
                event.stopImmediatePropagation();
                event.stopPropagation();
                event.preventDefault();
                $(event.target).off('click.slick');
            });

            switch (_.swipeDirection()) {
                case 'left':
                    _.slideHandler(_.currentSlide + _.options.slidesToScroll);
                    _.touchObject = {};
                    break;

                case 'right':
                    _.slideHandler(_.currentSlide - _.options.slidesToScroll);
                    _.touchObject = {};
                    break;
            }
        } else {
            if(_.touchObject.startX !== _.touchObject.curX) {
                _.slideHandler(_.currentSlide);
                _.touchObject = {};
            }
        }

    };

    Slick.prototype.swipeHandler = function(event) {

        var _ = this;

        if ('ontouchend' in document && _.options.swipe === false) {
            return false;
        } else if (_.options.draggable === false && !event.originalEvent.touches) {
            return false;
        }

        _.touchObject.fingerCount = event.originalEvent && event.originalEvent.touches !== undefined ?
            event.originalEvent.touches.length : 1;

        _.touchObject.minSwipe = _.listWidth / _.options
            .touchThreshold;

        switch (event.data.action) {

            case 'start':
                _.swipeStart(event);
                break;

            case 'move':
                _.swipeMove(event);
                break;

            case 'end':
                _.swipeEnd(event);
                break;

        }

    };

    Slick.prototype.swipeMove = function(event) {

        var _ = this,
            curLeft, swipeDirection, positionOffset, touches;

        touches = event.originalEvent !== undefined ? event.originalEvent.touches : null;

        curLeft = _.getLeft(_.currentSlide);

        if (!_.$list.hasClass('dragging') || touches && touches.length !== 1) {
            return false;
        }

        _.touchObject.curX = touches !== undefined ? touches[0].pageX : event.clientX;
        _.touchObject.curY = touches !== undefined ? touches[0].pageY : event.clientY;

        _.touchObject.swipeLength = Math.round(Math.sqrt(
            Math.pow(_.touchObject.curX - _.touchObject.startX, 2)));

        swipeDirection = _.swipeDirection();

        if (swipeDirection === 'vertical') {
            return false;
        }

        if (event.originalEvent !== undefined) {
            event.preventDefault();
        }

        positionOffset = _.touchObject.curX > _.touchObject.startX ? 1 : -1;

        if (_.options.vertical === false) {
            _.swipeLeft = curLeft + _.touchObject.swipeLength * positionOffset;
        } else {
            _.swipeLeft = curLeft + (_.touchObject
                .swipeLength * (_.listHeight / _.listWidth)) * positionOffset;
        }

        if (_.options.fade === true || _.options.touchMove === false) {
            return false;
        }

        if (_.animating === true) {
            _.swipeLeft = null;
            return false;
        }

        _.setCSS(_.swipeLeft);

    };

    Slick.prototype.swipeStart = function(event) {

        var _ = this,
            touches;

        if (_.touchObject.fingerCount !== 1 || _.slideCount <= _.options.slidesToShow) {
            _.touchObject = {};
            return false;
        }

        if (event.originalEvent !== undefined && event.originalEvent.touches !== undefined) {
            touches = event.originalEvent.touches[0];
        }

        _.touchObject.startX = _.touchObject.curX = touches !== undefined ? touches.pageX : event.clientX;
        _.touchObject.startY = _.touchObject.curY = touches !== undefined ? touches.pageY : event.clientY;

        _.$list.addClass('dragging');

    };

    Slick.prototype.unfilterSlides = function() {

        var _ = this;

        if (_.$slidesCache !== null) {

            _.unload();

            _.$slideTrack.children(this.options.slide).remove();

            _.$slidesCache.appendTo(_.$slideTrack);

            _.reinit();

        }

    };

    Slick.prototype.unload = function() {

        var _ = this;

        $('.slick-cloned', _.$slider).remove();
        if (_.$dots) {
            _.$dots.remove();
        }
        if (_.$prevArrow) {
            _.$prevArrow.remove();
            _.$nextArrow.remove();
        }
        _.$slides.removeClass(
            'slick-slide slick-active slick-visible').removeAttr('style');

    };

    Slick.prototype.updateArrows = function() {

        var _ = this;

        if (_.options.arrows === true && _.options.infinite !==
            true && _.slideCount > _.options.slidesToShow) {
            _.$prevArrow.removeClass('slick-disabled');
            _.$nextArrow.removeClass('slick-disabled');
            if (_.currentSlide === 0) {
                _.$prevArrow.addClass('slick-disabled');
                _.$nextArrow.removeClass('slick-disabled');
            } else if (_.currentSlide >= _.slideCount - _.options.slidesToShow) {
                _.$nextArrow.addClass('slick-disabled');
                _.$prevArrow.removeClass('slick-disabled');
            }
        }

    };

    Slick.prototype.updateDots = function() {

        var _ = this;

        if (_.$dots !== null) {

            _.$dots.find('li').removeClass('slick-active');
            _.$dots.find('li').eq(_.currentSlide / _.options.slidesToScroll).addClass(
                'slick-active');

        }

    };

    $.fn.slick = function(options) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick = new Slick(element, options);

        });
    };

    $.fn.slickAdd = function(slide, slideIndex, addBefore) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.addSlide(slide, slideIndex, addBefore);

        });
    };

    $.fn.slickFilter = function(filter) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.filterSlides(filter);

        });
    };

    $.fn.slickGoTo = function(slide) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.slideHandler(slide);

        });
    };

    $.fn.slickNext = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.changeSlide({
                data: {
                    message: 'next'
                }
            });

        });
    };

    $.fn.slickPause = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.autoPlayClear();
            element.slick.paused = true;

        });
    };

    $.fn.slickPlay = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.paused = false;
            element.slick.autoPlay();

        });
    };

    $.fn.slickPrev = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.changeSlide({
                data: {
                    message: 'previous'
                }
            });

        });
    };

    $.fn.slickRemove = function(slideIndex, removeBefore) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.removeSlide(slideIndex, removeBefore);

        });
    };

    $.fn.slickSetOption = function(option, value, refresh) {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.options[option] = value;

            if (refresh === true) {
                element.slick.unload();
                element.slick.reinit();
            }

        });
    };

    $.fn.slickUnfilter = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.unfilterSlides();

        });
    };

    $.fn.unslick = function() {
        var _ = this;
        return _.each(function(index, element) {

            element.slick.destroy();

        });
    };

}));



      $('.responsive').slick({
        // To use lazy loading, set a data-lazy attribute
        // on your img tags and leave off the src
        // <img data-lazy="img/lazyfonz1.png"/>
        lazyLoad: 'ondemand',
        centerMode: false,
        centerPadding: '60px',
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              arrows: true,
              centerMode: false,
              centerPadding: '40px',
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              arrows: true,
              centerMode: false,
              centerPadding: '40px',
              slidesToShow: 2,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: true,
              centerMode: false,
              centerPadding: '40px',
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });

</script>

                    @endsection
