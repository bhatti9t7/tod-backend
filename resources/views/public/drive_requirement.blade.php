@extends('layout.public.publicheader')
@section('content')
<div class="_style_4aX1Qk" data-reactid="260"><div class="gutter" data-reactid="261"><div class="dotcom-body app-body" data-reactid="262"><div><div class="position--relative pull-app-gutter--sides clearfix"><div><div class="wcb-billboard-side-photo layout layout--flush _style_494Obg"><div class="layout__item _style_37zZFb"><div class="_style_4EfqLw"><div class="_style_1anY0i" role="img" title="Billboard Hero Image" aria-label="Billboard Hero Image"></div></div><div class="_style_15Nhrc"><a href="#" class="wcb-cta-bit _style_3MCQ9c" target="_self"><div class="_style_TvBQt">Start driving with Loader</div><div class="_style_35FPJo"><!-- react-text: 2926 -->Sign up<!-- /react-text --><span class="_style_LquIg _style_w3DaW"><svg viewBox="0 0 64 64" width="20px" height="20px" class=" _style_4wJp4e"><path fill-rule="evenodd" clip-rule="evenodd" d="M59.927 31.985l.073.076-16.233 17.072-3.247-3.593L51.324 34H4v-4h47.394L40.52 18.407l3.247-3.494L60 31.946l-.073.039z"></path></svg></span></div></a></div></div><div class="layout__item _style_h0Pzx"><div class="_style_2NjwKt"><div class="_style_27rK6s"><div class="_style_HHcgA"><h1 id="maincontent" class="_style_12KMB8">Driver requirements</h1><p class="_style_3hjxI4">How to drive with Loader</p></div><p class="_style_3aVw12" style="margin-bottom: 16px;">Make money driving people around your city. Sign up now to get more information about driving in your city.</p></div></div></div><div class="layout__item _style_4gXHCQ"></div></div></div><!-- react-empty: 2938 --></div><div class="position--relative pull-app-gutter--sides clearfix"><div><div class="_style_3ye16P wcb-vp-illustrated"><div class="_style_eeI8o"><div class="_style_4jfdOQ"><p class="_style_bsxwn">Hit the road, start earning!</p><h2 class="_style_3zaJwR">Get Started</h2></div><div class="layout _style_4ykyzI"><div class="layout__item _style_UttdR"><div class="_style_VxPAE"><div class="_style_UqRVV"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label=" " class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title> </title><desc> </desc>
    <path d="M0 81.43h88v2.06A4.51 4.51 0 0 1 83.49 88h-79A4.51 4.51 0 0 1 0 83.49v-2.06z" style="fill: rgb(241, 102, 34)"></path>
    <path d="M12.11 33.66h65.08a2.6 2.6 0 0 1 2.6 2.6v45.17H9.51V36.27a2.6 2.6 0 0 1 2.6-2.61z" fill="#f16622"></path>
    <path fill="#fff" d="M75.58 37.88v38.33H13.73V37.88z"></path>
    <path d="M48.54 81.43H36.46a2.5 2.5 0 0 0 2.5 2.5H49a2.5 2.5 0 0 0 2.5-2.5z"></path>
    <path d="M17.97 76.21h57.61V37.88H56.3L17.97 76.21z" style="fill: #fff"></path>
    <path d="M75.58 76.21V37.88L37.25 76.21h38.33z" style="fill:#fff"></path>
</svg>
</div></div></div></div></div><div class="_style_MPrPp"><h4 class="_style_4okShP">1. Signup online</h4><div class="_style_1szug"><div class="cmln__markdown"><p class="cmln__paragraph">Got an email address and a cell phone? Great, now give us a bit more information and start earning immediately!</p>
</div><div class="_style_2VA39t"><a target="_blank" class="wcb-button _style_1JtngI" href="#"><!-- react-text: 2960 -->SIGN UP NOW<!-- /react-text --><span class="_style_1gVw1O"><svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e"><path d="M23.161 53.213l-4.242-4.242a1 1 0 0 1 0-1.415L34.475 32 18.919 16.444a1 1 0 0 1 0-1.415l4.242-4.242a1 1 0 0 1 1.414 0L40.84 27.05l4.242 4.243a1 1 0 0 1 0 1.414L40.84 36.95 24.575 53.213a1 1 0 0 1-1.414 0z"></path></svg></span></a></div></div></div></div></div><div class="layout__item _style_UttdR"><div class="_style_VxPAE"><div class="_style_UqRVV"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="Decorative Illustration" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <defs>
        <pattern id="a___1118153263" data-name="2/2 - black" width="4" height="4" patternUnits="userSpaceOnUse" viewBox="0 0 4 4">
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path d="M0 0h2v2H0z"></path>
        </pattern>
    </defs>
    <path d="M5.66 81.67V44A6.33 6.33 0 0 1 12 37.65H6.33A6.33 6.33 0 0 0 0 44v37.67A6.33 6.33 0 0 0 6.33 88H12a6.33 6.33 0 0 1-6.34-6.33z"></path>
    <rect x="5.66" y="37.65" width="82.34" height="50.35" rx="6.33" ry="6.33" fill="#f16622"></rect>
    <path d="M52.67 53.97h26.35v2H52.67zm0-6.44h26.35v2H52.67zm0 12.95h26.35v2H52.67z"></path>
    <path d="M14.53 78.1V47.55h30.55V78.1z" style="fill:#000"></path>
    <path d="M45.08 47.55L14.52 78.1V47.55h30.56" style="fill: #fff"></path>
    <circle cx="28.6" cy="61.2" r="8.19" fill="#f8f8f9"></circle>
    <path d="M32.1 78.1h-5.88v-3a8.91 8.91 0 0 1 2.61-6.3l.69-.69a8.91 8.91 0 0 0 2.61-6.3h1.64V78.1H32.1z"></path>
    <path fill="#f8f8f9" d="M20.41 65.44h10.85V78.1H20.41z"></path>
    <path d="M28.6 72.67h6.67a1.53 1.53 0 0 0 1.53-1.53v-10h-8.2v11.53z" fill="#f8f8f9"></path>
    <path d="M36.79 62.9v3.26h.82a.38.38 0 0 0 .27-.65 3.7 3.7 0 0 1-1.09-2.61z" fill="#f8f8f9"></path>
    <path d="M28.6 53a8 8 0 0 0-8.19 8v12l2.83-2.83a3.58 3.58 0 0 0 1.05-2.53v-4.15a1.27 1.27 0 0 1 1.27-1.27 1.27 1.27 0 0 1 1.27 1.27v.19h.73a2.22 2.22 0 0 0 2.22-2.22v-1a1.61 1.61 0 0 1 1.61-1.61h5.05A8.2 8.2 0 0 0 28.6 53z"></path>
    <path d="M28.78 55.76h6.59a3.73 3.73 0 0 1 3.73 3.73v1.26H28.78v-5 .01zM20.41 78.1h-5.89v-3a8.91 8.91 0 0 1 2.61-6.3l.69-.69a8.91 8.91 0 0 0 2.61-6.3h3.86v12.41a3.88 3.88 0 0 1-3.88 3.88z"></path>
    <path fill="url(#a___1118153263)" d="M52.67 68.06h26.35V78.1H52.67z"></path>
</svg>
</div></div></div></div></div><div class="_style_MPrPp"><h4 class="_style_4okShP">2. Upload your documents</h4><div class="_style_1szug"><div class="cmln__markdown"><p class="cmln__paragraph">Time for some paperwork. Here are the documents we need to see:</p>
<ul class="cmln__list"><li class="cmln__list-item">A valid National ID Card (Can be obtained from your nearest <a class="cmln__link" href="#">NADRA office</a>. You need one of parent's original NIC and your Child Registration Certificate OR your old original ID card)</li>
<li class="cmln__list-item">Valid Driving License (This can be obtained from any traffic licensing office, and you will need your learner's license, National ID card, fee, 2 passport size photos and doctor's certificate to obtain a new one)</li>
<li class="cmln__list-item">Vehicle Registration Document (To obtain a new one, you need CNIC, proof of purchase, registration number fee, proof of residence, application form and taxes. Can be obtained from authorized dealers or excise office)</li>
<li class="cmln__list-item">A driver profile photo<ul class="cmln__list"><li class="cmln__list-item">Must be a forward-facing, centered photo including the driver’s full face and top of shoulders, with no sunglasses</li>
<li class="cmln__list-item">Must be a photo only of the driver with no other subject in the frame, well-lit, and in focus. It cannot be a driver’s license photo or other printed photograph</li>
</ul>
</li>
</ul>
</div></div></div></div></div><div class="layout__item _style_UttdR"><div class="_style_VxPAE"><div class="_style_UqRVV"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label=" " class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;">
   <img src="{{asset('assets/public/images/delivery.png')}}">
</div></div></div></div></div><div class="_style_MPrPp"><h4 class="_style_4okShP">3. Get a vehicle</h4><div class="_style_1szug"><div class="cmln__markdown"><p class="cmln__paragraph">People like to ride in nice cars. Let’s give them what they want. Make sure that your vehicle is in excellent working and physical condition.</p>
</div><div class="_style_2VA39t"><a target="_blank" class="wcb-button _style_1JtngI" href="#"><!-- react-text: 2988 -->VEHICLE REQUIREMENTS<!-- /react-text --><span class="_style_1gVw1O"><svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e"><path d="M23.161 53.213l-4.242-4.242a1 1 0 0 1 0-1.415L34.475 32 18.919 16.444a1 1 0 0 1 0-1.415l4.242-4.242a1 1 0 0 1 1.414 0L40.84 27.05l4.242 4.243a1 1 0 0 1 0 1.414L40.84 36.95 24.575 53.213a1 1 0 0 1-1.414 0z"></path></svg></span></a></div></div></div></div></div><div class="layout__item _style_UttdR"><div class="_style_VxPAE"><div class="_style_UqRVV"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="Driver hands on steering wheel" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Driver hands on steering wheel</title><desc>Driver hands on steering wheel</desc>
    <defs>
        <clipPath id="a___649188101">
            <path d="M25.76 24.25v2.65l-.86 1a.81.81 0 0 1-1.44-.51V16.72A1.73 1.73 0 0 0 21.74 15h-.35a1.72 1.72 0 0 0-1.64 1.22h-2.3a1.66 1.66 0 0 0-1.6 1.22h-2a2.3 2.3 0 0 0-2.3 2.3v-.07h-1.17a2.26 2.26 0 0 0-2.26 2.26v7.28a18.25 18.25 0 0 0 1.62 7.52L11 39.45a5.75 5.75 0 0 1 .34 3.78l-.67 2.38h12.49v-2.24a3.89 3.89 0 0 1 1.38-3l1.86-1.57a7 7 0 0 0 2.41-4.23l.78-4.77v-7.91a31.38 31.38 0 0 0-3.83 2.36z" fill="none"></path>
        </clipPath>
        <clipPath id="b___649188101">
            <path d="M62.24 24.25v2.65l.86 1a.81.81 0 0 0 1.44-.51V16.72A1.73 1.73 0 0 1 66.26 15h.35a1.72 1.72 0 0 1 1.64 1.22h2.3a1.66 1.66 0 0 1 1.6 1.22h2a2.3 2.3 0 0 1 2.3 2.3v-.07h1.19a2.26 2.26 0 0 1 2.26 2.26v7.28a18.25 18.25 0 0 1-1.62 7.52L77 39.45a5.75 5.75 0 0 0-.34 3.78l.67 2.38H64.84v-2.24a3.89 3.89 0 0 0-1.38-3l-1.86-1.54a7 7 0 0 1-2.41-4.23l-.78-4.77v-7.94a31.38 31.38 0 0 1 3.83 2.36z" fill="none"></path>
        </clipPath>
    </defs>
    <path d="M44 18.37a31.33 31.33 0 1 1-31.33 31.34A31.37 31.37 0 0 1 44 18.37m0-7a38.29 38.29 0 1 0 38.3 38.34A38.29 38.29 0 0 0 44 11.41z"></path>
    <path d="M37.51 80.47q-1-.21-2-.49a31.17 31.17 0 0 0 17 0q-1 .28-2 .49a2.36 2.36 0 0 1-2.85-2.3v-6.1a15.65 15.65 0 0 1 3.84-10.31c2.88-3.31 5-5.4 5-5.4h15.04a2.35 2.35 0 0 1 2.24 3.07 31.2 31.2 0 0 0 1.55-9.72c0-.91 0-1.81-.12-2.7a1.74 1.74 0 0 1-2.36 1.73l-8.93-3.46H24.09l-8.93 3.46A1.74 1.74 0 0 1 12.79 47c-.08.89-.12 1.79-.12 2.7a31.2 31.2 0 0 0 1.55 9.72 2.35 2.35 0 0 1 2.24-3.07h15.07s2.1 2.09 5 5.4a15.65 15.65 0 0 1 3.86 10.31v6.1a2.36 2.36 0 0 1-2.88 2.31z" style="fill: rgb(242, 102, 29)"></path>
    <path d="M28.3 43.46c1.19-.51 2.41-1 3.63-1.35a39.67 39.67 0 0 1 24.14 0c1.23.39 2.44.84 3.63 1.35l4.21 1.81-5.71 15.81a6.35 6.35 0 0 1-6 4.19H35.48a6.35 6.35 0 0 1-6-4.29l-5.38-15.7z" style="fill: rgb(242, 102, 29)"></path>
    <circle cx="44" cy="52.32" r="5.22" fill="#fff"></circle>
    <path d="M25.76 24.25v2.65l-.86 1a.81.81 0 0 1-1.44-.51V16.72A1.73 1.73 0 0 0 21.74 15h-.35a1.72 1.72 0 0 0-1.64 1.22h-2.3a1.66 1.66 0 0 0-1.6 1.22h-2a2.3 2.3 0 0 0-2.3 2.3v-.07h-1.17a2.26 2.26 0 0 0-2.26 2.26v7.28a18.25 18.25 0 0 0 1.62 7.52L11 39.45a5.75 5.75 0 0 1 .34 3.78l-.67 2.38h12.49v-2.24a3.89 3.89 0 0 1 1.38-3l1.86-1.57a7 7 0 0 0 2.41-4.23l.78-4.77v-7.91a31.38 31.38 0 0 0-3.83 2.36z" fill="#d6d6d5"></path>
    <path fill="#fff" d="M23.2 48.37H10.01l.62-2.77h12.53l.04 2.77z"></path>
    <path d="M24.74 88H0l10-39.63h13.19l.87 2.46a11.84 11.84 0 0 1 .68 4z" style="fill: rgb(242, 102, 29)"></path>
    <g clip-path="url(#a___649188101)" fill="#a5a5a5">
        <path d="M19.56 22.6a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm-3.88 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm-4.1 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 0 1 1 0v7.73a.5.5 0 0 1-.5.5z"></path>
    </g>
    <path d="M62.24 24.25v2.65l.86 1a.81.81 0 0 0 1.44-.51V16.72A1.73 1.73 0 0 1 66.26 15h.35a1.72 1.72 0 0 1 1.64 1.22h2.3a1.66 1.66 0 0 1 1.6 1.22h2a2.3 2.3 0 0 1 2.3 2.3v-.07h1.19a2.26 2.26 0 0 1 2.26 2.26v7.28a18.25 18.25 0 0 1-1.62 7.52L77 39.45a5.75 5.75 0 0 0-.34 3.78l.67 2.38H64.84v-2.24a3.89 3.89 0 0 0-1.38-3l-1.86-1.54a7 7 0 0 1-2.41-4.23l-.78-4.77v-7.94a31.38 31.38 0 0 1 3.83 2.36z" fill="#d6d6d5"></path>
    <path fill="#fff" d="M64.8 48.37h13.19l-.62-2.77H64.84l-.04 2.77z"></path>
    <path d="M63.26 88H88L78 48.37H64.81l-.87 2.46a11.84 11.84 0 0 0-.68 4z" style="fill: rgb(242, 102, 29)"></path>
    <g clip-path="url(#b___649188101)" fill="#a5a5a5">
        <path d="M68.44 22.6a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm3.88 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5zm4.1 0a.5.5 0 0 1-.5-.5v-7.73a.5.5 0 1 1 1 0v7.73a.5.5 0 0 1-.5.5z"></path>
    </g>
</svg>
</div></div></div></div></div><div class="_style_MPrPp"><h4 class="_style_4okShP">4. Activate your account</h4><div class="_style_1szug"><div class="cmln__markdown"><p class="cmln__paragraph">If you’ve completed all the above steps, congratulations! Activate your account and hit the road.</p>
</div></div></div></div></div></div></div></div><div class="_style_2Pec26 wcb-promo-side-photo"><div class="_style_3rYMqG" role="img" title="Promotional Photo" aria-label="Promotional Photo"></div><div class="layout"><div class="layout__item _style_1WlFBc"></div><div class="layout__item _style_3jpoOU"><div class="_style_6uuZb"><div class="_style_eeI8o"><div class="_style_VxPAE"><p class="_style_bsxwn">More information</p><h2 class="_style_3zaJwR">Lahore vehicle requirements</h2></div><div class="cmln__markdown"><p class="cmln__paragraph">In addition to the minimum requirements above, Lahore has its own regulations for vehicles.</p>
</div><a class="wcb-button _style_1JtngI" href="#"><!-- react-text: 3015 -->Get the details<!-- /react-text --><span class="_style_1gVw1O"><svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e"><path d="M23.161 53.213l-4.242-4.242a1 1 0 0 1 0-1.415L34.475 32 18.919 16.444a1 1 0 0 1 0-1.415l4.242-4.242a1 1 0 0 1 1.414 0L40.84 27.05l4.242 4.243a1 1 0 0 1 0 1.414L40.84 36.95 24.575 53.213a1 1 0 0 1-1.414 0z"></path></svg></span></a></div></div></div></div></div><div class="_style_34KYWo wcb-vp-2side"><div class="_style_eeI8o"><div class="_style_2zcWJ1"><p class="_style_bsxwn">What else do you need?</p><h2 class="_style_3zaJwR">Get more from Loader</h2></div><div class="layout layout--flush"><div class="layout__item _style_3hkARR"><div class="_style_1j0XZn"><div class="_style_VxPAE"><div class="_style_DRfwS"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label=" " class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title> </title><desc> </desc>
    <path d="M12.3 13.64L7 19.32 9.88 21a4.39 4.39 0 0 0 5.58-1 28.47 28.47 0 0 1 44.23 0 4.39 4.39 0 0 0 5.58 1l2.86-1.65-5.32-5.68a34.63 34.63 0 0 0-50.51-.03z" fill="#f2661d"></path>
    <path d="M37.58.54A37.59 37.59 0 0 0 0 38.16v9.69h3.6v-9.69a33.93 33.93 0 1 1 67.86 0v9.11L67 58.45l8.13-7.82V38.16A37.59 37.59 0 0 0 37.58.54z" style="fill: rgb(242, 102, 29)"></path>
    <circle cx="75.13" cy="55.94" r="2.77"></circle>
    <path d="M54.73 84.28H37.55v-2h17.18A20.3 20.3 0 0 0 75 62v-6.06h2V62a22.3 22.3 0 0 1-22.27 22.28z" fill="#f2651c"></path>
    <path d="M34.16 88.54H41a5.26 5.26 0 0 0 5.26-5.26H28.9a5.26 5.26 0 0 0 5.26 5.26z"></path>
    <path d="M41 78h-6.84a5.26 5.26 0 0 0-5.26 5.26h17.35A5.26 5.26 0 0 0 41 78z" style="fill: rgb(242, 102, 29)"></path>
    <path d="M62.45 70.11h3.15V41.76h-3.17a3.34 3.34 0 0 0-3.33 3.34v21.67a3.34 3.34 0 0 0 3.35 3.34z"></path>
    <path d="M71.5 47.27A5.19 5.19 0 0 0 70.36 44l-1.82-2.26h-3v14.2h5.91z" style="fill: rgb(242, 102, 29)"></path>
    <path d="M65.59 55.94v14.17h3l1.81-2.26a5.19 5.19 0 0 0 1.14-3.25v-8.66z" style="fill:rgb(242, 102, 29)"></path>
    <path d="M71.51 47v8.93h3.63v-5.3A3.62 3.62 0 0 0 71.51 47z" fill="#d6d6d5"></path>
    <path d="M71.51 55.94v8.93a3.62 3.62 0 0 0 3.62-3.62v-5.3z" style="fill: rgb(242, 102, 29)"></path>
    <path d="M1.61 52.32h4.06v3.74H1.61z" style="fill:rgb(242, 102, 29);"></path>
    <path d="M1.61 56.06h4.06v3.49H1.61z"></path>
    <rect x="5.68" y="49.69" width="2.7" height="12.49" rx="1.35" ry="1.35"></rect>
    <path fill="#d6d6d5" d="M.02 47.85h3.63v8.2H.02z"></path>
    <path d="M3.64 56.05v6.15A1.81 1.81 0 0 1 1.83 64 1.81 1.81 0 0 1 0 62.2v-6.14z" style="fill:rgb(242, 102, 29);"></path>
    <path fill="#e5e5e4" d="M64.59 41.76h2v14.18h-2z"></path>
    <path d="M64.6 55.94h2v14.18h-2z" style="fill: rgb(248, 248, 249)"></path>
</svg>
</div></div></div></div></div><div class="_style_3X5Sdg"><h4 class="_style_4okShP">Get support</h4><div class="_style_3CqS62"><div class="cmln__markdown"><p class="cmln__paragraph">Let’s make every Loader trip hassle-free. We can help you with account setup, fare adjustments, and more — whatever you need.</p>
</div><div class="_style_2VA39t"><a target="_blank" class="wcb-button _style_1JtngI" href="#"><!-- react-text: 3039 -->GET HELP<!-- /react-text --><span class="_style_1gVw1O"><svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e"><path d="M23.161 53.213l-4.242-4.242a1 1 0 0 1 0-1.415L34.475 32 18.919 16.444a1 1 0 0 1 0-1.415l4.242-4.242a1 1 0 0 1 1.414 0L40.84 27.05l4.242 4.243a1 1 0 0 1 0 1.414L40.84 36.95 24.575 53.213a1 1 0 0 1-1.414 0z"></path></svg></span></a></div></div></div></div></div></div><div class="layout__item _style_4vDxhh"><div class="_style_4afMpk"><div class="_style_VxPAE"><div class="_style_DRfwS"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label=" " class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title> </title><desc> </desc>
    <path fill="#fff" d="M27.52 40.97h40.32V88H27.52z"></path>
    <path d="M67.84 40.96H88V88H67.84z" style="fill: rgb(242, 102, 29)"></path>
    <path d="M47.68 27.52L27.52 40.97h40.32L47.68 27.52z" style="fill: rgb(242, 102, 29)"></path>
    <path d="M67.84 27.52L88 40.97H67.84L47.68 27.52h20.16zM49.35 71.19h2v16.77h-2z" fill="#f2661d"></path>
    <path d="M54 69.73v-4.79a3.77 3.77 0 0 0-3.26-3.82 3.63 3.63 0 0 0-4 3.61v5.08a7.3 7.3 0 0 1-1.38 4.09 6 6 0 0 0-.94 4.43 6 6 0 0 0 12-1.07 5.94 5.94 0 0 0-1-3.32A7.58 7.58 0 0 1 54 69.73z" style="fill: rgb(242, 102, 29);"></path>
    <path fill="#f1f1f1" d="M59.14 47.68h3.97v6.72h-3.97zm-17.92 0h3.97v6.72h-3.97zm17.92 13.44h3.97v6.72h-3.97zm-8.96-13.44h3.97v6.72h-3.97zm-17.92 0h3.97v6.72h-3.97z"></path>
    <path fill="#fff" d="M0 67.41h20.64V88H0z"></path>
    <path d="M41.28 88H20.64V67.41h20.64z" style="fill: rgb(242, 102, 29);"></path>
    <path d="M10.32 57.09L0 67.41h20.64L10.32 57.09z" style="fill: rgb(242, 102, 29);"></path>
    <path d="M12.49 88H8.15v-8.12a2.17 2.17 0 0 1 2.17-2.17 2.17 2.17 0 0 1 2.17 2.17zm28.79-20.59L30.96 57.09H10.32l10.32 10.32h20.64z" fill="#f2661d"></path>
    <path d="M14.05 11.11l1.06 1.06a5 5 0 0 0 3.5 1.45h3.1a5 5 0 0 1 3.5 1.45l.63.63a5 5 0 0 0 3.5 1.45h5.12a1.72 1.72 0 0 1 1.21.5 1.72 1.72 0 0 1-1.21 2.93h-33A1.44 1.44 0 0 1 0 19.14v-2a8.31 8.31 0 0 1 8.31-8.33h.2a7.84 7.84 0 0 1 5.54 2.3z" fill="#f1f1f1"></path>
    <path d="M56.4 77.25a5.94 5.94 0 0 0-1-3.32 7.58 7.58 0 0 1-1.4-4.2v-4.79a3.77 3.77 0 0 0-3.26-3.82 3.62 3.62 0 0 0-.37 0V83.3a6 6 0 0 0 6.03-6.05z" style="fill: rgb(242, 102, 29)"></path>
</svg>
</div></div></div></div></div><div class="_style_3X5Sdg"><h4 class="_style_4okShP">Contact Loader</h4><div class="_style_3CqS62"><div class="cmln__markdown"><p class="cmln__paragraph">Have questions? Get in-person support at an Loader Greenlight Hub in the city you drive in. We’re here for whatever you need.</p>
</div><div class="_style_2VA39t"><a target="_blank" class="wcb-button _style_1JtngI" href="#"><!-- react-text: 3057 -->CONTACT US<!-- /react-text --><span class="_style_1gVw1O"><svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e"><path d="M23.161 53.213l-4.242-4.242a1 1 0 0 1 0-1.415L34.475 32 18.919 16.444a1 1 0 0 1 0-1.415l4.242-4.242a1 1 0 0 1 1.414 0L40.84 27.05l4.242 4.243a1 1 0 0 1 0 1.414L40.84 36.95 24.575 53.213a1 1 0 0 1-1.414 0z"></path></svg></span></a></div></div></div></div></div></div></div></div></div><div class="_style_4siNiz"><div class="_style_3POd9l"><div class="_style_syRoi"><h2 class="_style_R83rO">Ready to make money?</h2><h3 class="_style_3jexwW">The first step is to sign up</h3></div><a class="wcb-button _style_1m6gck" href="#"><!-- react-text: 3067 -->Sign up to drive<!-- /react-text --><span class="_style_28zWyA"><svg viewBox="0 0 64 64" width="20px" height="20px" class=" _style_4wJp4e"><path fill-rule="evenodd" clip-rule="evenodd" d="M59.927 31.985l.073.076-16.233 17.072-3.247-3.593L51.324 34H4v-4h47.394L40.52 18.407l3.247-3.494L60 31.946l-.073.039z"></path></svg></span></a></div></div><div class="_style_3ye16P wcb-legal"><div class="_style_1fRwDe"><div class="_style_1cp0WB"><div class="cmln__markdown"><p class="cmln__paragraph cmln__paragraph--legal">The information provided on this web page is intended for informational purposes only and may not be applicable in your country, region, or city. It is subject to change and may be updated without notice.</p>
</div></div></div></div></div></div></div></div></div></div>
@endsection