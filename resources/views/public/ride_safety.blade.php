@extends('layout.public.publicheader')
@section('content')
<div class="_style_4aX1Qk" data-reactid="260"><div class="gutter" data-reactid="261"><div class="dotcom-body app-body" data-reactid="262"><div data-reactid="263"><div class="position--relative pull-app-gutter--sides clearfix"><div><div class="wcb-billboard-side-photo layout layout--flush _style_494Obg"><div class="layout__item _style_37zZFb"><div class="_style_4EfqLw"><div class="_style_2fV8Ce" role="img" title="Photo of an Loader driver-partner opening the door for a couple taking an Loader to the airport.
" aria-label="Photo of an Loader driver-partner opening the door for a couple taking an Loader to the airport.
"></div></div><div class="_style_15Nhrc"><a href="#" class="wcb-cta-bit _style_3MCQ9c" target="_self"><div class="_style_TvBQt">Start riding with Loader</div><div class="_style_35FPJo"><!-- react-text: 4223 -->Sign up<!-- /react-text --><span class="_style_LquIg _style_w3DaW"><svg viewBox="0 0 64 64" width="20px" height="20px" class=" _style_4wJp4e"><path fill-rule="evenodd" clip-rule="evenodd" d="M59.927 31.985l.073.076-16.233 17.072-3.247-3.593L51.324 34H4v-4h47.394L40.52 18.407l3.247-3.494L60 31.946l-.073.039z"></path></svg></span></div></a></div></div><div class="layout__item _style_h0Pzx"><div class="_style_2NjwKt"><div class="_style_27rK6s"><div class="_style_HHcgA"><h1 id="maincontent" class="_style_12KMB8">Trip safety</h1><p class="_style_3hjxI4">Our commitment to riders</p></div><p class="_style_3aVw12" style="margin-bottom: 16px;">Loader is dedicated to keeping people safe on the road. Our technology enables us to focus on rider safety before, during, and after every trip. Here's how. </p></div></div></div><div class="layout__item _style_4gXHCQ"></div></div><div class="_style_3ye16P wcb-vp-single"><div class="_style_27rK6s"><div class="_style_VxPAE"><div class="_style_3GV25U"><div class="_style_1iLMt8"><div aria-label="Decorative Illustration" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <defs>
        <clipPath id="a___1569785765">
            <circle cx="44.04" cy="44" r="43.65" fill="none"></circle>
        </clipPath>
        <pattern id="b___1569785765" data-name="2x2 - Black" width="4" height="4" patternTransform="matrix(.8 0 0 .8 5.7 4.5)" patternUnits="userSpaceOnUse" viewBox="0 0 4 4">
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path d="M0 0h2v2H0z"></path>
        </pattern>
    </defs>
    <path d="M17.07 74.68h10.25v8.95H17.07z" style="fill:#fcb666;"></path>
    <g clip-path="url(#a___1569785765)">
        <path fill="#d6d6d5" d="M90.75 90.31H-1.6v-93.5h92.35z"></path>
        <path d="M71.01 87.65H17.08V61.3a14.8 14.8 0 0 1 14.8-14.8h24.33a14.8 14.8 0 0 1 14.8 14.8v26.35z" style="fill:rgb(241, 102, 34)"></path>
        <circle cx="40.02" cy="22.61" r="11.58" fill="#f8f8f9"></circle>
        <path d="M35.06 46.5h8.32v-4.3a12.59 12.59 0 0 0-3.69-8.9l-1-1A12.59 12.59 0 0 1 35 23.42h-2.29V46.5h2.35z"></path>
        <path fill="#f8f8f9" d="M51.6 46.5H36.26V28.61H51.6z"></path>
        <path fill="#f16622" d="M17.07 63.76h53.94v23.89H17.07z"></path>
        <path d="M40 38.83h-9.41a2.16 2.16 0 0 1-2.16-2.16V22.61H40v16.22z" fill="#f8f8f9"></path>
        <path d="M28.44 25v4.61h-1.16a.54.54 0 0 1-.38-.92A5.23 5.23 0 0 0 28.44 25z" fill="#f8f8f9"></path>
        <path d="M40 11a11.24 11.24 0 0 1 11.6 11.29v17.06l-4-4a5.07 5.07 0 0 1-1.48-3.58v-5.93a1.79 1.79 0 0 0-1.8-1.84 1.79 1.79 0 0 0-1.79 1.79v.27h-1A3.14 3.14 0 0 1 38.36 23v-1.5a2.28 2.28 0 0 0-2.28-2.28h-7.14A11.59 11.59 0 0 1 40 11z"></path>
        <path d="M39.77 21.97H25.18v-.48a6.57 6.57 0 0 1 6.56-6.6h8v7.08z"></path>
        <path d="M36.27 46.5h15.34v41.03H36.27z" style="fill: rgb(137, 218, 193);"></path>
        <path d="M43.97 54.13h-.1a7.63 7.63 0 0 1-7.63-7.63H51.6a7.63 7.63 0 0 1-7.63 7.63z" fill="#f8f8f9"></path>
        <path d="M51.6 46.5h8.32v-4.3a12.59 12.59 0 0 0-3.69-8.9l-1-1a12.59 12.59 0 0 1-3.69-8.9h-5.42V41a5.49 5.49 0 0 0 5.48 5.5z"></path>
    </g>
    <ellipse cx="67.07" cy="10.13" rx="10.32" ry="9.78" fill="#fff"></ellipse>
    <path d="M57 12.35l7.84 23.31a2.32 2.32 0 0 0 4.41 0l7.84-23.31H57z" fill="#fff"></path>
    <ellipse cx="67.07" cy="10.13" rx="4.75" ry="4.5" style="fill: rgb(137, 218, 193);"></ellipse>
    <path fill="#f8f8f9" d="M7.16 54.98h6.76v3.27H7.16z"></path>
    <path d="M5.77 58.25h11.3v25.38h-3.3a8 8 0 0 1-8-8V58.25z" style="fill: rgb(241, 102, 34);"></path>
    <rect x="8.84" y="38.38" width="8.23" height="17.2" rx="1.18" ry="1.18" style="fill: rgb(137, 218, 193);"></rect>
    <rect x="7.66" y="38.38" width="8.23" height="17.2" rx="1.18" ry="1.18"></rect>
    <path d="M13.92 52.59v-4.25H12a1.93 1.93 0 0 0 1.93-1.93H6.54a2.21 2.21 0 0 0-2.21 2.21v2.47a3.09 3.09 0 0 0 .9 2.18l1.87 1.88a4 4 0 0 0 2.83 1.17h.25a3.74 3.74 0 0 0 3.74-3.73z" fill="#f8f8f9"></path>
</svg>
</div></div></div></div></div><div class="_style_vIEMw"><div class="_style_VxPAE"><p class="_style_bsxwn">Before the trip</p><h2 class="_style_3zaJwR">Getting a safe ride</h2></div><div class="_style_4iUkli"><div class="cmln__markdown"><p class="cmln__paragraph"><strong class="cmln__strong">Safe pickups</strong><br>The Loader app automatically finds your location to provide door-to-door service. That means you stay safe and comfortable wherever you are until your driver arrives.</p>
<p class="cmln__paragraph"><strong class="cmln__strong">Open to everyone, everywhere</strong><br>All ride requests are blindly matched with the closest available driver. So there is no discrimination based on race, gender, or destination.</p>
<p class="cmln__paragraph"><strong class="cmln__strong">Driver profiles</strong><br>When you’re matched with a driver, you’ll see their name, license plate number, photo, and rating—so you know who’s picking you up ahead of time. And even after the trip, you’re able to contact your driver if you left something behind.</p>
</div></div></div></div></div><div class="_style_23dQ2a wcb-promo-side-photo"><div class="_style_4oUsqG" role="img" title="Two men looking at the Loader app on their phones" aria-label="Two men looking at the Loader app on their phones"></div><div class="layout"><div class="layout__item _style_1WlFBc"></div><div class="layout__item _style_3jpoOU"><div class="_style_6uuZb"><div class="_style_eeI8o"><div class="_style_VxPAE"><p class="_style_bsxwn">On trip</p><h2 class="_style_3zaJwR">Getting to your destination</h2></div><div class="cmln__markdown"><p class="cmln__paragraph"><strong class="cmln__strong">Share your ETA</strong><br>Once your driver has picked you up, share your ETA with your friends and family so they can follow your route and know when to expect you.</p>
<p class="cmln__paragraph"><strong class="cmln__strong">Always on the map</strong><br>Follow your trip in real-time so you always know where you are. And if you use LoaderPOOL, you’ll know exactly who’s riding with you.</p>
</div></div></div></div></div></div><div class="_style_3ye16P wcb-vp-single"><div class="_style_27rK6s"><div class="_style_VxPAE"><div class="_style_3GV25U"><div class="_style_1iLMt8"><div aria-label="Illustration Title" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 126 121" height="121" width="126" style="display: block; height: 100%; width: 100%;"><title>Illustration Title</title><desc>Illustration Description</desc>
    <path fill="#D6D6D6" d="M4 58h117.2v2H4z"></path>
    <path d="M78 60H62.9c-4 0-7.9 1.6-10.7 4.4L50 66.6c-2.8 2.8-6.7 4.4-10.7 4.4-8.4 0-15.2 6.8-15.2 15.2V118h54V60z" style="fill: rgb(241, 102, 34)"></path>
    <path fill="#f16622" d="M54 51h33v70H54z"></path>
    <path fill="#D6D6D6" d="M76 107v-4l-18 18h4m25-70h24v70H87zm-33 2h33v2H54zM4 21h118v2H4z"></path>
    <path fill="#D6D6D6" d="M38 5h2v55h-2z"></path>
    <circle fill="#D6D6D6" cx="39" cy="59" r="4"></circle>
    <circle fill="#D6D6D6" cx="4" cy="59" r="4"></circle>
    <circle fill="#D6D6D6" cx="122" cy="22" r="4"></circle>
    <circle fill="#D6D6D6" cx="4" cy="22" r="4"></circle>
    <circle fill="#D6D6D6" cx="39" cy="4" r="4"></circle>
    <circle fill="#D6D6D6" cx="122" cy="75" r="4"></circle>
    <circle fill="#D6D6D6" cx="122" cy="59" r="4"></circle>
    <circle cx="74" cy="19" r="11" fill="#f16622"></circle>
    <path d="M63.5 22l7.7 22.8c.9 2.7 4.8 2.7 5.7 0L84.5 22h-21z" fill="#f16622"></path>
    <circle fill="#D6D6D6" cx="74" cy="19" r="5"></circle>
    <path d="M90 52h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-16 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-16 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-12 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-12 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-12 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-12 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-12 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-12 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm-16 4h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h2v2h-2zm4 0h1v2h-1zm-20 4h2v1h-2zm-3-68h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v2h-1zm0 4h1v1h-1zm7 0h2v1h-2zm4 0h2v1h-2zm4 0h2v1h-2zm4 0h2v1h-2zm4 0h1v1h-1zm0-68h1v2h-1z"></path>
    <path fill="#D6D6D6" d="M61.5 62c.8 0 1.5.7 1.5 1.5V75h-3V63.5c0-.8.7-1.5 1.5-1.5m0-2c-1.9 0-3.5 1.6-3.5 3.5V77h7V63.5c0-1.9-1.6-3.5-3.5-3.5zm9 2c.8 0 1.5.7 1.5 1.5V75h-3V63.5c0-.8.7-1.5 1.5-1.5m0-2c-1.9 0-3.5 1.6-3.5 3.5V77h7V63.5c0-1.9-1.6-3.5-3.5-3.5zm9 2c.8 0 1.5.7 1.5 1.5V75h-3V63.5c0-.8.7-1.5 1.5-1.5m0-2c-1.9 0-3.5 1.6-3.5 3.5V77h7V63.5c0-1.9-1.6-3.5-3.5-3.5zm-18 28c.8 0 1.5.7 1.5 1.5V101h-3V89.5c0-.8.7-1.5 1.5-1.5m0-2c-1.9 0-3.5 1.6-3.5 3.5V103h7V89.5c0-1.9-1.6-3.5-3.5-3.5zm9 2c.8 0 1.5.7 1.5 1.5V101h-3V89.5c0-.8.7-1.5 1.5-1.5m0-2c-1.9 0-3.5 1.6-3.5 3.5V103h7V89.5c0-1.9-1.6-3.5-3.5-3.5z"></path>
    <path d="M79.5 86c1.9 0 3.5 1.6 3.5 3.5V103h-7V89.5c0-1.9 1.6-3.5 3.5-3.5z"></path>

    <path fill="#D6D6D6" d="M52.8 116v-.4c0-.6-.1-1.1-.4-1.6h-3.1c-.6 0-1 .5-1 1 0 .6.5 1 1 1h3.5zM83 103v18H65"></path>
    <path d="M116 95.7V89c0-2.8-2.2-5-5-5s-5 2.2-5 5v6.7c0 2.6-.7 5.1-1.9 7.4-.8 1.4-1.2 3.2-1 5 .4 3.6 3.4 6.5 7 6.9 4.8.5 8.9-3.2 8.9-7.9 0-1.5-.4-2.9-1.2-4.1-1.3-2.3-1.8-4.8-1.8-7.3z" style="fill: rgb(241, 102, 34)"></path>
    <path d="M110 114h2v7h-2z" style="fill: rgb(88, 121, 218);"></path>
    <path fill="#D6D6D6" d="M110 74h12v2h-12z"></path>
    <path fill="none" d="M64 121l18-18"></path>
    <path d="M116 95.7V89c0-2.8-2.2-5-5-5-.3 0-.7 0-1 .1v30.8h.1c4.8.5 8.9-3.2 8.9-7.9 0-1.5-.4-2.9-1.2-4.1-1.3-2.2-1.8-4.7-1.8-7.2z" style="fill:rgb(241, 102, 34)"></path>
    <path fill="#FFF" d="M83 89h1v1h-1z"></path>
</svg>
</div></div></div></div></div><div class="_style_vIEMw"><div class="_style_VxPAE"><p class="_style_bsxwn">After the trip</p><h2 class="_style_3zaJwR">Always here for you</h2></div><div class="_style_4iUkli"><div class="cmln__markdown"><p class="cmln__paragraph"><strong class="cmln__strong">Anonymous feedback</strong><br>After every trip, you can rate the driver and provide anonymous feedback about your ride. We review all feedback because our goal is to make every ride a great experience.</p>
<p class="cmln__paragraph"><strong class="cmln__strong">24/7 support</strong><br>Our support team is always ready to respond to any questions you may have about your trip and help you retrieve lost items.</p>
<p class="cmln__paragraph"><strong class="cmln__strong">Rapid response</strong><br>Our specially-trained incident response teams are available around the clock to handle any urgent concerns that arise.</p>
</div></div></div></div></div><div class="_style_1F0PTn wcb-testimonial"><div class="_style_3w6bzg" role="img" title="Photo of contemplating Loader rider gazing into space" aria-label="Photo of contemplating Loader rider gazing into space"></div><div class="layout"><div class="layout__item _style_4Bge0S"></div><div class="layout__item _style_3jpoOU"><div class="_style_3puih4"><div class="_style_eeI8o"><blockquote class="_style_2MNLxX">“I work events late at night, and with Loader, I feel safer knowing I don’t have to go wait outside and hope I can flag down a ride.”</blockquote><div class="wcb__quotee-name _style_24nR8S">Brittany,</div><div class="wcb__quotee-title _style_37su0r">Rider from Portland</div></div></div></div></div></div><div class="_style_3ye16P wcb-vp-illustrated"><div class="_style_eeI8o"><div class="_style_4jfdOQ"><p class="_style_bsxwn">In the App and Offline</p><h2 class="_style_3zaJwR">Improving experiences with technology</h2></div><div class="layout _style_4ykyzI"><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="Decorative Illustration" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <path d="M78.79.29H28a8.77 8.77 0 0 0-8.77 8.77l.32 35.28a8.77 8.77 0 0 0 8.77 8.77h42.79l13.43 13.43a2 2 0 0 0 3.34-1.38v-12L88 9.07C88 4.22 83.63.29 78.79.29z" style="fill: rgb(241, 102, 34)"></path>
    <path d="M47.34 46.55H6.79a7 7 0 0 0-7 7v32.12a2 2 0 0 0 3.34 1.38l10-10h34.21a7 7 0 0 0 7-7v-16.5a7 7 0 0 0-7-7z" style="fill: rgb(241, 102, 34)"></path>
    <path d="M47.34 46.43H19.82A8.77 8.77 0 0 0 28.3 53h26a7 7 0 0 0-6.96-6.57z"></path>
    <path fill="#fff" d="M30.61 20.32h27.18v2H30.61zm0 8.08h35.81v2H30.61zm0-16.16h41.68v2H30.61zm0 24.24h44.92v2H30.61z"></path>
    <circle cx="13.36" cy="62.93" r="3.43" fill="#fff"></circle>
    <circle cx="27.07" cy="62.93" r="3.43" fill="#fff"></circle>
    <circle cx="40.77" cy="62.93" r="3.43" fill="#fff"></circle>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Substitute phone numbers</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">In many locations around the world, Loader uses technology that anonymizes phone numbers to keep contact details confidential. So when you and your driver need to contact each other, your personal information stays private.</p>
</div></div></div></div></div><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="Decorative Illustration" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 88 88" height="88" width="88" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <defs>
        <pattern id="a___-539188454" data-name="2/2 - black" width="4" height="4" patternUnits="userSpaceOnUse" viewBox="0 0 4 4">
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path d="M0 0h2v2H0z"></path>
        </pattern>
    </defs>
    <path fill="#d6d6d5" d="M0 0h88v88H0z"></path>
    <path d="M44 44h44v44H44z" style="fill: rgb(241, 102, 34)"></path>
    <path fill="#fff" d="M22 43h66v2H22zm0 22h66v2H22z"></path>
    <path fill="#fff" d="M43 0h2v88h-2zm22 22h2v44h-2zm-44-.66h2V88h-2z"></path>
    <path fill="#fff" d="M0 21h88v2H0z"></path>
    <path d="M45 67H21V44h2v21h20V21h24v23.31h-2V23H45v44" style="fill: rgb(0, 0, 0)"></path>
    <path d="M22 18.73v47.88a3.27 3.27 0 0 0 3.18-2.32l8.77-27.53A13 13 0 0 0 22 18.73zM9 31.7a13 13 0 0 0 .35 2.94 12.9 12.9 0 0 0 .64 2l8.8 27.6A3.27 3.27 0 0 0 22 66.61V18.73A13 13 0 0 0 9 31.7z" style="fill:rgb(241, 102, 34)"></path>
    <path d="M22 18.8v47.89a3.27 3.27 0 0 0 3.18-2.32l8.77-27.53A13 13 0 0 0 22 18.8z" fill="url(#a___-539188454)"></path>
    <circle cx="22" cy="31.7" r="4.89" fill="#fff"></circle>
    <circle cx="66" cy="44" r="4.89"></circle>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Always on the map</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">GPS data is logged for every trip so we know where and when you’re riding and who’s behind the wheel. It also helps us ensure that drivers are taking the best route every time.</p>
</div></div></div></div></div><div class="layout__item _style_1YSqxy"><div class="_style_VxPAE"><div class="_style_2ehyHb"><div class="_style_2XpHEy"><div class="_style_1iLMt8"><div aria-label="Decorative Illustration" class="_style_4EfqLw"><div class="isvg loaded" style="height: 100%;"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 144 144" height="144" width="144" style="display: block; height: 100%; width: 100%;"><title>Decorative Illustration</title><desc>Decorative Illustration</desc>
    <defs>
        <pattern id="a___-2025359049" data-name="SVGID 1" width="4" height="4" patternTransform="matrix(1.6 0 0 1.6 5197.6 21823)" patternUnits="userSpaceOnUse" viewBox="0 0 4 4">
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path fill="none" d="M0 0h4v4H0z"></path>
            <path d="M0 0h2v2H0z"></path>
        </pattern>
    </defs>
    <path d="M33.7 15.7A54.36 54.36 0 0 0 72-.1a54.36 54.36 0 0 0 38.3 15.8h11.9V74a47.53 47.53 0 0 1-13.9 33.6L72 143.9l-36.2-36.3A47.53 47.53 0 0 1 21.9 74V15.7z" fill="#d6d6d5"></path>
    <path d="M21.8 74V15.7h11.9A54.19 54.19 0 0 0 72-.1v144l-36.2-36.3a47.21 47.21 0 0 1-14-33.6z" fill="url(#a___-2025359049)"></path>
    <path d="M99.2 98.6A34.91 34.91 0 0 0 109.4 74V28.5H34.6V74a34.49 34.49 0 0 0 10.2 24.6L72 125.8z" style="fill: rgb(241, 102, 34)"></path>
    <path d="M72 43.3a25.4 25.4 0 1 0 0 50.8z" style="fill: rgb(241, 102, 34)"></path>
    <path d="M72 43.3a25.4 25.4 0 1 0 0 50.8z" fill="url(#a___-2025359049)"></path>
    <path d="M72 43.3a25.4 25.4 0 1 1 0 50.8z"></path>
</svg>
</div></div></div></div></div><div class="_style_3ZBSIh"><h4 class="_style_4okShP">Assisting law enforcement</h4><div class="_style_31qDzC"><div class="cmln__markdown"><p class="cmln__paragraph">In cases where local law enforcement provides us with valid legal process, we provide them with useful data to help in their investigations.</p>
</div></div></div></div></div></div></div></div><div class="_style_4siNiz wcb-messaging"><div class="_style_eeI8o"><div class="_style_VxPAE"><p class="_style_bsxwn">Rider safety tips</p><h2 class="_style_3zaJwR">How to keep yourself safe</h2></div><div class="_style_3IuFz3"><div class="cmln__markdown"><p class="cmln__paragraph">We worked with law enforcement to create this guide on how to ride safely with Loader. It explains what you can do to stay safe while riding. </p>
</div></div><a class="wcb-button _style_3ZmDGi _style_1JtngI" href="#"><!-- react-text: 4329 -->Read guide<!-- /react-text --><span class="_style_1gVw1O"><svg viewBox="0 0 64 64" width="16px" height="16px" class=" _style_4wJp4e"><path d="M23.161 53.213l-4.242-4.242a1 1 0 0 1 0-1.415L34.475 32 18.919 16.444a1 1 0 0 1 0-1.415l4.242-4.242a1 1 0 0 1 1.414 0L40.84 27.05l4.242 4.243a1 1 0 0 1 0 1.414L40.84 36.95 24.575 53.213a1 1 0 0 1-1.414 0z"></path></svg></span></a></div></div><div class="_style_34KYWo"><div class="_style_3POd9l"><div class="_style_syRoi"><h2 class="_style_R83rO">Get a safe ride  with Loader</h2></div><a class="wcb-button _style_1m6gck" href="#"><!-- react-text: 4338 -->Sign up to ride<!-- /react-text --><span class="_style_28zWyA"><svg viewBox="0 0 64 64" width="20px" height="20px" class=" _style_4wJp4e"><path fill-rule="evenodd" clip-rule="evenodd" d="M59.927 31.985l.073.076-16.233 17.072-3.247-3.593L51.324 34H4v-4h47.394L40.52 18.407l3.247-3.494L60 31.946l-.073.039z"></path></svg></span></a></div></div></div></div></div></div></div></div>

@endsection