
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link href="{{asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
 </head>
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-head">
                        <div class="page-title">
                            <h1>Ride
                                <small></small>
                            </h1>
                        </div>
                    </div>
                    <div class="invoice">
                        <div class="row invoice-logo">
                            <div class="col-xs-6 invoice-logo-space">
                               <!--  <img src="{{ asset('assets/pages/img/loader.png')}}" class="img-responsive" alt="" /> </div> -->
                            <div class="col-xs-6">
                                <p> <strong>Joining Date </strong>  
                                    <span class="muted"> Consectetuer adipiscing elit </span>
                                </p>
                            </div>
                        </div>
                        <hr/>
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="table table-striped table-hover">
                                    <thead>
                                        <tr>
                                            <th> Ride id#</th>
                                            <th> User </th>
                                            <th> pickup_place </th>
                                            <th> drop_place </th>
                                            <th> distance </th>
                                            <th> amount </th>
                                            <th> status </th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($captains as $ride)
                                        <tr>
                                            <td> {{ $ride->id}} </td>
                                            <td> {{ $ride->user->fname}} {{ $ride->user->lname}} </td>
                                            <td> {{ $ride->booking['pickup_place']}} </td>
                                            <td>  {{ $ride->booking_destinations['drop_place']}} </td>
                                            <td> {{ $ride->booking['distance'] }}Km </td>
                                            <td> {{ $ride->booking->amount}} </td>
                                            <td> {{ $ride->status}} </td>
                                        </tr>
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="well">
                                    <address>
                                        <strong>Loader, Inc.</strong>
                                        <br/> Fiaz Road,near Wadahat Road Lahore
                                        <br/> 03003413778
                                        <br/>
                                        <abbr title="Phone">P:</abbr> (300) 4277528 </address>
                                    <address>
                                        <strong>Full Name</strong>
                                        <br/>
                                        <a href="mailto:#"> loader.aktechzone@email.com </a>
                                    </address>
                                </div>
                            </div>
                            <div class="col-xs-8 invoice-block">
                                <ul class="list-unstyled amounts">
                                    <li>
                                        <strong>Sub - Total amount:</strong> {{$ride->booking['amount']}} </li>
                                    <li>
                                        <strong>Discount:</strong> 12.9% </li>
                                    <li>
                                        <strong>VAT:</strong> ----- </li>
                                    <li>
                                        <strong>Grand Total:</strong> $12489 </li>
                                </ul>
                                <br/>
                                     @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
