<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
<div class="m-portlet m-portlet--mobile">

   <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
        <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              <i class="fa fa-gift"></i>Finance Report
          </h3>
      </div>
  </div>
</div>
<div class="page-content-wrapper">
   <div class="page-content">
    <div class="col-md-6" style="margin: 0 auto; border: 1px solid #ccc; box-shadow: 1px 1px 3px;">
      <div class="portlet box red">                       
        <!-- here is current day report gererate -->
        <div class="portlet-body form">
            <div class="form-body">
                <div class="form-group">

                    <form action="{{route('report-user-filter')}}" class="mt-repeater form-horizontal" method="GET">

                        <h3 class="mt-repeater-title">Today finance Report</h3>
                        <div data-repeater-list="group-a">
                            <div data-repeater-item class="mt-repeater-item">



                                <div class="mt-repeater-input">
                                    <label class="control-label">start Date</label>
                                    <br/>
                                    <input  class="input-group form-control  form-control-inline date" size="16" type="date" value="{{ date('Y-m-d') }}" name="todate"  /> 
                                </div>
                                <br>
                                <button type="submit" class="btn btn-success green">Submit</button>
                            </div>
                        </div>                                                                         
                    </form>
                </div>
            </div>
        </div>
        <!-- here is current da report end -->
        <div class="portlet-body form">
            <div class="form-body">
                <div class="form-group">
                    <form action="{{route('report-betwenreport')}}" class="mt-repeater form-horizontal" method="POST">
                        {{ csrf_field() }}
                        <h3 class="mt-repeater-title">finance Report</h3>
                        <div data-repeater-list="group-a">
                            <div data-repeater-item class="mt-repeater-item">

                               <div class="mt-repeater-input">
                                <label class="control-label">Captain </label>
                                <br/>
                                <select id="select2" name="captain_id" class="js-example-data-ajax form-control">
                                  <option value="" selected="selected">select2/select2</option>
                              </select> 
                          </div>  

                          <div class="mt-repeater-input">
                            <label class="control-label">start Date</label>
                            <br/>
                            <input class="input-group form-control form-control-inline date date-picker" size="16" type="date" value="2018-08-27" name="start_date" data-date-format="yyyy-mm-dd" /> 
                        </div>
                        <div class="mt-repeater-input">
                            <label class="control-label">Till Date</label>
                            <br/>
                            <input class="input-group form-control form-control-inline date date-picker" size="16" type="date" value="2018-09-27" name="last_date" data-date-format="yyyy-mm-dd" /> 
                        </div>                                                
                        <div class="">
                            <br>
                            <button type="submit" class="btn btn-success green">Submit</button>
                             </form>
                             <a href="{{route('finance-report.index')}}" class="btn btn-info green"> Cancel</a>
                        </div>
                    </div>
                </div>


        </div>
    </div>
</div>
</div>
</div>
</div>



@endsection

@push('post-scripts')


<script type="text/javascript">
 $('#select2').select2({
  ajax: {
    url: "{{route('captain_account')}}",
    method:"post",
    dataType: 'json',
    processResults: function (_data, params) {
      
        var data1= $.map(_data, function (obj) {
        var newobj = {};
        newobj.id = obj.id;
        newobj.text= obj.fname+" "+obj.lname;
        newobj.balance= obj.wallet.credit_amount;
        return newobj;
      });
       //console.log(data1);return false;
      return { results:data1};
    }
  }
}).on('change', function() {

  console.log($(this).select2('data')[0].balance);
  var balance = $(this).select2('data')[0].balance;
  $('#title').val(balance);
});

</script>


@endpush

