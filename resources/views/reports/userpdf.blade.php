<div class="page-content-wrapper">
    <div class="page-content">
    <div class="row">
      <div class="col-md-12">
       
        <div class="portlet light portlet-fit bordered">
          <div class="portlet-title">
            <div class="caption">
              <i class="icon-settings font-red"></i>
              <span class="caption-subject font-red sbold uppercase">User history</span>
            </div>

          </div>
          <div class="portlet-body">
            <div class="table-toolbar">
              <div class="row">

              </div>


            </div>
            <table class="table table-striped table-hover table-bordered" id="goods-table">
             <thead>
               <tr>

                <th> Captain </th>
                <th>  Amount  </th>
                <th> Collect Amount  </th>
                <th> Left Amount  </th>
                <th> Commision  </th>
                <th> Date </th>
        

              </tr>
            </thead>
            <tbody>
              @foreach($user as $ride)
              <tr>

               <td> {{ $ride->captian->fname }}  {{ $ride->captian->lname }} </td>
               <td> {{ $ride->amount}} </td>
               <td>{{ $ride->collect_amount }} </td>
               <td> {{ $ride->left_amount }} </td>
               <td>{{$ride->commisions }}</td>

              

               <td>{{$ride->created_at }} </td>


             </tr>
             @endforeach


           </tbody>
         </table>


       </div>


     </div>   
   </div>
 </div>

</div>
<!-- END CONTENT BODY -->
</div>