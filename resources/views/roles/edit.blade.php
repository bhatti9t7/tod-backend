<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')
   
       <div class="page-content">
           <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">       
              <i class="fa fa-key" style="color: #fff;"></i>
              <span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 20px!important;">  Edit Role: {{$role->name}}</span>
          </div> </div>  
          <div class="col-md-11 box_shadow_form">        
           <div class="col-md-11">
                {{ Form::model($role, array('route' => array('roles.update', $role->id), 'method' => 'PUT')) }}

                <div class="form-group">
                    {{ Form::label('name', 'Role Name') }}
                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                </div>
            </div>
                 <div class="col-md-12">
                     <h5><b>Assign Permissions</b></h5>  
                 </div>
                <div class="form-group">
                    <div class="col-md-6"><label class="control-label">Permissions</label>
                    </div>
                    <div class="col-md-11">
                        <select multiple="multiple" class="multi-select" id="permission_select" name="permissions[]">
                            @foreach($permissions as $permission)
                            <option @if($role->hasDirectPermission($permission)) selected @endif value="{{$permission->id}}">{{$permission->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                  <div class="col-md-11">
                <div class="form-group">
                   {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}  
              <a href="{{route('permissions.index')}}" class="btn btn-success"> Cancel</a> 
              </div>
          </div>
          <br>
            </div>
 <br>
         </div>
        </div>
    </div>
</div>




        @endsection
        @stack('post-styles')
       
        <link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{asset('assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        @push('post-scripts')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

        <script type="text/javascript">

            $('#permission_select').multiSelect();
        </script>


        @endpush