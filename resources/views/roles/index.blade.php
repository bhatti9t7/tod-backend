<?php
/**
 * Project: Loader.
 * User: Shafqat
 * Date: 23/07/2018
 * Time: 7:03 PM
 */
?>

@extends('_layouts.admin.app')
@section('content')


        <div class="page-content">
                     <div class="row">
              <div class="col-md-12">
                <div class="portlet light portlet-fit bordered">
                        <div class="portlet-title">
                                 <div class="caption">
                              <i class="fa fa-key" style="color: #fff;"></i>
                                <span class="caption-subject  sbold uppercase" style="color: #fff; font-size: 20px!important;"> Roles</span>
                            </div>
                           <a href="{{ route('permissions.index') }}" class="btn btn-default pull-right">Permissions</a>
                        </div>
             
        

                <div class="col-md-12">
                    
            <div class="table-responsive table_1">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Role</th>
                            <th>Permissions</th>
                            <th>Operation</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($roles as $role)
                        <tr>

                            <td>{{ $role->name }}</td>

                            <td>{{ str_replace(array('[',']','"'),'', $role->permissions()->pluck('name')) }}</td>{{-- Retrieve array of permissions associated to a role and convert to string --}}
                            <td>
                            <a href="{{ URL::to('admin/roles/'.$role->id.'/edit') }}" class="btn btn-info pull-left">Edit</a>

                            {!! Form::open(['method' => 'DELETE', 'route' => ['roles.destroy', $role->id] ]) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}

                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                    </table>
                     <a href="{{ URL::to('admin/roles/create') }}" class="btn btn-success" style="margin-bottom: 10px;">Add Role</a>    
                    </div>

               </div>


       
</div>
</div>
@endsection
@stack('post-styles')
<link href="{{asset('assets/global/css/components-rounded.css')}}" rel="stylesheet" id="style_components" type="text/css" />

@push('post-scripts')
           

 @endpush