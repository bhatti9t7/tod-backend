@extends('layout.users.userheader')
@section('content')
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-head">
                        <div class="page-toolbar">
                        </div>
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    

                                    <div class="tab-pane" id="tab_1">
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                
                                            </div>
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="#" class="horizontal-form">
                                                    @foreach($Booking as $book)

                                                    <div class="form-body">
                                                        <h3 class="form-section">Booking Info</h3>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">User Name</label>
                                                                    <input type="text" id="firstName" class="form-control" value="{{ $book->user['fname']}} {{ $book->user['lname']}}">
                                                                    <span class="help-block">  </span>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                @foreach($ride as $book)
                                                                <div class="form-group ">
                                                                    <label class="control-label">Captain Name</label>
                                                                    <input type="text" id="lastName" class="form-control" value="{{ $book->captain['fname']}} {{ $book->captain['lname']}}" ">
                                                                    <span class="help-block">  </span>
                                                                </div>
                                                            </div>
                                                            <!--/span-->
                                                            @endforeach
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                             @foreach($Booking as $book)
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Vehicle</label>
                                                                    <input type="text" id="lastName" class="form-control" value="{{ $book->vehical['vehical_name']}} ">
                                                                    <span class="help-block">  </span>
                                                                </div>
                                                                
                                                            </div>
                                                            <!--/span-->
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Good Name</label>
                                                                    <input type="text" class="form-control" placeholder="Good Name" value="{{ $book->good['good_name'] }}"> </div>
                                                            </div>
                                                            <!--/span-->
                                                        </div>
                                                        <!--/row-->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">Pickup Place</label>
                                                                    <input type="text" class="form-control" placeholder="PickUp place" value="{{ $book->pickup_place }}"> </div>
                                                                </div>
                                                            </div>
                                                            <!--/span-->@endforeach
                                                            
                                                            <!--/span-->
                                                        </div>
                                                        <h3 class="form-section">Address</h3>
                                                        <!--/row-->

                                                        <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>drop off</label>
                                                                    <input type="text" class="form-control" value="{{ $book['BookingDestination->drop_place'] }}"> </div>
                                                            </div>
                                                            <!--/span-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Payment</label>
                                                                        <input type="text" class="form-control" value="{{ $book->PaymentMethod }}"> 
                                                                    </div>
                                                                </div>
                                                                <!--/span-->

                                                                
                                                            </div>

                                                        
                                                            <div class="col-md-6 ">
                                                                <div class="form-group">
                                                                    <label>distance</label>
                                                                    <input type="text" class="form-control" value="{{ $book->distance }}"> </div>
                                                            </div>
                                                        
                                                        
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Amount</label>
                                                                    <input type="text" class="form-control" value="{{ $book->amount }}"> </div>
                                                            </div>

                                                            
                                                            <!--/span-->
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Payment</label>
                                                                        <input type="text" class="form-control" value="{{ $book->PaymentMethod }}"> 
                                                                    </div>
                                                                </div>
                                                                <!--/span-->

                                                                
                                                            </div>
                                                        <!--/row-->
                                                        
                                                    </div>
                                                    
                                                    
                                                </form>
                                                <!-- END FORM-->
                                            </div>
                                        </div>
                                        


                                    @endsection