@extends('layout.users.userheader')
@section('content')
            <div class="page-content-wrapper">

                <div class="page-content">

                    <div class="page-head">

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light portlet-fit bordered">

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-settings font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">Rides Record</span>
                                    </div>
                                    <div class="actions">
                                        
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                               <div id="lets_search" class="dataTables_filter">
                                                    <label for="search" >Search:<input type="search" class="form-control input-sm input-small input-inline" placeholder="" onkeyup="search" id="search" aria-controls="search"></label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="btn-group pull-right">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <table class="table table-striped table-hover table-bordered" id="rides">
                                       <thead>
                                            <tr>
                                             <th align="left">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                                        <span></span>
                                                    </label>
                                                </th>
                                               
                                                <th> Destination Name </th>
                                                <th> Delivery Proof </th>
                                                <th>Route Map image</th>  
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                             
                                                 
                                                <tr>
                                                   
                                                  <td align="center">
                                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="1" />
                                                        <span></span>
                                                    </label>
                                                </td>
                                        
                                                    <td>{{ $ride_complete->BookingDestination['drop_place'] }} </td>
                                                    <td><img src="{{asset('images/ride_complete/' . $ride_complete->image)}}" alt=" image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" > </td>
                                                    <td> <img src="{{asset('images/ride_complete/' . $ride_complete->map_image)}}" alt="map image" class="logo-default" style="width: 40px; height: 40px; margin-top: 0px;" >  </td>

                                                </tr>
                                               
                                             
                                        </tbody>
                                    </table>
                                    
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5">
                                            <div class="dataTables_info" id="sample_editable_1_info" role="status" aria-live="polite"></div>
                                        </div>
                                        <div class="col-md-7 col-sm-7">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:;" class="page-quick-sidebar-toggler">
                <i class="icon-login"></i>
            </a>
            
             </div>
          </div>  
          




           
          </div> 


 
    
      
        <!-- END CONTAINER -->
 @endsection       <!-- BEGIN FOOTER -->


@section('styles')
    @parent
   

    <link href="{{ asset('assets/apps/css/stylebutton.css') }}" rel="stylesheet" type="text/css" />
     <link href="{{ asset('assets/apps/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />

    <!--  -->


@endsection

@section('javascripts')
@parent

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript"></script>






<script type="text/javascript">
    $(document).ready(function() {
    $('#rides').DataTable( {
        dom: 'Bfrtip',
         "paging": false,
        "bInfo": false,
        "searching": false,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>





@endsection