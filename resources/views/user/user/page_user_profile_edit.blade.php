@extends('layout.users.userheader')
@section('content')
            <div class="page-content-wrapper">
                <div class="page-content" style="min-height: 1279px;">
                    
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <div class="tab-content" style="background-color: #fff;">
                                <div class="tab-pane active" id="tab_1_3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-content">
                                                <div id="tab_1-1" class="tab-pane active">

                            {!! Form::open(['method' => 'PATCH' ,'files'=>true, 'route' => ['userResources.update',$User->id], 'class'=>'form-horizontal']) !!}
                                                       <div class="form-group col-md-6">
                                                            <label class="control-label">First Name</label>
                                                            <input name = "fname" type="text" placeholder="John" class="form-control" 
                                                            value="{{ $User->fname }}"> </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Last Name</label>
                                                            <input type="text" name = "lname" placeholder="Doe" class="form-control" value="{{ $User->lname }}"> </div>
                                                            <div class="form-group col-md-6">
                                                            <label class="control-label">Email</label>
                                                            <input name = "email" type="email" class="form-control" placeholder="email" value="{{ $User->email }}"> </div>
                                                        
                                                            <div class="form-group col-md-6">
                                                            <label class="control-label">phone</label>
                                                            <input type="text" name = "phone" placeholder="phone" class="form-control" value="{{ $User->phone }}"> </div>
                                                            <div class="form-group col-md-6">
                                                            <label for="gender">Gender</label>
                                                              <select name = "gender" class="form-control" value="{{ $User->gender }}">
                                                                <option selected="">Choose</option>
                                                                <option value = "Male">Male</option>
                                                                <option value = "Female">Female</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Password</label>
                                                            <input type="password" class="form-control" name = "password" >
                                                        </div>
                                                         <div class="form-group col-md-6">
                                                            <label class="control-label">Confirm Password</label>
                                                            <input name = "password_confirmation" type="password" class="form-control"> 
                                                        </div>
                                                    <div class="col-md-12">
                                                    <div class = "col-md-4"  style="margin-left: 5px;">
                                               <div class="fileinput fileinput-new" data-provides="fileinput" >
                                                  <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <img src="{{asset('images/user/profile/' . $User->images)}}" alt="" />
                                                        <img src="#" id="blah" height="250px" width = "250px">
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                             <label for="images" class="btn btn-primary">Upload Profile</label>
                                             <input type="file" id="images" name="images" class="hide">
                                         </div>
                                                    </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="margiv-top-10 pull-right">
                                                                   <button class="btn green" type = "Submit" name = "submit">Save</button>
                                                                   <button class="btn red" type="reset">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
@endsection
@section('styles')
@parent
<link href="{{asset('assets/global/css/components-rounded.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />

     <link href="{{asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/jquery-multi-select/css/multi-select.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

@endsection
        @section('javascripts')

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="{{asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>

                      <script type="text/javascript">
                          
                           $('#role_select').multiSelect();
                           $('#permission_select').multiSelect();
                      </script>

                <script type="text/javascript">
                    function readURL(input) {

                        if (input.files) {
                            var reader = new FileReader();
                            var totalfiles = input.files.length;
                            for(i = 0; i < totalfiles; i ++){
                                reader.onload = function(e) {
                                    $('#blah').attr('src', e.target.result);
                                }
                                reader.readAsDataURL(input.files[i]);
                            }
                        }
                    }
                    $("#images").change(function() {
                        readURL(this);
                    });
                </script>
                <script type="text/javascript">
                    $(function() {
                        // Multiple images preview in browser
                        var imagesPreview = function(input, placeToInsertImagePreview) {

                            if (input.files) {
                                var filesAmount = input.files.length;

                                for (i = 0; i < filesAmount; i++) {
                                    var reader = new FileReader();

                                    reader.onload = function(event) {
                                        $($.parseHTML('<img height = "250" width = "250" style = "margin:0px 10px 10px 0px;" >')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                                    }

                                    reader.readAsDataURL(input.files[i]);
                                }
                            }

                        };

                        
                    });
                </script>


@endsection