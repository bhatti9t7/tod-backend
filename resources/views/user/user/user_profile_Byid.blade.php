@extends('layout.users.userheader')
@section('content')
 <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">

        <div class="page-head">

            <div class="page-title">

            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings font-red"></i>
                            <span class="caption-subject font-red sbold uppercase">User Booking Detail</span>
                        </div>
                        <div class="actions">

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                 <div class="col-md-6 col-sm-6">
                                    <div id="lets_search" class="dataTables_filter">
                                        <label for="search" >Search:<input type="search" class="form-control input-sm input-small input-inline" placeholder="" onkeyup="search" id="search" aria-controls="search"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="btn-group pull-right">

                                </div>
                            </div>
                        </div>

                    </div>
                    <table class="table table-striped table-hover table-bordered" id="booking-table">
                       <thead>
                        <tr>
                         <th align="left">
                            <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                <span></span>
                            </label>
                        </th>

                        <th>Good</th>
                        <th>Vehicle</th>
                        <th> Pickup Place </th> 
                        <th> Booking date </th>
                        <th> amount </th>
                        <th> distance </th>
                        <th> payment  </th>
                        <th> Status </th>                        
                        <th> Ride </th>
                    </tr>
                </thead>
                <tbody>

                 @foreach ($users as $i) 
                 <tr>

                  <td align="center">
                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                        <input type="checkbox" class="checkboxes" value="1" />
                        <span></span>
                    </label>
                </td>
                <td> {{ $i->good->good_name }} </td>
                <td> {{ $i->vehical->vehical_name }} </td>
                <td> {{ $i->pickup_place }}  </td>
                <td>{{ $i->booking_date }}</td>
                <td>{{ $i->amount }}</td>
                <td>{{ $i->distance }}</td>
                <td>{{ $i->PaymentMethod }}</td>
                @if($i->status === 0 )
                <td>complete</td>
                @else
                <td>pending</td>
                @endif

                
                 <td>
                     <a href="{{route('booking-detail', $i['id'])}}"><button class="btn btn-primary btn-xs"  data-title="Edit" data-toggle="modal" data-target="#editModal" data-placement="top" rel="tooltip"><span class="glyphicon glyphicon-eye-open"></span></button></a>
                </td> 



            </tr>
            @endforeach

        </tbody>
    </table>

    <div class="row">
        <div class="col-md-5 col-sm-5">
            <div class="dataTables_info" id="sample_editable_1_info" role="status" aria-live="polite"></div>
        </div>
        <div class="col-md-7 col-sm-7">
            <div class="dataTables_paginate paging_bootstrap_number" id="sample_editable_1_paginate">
                <ul class="pagination" style="visibility: visible; float: right;">
                  
                </ul>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>


</div>

</div>

@endsection       <!-- BEGIN FOOTER -->


@section('styles')
@parent
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />

<link href="{{ asset('assets/apps/css/stylebutton.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/apps/css/jquery.dataTables.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('javascripts')
@parent


<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript"></script>






<script type="text/javascript">
    $(document).ready(function() {
        $('#booking-table').DataTable( {
            dom: 'Bfrtip',
            "paging": false,
            "bInfo": false,
            "searching": false,
            buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        } );
    } );
</script>





@endsection