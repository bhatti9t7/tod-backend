<?php

Route::get('/', 'PublicHomeController@index');
// Route::get('/', function () {
// 	//return Redirect::route('user-register.index');

//    // return view('public.home');
// });


Route::get('/check-me', function(){
	$captains =App\Captain::with('rates')->get();
  //App\Http\Resources\CaptainCollection::withoutWrapping();
	$data=new App\Http\Resources\CaptainCollection($captains);

	return $data;
});

Route::get('/custom_offer' ,'ScheduleController@custom_offer')->name('custom_offer');
Route::get('/weakly_offer' ,'ScheduleController@weakly_offer')->name('weakly_offer');
Route::get('/yearly_offer' ,'ScheduleController@yearly_offer')->name('yearly_offer');
Route::get('/monthly_offer' ,'ScheduleController@monthly_offer')->name('monthly_offer');
//////Admin Routes;
Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
	Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
	

/////user Dashboard
Route::prefix('user')->group(function(){
 // Authentication Routes...
	Auth::routes();
	Route::get('/profile', 'User\UserResource@profile')->name('user.profile');
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');
	Route::post('/logout', 'Auth\LoginController@logout')->name('user.logout');
	Route::Resource('advance-booking','User\AdvanceBookingController');
	Route::Resource('good','User\GoodsController');
	Route::Resource('vehical','User\VehicalTypeController');
	Route::Resource('booking','User\BookingController');
	Route::Resource('Ride','User\RidesController');
	Route::get('pending-Ridess', 'User\RidesController@pendingride')->name('pending-Rides');
	Route::Resource('googlemap' ,'User\GoogleMapController');
	Route::Resource('userResources' ,'User\UserResource');
	Route::get('user-walletss/{id}', 'User\UserResource@walletShow');
	

});

Route::prefix('admin')->group(function(){

	Route::get('/giveme','Admin\RoleController@giveme');
	Route::get('/checkme','Admin\RoleController@checkme');

	////Accounts
	Route::resource('code','Admin\Account\AccountCodeController');
	Route::resource('bank-deposit','Admin\Account\BankDepositController');
	Route::resource('bank-payment','Admin\Account\BankPaymentController');
	Route::resource('cash-payment','Admin\Account\CashPaymentController');
	Route::resource('cash-reciept','Admin\Account\CashRecieptController');
	Route::resource('invoice','Admin\Account\InvoiceController');
	Route::resource('journal-voucher','Admin\Account\JvController');
	Route::resource('ledger','Admin\Account\LedgerController');
	Route::resource('financials','Admin\Account\FinancialController');
	Route::resource('payable','Admin\Account\PayableAccountController');
	Route::resource('receivable','Admin\Account\RecivableController');
	Route::get('/accounts/detail','Admin\Account\LedgerController@all_account_record')->name('code.all_account');
	Route::post('code/create','Admin\Account\AccountCodeController@create_form')->name('account.new_acc');
Route::post('code/get_account', 'Admin\Account\AccountCodeController@get_account')->name('get_account');
Route::post('recivable/reciveable_account_record','Admin\Account\RecivableController@reciveable_account_record')->name('reciveable_account_record');
Route::post('payable/payables_account_record','Admin\Account\PayableAccountController@payables_account_record')->name('payables_account_record');
Route::post('/ledger/ledger_detail', 'Admin\Account\LedgerController@ledger_detail')->name('ledger_detail');
Route::post('ledger/search/record','Admin\Account\LedgerController@search_ledger_amount')->name('search_ledger_amount');

	Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
	Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
	
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::Resource('userResource' ,'UserResource');

	Route::get('/user/send','UserResource@send');
Route::get('delete_record/{id}' ,'UserResource@delete_record');
	Route::post('/user_data','UserResource@get_users')->name('get-data');
	Route::post('/user_status','UserResource@user_change_status')->name('user-change-status');

	Route::get('/booking-detail/{id}' ,'UserResource@bookingdetail')->name('booking-detail');
	Route::get('user-wallet/{id}', 'UserResource@walletShow')->name('user-wallet');

	Route::Resource('adminResource' ,'AdminResource');

	Route::post('/admin_data','AdminResource@get_admin')->name('get-admin');
	Route::post('/admin_status','AdminResource@admin_change_status')->name('admin-change-status');
Route::get('admin_delete_record/{id}' ,'AdminResource@delete_record');

	Route::Resource('captainResource' ,'CaptainResource');

	Route::post('/captain_account', 'CaptainResource@captain_account')->name('captain_account');

	Route::post('/captain_getsss','CaptainResource@get_captain')->name('get-captainss');
	Route::post('/change_status','CaptainResource@captain_change_status')->name('captain-change-status');
Route::get('captain/delete_record/{id}' ,'CaptainResource@delete_record');
	Route::get('captain-wallet/{id}', 'CaptainResource@walletShow')->name('captain-wallet');


	Route::Resource('goods' ,'GoodsController');
	Route::get('good_delete_record/{id}' ,'GoodsController@delete_record');
	Route::get('good/delete_record/{id}' ,'GoodsController@delete_record');
	Route::post('/get_good','GoodsController@get_good')->name('get-good');
	Route::post('/good_status','GoodsController@good_change_status')->name('good-change-status');

	Route::Resource('captain-wallet' ,'CaptainWalletController');
	Route::Resource('googlemaps' ,'GoogleMapController');
	Route::Resource('vehicals', 'VehicalTypeController');
	Route::get('vehicals/delete_record/{id}' ,'VehicalTypeController@delete_record');
	Route::post('/get_vehical','VehicalTypeController@get_vehical')->name('get_vehical');
	Route::post('/vehical_change_status','VehicalTypeController@vehical_change_status')->name('vehical-change-status');
	

	Route::Resource('bookings', 'BookingController');
	Route::post('/booking_data','BookingController@get_booking')->name('get-booking');

	Route::Resource('pending-booking', 'PendingBookingController');
	Route::post('/pending_booking_data','PendingBookingController@get_pendingbooking')->name('get-pending-booking');



	Route::Resource('advance-bookings', 'AdvanceBookingController');

	Route::Resource('Rides' , 'RidesController');
	Route::post('/ride-data','RidesController@get_ride')->name('get-ride');
	
	Route::get('pending-Rides', 'RidesController@pendingride')->name('pendingRide');
	Route::post('/pending_data','RidesController@get_pending')->name('get-pending');

	Route::post('/upload_document_file','CaptainResource@upload_document_file')->name('upload_document_file');
	Route::post('/remove_document_file','CaptainResource@remove_document_file')->name('remove_document_file');

	Route::get('search','CaptainResource@searchbar')->name('searching');
	Route::Resource('good-categories' , 'GoodCategoriesController');
	Route::post('/get_cat','GoodCategoriesController@get_cat')->name('get_cat');
	Route::post('/cat_status','GoodCategoriesController@categories_change_status')->name('cat-change-status');

	Route::Resource('vehicle-sub-categories' , 'VehicleSubCategoriesController');
	Route::get('vehicle_delete_record/{id}' ,'VehicleSubCategoriesController@delete_record');
	Route::post('/get_sub_vehical','VehicleSubCategoriesController@get_sub_vehical')->name('get_sub_vehical');
	Route::post('/vcategories_change','VehicleSubCategoriesController@vcategories_change_status')->name('vcategories_change_status');
	Route::get('vehicle_categoires', 'VehicleSubCategoriesController@select_vehicle_categories')->name('vehicle-categories_data');

	Route::Resource('vehical-categories' , 'VehicalCategoriesController');
		Route::get('vehicle_categories_delete_record/{id}' ,'VehicalCategoriesController@delete_record');
	Route::post('/get_cat_vehical','VehicalCategoriesController@get_cat_vehical')->name('get-cat-vehical');
	Route::post('/categories_change_status','VehicalCategoriesController@categories_change_status')->name('category-change-status');

	Route::resource('roles', 'RoleController');
	Route::resource('permissions', 'PermissionController');
	Route::resource('financial', 'FinancialController');

	Route::get('report/{id}', 'CaptainReportController@report');
	Route::get('captain_report' ,'CaptainReportController@filtercaptain')->name('captain_report');
	
	Route::resource('report' , 'CaptainReportController');

	Route::get('/get-captains-ajax' ,'CaptainResource@get_captains_ajax');
	Route::resource('inflow' , 'TransactionController' );
	Route::post('/traction','TransactionController@tracking_datas')->name('transaction-history');
	Route::resource('/emergencies','EmergencyController');
	Route::get('emergency_delete_record/{id}' ,'EmergencyController@delete_record');
		Route::post('/get_emergencies','EmergencyController@get_emergnecies')->name('get-emergencies');
		Route::post('/emergencies_change_status','EmergencyController@emergnecies_change_status')->name('emergencies-change-status');


	Route::resource('/offers','OfferController');
	Route::get('offer_delete_record/{id}' ,'OfferController@delete_record');
		Route::post('/get_offer','OfferController@get_offers')->name('get-offer');
		Route::post('/offer_change_status','OfferController@offerschange_status')->name('offer-change-status');

	Route::Resource('skip-reasons' ,'SkipReasonController');
		Route::get('reasons_delete_record/{id}' ,'SkipReasonController@delete_record');
	Route::post('/get_reasons','SkipReasonController@get_reasons')->name('get_reasons');
	Route::post('/reasons_status','SkipReasonController@reasons_change_status')->name('reasons-change-status');

	Route::Resource('activity-logs' ,'AcivityLogController');
	Route::post('/get-activity-log','AcivityLogController@get_logs')->name('get_activity_log');

///////CMS setting updated here ////////
	Route::resource('logo', 'Cms\LogoController');
	Route::resource('slide', 'Cms\SliderController');
	Route::resource('public-home-page' , 'Cms\PublicHomeController');
	Route::resource('drive-section' , 'Cms\DriveController');
	Route::resource('Bussiness-section', 'Cms\BussinessController');
	Route::resource('companies-section' , 'Cms\CompaniesController');
	Route::resource('news-section' , 'Cms\NewsController');
	Route::resource('out-flow', 'OutflowController');
	Route::post('/outflow_data','OutflowController@outflow_data')->name('outflow-data');

	Route::get('/main_wallet','OutflowController@main_wallet')->name('main-wallet-data');

	Route::get('user-report/{id}', 'UserReportController@report');
	Route::get('report-filter' ,'UserReportController@filteruser')->name('report-user-filter');
	Route::resource('finance-report' , 'UserReportController');

	Route::post('report-betwenreport' ,'UserReportController@betwenreport')->name('report-betwenreport');

	Route::Resource('cities' ,'CityController');
	Route::get('city_delete_record/{id}' ,'CityController@delete_record');
	Route::get('city-data' ,'CityController@city_on_select')->name('city_data');
	Route::post('/get_citi','CityController@get_citi')->name('get-citi');
	Route::post('/citi_status','CityController@citi_change_status')->name('citi-change-status');

	Route::Resource('country' ,'CountriesController');
	Route::get('country_delete_record/{id}' ,'CountriesController@delete_record');
	Route::post('/get_country','CountriesController@get_country')->name('get-country');
	Route::post('/country_status','CountriesController@country_change_status')->name('country-change-status');

 
 	Route::get('branch-data' ,'BranchController@branch_on_select')->name('branch_data');
	Route::Resource('branch' ,'BranchController');
	Route::get('branch_delete_record/{id}' ,'BranchController@delete_record');
	Route::post('/get_branch','BranchController@get_branch')->name('get-branch');
	Route::post('/branch_status','BranchController@branch_change_status')->name('branch-change-status');

	Route::Resource('franchise' ,'FranchiseController');
	Route::get('franchise_delete_record/{id}' ,'FranchiseController@delete_record');
	Route::post('/get_franchise','FranchiseController@get_franchise')->name('get-franchise');
	Route::post('/franchise_status','FranchiseController@franchise_change_status')->name('franchise-change-status');

// Route for export/download tabledata to .csv, .xls or .xlsx
Route::get('completeRideExcel/{type}', 'RidesController@complete_ride_download')->name('completeRideExcel');
Route::get('adminExcel/{type}', 'AdminResource@downloadExcel')->name('adminExcel');
Route::get('captainExcel/{type}', 'CaptainResource@downloadExcel')->name('downloadExcel');

Route::get('transactionExcel/{type}', 'TransactionController@downloadExcel')->name('downloadExcel');
Route::get('bookingExcel/{type}', 'BookingController@downloadExcel')->name('downloadExcel');
Route::get('pbookingExcel/{type}', 'PendingBookingController@downloadExcel')->name('downloadExcel');
Route::get('transactionExcel/{type}', 'TransactionController@downloadExcel')->name('transactionExcel');
Route::get('pendingExcel/{type}', 'RidesController@pending_ride_download')->name('pendingExcel');
Route::get('captainCommisionExcel/{type}', 'OutflowController@downloadExcel')->name('captainCommissionExcel');



});
///////////// end Admin DASHBOARD START HERE............. /////////////////

//////Captain Routes;   ////////
Route::prefix('captain')->group(function(){
	Route::get('/', 'CaptainController@index')->name('captain.dashboard');
	Route::post('/register', 'Auth\CaptainRegisterController@create')->name('captain.register');
	Route::get('/login', 'Auth\CaptainLoginController@showLoginForm')->name('captain.login');
	Route::post('/login', 'Auth\CaptainLoginController@login')->name('captain.login.submit');
	Route::get('/logout', 'Auth\CaptainLoginController@captainLogout')->name('captain.logout');
	Route::post('/logout', 'Auth\CaptainLoginController@captainLogout')->name('captain.logout');
///////captain

	Route::get('/password/reset', 'Auth\CaptainForgotPasswordController@showLinkRequestForm')->name('captain.password.request');
	Route::post('/password/email', 'Auth\CaptainForgotPasswordController@sendResetLinkEmail')->name('captain.password.email');
	Route::get('/password/reset/{token}', 'Auth\CaptainResetPasswordController@showResetForm')->name('captain.password.reset');
	Route::post('/password/reset', 'Auth\CaptainResetPasswordController@reset');

//////////captain dashboard

	Route::Resource('advancebooking','captain\AdvanceBookingController');
// Route::Resource('good-categories' , 'GoodCategoriesController');
// Route::Resource('vehical-categories' , 'VehicalCategoriesController');
	Route::Resource('captain-good','captain\GoodsController');
	Route::Resource('captain-vehical','captain\VehicalTypeController');
	Route::Resource('captain-booking','captain\BookingController');
	Route::Resource('captain-Ride','captain\RidesController');
	Route::get('pendingRidese', 'captain\RidesController@pendingride')->name('pendingRidee');
	Route::Resource('captain-googlemap' ,'captain\GoogleMapController');
	Route::Resource('captain-Resources' ,'captain\CaptainResource');
	Route::get('captain-wallet/{id}', 'captain\CaptainResource@walletShow');
//get-captains-ajax
});

Route::get('ride', 'PublicHomeController@ride')->name('ride');
Route::get('sign', 'PublicHomeController@signin')->name('sign-in');
Route::post('logout', 'PublicHomeController@logout')->name('public.logout');
Route::Resource('user-register', 'PublicHomeController');
Route::post('login', 'PublicHomeController@login')->name('login');
Route::Resource('fare-estimate' ,'web\FareEstimateController');
Route::get('drive' ,'web\DriveController@index')->name('drive');
Route::get('drive-app' ,'web\DriveController@drivapp')->name('drive-app');
Route::get('drive-requirement' ,'web\DriveController@drive_requirement')->name('drive-requirement');
Route::get('drive_safety' , 'web\DriveController@drive_safety')->name('drive_safety');
Route::get('how-it-work', 'web\RideController@howitwork')->name('how-it-work');
Route::get('ride_safety' , 'web\RideController@ride_safety')->name('ride-safety');
